//
//  BaseViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import RxSwift
import RxCocoa

class BaseViewModel {
    
    
    // MARK: Public Properties

    var isLoading = BehaviorRelay<Bool?>(value: nil)
    
    let error = BehaviorRelay<String?>(value: nil)
    
    // MARK: Public Methods
    
    func viewDidLoad() {
    }
    
    func viewWillAppear() {
    }
    
    func viewDidAppear() {
    }
    
}

