//
//  DownloadAlertViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/01/2022.
//

import Foundation

class DownloadAlertViewModel {
    
    var resource: DiniResource
    var files: [Download]
    var media: String
    
    init (resource: DiniResource, media: String, files: [Download]) {
        self.resource = resource
        self.media = media
        self.files = files
    }
}
