//
//  DetailsViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import RxSwift
import SVGKit
import CoreData
import BMPlayer

class DetailsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileButton: UIButton!
    
    var disposeBag = DisposeBag()
    var viewModel: DetailsViewModel!
    
    var currentResourceTitle = ""
    var currentResourceId: Int?
    var timeCodeFromViewed: Int?
    
    var currentTrailerPlayer: BMPlayer?
    
    internal static func instantiate(with viewModel: DetailsViewModel) -> DetailsViewController {
        let vc = StoryboardScene.Details.detailsViewController.instantiate()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        initViewModel()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProgressDisplay), name: Notification.Name("DiniDownload"), object: nil)
    }
    
    @objc func updateProgressDisplay(_ notification: Notification) {
        guard let download = notification.userInfo?["download"] as? DownloadVideo else {return }
        
        DispatchQueue.main.async {
            if let resourcesTableViewCell = self.tableView
                .cellForRow(at: IndexPath(row: 2, section: 0)) as? ResourcesTableViewCell {
                resourcesTableViewCell.updateDisplay(download: download)
            }
        }
    }
    
    func initViews() {
        tableView.register(cellType: ShowTableViewCell.self)
        tableView.register(cellType: InfosTableViewCell.self)
        tableView.register(cellType: ResourcesTableViewCell.self)
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
    }
    
    func initViewModel() {
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.reloadData.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.backToHome.asObservable().skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                NavigationHelper.shared.startAppFromMainTab()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.currentVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                guard let self = self else { return }
                if let media = self.viewModel.mediaToBeDownloaded,
                   let uri = video.uri, uri.contains(media),
                   let files = video.files {
                    DispatchQueue.main.async {
                        self.viewModel.mediaToBeDownloaded = nil
                        guard let res = self.viewModel.availableResources?.filter({$0.media == media}).first else {
                            return
                        }
                        let vm = DownloadAlertViewModel(resource: res, media: media, files: files)
                        let vc = DownloadAlertViewController.instantiate(with: vm)
                        
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                } else {
                    self.configureNetworkedPlayer(video: video)
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getCollectionDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        //Make sure that infoState ws has been called ==> refresh home list
        if let _ = viewModel.infoStateToBeUpdatedNow {
            viewModel.delegate?.onInfoStateChanged()
        }
        super.viewWillDisappear(animated)
        
        self.currentTrailerPlayer?.pause()
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.row]
        
        if let showTableViewModel = item as? ShowTableViewModel {
            //let cell: ShowTableViewCell = ShowTableViewCell(style: .default, reuseIdentifier: "ShowTableViewCell")
            let cell: ShowTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.delegate = self
            cell.viewModel = showTableViewModel
            self.currentTrailerPlayer = cell.player
            currentTrailerPlayer?.avPlayer?.isMuted = true
            return cell
        } else if let infosTableViewModel = item as? InfosTableViewModel {
            let cell: InfosTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = infosTableViewModel
            return cell
        } else if let resourcesTableViewModel = item as? ResourcesTableViewModel {
            let cell: ResourcesTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.delegate = self
            cell.viewModel = resourcesTableViewModel
            return cell
        }
        
        return UITableViewCell()
    }
}

extension DetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = viewModel.items[indexPath.row]
        if let _ = item as? ShowTableViewModel {
            return (tableView.frame.width/1.77)+125
        } else {
            return item.cellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ShowTableViewCell {
            cell.player?.pause()
        }
    }
}

extension DetailsViewController: ResourcesDelegate {
    func didSeasonChanged(row: Int) {
        viewModel.seasonSelected = row
        viewModel.updateItems()
    }
    
    func didSelectItem(resources: [DiniResource], clickedIndex: Int) {
        viewModel.currentResources = resources
        viewModel.currentClickedIndex = clickedIndex
        let resource = resources[clickedIndex]
        if let media = resource.media {
            if media.contains(".mp3") {
                let vc = StoryboardScene.Details.detailsAudioViewController.instantiate()
                vc.resource = resource
                vc.viewModel.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                if let bool = PersistenceHelper.shared.isPersisted(media: media), bool {
                    if let videos = PersistenceHelper.shared.retriveLocalMedia(),
                       let videoItems = videos as? [DiniVideo],
                       let videoItem = videoItems.filter({ $0.media == media }).first {
                        print(videoItem)
                        let video = VideoResponse(uri: videoItem.uri,
                                                  name: videoItem.name,
                                                  files: nil,
                                                  media: media,
                                                  localDuration: Int(videoItem.duration),
                                                  localPicture: videoItem.thumbnail,
                                                  isLocalResource:true)
                        configurePersistedPlayer(video: video, media: media)
                    }
                } else {
                    currentResourceId = resource.id
                    currentResourceTitle = resource.title ?? ""
                    timeCodeFromViewed = resource.timeCodeFromViewed
                    viewModel.currentResourceId = resource.id
                    viewModel.getMedia(media: media)
                }
            }
        }
    }
    
    func didSelectItemToDownload(resources: [DiniResource], clickedIndex: Int) {
        let resource = resources[clickedIndex]
        if let media = resource.media {
            if media.contains(".mp3") {
                guard let url = Helper.concatinatePaths(path1: DefaultsHelper.podcastBaseURL, path2: media) else { return }
                let download = Download(quality: nil, type: "mp3", width: nil, height: nil, expires: nil, link: url.absoluteString, createdTime: nil, fps: nil, size: nil, md5: nil, publicName: resource.title, sizeShort: nil)
                DownaloadServices.shared.start(with: download,
                                               resource:resource,
                                               downloadState: .downloading)
            } else {
                viewModel.mediaToBeDownloaded = media
                viewModel.getMedia(media: media)
            }
            
        }
    }
    
    func didSelectItemToCancel(resources: [DiniResource], clickedIndex: Int) {
        
        let title = resources[clickedIndex].title ?? "Untitled"
        let alert = UIAlertController(title: "Dini", message: "Voulez-vous annuler le téléchargement de l'épisode : \(title)", preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "OUI", style: .default) { _ in
            DownaloadServices.shared.urlSession.getAllTasks { tasks in
                for task in tasks {
                    if let url = task.originalRequest?.url,
                       let download = DownaloadServices.shared.activeDownloads[url],
                       let media = resources[clickedIndex].media, download.media == media {
                        task.cancel()
                        
                        DispatchQueue.main.async {
                            if let resourcesTableViewCell = self.tableView
                                .cellForRow(at: IndexPath(row: clickedIndex, section: 0)) as? ResourcesTableViewCell {
                                download.dowloadState = .none
                                resourcesTableViewCell.updateDisplay(download: download)
                            }
                        }
                    }
                }
            }
        }
        
        alert.addAction(action)
        alert.addAction(UIAlertAction(title: "Non", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
    
    func didSelectItemToDelete(resources: [DiniResource], clickedIndex: Int) {
        
        let title = resources[clickedIndex].title ?? "Untitled"
        let alert = UIAlertController(title: "Dini", message: "Voulez-vous supprimer l'épisode : \(title)", preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "OUI", style: .default) { _ in
            
            
            if let media = resources[clickedIndex].media {
                
                if let videos = PersistenceHelper.shared.retriveLocalMedia(),
                   let videoItems = videos as? [DiniVideo],
                   let videoItem = videoItems.filter({ $0.media == media }).first,
                   let uri = videoItem.uri {
                    print(videoItem)
                    
                    let dir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                    let dataPath = dir.appendingPathComponent("DownloadedData")
                    let destinationURLForFile = dataPath.appendingPathComponent(uri)
                    
                    do {
                        let fileNames = try FileManager.default.contentsOfDirectory(atPath: "\(dataPath.path)")
                        print("all files in cache: \(fileNames)")
                        for fileName in fileNames {
                            
                            if (fileName == uri) {
                                let filePathName = "\(destinationURLForFile.path)"
                                try FileManager.default.removeItem(atPath: filePathName)
                                let _ = PersistenceHelper.shared.delete(media: media)
                                DispatchQueue.main.async {
                                    if let resourcesTableViewCell = self.tableView
                                        .cellForRow(at: IndexPath(row: 2, section: 0)) as? ResourcesTableViewCell {
                                        resourcesTableViewCell.updateDisplay(media: media)
                                    }
                                }
                            }
                        }
                        
                        let files = try FileManager.default.contentsOfDirectory(atPath: "\(dataPath.path)")
                        print("all files in cache after deleting images: \(files)")
                        
                        
                    } catch {
                        print("Could not clear temp folder: \(error)")
                    }
                }
                
            }
            
            
        }
        
        alert.addAction(action)
        alert.addAction(UIAlertAction(title: "Non", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
}

extension DetailsViewController: ShowDelegate {
    
    func didClickPlay(resource: DiniResource) {
        let vc = StoryboardScene.Details.detailsAudioViewController.instantiate()
        vc.resource = resource
        vc.viewModel.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func unmuteVolume(isMuted: Bool) {
        currentTrailerPlayer?.avPlayer?.isMuted = isMuted
    }
    
    func didClickPlay(collectionId: Int) {
        viewModel.getCollectionWatch(collectionId: collectionId)
    }
    
    func didSelectFav() {
        viewModel.setInfoState(info: .fav)
    }
    
    func didSelectLiked() {
        viewModel.setInfoState(info: .liked)
    }
    
    func didSelectDisliked() {
        viewModel.setInfoState(info: .disliked)
    }
}
