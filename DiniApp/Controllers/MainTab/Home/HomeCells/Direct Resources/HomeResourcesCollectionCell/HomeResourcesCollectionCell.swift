//
//  HomeResourcesCollectionCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 11/01/2022.
//

import UIKit
import Reusable

class HomeResourcesCollectionCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var newCollectionImageView: UIImageView!
    
    var delegate: HomeCellDelegate?
    var vIndex: Int?
    var hIndex: Int?
    var progressValue: Float = 0.0
    
    override func prepareForReuse() {
        self.progressView.setProgress(0.0, animated: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        newCollectionImageView.isHidden = false
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setupContent(item : DiniResource)  {
        
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPictureResourcesPath,
                                             path2: item.portraitPicture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
        
        if let progress = item.timeCodeFromViewed,
           let duration = item.duration {
            self.progressView.setProgress(Float(progress)/Float(duration), animated: false)
        }
        newCollectionImageView.kf.setImage(with: URL(string: DefaultsHelper.newCollectionPicture ?? ""))
        
        if let isNewResource = item.isNewResource {
            newCollectionImageView.isHidden = !isNewResource
        }
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if let vIndex = vIndex, let hIndex = hIndex  {
            delegate?.itemHomeResourcesTapped(verticalIndex: vIndex, horizontalIndex: hIndex)
        }
    }
    
    @IBAction func playAction(_ sender: UIButton) {
        if let vIndex = vIndex, let hIndex = hIndex  {
            delegate?.itemHomeResourcesTapped(verticalIndex: vIndex, horizontalIndex: hIndex)
        }
    }
}

