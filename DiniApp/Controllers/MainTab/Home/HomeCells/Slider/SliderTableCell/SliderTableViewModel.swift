//
//  SliderTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import RxSwift

protocol SliderCellDelegate: AnyObject {
    func onFavStateChanged()
    func didClickPlay(collectionId: Int)
}

class SliderTableViewModel: BaseViewModel {
    
    var sliders: [CollectionListData] = []
    weak var delegate: SliderCellDelegate?
    var infos: [CollectionInfoState] = []
    var reloadSliderFavButton = BehaviorSubject<Bool>(value: false)

    func setInfoState(info: CollectionInfoState) {
        guard let profileId = DefaultsHelper.profilId,
              let collectionId = sliders.first?.id,
              let token = DefaultsHelper.userToken
        else { return }
        
        isLoading.accept(true)
        ApiCall.Details.setUnsetInfo(token: token, infoState: info.rawValue, collectionId: collectionId, profileId: profileId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension SliderTableViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: SetInfoResponse?) {
        isLoading.accept(false)
        if let response = response,
           let status = response.status, status,
           let result = response.result {
            
            self.reloadSliderFavButton.onNext(result == .add)
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
