//
//  DetailsViewController.swift
//  DiniApp
//
//  Created by Rached Khoudi on 18/02/2022.
//

import UIKit
import AVKit
import RxSwift

class DetailsReelViewController: BaseViewController {

    // MARK:- PROPERTIES
    
    private var isPlayed: Bool = false
    private var gradient : CAGradientLayer?
    private var disposeBag = DisposeBag()
    var viewModel: DetailsReelViewModel!

    private var currentAvQueuePlayer: AVQueuePlayer?
    private var currentAvPlayerLayer: AVPlayerLayer?
    
    let gradientView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    internal static func instantiate(with viewModel: DetailsReelViewModel) -> DetailsReelViewController {
        let vc = DetailsReelViewController()
        vc.viewModel = viewModel
        return vc
    }
    
    lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewLayout.init())
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        
        // Adds contraints to collectionview cell including safe area.
        cv.contentInsetAdjustmentBehavior = .never
        
        cv.setCollectionViewLayout(layout, animated: false)
        cv.delegate = self
        cv.dataSource = self
        cv.register(cellType: DetailsReelCollectionViewCell.self)
        cv.backgroundColor = .black
        cv.isPagingEnabled = true
        return cv
    }()
    
    // MARK:- MAIN
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
        setUpViews()
        setUpViewModel()
        setUpConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getCollectionDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        guard let currentAvQueuePlayer = self.currentAvQueuePlayer else { return }
        currentAvQueuePlayer.pause()
    }
    // MARK:- FUNCTIONS

    func setUpNavBar(){
        //self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.hidesBackButton = true

        self.title = "Reels"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(image: Asset.pictoClose.image,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(self.closeReels))]
    }
    
    deinit {
        guard let player = self.currentAvPlayerLayer?.player else { return }
        guard let currentAvQueuePlayer = self.currentAvQueuePlayer else { return }
        player.pause()
        currentAvQueuePlayer.pause()
    }
    
    func setUpViews(){
        view.addSubview(collectionView)
    }
    
    func setUpViewModel() {
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.reloadData.asObservable().skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.collectionView.reloadData()

                if let media = self.viewModel.items[self.viewModel.id].media {
                    self.viewModel.getMedia(media: media)
                }
            })
            .disposed(by: self.disposeBag)
        
        viewModel.currentVideo.asObservable()
            .observeOn(MainScheduler.instance)
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                guard let self = self else { return }
                if let file = video.files?.first,
                    let link = file.link,
                    let url = URL(string: link) {
                    
                    if let cell = self.collectionView.visibleCells.first as? DetailsReelCollectionViewCell {
                        cell.addPlayer(for: url, bounds: self.collectionView.frame)
                        self.currentAvQueuePlayer = cell.avQueuePlayer
                        if !self.isPlayed {
                            cell.avQueuePlayer?.play()
                            self.isPlayed = true
                        }
                    }
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    func setUpConstraints(){
        
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
        ])
    }
    
    @objc private func closeReels() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension DetailsReelViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DetailsReelCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        let data = viewModel.items[indexPath.row]
        cell.data = data
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        collectionView.visibleCells.forEach { cell in
            // TODO: write logic to stop the video before it begins scrolling
            let cell = cell as! DetailsReelCollectionViewCell
            cell.avQueuePlayer?.pause()
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        collectionView.visibleCells.forEach { [weak self] cell in
            // TODO: write logic to start the video after it ends scrolling
            let cell = cell as! DetailsReelCollectionViewCell
            cell.avQueuePlayer?.seek(to: CMTime.zero)
            self?.currentAvQueuePlayer = cell.avQueuePlayer

            if let index = collectionView.indexPath(for: cell)?.row,
               let media = self?.viewModel.items[safe: index]?.media {
                self?.viewModel.getMedia(media: media)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! DetailsReelCollectionViewCell
        self.currentAvQueuePlayer = cell.avQueuePlayer
        cell.avQueuePlayer?.pause()
    }
    
}

extension DetailsReelViewController: feedDataCellDelegate {
    
    func hideWhenLongPressBegan() {
        navigationController?.navigationBar.isHidden = true
    }
    
    func showWhenLongPressEnded() {
        navigationController?.navigationBar.isHidden = false
    }
    
}
