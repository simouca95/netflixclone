//
//  TeasersCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import UIKit

class TeasersCollectionViewCell: UICollectionViewCell {

    static let identifier = "teasersCollectionCell"

    @IBOutlet weak var movieAvatar: UIImageView!
    
    @IBOutlet weak var movieImage: UIImageView!
    
    var video : Video!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.movieImage.layer.cornerRadius = self.movieImage.frame.height / 2
        
        self.movieImage.clipsToBounds = true
    }

    
    
    func setupContent(video : Video)  {
        
        self.video = video
        
        self.movieImage.image = UIImage(named: video.image)

    }
}
