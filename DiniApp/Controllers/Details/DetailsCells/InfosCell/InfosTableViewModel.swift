//
//  InfosTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit

class InfosTableViewModel: BaseCellViewModel {
    
    var type: CollectionType
    var title: String
    var speakers: String
    var category: String
    var tags: String
    var contentNumber: Int
    

    init(cellHeight: CGFloat, type: CollectionType,
         title: String, speakers: String,
         category: String, tags: String, contentNumber: Int) {
        self.type = type
        self.title = title
        self.speakers = speakers
        self.category = category
        self.tags = tags
        self.contentNumber = contentNumber
        super.init(cellHeight: cellHeight)
    }
}
