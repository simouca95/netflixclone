//
//  TrailerTableViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/28/20.
//

import UIKit

class TrailerTableViewCell: UITableViewCell {

    static let identifier = "trailerTableViewCell"
    
    @IBOutlet weak var trailerThumbnail: UIImageView!
    @IBOutlet weak var trailerTitle: UILabel!
    
    var video : Video!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func playAction(_ sender: Any) {
    }
    
    
    func setupContent(video : Video)  {
        
        self.video = video
        
        self.trailerThumbnail.image = UIImage(named: video.image)
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
