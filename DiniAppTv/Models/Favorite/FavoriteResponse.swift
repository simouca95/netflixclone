//
//  FavoriteResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Foundation

// MARK: - FavoritesResponse
public struct FavoritesResponse: Codable {
    public let collectionsList: FavoriteCollectionsList?
    public let collectionsInfos: CollectionsInfos?
}

// MARK: - CollectionsList
public struct FavoriteCollectionsList: Codable {
    public let favorites: [[DiniSlider]]?
}

// MARK: - Favorite
public struct Favorite: Codable {
    let id: Int?
    let title, summary, tagline: String?
    let trailer: String?
    let picture, background: String?
    let isInFront: Bool?
    let category: CollectionListCategoryList?
    let tags: [CollectionListCategoryList]?
    let resources: [CollectionsListResource]?
    let type: Int?
    let seasons: [CollectionsListSeason]?
    let enabled, isSafe: Bool?
    let sliderPicture: String?
    let keywords: JSONNull?
    let portraitPicture: String?
//    let id: Int?
//    let title, summary, tagline, trailer: String?
//    let picture: String?
//    let picture2, picture3: JSONNull?
//    let background: String?
//    let isInFront: Bool?
//    let category: Category?
//    let tags: [Category]?
//    let resources: [Resource]?
//    let type: Int?
//    let seasons: [JSONAny]?
//    let enabled, isSafe: Bool?
//    let sliderPicture: String?
//    let keywords: JSONNull?
}
