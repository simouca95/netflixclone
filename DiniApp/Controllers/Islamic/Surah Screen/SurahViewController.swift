//
//  SurahViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/03/2021.
//

import UIKit
import SVProgressHUD
import RxSwift
import RxCocoa
import AVFoundation

enum AudioAction {
    case play
    case next
    case previous
}

class SurahViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var surahNameLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var switchButton: UISwitch!
    
    var surahId: String = ""
    
    private var audioPlayer: AVAudioPlayer? = nil
    private var ayahs: [Ayah] = []
    private var ayahsFrench: [AyahFrench] = []
    private var currentAyahRow = BehaviorRelay(value: 0)
    private var currentSpeaker = UserDefaults.standard.string(forKey: Constants.defaultSpeaker) ?? DataMock.speakers[4].path
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SVProgressHUD.show()
        
        self.ayahs = []
        self.ayahsFrench = []
        self.currentAyahRow = BehaviorRelay(value: 0)
        self.currentSpeaker = UserDefaults.standard.string(forKey: Constants.defaultSpeaker) ?? DataMock.speakers[4].path
        updateButtons()
        self.tableView.reloadData()
        
        ApiCall.Islamic.ayahs(id: self.surahId, speaker: self.currentSpeaker, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.audioPlayer?.stop()
        self.playButton.setImage(Asset.pictoPlay.image, for: .normal)
        self.currentAyahRow.accept(0)
    }
    
    func setupViews() {
        //Table
        self.tableView.register(UINib(nibName: "AyahTableViewCell", bundle: nil), forCellReuseIdentifier: "AyahTableViewCell")
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableView.automaticDimension
        
        //Switch
        self.switchButton.set(width: 30, height: 20)
        self.switchButton.onTintColor = .black
        self.switchButton.tintColor = .black
        self.switchButton.backgroundColor = .black
        self.switchButton.layer.cornerRadius = 15
        self.switchButton.clipsToBounds = true
    }
    
    @IBAction func didSelectPlay(_ sender: UIButton) {
        actionAudio(.play)
    }
    
    @IBAction func didSelectNext(_ sender: UIButton) {
        actionAudio(.next)
    }
    
    @IBAction func didSelectPrevious(_ sender: UIButton) {
        actionAudio(.previous)
    }
    
    @IBAction func didSelectAudio(_ sender: UIButton) {
        let speakersVc = StoryboardScene.Islamic.speakersViewController.instantiate()
        self.navigationController?.pushViewController(speakersVc, animated: true)
    }
    
    @IBAction func didTraductionSwitch(_ sender: UISwitch) {
        sender.isSelected.toggle()
        if sender.isSelected && self.ayahsFrench.isEmpty {
            SVProgressHUD.show()
            ApiCall.Islamic.ayahsFrench(id: self.surahId, onSuccess: self.onSuccess, onError: self.onFail)
            self.switchButton.isEnabled = false
        }
        self.tableView.reloadData()
    }
}

// MARK: AVPlayer Methods
extension SurahViewController: AVAudioPlayerDelegate {
    func setupAudioPlayer(row: Int) {
        do { try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback))) }
        catch _ { }
        
        do { try AVAudioSession.sharedInstance().setActive(true) }
        catch _ { }
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        do {
            guard let url = URL(string: self.ayahs[exist: row]?.audio ?? "") else { return }
            let dataURL = try Data(contentsOf: url)
            audioPlayer = try AVAudioPlayer(data: dataURL)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
        } catch {
            print(error)
        }
    }
    
    fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
        return input.rawValue
    }
    
    func playAudio(isDifferentRow : Bool = true) {
        let indexPath = IndexPath.init(row: self.currentAyahRow.value, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        
        if let audioPlayer = self.audioPlayer {
            if audioPlayer.isPlaying {
                audioPlayer.pause()
                self.playButton.setImage(Asset.pictoPlay.image, for: .normal)
            } else {
                if isDifferentRow {
                    setupAudioPlayer(row: self.currentAyahRow.value)
                }
                self.playButton.setImage(Asset.pictoPause.image, for: .normal)
                self.audioPlayer?.play()
            }
        }
    }
    
    func actionAudio(_ action: AudioAction) {
        switch action {
        case .play:
            playAudio(isDifferentRow: false)
        case .next:
            self.audioPlayer?.stop()
            self.audioPlayer?.currentTime = 0
            if self.currentAyahRow.value + 1 == self.ayahs.count {
                self.currentAyahRow.accept(0)
                setupAudioPlayer(row: self.currentAyahRow.value)
                self.playButton.setImage(Asset.pictoPlay.image, for: .normal)
            } else {
                self.currentAyahRow.accept(self.currentAyahRow.value + 1)
                playAudio()
            }

        case .previous:
            self.audioPlayer?.stop()
            self.audioPlayer?.currentTime = 0
            self.currentAyahRow.accept(self.currentAyahRow.value - 1)
            playAudio()
        }
        updateButtons()
        self.tableView.reloadData()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        actionAudio(.next)
    }
    
    func updateButtons() {
        if self.currentAyahRow.value == 0 {
            self.previousButton.isEnabled = false
        } else if self.currentAyahRow.value == self.ayahs.count - 1 {
            self.nextButton.isEnabled = false
        } else {
            self.previousButton.isEnabled = true
            self.nextButton.isEnabled = true
        }
    }
}

// MARK: WS Callbacks
extension SurahViewController {
    private func onSuccess(_ response: AyahResponse?) {
        if let response = response, response.code == 200 {
            SVProgressHUD.dismiss()
            self.ayahs = response.data.ayahs
            self.surahNameLabel.text = "\(response.data.name)    \(response.data.englishName)"
            setupAudioPlayer(row: 0)
            tableView.reloadData()
        }
    }
    
    private func onSuccess(_ response: AyahFrenchResponse?) {
        SVProgressHUD.dismiss()
        if let response = response, response.code == 200 {
            SVProgressHUD.dismiss()
            self.switchButton.isEnabled = true
            self.ayahsFrench = response.data.ayahs
            tableView.reloadData()
        }
    }
    
    private func onFail(_ error: DiniError?) {
        self.switchButton.isEnabled = true
        SVProgressHUD.dismiss()
    }
}

// MARK: TableView Methods
extension SurahViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ayahs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AyahTableViewCell") as! AyahTableViewCell
        
        if let _ = self.ayahs[exist: indexPath.row] {
            cell.arabicLabel.text = self.ayahs[indexPath.row].text
        }
        
        if let _ = self.ayahsFrench[exist: indexPath.row] {
            cell.frenchLabel.text = "\(self.ayahs[indexPath.row].numberInSurah). \(self.ayahsFrench[indexPath.row].text)"
        }
        
        cell.ayahNumberLabel.text = "\(self.ayahs[indexPath.row].numberInSurah)"
        cell.frenchLabel.isHidden = !self.switchButton.isSelected
        
        cell.backgroundColor = indexPath.row == currentAyahRow.value ? UIColor(rgb: 0xEFE8DD) : .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
