//
//  AvatarsViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/12/2021.
//

import UIKit
import RxSwift
import SVGKit

class AvatarsViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var viewModel: AvatarsViewModel!
    private var disposeBag = DisposeBag()
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = "Choisir un avatar"
        self.initViews()
        self.initViewModels()
    }
    
    func initViews() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.register(cellType: SimpleAvatarCollectionViewCell.self)
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            }
        }
    }
    
    func initViewModels() {
        viewModel.avatarPictureList()
        viewModel.reloadData
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext:{[weak self] value in
                guard let self = self else { return }
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
    }
}

extension AvatarsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SimpleAvatarCollectionViewCell.reuseIdentifier, for: indexPath) as! SimpleAvatarCollectionViewCell
        cell.setupView(path: viewModel.pictures[indexPath.row])
        return cell
    }
}

extension AvatarsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = (collectionView.frame.width - 30 - 15 - 15) / CGFloat(3)
        
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(15)
    }

}

extension AvatarsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true) { [weak self] in
//            guard let self = self else { return }
            if let path = self.viewModel.pictures[indexPath.row].split(separator: "/").last {
                self.viewModel.delegate?.updateImage(imagePath: String(path))
            }
//        }
    }
}
