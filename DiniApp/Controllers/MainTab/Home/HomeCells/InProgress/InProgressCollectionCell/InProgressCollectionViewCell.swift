//
//  ContinueWatchingCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import UIKit
import Reusable

class InProgressCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var newResourceImageView: UIImageView!
    @IBOutlet weak var newCollectionImageView: UIImageView!
    @IBOutlet weak var infosButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var infos: UILabel!
    weak var delegate: HomeCellDelegate?
    var vIndex: Int?
    var hIndex: Int?
    var progressValue: Float = 0.0
    
    override func prepareForReuse() {
        self.progressView.setProgress(0.0, animated: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        infosButton.setTitle("", for: .normal)
        moreButton.setTitle("", for: .normal)
        newResourceImageView.isHidden = false
        newCollectionImageView.isHidden = false
    }
    
    func setupContent(item : CollectionListData, itemsViewedsInfos: [CollectionViewedsInfo])  {
        newCollectionImageView.kf.setImage(with: URL(string: DefaultsHelper.newCollectionPicture ?? ""))
        newResourceImageView.kf.setImage(with: URL(string: DefaultsHelper.newResourcePicturePath ?? ""))
        let isNewResourceIntoThisCollection = item.isNewResourcesIntoThisCollection ?? false
        let isNewCollection = item.isNewCollection ?? false
        newResourceImageView.isHidden = !isNewResourceIntoThisCollection
        newCollectionImageView.isHidden = !isNewCollection
        if item.isNewCollection ?? false && item.isNewResourcesIntoThisCollection ?? false {
            newResourceImageView.isHidden = true
        }
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
    
        if let itemViewedsInfos = itemsViewedsInfos.filter({$0.collectionID == item.id}).first,
           let progress = itemViewedsInfos.percentProgression {
            self.progressView.setProgress(Float(progress)/Float(100), animated: false)
        }
    }
    
    @IBAction func infoTapped(_ sender: UIButton) {
        if let vIndex = vIndex, let hIndex = hIndex  {
            delegate?.itemProgressInfoTapped(verticalIndex: vIndex, horizontalIndex: hIndex)
        }
    }
        
    @IBAction func playAction(_ sender: UIButton) {
        if let vIndex = vIndex, let hIndex = hIndex  {
            delegate?.itemProgressTapped(verticalIndex: vIndex, horizontalIndex: hIndex)
        }
    }
        
    @IBAction func moreActionTapped(_ sender: UIButton) {
        if let vIndex = vIndex, let hIndex = hIndex  {
            self.delegate?.itemProgressMoreActionsTapped(verticalIndex: vIndex, horizontalIndex: hIndex)
        }
    }
}
