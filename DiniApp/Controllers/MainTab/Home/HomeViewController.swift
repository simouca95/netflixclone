//
//  HomeViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import RxSwift
import SVGKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var headerBackground: UIView!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var appLogoButton: UIButton!
    @IBOutlet weak var switchProfilButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    
    
    var viewModel: HomeViewModel!
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViews()
        self.initViewModels()
        viewModel.getNotification()
    }
    
    func initViews() {
        let notificationImage = UIImage(named: "notification")
        notificationButton.tintColor = .white
        notificationButton.imageEdgeInsets = UIEdgeInsets(top: 2.0, left: 2.0, bottom: 2.0, right: 2.0)
        notificationButton.setImage(notificationImage, for: .normal)
        switchProfilButton.setTitleColor(.white, for: .normal)
        switchProfilButton.tintColor = .white
        switchProfilButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        switchProfilButton.setTitle(DefaultsHelper.nameToDisplayInSwitchProfileDefault, for: .normal)
        mainTableView.separatorStyle = .none
        self.registerTableCells()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        mainTableView.addSubview(refreshControl)
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
        
        // add header tap gesture
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.scrollToTop))
        self.headerBackground.addGestureRecognizer(tap)
        self.headerBackground.isUserInteractionEnabled = true
        manageLogo()
    }
    
    private func manageLogo() {
        if DefaultsHelper.nameToDisplayInSwitchProfileDefault == "Section tout public" {
            if let logoPath = URL(string: DefaultsHelper.logoChild ?? "") {
                appLogoButton.getURL2(url: logoPath)
            }
        } else {
            if let logoPath = URL(string: DefaultsHelper.logo ?? "") {
                appLogoButton.getURL2(url: logoPath)
            }
        }
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewModel.getCollectionList()
    }
    
    @objc func scrollToTop() {
        mainTableView.setContentOffset(.zero, animated: true)
    }
    
    func initViewModels() {
        viewModel = HomeViewModel()
        viewModel.reloadData.asObservable()
            .subscribe (onNext:{ [weak self] value in
                guard let self = self else { return }
                self.mainTableView.reloadData()
                self.switchProfilButton.setTitle(DefaultsHelper.nameToDisplayInSwitchProfileDefault, for: .normal)
                self.manageLogo()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.refreshControl.endRefreshing()
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.isExpiredAccount.asObservable()
            .bind { _ in
                self.displaySimpleAlert(title: "", message: "")
                DefaultsHelper.deleteAllDefaults()
                NavigationHelper.shared.startAppFromLogin()
            }
            .disposed(by: disposeBag)
        
        
        viewModel.currentWatchVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                guard let self = self else { return }
                self.configurePlayer(video: video)
            })
            .disposed(by: self.disposeBag)
        viewModel.launchVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                let vc = StoryboardScene.Video.videoPlayViewController.instantiate()
                let vm = VideoPlayViewModel(video: video,
                                            resourceId: self?.viewModel.resourceId ?? 0,
                                            type: .episode,
                                            resources: self?.viewModel.resources ?? [DiniResource](),
                                            seasons: nil)
                vm.delegate = self
                vc.viewModel = vm
                vc.modalPresentationStyle = .fullScreen
                self?.navigationController?.present(vc, animated: true, completion: nil)
            })
            .disposed(by: self.disposeBag)
        switchProfilButton.rx.tap.bind { [weak self] in
            self?.viewModel.switchProfile()
            
        }.disposed(by: disposeBag)
        
        viewModel.isNewNotification.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isNewNotification in
                guard let self = self else { return }
                if isNewNotification ?? false {
                    let notificationImage = UIImage(named: "new-notification")
                    self.notificationButton.setImage(notificationImage, for: .normal)
                }
                
            })
            .disposed(by: disposeBag)
        
        notificationButton.rx.tap.bind { [weak self] in
            let vc = NotificationViewController.instantiate(with: NotificationViewModel(),
                                                            and: self?.viewModel.notificationData ?? [NotificationData]())
            self?.navigationController?.pushViewController(vc, animated: true)
        }.disposed(by: disposeBag)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.headerBackground.alpha = 0.7
        viewModel.getCollectionList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch PushNotificationHandler.shared.didReceiveNotification {
        case .resource(resourceId: let resourceId, collectionId: let collectionId):
            self.openVideoFromNotification(resourceId: resourceId, collectionId: collectionId)
        case .collection(collectionId: let collectionId):
            self.openCollectionFromNotification(collectionId: collectionId)
        case .none:
            break
        case .simpleNotification(title: let title, body: let body):
            self.showNotificationSheet(title: title, body: body)
        }
        PushNotificationHandler.shared.clickNotfication.observeOn(MainScheduler.instance)
            .bind(onNext:  { [weak self] (error)  in
                PushNotificationHandler.shared.reset()
                switch error {
                case .resource(resourceId: let resourceId, collectionId: let collectionId):
                    self?.openVideoFromNotification(resourceId: resourceId, collectionId: collectionId)
                case .collection(collectionId: let collectionId):
                    self?.openCollectionFromNotification(collectionId: collectionId)
                case .none:
                    PushNotificationHandler.shared.clickNotfication = PublishSubject<NotificationType>()
                case .simpleNotification(title: let title, body: let body):
                    self?.showNotificationSheet(title: title, body: body)
                }
            }).disposed(by: disposeBag)
    }
    
    private func openVideoFromNotification(resourceId: Int, collectionId: Int) {
        self.viewModel.resourceId = resourceId
        self.viewModel.getCollectionFromNotification(connectionId: collectionId)
        self.viewModel.isFromNotification = true
        PushNotificationHandler.shared.reset()
    }
    
    private func openCollectionFromNotification(collectionId: Int) {
        let viewmodel = DetailsViewModel(collectionId: collectionId , infoState: [], delegate: self)
        let detailsViewController = DetailsViewController.instantiate(with: viewmodel)
        self.navigationController?.pushViewController(detailsViewController, animated:true)
        PushNotificationHandler.shared.reset()
    }
    
    private func showNotificationSheet(title: String, body: String) {
        let vc = NotificationSheet.instantiate(with: title, body: body)
        vc.delegate = self
        self.navigationController?.present(vc, animated: true,completion: nil)
        PushNotificationHandler.shared.reset()
    }
    
    func registerTableCells() {
        self.mainTableView.register(cellType: SliderTableViewCell.self)
        self.mainTableView.register(cellType: HomeSpeakerTableViewCell.self)
        self.mainTableView.register(cellType: InProgressTableViewCell.self)
        self.mainTableView.register(cellType: HomeResourcesTableViewCell.self)
        self.mainTableView.register(cellType: StandardTableViewCell.self)
    }
    
    func startTimer(coll: UICollectionView, slidersCount: Int) {
        struct Holder { static var called = false }
        if !Holder.called {
            Holder.called = true
            let dic: [String: Any] = ["coll": coll, "sliders": slidersCount]
            _ =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(scrollAutomatically), userInfo: dic, repeats: true)
        }
    }
    
    @objc func scrollAutomatically(_ timer: Timer) {
        if let dictionary: [String : Any] = timer.userInfo! as? [String : Any],
           let coll: UICollectionView = dictionary["coll"] as? UICollectionView,
           let slidersCount: Int = dictionary["sliders"] as? Int {
            
            for cell in coll.visibleCells {
                if let indexPath: IndexPath = coll.indexPath(for: cell) {
                    if indexPath.row < slidersCount - 1 {
                        let indexPath1 = IndexPath(row: indexPath.row + 1, section: indexPath.section)
                        coll.scrollToItem(at: indexPath1, at: .right, animated: true)
                    } else {
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: indexPath.section)
                        coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                    }
                }
            }
        }
    }
    
    func displaySimpleAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension HomeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = viewModel.items[indexPath.section]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if item.type == HomeCellType.main.rawValue {
                return tableView.frame.width * 0.677
            } else if item.type == HomeCellType.speaker.rawValue {
                return 220
                
            } else if item.type == HomeCellType.inProgress.rawValue {
                return 220+35
                
            } else {
                return 220
            }
        }
        
        if item.type == HomeCellType.main.rawValue {
            return tableView.frame.width/0.677
            
        } else if item.type == HomeCellType.speaker.rawValue {
            return (tableView.frame.width*0.27)/0.677
            
        } else if item.type == HomeCellType.inProgress.rawValue {
            return ((tableView.frame.width*0.27)/0.677)+35
            
        } else {
            return (tableView.frame.width*0.27)/0.677
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let item = viewModel.items[section]
        
        if item.type == HomeCellType.main.rawValue {
            return 0
        } else if item.type == HomeCellType.speaker.rawValue {
            return 0
        } else {
            
            let label = UILabel()
            label.font = .boldSystemFont(ofSize: 17)
            label.numberOfLines = 2
            label.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.width-20, height: 100)
            label.text = item.title
            
            if label.actualNumberOfLines == 1 {
                return 40
            } else {
                return 55
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let item = viewModel.items[section]
        
        if item.type == HomeCellType.main.rawValue || item.type == HomeCellType.speaker.rawValue {
            return nil
        } else {
            let headerViewWidth = tableView.frame.width
            let headerViewHeight = tableView.rectForHeader(inSection: section).size.height
            let headerView = UIView(frame: CGRect.init(x: 0, y: 0,
                                                       width: headerViewWidth, height: headerViewHeight))
            
            let label = UILabel()
            label.numberOfLines = 2
            label.frame = CGRect.init(x: 10, y: 10, width: headerView.frame.width-20, height: headerView.frame.height-10)
            label.text = item.title
            label.font = .boldSystemFont(ofSize: 17)
            headerView.addSubview(label)
            
            headerView.backgroundColor = .black
            label.backgroundColor = .black
            label.textColor = .white
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
}

extension HomeViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = viewModel.items[indexPath.section]
        
        //Main
        if item.type == HomeCellType.main.rawValue {
            let cell: SliderTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                var infoState: [CollectionInfoState] = []
                startTimer(coll: cell.collectionView, slidersCount: data.count)
                if let listData = data.first, let id = listData.id {
                    infoState = self.viewModel.getCollectionInfoState(collectionId: id)
                }
                cell.delegate = self
                cell.viewModel.delegate = self
                cell.setupContent(data: data, infos: infoState)
            }
            return cell
        }
        //Speaker
        else if item.type == HomeCellType.speaker.rawValue {
            let cell: HomeSpeakerTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.vIndex = indexPath.section
            cell.delegate = self
            if let data = item.speakerData {
                cell.setupContent(data: data)
            }
            return cell
        }
        //inProgress
        else if item.type == HomeCellType.inProgress.rawValue {
            let cell: InProgressTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                cell.setupContent(data: data)
                cell.itemsViewedsInfos = viewModel.itemsViewedsInfos
                cell.vIndex = indexPath.section
                cell.delegate = self
            }
            return cell
        }
        //Resources
        else if item.type == HomeCellType.homeResources.rawValue ||
                    item.type == HomeCellType.homeAudioResources.rawValue {
            let cell: HomeResourcesTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData?.first?.resources {
                cell.setupContent(data: data)
                cell.itemsViewedsInfos = viewModel.itemsViewedsInfos
                cell.vIndex = indexPath.section
                cell.delegate = self
            }
            return cell
        }
        // Seasons
        else if item.type == HomeCellType.seasons.rawValue {
            let cell: StandardTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                cell.vIndex = indexPath.section
                cell.delegate = self
                cell.setupContent(data: data, type: .seasons)
            }
            return cell
        }
        //Live
        else if item.type == HomeCellType.live.rawValue {
            let cell: StandardTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                cell.vIndex = indexPath.section
                cell.delegate = self
                cell.setupContent(data: data, type: .live)
            }
            return cell
        }
        //Reel
        else if item.type == HomeCellType.reel.rawValue {
            let cell: StandardTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                cell.vIndex = indexPath.section
                cell.delegate = self
                cell.setupContent(data: data, type: .reel)
            }
            return cell
        }
        else {
            let cell: StandardTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            if let data = item.collectionData {
                cell.vIndex = indexPath.section
                cell.delegate = self
                cell.setupContent(data: data, type: .standard)
            }
            return cell
        }
    }
}

extension HomeViewController: HomeCellDelegate {
    
    func itemHomeSeasonsTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[verticalIndex].collectionData?.first,
           let seasons = data.seasons,
           let itemData = seasons[safe: horizontalIndex],
           let id = data.id, let seasonId = itemData.id {
            let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
            let vm = DetailsViewModel(collectionId: id, seasonId: seasonId, infoState: infoState, delegate: self)
            let vc = DetailsViewController.instantiate(with: vm)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func itemProgressMoreActionsTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.collectionData,
           let itemData = data[safe: horizontalIndex] {
            let vc = MoreActionsBottomSheet.instantiate(with: itemData.title ?? "", vIndex: verticalIndex, hIndex: horizontalIndex)
            vc.delegate = self
            self.navigationController?.present(vc, animated: true,completion: nil)
        }
    }
    
    func itemTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[verticalIndex].collectionData,
           let itemData = data[safe: horizontalIndex],
           let id = itemData.id {
            let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
            let vm = DetailsViewModel(collectionId: id, infoState: infoState, delegate: self)
            let vc = DetailsViewController.instantiate(with: vm)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func itemSpeakerTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.speakerData,
           let item = data[safe: horizontalIndex],
           let id = item.id {
            let vc = DiniSpeakersViewController.instantiate(with: DiniSpeakersViewModel(speakerId: id))
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func itemProgressTapped(verticalIndex: Int, horizontalIndex: Int) {
      
        if let id = viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.id {
            if   let media = viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.resources?[0].media {
                if media.contains(".mp3") {
                    let vc = StoryboardScene.Details.detailsAudioViewController.instantiate()
                    vc.resource = viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.resources?[horizontalIndex]
                    vc.viewModel.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                viewModel.playHomeVideoType = .fromInProgress
                viewModel.currentHomeVideoVerticalItemIndex = verticalIndex
                viewModel.currentHomeVideoHorizontalItemIndex = horizontalIndex
                viewModel.getCollectionWatch(collectionId: id)
                }

            }
        }
    }
    
    func itemProgressInfoTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.collectionData,
           let itemData = data[safe: horizontalIndex],
           let id = itemData.id {
            let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
            let vm = DetailsViewModel(collectionId: id, infoState: infoState, delegate: self)
            let vc = DetailsViewController.instantiate(with: vm)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func itemHomeResourcesTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.collectionData,
           let itemDataResources = data.first?.resources,
           let resource = itemDataResources[safe: horizontalIndex],
           let media = resource.media {
            viewModel.playHomeVideoType = .fromHomeResources
            viewModel.currentHomeVideoVerticalItemIndex = verticalIndex
            viewModel.currentHomeVideoHorizontalItemIndex = horizontalIndex
            viewModel.currentHomeVideoId = resource.id
            if media.contains("mp3") {
                self.configureAudioPlayer(resource: resource)
            } else {
                viewModel.getMedia(media: media)
            }
        }
    }
    
    func itemLiveTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.collectionData,
           let itemData = data[safe: horizontalIndex],
           let resource = itemData.resources?.first,
           let webPathToLive = resource.webPathToLive,
           let url = URL(string: webPathToLive) {
            UIApplication.shared.open(url)
        }
    }
    
    func itemReelTapped(verticalIndex: Int, horizontalIndex: Int) {
        if let data = viewModel.items[safe: verticalIndex]?.collectionData?.first,
           let id = data.id {
            let vc = DetailsReelViewController.instantiate(with: DetailsReelViewModel(collectionId: id, id: horizontalIndex))
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HomeViewController: DetailsDelegate {
    func onInfoStateChanged() {
        viewModel.getCollectionList()
    }
}

extension HomeViewController: SliderCellDelegate {    
    func didClickPlay(collectionId: Int) {
        viewModel.playHomeVideoType = .fromMain
        viewModel.getCollectionWatch(collectionId: collectionId)
    }
    
    func onFavStateChanged() {
    }
}
extension HomeViewController: MoreActionsDelegete {
    func itemProgressMoreActionsTapped(action: MoreAction, verticalIndex: Int, horizontalIndex: Int) {
        
        switch action {
        case .info:
            if let data = viewModel.items[safe: verticalIndex]?.collectionData,
               let itemData = data[safe: horizontalIndex],
               let id = itemData.id {
                let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
                let vm = DetailsViewModel(collectionId: id, infoState: infoState, delegate: self)
                let vc = DetailsViewController.instantiate(with: vm)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case .download:
            break
        case .none:
            break
        }
    }
}
extension UIButton {
    func getURL2(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data),
                httpURLResponse.url == url
            else { return }
            DispatchQueue.main.async() {
                self.setImage(image, for: .normal)
                self.imageView?.contentMode = mode
            }
        }.resume()
    }
    
    public func downloadedFrom2(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        getURL2(url: url, contentMode: mode)
        
    }
    
}

extension HomeViewController: NotificationSheetDelegete {
    func showNotification() {
        let vc = NotificationViewController.instantiate(with: NotificationViewModel(),
                                                        and: viewModel.notificationData)
        navigationController?.pushViewController(vc, animated: true)
    }
}
