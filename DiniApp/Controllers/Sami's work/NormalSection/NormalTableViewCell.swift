//
//  NormalTableViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import UIKit

class NormalTableViewCell: UITableViewCell {

    static let identifier = "normalSection"

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sectionTitle: UILabel!
    
    
    var section : Section!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCell()
    }

    func setupContent(section : Section)  {
        
        self.section = section
        
        self.sectionTitle.text = section.title
        
        self.collectionView.reloadData()
        
        
    }
    
    func registerCell() {
    
        self.collectionView.register(UINib(nibName: "NormalSectionCollectionViewCell", bundle: nil),
                                     forCellWithReuseIdentifier: NormalSectionCollectionViewCell.identifier)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension NormalTableViewCell : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: self.collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension NormalTableViewCell : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.section.video.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalSectionCollectionViewCell.identifier, for: indexPath) as! NormalSectionCollectionViewCell
        
        cell.setupContent(video: self.section.video[indexPath.item])
        
        return cell
        
    }
    
    
}
