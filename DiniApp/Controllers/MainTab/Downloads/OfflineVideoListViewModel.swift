//
//  OfflineVideoListViewModel.swift
//  DiniApp
//
//  Created by sami hazel on 25/02/2022.
//

import Foundation
import RxCocoa
import RxSwift

class OfflineVideoListViewModel: BaseViewModel {
    
    var items: [VideoResponse] = []
    var reloadData = BehaviorSubject<Void>(value: ())

    func getCollectionList() {
        isLoading.accept(true)
        if let videos = PersistenceHelper.shared.retriveLocalMedia(),
           let videoItems = videos as? [DiniVideo] {
            print(videoItems)
            let videoRessources = videoItems.compactMap { videoItem in
                VideoResponse(uri: videoItem.uri,
                              name: videoItem.name,
                              files: nil,
                              media: videoItem.media,
                              localDuration: Int(videoItem.duration),
                              localPicture:videoItem.thumbnail,
                              isLocalResource: true)
            }
            self.onSuccess(videoRessources)
            
        }
    }

}

extension OfflineVideoListViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: [VideoResponse]) {

        
        items.removeAll()
        items.append(contentsOf: response)
        reloadData.onNext(())
        isLoading.accept(false)
    }

    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
