//
//  VideoPlayViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 25/12/2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol AVPlayerDelegate: AnyObject {
    func onSuccessTimeCodeSend()
    func onFailTimeCodeSend()
}

class VideoPlayViewModel: BaseViewModel {
    
    weak var delegate: AVPlayerDelegate?
    var currentNetworkedVideo = BehaviorRelay<VideoResponse?>(value: nil)
    var currentPersistedVideo = BehaviorRelay<VideoResponse?>(value: nil)

    var videoTitle: String
//    var videoURL: URL
    var timeCodeFromViewed: Int?
    var subtitle: String?
    var resourceSubtitles: [ResourceSubtitle]?
    var video: VideoResponse
    
    var persistedMedia: String?
    var persistedSubtitles: [ResourceSubtitle]?

    var type: CollectionType
    var seasons: [Season]?
    var resources: [DiniResource]
    var currentResourceIndex: Int = 0
    var currentSeasonIndex: Int?
    var currentResourceId: Int?
    var currentFileIndex = 0
    var currentSubtitleResourceId = -1
    var currentResource: DiniResource?

    //Online init
    init(video: VideoResponse, resourceId: Int, type: CollectionType, resources: [DiniResource], seasons: [Season]?) {
        
        self.video = video
        self.currentResourceId = resourceId
        self.videoTitle = resources.filter({ $0.id == resourceId }).first?.title ?? "Untitled"
        self.timeCodeFromViewed = resources
            .filter({ $0.id == resourceId }).first?.timeCodeFromViewed
        self.subtitle = resources
            .filter({ $0.id == resourceId }).first?.subTitle
        self.resourceSubtitles = resources
            .filter({ $0.id == resourceId }).first?.resourceSubtitles
        self.currentResource = resources.filter({ $0.id == resourceId }).first

        self.resources = resources
        self.type = type

        switch type {
        case .episode, .season, .audio:
            for i in resources.indices {
                if resources[i].id == resourceId {
                    currentResourceIndex = i
                }
            }
        case .manySeasons:
            guard let seasons = seasons else { return }
            self.seasons = seasons
            for i in seasons.indices {
                guard let resources = seasons[i].resources else { return }
                for j in resources.indices {
                    if seasons[i].resources?[j].id == resourceId {
                        currentSeasonIndex = i
                        currentResourceIndex = j
                    }
                }
            }
        }
    }
    
    //Offline init
    init(video: VideoResponse, type: CollectionType, media: String?, subtitles: [ResourceSubtitle]?) {
        self.videoTitle = video.name ?? "Untitled"
        self.video = video
        self.type = type
        self.persistedMedia = media
        self.persistedSubtitles = subtitles
        self.resources = []
    }
    
    func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func didPressOnNextEp() {
        switch type {
        case .episode, .season, .audio:
            guard let media = resources[safe: currentResourceIndex+1]?.media
            else { return }
            updateCurrentVideoIndexes(resourceIndex: currentResourceIndex+1, seasonIndex: nil)
            getMedia(media: media)
        case .manySeasons:
            guard let seasons = seasons,
                  let seasonIndex = currentSeasonIndex
            else { return }

            if let resourse = seasons[safe: seasonIndex]?.resources?[safe: currentResourceIndex+1],
               let media = resourse.media {
                
                updateCurrentVideoIndexes(resourceIndex: currentResourceIndex+1, seasonIndex: seasonIndex)
                getMedia(media: media)
                
            } else if let resourse = seasons[safe: seasonIndex+1]?.resources?[0],
                      let media = resourse.media {
                
                updateCurrentVideoIndexes(resourceIndex: 0, seasonIndex: seasonIndex+1)
                getMedia(media: media)
            }
        }
    }
    
    func hasNext() -> Bool {
        switch type {
        case .episode, .season, .audio:
            return currentResourceIndex < resources.count - 1
        case .manySeasons:
            guard let seasons = seasons,
                  let seasonIndex = currentSeasonIndex
            else { return false }

            return currentResourceIndex < (seasons[safe: seasonIndex]?.resources?.count ?? 1) - 1

        }
    }
    func onSpecificEpTapped(media: String, seasonIndex: Int?, resourceIndex: Int) {
        updateCurrentVideoIndexes(resourceIndex: resourceIndex, seasonIndex: seasonIndex)
        getMedia(media: media)
    }
    
    func updateCurrentVideoIndexes(resourceIndex: Int, seasonIndex: Int?) {
        currentResourceIndex = resourceIndex
        currentSeasonIndex = seasonIndex
        
        switch type {
        case .episode, .season, .audio:
            guard let resource = resources[safe: currentResourceIndex] else { return }
            self.videoTitle = resource.title ?? "Untitled"
            self.timeCodeFromViewed = resource.timeCodeFromViewed
            self.currentResourceId = resource.id
        case .manySeasons:
            guard let seasons = seasons,
                  let seasonIndex = seasonIndex,
                  let resource = seasons[safe: seasonIndex]?.resources?[currentResourceIndex]
            else { return }
            self.videoTitle = resource.title ?? "Untitled"
            self.timeCodeFromViewed = resource.timeCodeFromViewed
            self.currentResourceId = resource.id
        }
    }

    func timeCode(resourceId: Int, profileId: Int, timeCode: Int) {
        guard let token = DefaultsHelper.userToken
        else { return }
        ApiCall.Video.timeCode(token:token, resourceId: resourceId, profileId: profileId, timeCode: timeCode, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
}

extension VideoPlayViewModel {
    
    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        currentNetworkedVideo.accept(response)
    }
    
    private func onSuccess(_ response: TimeCodeResponse?) {
        guard let _ = response else { return }
        delegate?.onSuccessTimeCodeSend()
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
