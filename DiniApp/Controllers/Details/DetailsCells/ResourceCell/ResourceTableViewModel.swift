//
//  ResourceTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import Photos
import RxSwift

class ResourceTableViewModel: BaseCellViewModel {
        
    var title: String?
    var posterPath: String?
    var duration: Int?
    var progress: Float?
    var media: String?
    
    var downloadState: DownloadState = .none
    var durationInMinutes = 0
    var isNewResource = false
    
    var isOffline: Bool = false
//    var delegate: ResourcesDelegate?
//    var download: Download
//    var media: String

    init(cellHeight: CGFloat, title: String?,
         posterPath: String?, duration: Int?,
         progress: Int?, media: String?, isOffline: Bool? = nil, isNewResource: Bool) {
        
        self.title = title
        self.posterPath = posterPath
        self.duration = duration
        self.media = media
        self.isNewResource = isNewResource
        self.isOffline = isOffline ?? false

        if let d = duration {
            self.durationInMinutes = d/60
            
            if let p = progress {
                self.progress = Float(p)/Float(d)
            }
        }
        
        super.init(cellHeight: cellHeight)
    }
}
