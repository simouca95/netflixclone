//
//  VideoData.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/09/2021.
//

import Foundation

// MARK: - VideoData
public struct VideoResponse: Codable {
    public let uri, name: String?
    //    let videoDataDescription: JSONNull?
    //    let type: String?
    //    let link: String?
    //    let duration, width: Int?
    //    let language: JSONNull?
    //    let height: Int?
    //    let embed: Embed?
    //    let createdTime, modifiedTime, releaseTime: Date?
    //    let contentRating: [String]?
    //    let contentRatingClass: String?
    //    let ratingModLocked: Bool?
    //    let license: String?
    //    let privacy: Privacy?
    //    let pictures: Pictures?
    //    let tags: [JSONAny]?
    //    let stats: Stats?
    //    let categories: [JSONAny]?
    //    let uploader: Uploader?
    //    let metadata: VideoDataMetadata?
    //    let manageLink: String?
    //    let user: User?
    //    let parentFolder: JSONNull?
    //    let lastUserActionEventDate: Date?
    //    let reviewPage: ReviewPage?
    public let files: [Download]?
    //    let download: [Download]?
    //    let app: App?
    //    let status, resourceKey: String?
    //    let upload: Upload?
    //    let transcode: Transcode?
    //    let isPlayable, hasAudio: Bool?
    
    /// offline proprities
    let media: String?

    let localDuration: Int?
    let localPicture: String?
    let isLocalResource: Bool?
    enum CodingKeys: String, CodingKey {
        case uri, name
        //        case videoDataDescription = "description"
        //        case type, link, duration, width, language, height, embed
        //        case createdTime = "created_time"
        //        case modifiedTime = "modified_time"
        //        case releaseTime = "release_time"
        //        case contentRating = "content_rating"
        //        case contentRatingClass = "content_rating_class"
        //        case ratingModLocked = "rating_mod_locked"
        //        case license, privacy, pictures, tags, stats, categories, uploader, metadata
        //        case manageLink = "manage_link"
        //        case user
        //        case parentFolder = "parent_folder"
        //        case lastUserActionEventDate = "last_user_action_event_date"
        //        case reviewPage = "review_page"
        case files
        //        case download, app, status
        //        case resourceKey = "resource_key"
        //        case upload, transcode
        //        case isPlayable = "is_playable"
        //        case hasAudio = "has_audio"
        case media
        case localDuration
        case localPicture
        case isLocalResource
    }
}

// MARK: - App
struct App: Codable {
    let name, uri: String?
}

// MARK: - Download
public struct Download: Codable {
    public let quality: String?
    public let type: String?
    public let width, height: Int?
    public let expires: String?
    public let link: String?
    public let createdTime: String?
    public let fps: Double?
    public let size: Int?
    public let md5, publicName, sizeShort: String?
    
    enum CodingKeys: String, CodingKey {
        case quality, type, width, height, expires
        case link
        case createdTime = "created_time"
        case fps, size, md5
        case publicName = "public_name"
        case sizeShort = "size_short"
    }
}

// MARK: - Embed
struct Embed: Codable {
    let buttons: Buttons?
    let logos: Logos?
    let title: Title?
    let endScreen: EndScreen?
    let playbar, volume, speed: Bool?
    let color: String?
    let eventSchedule: Bool?
    let uri, html: String?
    let badges: Badges?
    
    enum CodingKeys: String, CodingKey {
        case buttons, logos, title
        case endScreen = "end_screen"
        case playbar, volume, speed, color
        case eventSchedule = "event_schedule"
        case uri, html, badges
    }
}

// MARK: - Badges
struct Badges: Codable {
    let hdr: Bool?
    let live: Live?
    let staffPick: StaffPick?
    let vod, weekendChallenge: Bool?
    
    enum CodingKeys: String, CodingKey {
        case hdr, live
        case staffPick = "staff_pick"
        case vod
        case weekendChallenge = "weekend_challenge"
    }
}

// MARK: - Live
struct Live: Codable {
    let streaming, archived: Bool?
}

// MARK: - StaffPick
struct StaffPick: Codable {
    let normal, bestOfTheMonth, bestOfTheYear, premiere: Bool?
    
    enum CodingKeys: String, CodingKey {
        case normal
        case bestOfTheMonth = "best_of_the_month"
        case bestOfTheYear = "best_of_the_year"
        case premiere
    }
}

// MARK: - Buttons
struct Buttons: Codable {
    let like, watchlater, share, embed: Bool?
    let hd, fullscreen, scaling: Bool?
}

// MARK: - EndScreen
struct EndScreen: Codable {
    let type: String?
}

// MARK: - Logos
struct Logos: Codable {
    let vimeo: Bool?
    let custom: Custom?
}

// MARK: - Custom
struct Custom: Codable {
    let active: Bool?
    let url, link: JSONNull?
    let sticky: Bool?
}

// MARK: - Title
struct Title: Codable {
    let name, owner, portrait: String?
}

// MARK: - VideoDataMetadata
struct VideoDataMetadata: Codable {
    let connections: PurpleConnections?
    let interactions: Interactions?
    let isVimeoCreate, isScreenRecord: Bool?
    
    enum CodingKeys: String, CodingKey {
        case connections, interactions
        case isVimeoCreate = "is_vimeo_create"
        case isScreenRecord = "is_screen_record"
    }
}

// MARK: - PurpleConnections
struct PurpleConnections: Codable {
    let comments, credits, likes, pictures: Albums?
    let texttracks: Albums?
    let related: JSONNull?
    let recommendations: Recommendations?
    let albums, availableAlbums: Albums?
    let versions: Versions?
    
    enum CodingKeys: String, CodingKey {
        case comments, credits, likes, pictures, texttracks, related, recommendations, albums
        case availableAlbums = "available_albums"
        case versions
    }
}

// MARK: - Albums
struct Albums: Codable {
    let uri: String?
    let options: [Option]?
    let total: Int?
}

enum Option: String, Codable {
    case delete = "DELETE"
    case optionGET = "GET"
    case patch = "PATCH"
    case post = "POST"
}

// MARK: - Recommendations
struct Recommendations: Codable {
    let uri: String?
    let options: [Option]?
}

// MARK: - Versions
struct Versions: Codable {
    let uri: String?
    let options: [Option]?
    let total: Int?
    let currentURI, resourceKey: String?
    
    enum CodingKeys: String, CodingKey {
        case uri, options, total
        case currentURI = "current_uri"
        case resourceKey = "resource_key"
    }
}

// MARK: - Interactions
struct Interactions: Codable {
    let watchlater: Watchlater?
    let report: Report?
    let viewTeamMembers, edit: Recommendations?
    let editContentRating: EditContentRating?
    let delete, canUpdatePrivacyToPublic, trim: Recommendations?
    
    enum CodingKeys: String, CodingKey {
        case watchlater, report
        case viewTeamMembers = "view_team_members"
        case edit
        case editContentRating = "edit_content_rating"
        case delete
        case canUpdatePrivacyToPublic = "can_update_privacy_to_public"
        case trim
    }
}

// MARK: - EditContentRating
struct EditContentRating: Codable {
    let uri: String?
    let options: [Option]?
    let contentRating: [String]?
    
    enum CodingKeys: String, CodingKey {
        case uri, options
        case contentRating = "content_rating"
    }
}

// MARK: - Report
struct Report: Codable {
    let uri: String?
    let options: [Option]?
    let reason: [String]?
}

// MARK: - Watchlater
struct Watchlater: Codable {
    let uri: String?
    let options: [String]?
    let added: Bool?
    let addedTime: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case uri, options, added
        case addedTime = "added_time"
    }
}

// MARK: - Pictures
struct Pictures: Codable {
    let uri: String?
    let active: Bool?
    let type: String?
    let sizes: [Size]?
    let resourceKey: String?
    let defaultPicture: Bool?
    
    enum CodingKeys: String, CodingKey {
        case uri, active, type, sizes
        case resourceKey = "resource_key"
        case defaultPicture = "default_picture"
    }
}

// MARK: - Size
struct Size: Codable {
    let width, height: Int?
    let link, linkWithPlayButton: String?
    
    enum CodingKeys: String, CodingKey {
        case width, height, link
        case linkWithPlayButton = "link_with_play_button"
    }
}

// MARK: - Privacy
struct Privacy: Codable {
    let view, embed: String?
    let download, add: Bool?
    let comments: String?
}

// MARK: - ReviewPage
struct ReviewPage: Codable {
    let active: Bool?
    let link: String?
}

// MARK: - Stats
struct Stats: Codable {
    let plays: Int?
}

// MARK: - Transcode
struct Transcode: Codable {
    let status: String?
}

// MARK: - Upload
struct Upload: Codable {
    let status: String?
    let link, uploadLink, completeURI, form: JSONNull?
    let approach, size, redirectURL: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case status, link
        case uploadLink = "upload_link"
        case completeURI = "complete_uri"
        case form, approach, size
        case redirectURL = "redirect_url"
    }
}

// MARK: - Uploader
struct Uploader: Codable {
    let pictures: Pictures?
}

// MARK: - User
struct User: Codable {
    let uri, name: String?
    let link: String?
    let capabilities: Capabilities?
    let location, gender: String?
    let bio, shortBio: JSONNull?
    let createdTime: Date?
    let pictures: Pictures?
    let websites: [JSONAny]?
    let metadata: UserMetadata?
    let locationDetails: LocationDetails?
    let skills: [JSONAny]?
    let availableForHire, canWorkRemotely: Bool?
    let preferences: Preferences?
    let contentFilter: [String]?
    let uploadQuota: UploadQuota?
    let resourceKey, account: String?
    
    enum CodingKeys: String, CodingKey {
        case uri, name, link, capabilities, location, gender, bio
        case shortBio = "short_bio"
        case createdTime = "created_time"
        case pictures, websites, metadata
        case locationDetails = "location_details"
        case skills
        case availableForHire = "available_for_hire"
        case canWorkRemotely = "can_work_remotely"
        case preferences
        case contentFilter = "content_filter"
        case uploadQuota = "upload_quota"
        case resourceKey = "resource_key"
        case account
    }
}

// MARK: - Capabilities
struct Capabilities: Codable {
    let hasLiveSubscription, hasEnterpriseLihp, hasSvvTimecodedComments: Bool?
}

// MARK: - LocationDetails
struct LocationDetails: Codable {
    let formattedAddress: String?
    let latitude, longitude, city, state: JSONNull?
    let neighborhood, subLocality, stateISOCode, country: JSONNull?
    let countryISOCode: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case formattedAddress = "formatted_address"
        case latitude, longitude, city, state, neighborhood
        case subLocality = "sub_locality"
        case stateISOCode = "state_iso_code"
        case country
        case countryISOCode = "country_iso_code"
    }
}

// MARK: - UserMetadata
struct UserMetadata: Codable {
    let connections: FluffyConnections?
}

// MARK: - FluffyConnections
struct FluffyConnections: Codable {
    let albums, appearances, categories, channels: Albums?
    let feed: Recommendations?
    let followers, following, groups, likes: Albums?
    let membership: Recommendations?
    let moderatedChannels, portfolios, videos, watchlater: Albums?
    let shared, pictures, watchedVideos: Albums?
    let foldersRoot: Recommendations?
    let folders, teams, block: Albums?
    
    enum CodingKeys: String, CodingKey {
        case albums, appearances, categories, channels, feed, followers, following, groups, likes, membership
        case moderatedChannels = "moderated_channels"
        case portfolios, videos, watchlater, shared, pictures
        case watchedVideos = "watched_videos"
        case foldersRoot = "folders_root"
        case folders, teams, block
    }
}

// MARK: - Preferences
struct Preferences: Codable {
    let videos: Videos?
}

// MARK: - Videos
struct Videos: Codable {
    let rating: [String]?
    let privacy: Privacy?
}

// MARK: - UploadQuota
struct UploadQuota: Codable {
    let space, periodic, lifetime: Lifetime?
}

// MARK: - Lifetime
struct Lifetime: Codable {
    let free, max, used: Int?
    let resetDate: JSONNull?
    let showing: String?
    
    enum CodingKeys: String, CodingKey {
        case free, max, used
        case resetDate = "reset_date"
        case showing
    }
}
