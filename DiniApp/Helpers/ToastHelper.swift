//
//  ToastHelper.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import Toast_Swift

struct ToastHelper {
    
    // MARK: Public Static Methods
    
    static func show(text: String?) {
        if let error = text, !error.isEmpty {
            if let view = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
                var style = ToastStyle()
                style.backgroundColor = .lightGray
                style.messageColor = .black
                view.makeToast(text, duration: 2.0, position: .bottom, style: style)
            }
        }
    }
}
