//
//  Notification.swift
//  DiniApp
//
//  Created by Wafa Labidi on 2/7/2022.
//

import Foundation

public class NotificationResponse: Codable {
    public var status: Bool
    public var displaySpotIsNewNotification: Bool
    public var notification: [NotificationData]
    
    enum CodingKeys: String, CodingKey {
        case status, displaySpotIsNewNotification
        case notification  = "data"
    }
}

public struct NotificationData: Codable {
    public let title: String?
    public let content: String?
    public let sendingAt: String?
    public let isNewNotification: Bool?
    public let collectionId: Int?
    public let collectionTitle: String?
    public let resourceId: Int?
    public let resourceTitle: String?
    public let collectionIdIfNotificationTypeResource: Int?
}
