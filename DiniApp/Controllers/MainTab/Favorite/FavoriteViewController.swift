//
//  FavoriteViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/10/2021.
//

import UIKit
import RxSwift
import Kingfisher
import SVGKit

class FavoriteViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerBackground: UIView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var appLogoButton: UIButton!
    
    var viewModel: FavoriteViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initViewModels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manageLogo()
        viewModel.getCollectionList()
    }
    
    
    private func manageLogo() {
        if DefaultsHelper.nameToDisplayInSwitchProfileDefault == "Section tout public" {
            if let logoPath = URL(string: DefaultsHelper.logoChild ?? "") {
                appLogoButton.getURL2(url: logoPath)
            }
        } else {
            if let logoPath = URL(string: DefaultsHelper.logo ?? "") {
                appLogoButton.getURL2(url: logoPath)
            }
        }
    }
    
    func initViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: StandardCollectionViewCell.self)
        headerBackground.alpha = 0.8
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 6
        layout.minimumInteritemSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        collectionView.setCollectionViewLayout(layout, animated: false)
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
    }
    
    func initViewModels() {
        viewModel = FavoriteViewModel()
        viewModel.getCollectionList()
        viewModel.reloadData.asObservable()
            .subscribe (onNext:{ [weak self] value in
                guard let self = self else { return }
                self.collectionView.reloadData()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
    }
}

extension FavoriteViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: StandardCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        
        let item = viewModel.items[indexPath.row]
        
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            cell.imageView.kf.setImage(with: url)
        } else {
            cell.imageView.image = UIImage(named: "ItemEmpty")
        }
        
        //        cell.leftConstraintConstant.constant = 53
        cell.imageView.layer.cornerRadius = 10
        cell.imageView.clipsToBounds = true
        cell.imageView.layer.masksToBounds = true
        
        cell.layer.cornerRadius = 6
        
        return cell
    }
}

extension FavoriteViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width - 30) / 3
        let height = width/0.67
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = viewModel.items[indexPath.row]
        
        if let id = item.id {
            let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
            let vm = DetailsViewModel(collectionId: id, infoState: infoState, delegate: self)
            let vc = DetailsViewController.instantiate(with: vm)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension FavoriteViewController: DetailsDelegate {
    func onInfoStateChanged() {
        viewModel.getCollectionList()
    }
}
