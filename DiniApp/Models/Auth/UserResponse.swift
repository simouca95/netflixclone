//
//  UserResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 13/09/2021.
//

public typealias ProfileAvatars = [String: [String]]

// MARK: - UserInfo
public struct UserResponse: Codable {
    public let id: Int?
    public let email: String?
    public let roles: [String]?
    public let identifier, firstname, lastname, address: String?
    public let city, zipcode, country: String?
    public let profils: [Profil]?
    public let subscription: SubscriptionUser?
    public let appDeviceManagement: [AppDeviceManagement]?
}

// MARK: - Profil
public struct Profil: Codable {
    public let id: Int?
    public let name, picture: String?
    public let type: ProfilType?
    public let avatarPicturePath: String?
    public let nameToDisplayInSwitchProfileDefault: String?
}

// MARK: - ProfilType
public struct ProfilType: Codable {
    public let id: Int?
    public let name, code: String?
}

// MARK: - Subscription
public struct SubscriptionUser: Codable {
    public let id: Int?
    public let endAt: String?
    let product: Product?
    let billing: Billing?
    public let reference: String?
    public let canceledAt: String?
    
}

// MARK: - Billing
struct Billing: Codable {
    let id, period: Int?
}

// MARK: - Product
struct Product: Codable {
    let id: Int?
    let name: String?
    let price: Int?
}

// MARK: -AppDeviceManagement

public struct AppDeviceManagement: Codable {
    let deviceId: String?
    let isNotificationAccepted: Bool?
}
