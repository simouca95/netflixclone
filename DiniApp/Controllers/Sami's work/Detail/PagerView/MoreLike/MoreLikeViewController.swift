//
//  MoreLikeViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/28/20.
//

import UIKit
import XLPagerTabStrip

class MoreLikeViewController: UIViewController {

    @IBOutlet weak var moreLikeCollectionView: UICollectionView!
    
    var videos = DataMock.instance.videos1

    var parentDelegate : UpdateScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.initView()
    }
    
    override func viewDidLayoutSubviews() {
        self.parentDelegate.updateScrollViewHeight(height: self.moreLikeCollectionView.contentSize.height)

    }

    func initView()  {
        self.registerCollectionViewCells()
    }
    
    func registerCollectionViewCells() {

        self.moreLikeCollectionView.register(UINib(nibName: "NormalSectionCollectionViewCell", bundle: nil),
                                     forCellWithReuseIdentifier: NormalSectionCollectionViewCell.identifier)
        
        self.moreLikeCollectionView.delegate = self
        self.moreLikeCollectionView.dataSource = self
        
    }


}
extension MoreLikeViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width - 10) / 3
        let height = width * 1.5
        return CGSize(width: width, height:height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension MoreLikeViewController : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalSectionCollectionViewCell.identifier, for: indexPath) as! NormalSectionCollectionViewCell
        
        cell.setupContent(video: self.videos[indexPath.item])
        
        return cell
        
    }
    
    
}
extension MoreLikeViewController : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title:"MoreLikeViewController")
        
    }
}
