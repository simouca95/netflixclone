//
//  ApiCall+Home.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Home {
        
        public static func getHomeCollectionList(token: String, id: Int,deviceId: String , onSuccess: @escaping (_ response: CollectionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.HomeController.getCollectionList(token: token, profileId: id, deviceId: deviceId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: CollectionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func switchProfileType(token: String, profileId: Int, userId: Int, onSuccess: @escaping (_ response: SwitchProfileResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.HomeController.switchProfile(token: token, userId: userId, profileId: profileId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: SwitchProfileResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
    }

}
