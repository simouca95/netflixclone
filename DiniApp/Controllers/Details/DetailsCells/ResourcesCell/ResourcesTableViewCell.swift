//
//  ResourcesTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import Reusable
import DropDown

protocol ResourcesDelegate: AnyObject {
    func didSelectItem(resources: [DiniResource], clickedIndex: Int)
    func didSelectItemToDownload(resources: [DiniResource], clickedIndex: Int)
    func didSelectItemToCancel(resources: [DiniResource], clickedIndex: Int)
    func didSelectItemToDelete(resources: [DiniResource], clickedIndex: Int)
    func didSeasonChanged(row: Int)
}

class ResourcesTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var type2View: UIView!
    @IBOutlet weak var type2ViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropDownTitleLabel: UILabel!
    @IBOutlet weak var seasonNameLabel: UILabel!
    
    let dropDown = DropDown()
    weak var delegate: ResourcesDelegate?
    
    var viewModel: ResourcesTableViewModel! {
        didSet {
            switch viewModel.type {
            case .episode, .season, .audio:
                type2View.isHidden = true
                type2ViewHeightConstraint.constant = 0
                tableView.reloadData()
            case .manySeasons:
                type2View.isHidden = false
                type2ViewHeightConstraint.constant = 70
                guard let seasons = viewModel.seasons else { return }
                dropDownTitleLabel.text = seasons[safe: viewModel.selectedSeason]?.name
                seasonNameLabel.text = seasons[safe: viewModel.selectedSeason]?.name
                populateDropDown(seasons: seasons)
                tableView.reloadData()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(cellType: ResourceTableViewCell.self)
        initDropDown()
    }
    
    func initDropDown() {
        dropDownView.layer.cornerRadius = 4
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dropDownView.addGestureRecognizer(tap)
        dropDown.anchorView = dropDownView
    }
    
    func populateDropDown(seasons: [Season]) {
        dropDown.dataSource = seasons.compactMap{$0.name}
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            dropDownTitleLabel.text = seasons[index].name
            seasonNameLabel.text = seasons[index].name
            if let data = seasons[index].resources {
                delegate?.didSeasonChanged(row: index)
                viewModel.resources = data
                tableView.reloadData()
            }
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        dropDown.show()
    }
    
    func updateDisplay(download: DownloadVideo) {
        guard let index = self.viewModel.resources.firstIndex(where: {$0.media == download.media})
        else {
            print("0????")
            return
        }
        DispatchQueue.main.async {
            if let resourceTableViewCell =
                self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ResourceTableViewCell {
                if download.dowloadState == .downloading {
                    resourceTableViewCell.updateDisplay(progress: download.progress)
                    resourceTableViewCell.viewModel.downloadState = .downloading
                } else {
                    resourceTableViewCell.makeDownloadDone()
                    resourceTableViewCell.viewModel.downloadState = .none
                }
            } else {
                print("1????")
            }
        }
    }
    
    func updateDisplay(media: String) {
        guard let index = self.viewModel.resources.firstIndex(where: {$0.media == media})
        else {
            print("0????")
            return
        }
        DispatchQueue.main.async {
            if let resourceTableViewCell =
                self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ResourceTableViewCell {
                resourceTableViewCell.resetCell()
            } else {
                print("1????")
            }
        }
    }
}

extension ResourcesTableViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel != nil {
            return viewModel.resources.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ResourceTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        if let item = viewModel.resources[safe: indexPath.row] {
            cell.viewModel = ResourceTableViewModel(cellHeight: 140, title: item.title,
                                                    posterPath: item.picture, duration: item.duration,
                                                    progress: item.timeCodeFromViewed,
                                                    media: item.media,
                                                    isNewResource: item.isNewResource ?? false)
        }
        cell.onDownloadTapped = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didSelectItemToDownload(resources: self.viewModel.resources,
                                              clickedIndex: indexPath.row)
        }
        
        cell.onCancelTapped = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didSelectItemToCancel(resources: self.viewModel.resources,
                                              clickedIndex: indexPath.row)
        }

        cell.onDeleteTapped = { [weak self] in
            guard let self = self else { return }
            self.delegate?.didSelectItemToDelete(resources: self.viewModel.resources,
                                              clickedIndex: indexPath.row)
        }
        return cell
    }
}

extension ResourcesTableViewCell: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectItem(resources: viewModel.resources, clickedIndex: indexPath.row)
    }
}
