//
//  ProfilType.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 08/12/2021.
//

import Foundation

public typealias ProfilTypes = [ProfilType]

// MARK: - ProfilAction
public struct ProfilActionResponse: Codable {
    public let status: Bool?
    public let action: String?
}
