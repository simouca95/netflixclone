// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum Details: StoryboardType {
    internal static let storyboardName = "Details"

    internal static let detailsViewController = SceneType<DetailsViewController>(storyboard: Details.self, identifier: "DetailsViewController")

    internal static let diniSpeakersViewController = SceneType<DiniSpeakersViewController>(storyboard: Details.self, identifier: "DiniSpeakersViewController")
  }
  internal enum Landing: StoryboardType {
    internal static let storyboardName = "Landing"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Landing.self)

    internal static let loginViewController = SceneType<LoginViewController>(storyboard: Landing.self, identifier: "LoginViewController")

    internal static let splashScreenViewController = SceneType<SplashScreenViewController>(storyboard: Landing.self, identifier: "SplashScreenViewController")
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"

    internal static let catalogViewController = SceneType<CatalogViewController>(storyboard: Main.self, identifier: "CatalogViewController")

    internal static let favoriteViewController = SceneType<FavoriteViewController>(storyboard: Main.self, identifier: "FavoriteViewController")

    internal static let homeViewController = SceneType<HomeViewController>(storyboard: Main.self, identifier: "HomeViewController")

    internal static let mainNVC = SceneType<UIKit.UINavigationController>(storyboard: Main.self, identifier: "MainNVC")

    internal static let tabBarController = SceneType<UIKit.UITabBarController>(storyboard: Main.self, identifier: "TabBarController")
  }
  internal enum Profile: StoryboardType {
    internal static let storyboardName = "Profile"

    internal static let initialScene = InitialSceneType<SelectProfileViewController>(storyboard: Profile.self)

    internal static let selectProfileViewController = SceneType<SelectProfileViewController>(storyboard: Profile.self, identifier: "SelectProfileViewController")
  }
  internal enum Video: StoryboardType {
    internal static let storyboardName = "Video"

    internal static let videoViewController = SceneType<UIKit.UIViewController>(storyboard: Video.self, identifier: "VideoViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
