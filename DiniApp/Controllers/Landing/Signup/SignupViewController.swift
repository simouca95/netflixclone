//
//  SignupViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 13/03/2022.
//

import Foundation
import RxSwift
import SkyFloatingLabelTextField
import SafariServices

class SignupViewController: BaseViewController {
    
    // MARK: Public Static Methods
    
    static func newViewController() -> UIViewController {
        let viewController = StoryboardScene.Landing.signupViewController.instantiate()
        return viewController
    }
    
    // MARK: Outlets
    @IBOutlet weak var logoButton: UIButton!

    @IBOutlet weak var signupButton: UIButton!

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var firstnameView: UIView!
    @IBOutlet weak var lastnameView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var emailtextField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstnameField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastnameField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberField: SkyFloatingLabelTextField!

    // MARK: Private Properties
    
    private var viewModel: SignupViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: Asset.back.image.withRenderingMode(.alwaysOriginal),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(self.backTapped))]
        self.viewModel = SignupViewModel(taskNavigator: self)
        self.initViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true

    }
    
    // MARK: Private Methods
    
    private func initViews() {
        if let logo = DefaultsHelper.logo {
            logoButton.kf.setImage(with: URL(string: logo), for: .normal)
        }
        
        emailtextField.placeholder = "E-mail"
        emailtextField.placeholderFont = UIFont.systemFont(ofSize: 18)
        emailtextField.titleFormatter = { $0 }
        emailtextField.font = UIFont.systemFont(ofSize: 18)
        
        firstnameField.placeholder = "Prénom"
        firstnameField.placeholderFont = UIFont.systemFont(ofSize: 18)
        firstnameField.titleFormatter = { $0 }
        firstnameField.font = UIFont.systemFont(ofSize: 18)
        
        lastnameField.placeholder = "Nom"
        lastnameField.placeholderFont = UIFont.systemFont(ofSize: 18)
        lastnameField.titleFormatter = { $0 }
        lastnameField.font = UIFont.systemFont(ofSize: 18)
        
        phoneNumberField.placeholder = "Numéro de téléphone"
        phoneNumberField.placeholderFont = UIFont.systemFont(ofSize: 18)
        phoneNumberField.titleFormatter = { $0 }
        phoneNumberField.font = UIFont.systemFont(ofSize: 18)
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        signupButton.backgroundColor = .primary
        signupButton.layer.cornerRadius = 4
        emailView.layer.cornerRadius = 4
        firstnameView.layer.cornerRadius = 4
        lastnameView.layer.cornerRadius = 4
        phoneNumberView.layer.cornerRadius = 4

    }

    private func initViewModel() {
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func signup(_ sender: UIButton) {
        guard let email = emailtextField.text, email.count > 0,
              let firstname = firstnameField.text, firstname.count > 0,
              let lastname = lastnameField.text, lastname.count > 0,
              let phoneNumber = phoneNumberField.text, phoneNumber.count > 0
        else {
            ToastHelper.show(text: "Veillez remplir tous les champs")
            return
        }
        
        viewModel.signup(email: email,
                         firstName: firstname,
                         lastname: lastname,
                         phoneNumber: phoneNumber)
    }
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SignupViewController: SignupTaskNavigator {
    
    func goToLoginScreen() {
        NavigationHelper.shared.startAppFromLogin()
    }
}
