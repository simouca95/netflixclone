//
//  SpeakerResourceTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import UIKit
import Reusable
import Kingfisher

class SpeakerResourceTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var newResourceImageView: UIImageView!

    var viewModel: SpeakerResourceTableViewModel! {
        didSet {
            newResourceImageView.kf.setImage(with: URL(string: DefaultsHelper.newResourcePicturePath ?? ""))
            newResourceImageView.isHidden = !viewModel.isNewResource
            if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                 path2: viewModel.posterPath) {
                posterImage.kf.setImage(with: url)
            } else {
                posterImage.image = UIImage(named: "ItemEmpty")
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        posterImage.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
