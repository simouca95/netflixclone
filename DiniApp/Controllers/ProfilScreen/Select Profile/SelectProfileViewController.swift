//
//  SelectProfileViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import RxSwift
import SVGKit

class SelectProfileViewController: BaseViewController {
    
    @IBOutlet weak var profilesCollectionView: UICollectionView!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var finishEditingButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addProfilButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var authorizeLabel: UILabel!
    @IBOutlet weak var authorizeCheckBox: Checkbox!
    @IBOutlet weak var restartSessionButton: UIButton!
    @IBOutlet weak var rgpdButton: UIButton!
    @IBOutlet weak var cookiesButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    
    var viewModel: SelectProfileViewModel!
    private var disposeBag = DisposeBag()
    private var rgpdLink = ""
    private var cookiesLink = ""
    private var privacyLink = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViews()
        self.initViewModels()
        authorizeCheckBox.isChecked = DefaultsHelper.isNotificationAuthorized ?? true
        rgpdButton.isHidden = true
        cookiesButton.isHidden = true
        privacyButton.isHidden = true
    }
    
    func initViews() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 40
        layout.sectionInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
        if UIDevice.current.userInterfaceIdiom == .pad {
            let top = self.profilesCollectionView.frame.height * 0.3
            layout.sectionInset = UIEdgeInsets(top: top, left: 120, bottom: 0, right: 120)
        }
        
        profilesCollectionView.setCollectionViewLayout(layout, animated: false)
        
        profilesCollectionView.register(cellType: ProfileCollectionViewCell.self)
        logoutButton.backgroundColor = .primary
        backButton.isHidden = true
        if let _ = DefaultsHelper.profilId {
            logoutButton.isHidden = false
        } else {
            logoutButton.isHidden = true
        }
        authorizeLabel.text = "Autoriser les notifications"
        authorizeLabel.textColor = .white
        authorizeCheckBox.borderStyle = .circle
        authorizeCheckBox.checkedBorderColor = .primary
        authorizeCheckBox.uncheckedBorderColor = .primary
        authorizeCheckBox.checkmarkStyle = .circle
        authorizeCheckBox.checkmarkColor = .primary
        authorizeCheckBox.valueChanged =  { (isChecked) in
            self.viewModel.authorizeNotification(isNotificationAccepted: String(isChecked))
        }
        
    }
    
    func initViewModels() {
        viewModel = SelectProfileViewModel()
        viewModel.getUserLegalInfo()
        viewModel.userInfos()
        viewModel.reloadData.asObservable()
            .subscribe (onNext:{[weak self] value in
                guard let self = self else { return }
                self.profilesCollectionView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.isEditingProfiles
            .asObservable()
            .skip(1)
            .compactMap{ $0 }
            .distinctUntilChanged()
            .subscribe (onNext: { [weak self] bool in
                guard let self = self else { return }
                if DefaultsHelper.profilId == nil {
                    self.backButton.isHidden = bool
                }
                self.finishEditingButton.isHidden = !bool
                self.addProfilButton.isHidden = !bool
                self.editProfileButton.isHidden = bool
                self.profilesCollectionView.reloadData()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.isChecked
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] (value) in
                self?.authorizeCheckBox.isChecked = value
            }).disposed(by: disposeBag)
        
        restartSessionButton.rx.tap.bind { [weak self] in
            self?.viewModel.restartSessionCounter()
        }.disposed(by: disposeBag)
        
        viewModel.showSessionPopup.skip(1).observeOn(MainScheduler.instance)
            .bind (onNext:{[weak self] message in
                guard let self = self else { return }
                if message != "" {
                let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            })
            .disposed(by: disposeBag)
        
        
        viewModel.legalInfo.asObservable()
            .observeOn(MainScheduler.instance)
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] legalInfo in
                guard let self = self else { return }
                let rgpd = legalInfo.data.userLegalInfos.rgpd
                let cookies = legalInfo.data.userLegalInfos.cookies
                let privacy = legalInfo.data.userLegalInfos.privacy
                self.rgpdButton.isHidden = rgpd.isVisible == "no"
                self.cookiesButton.isHidden = cookies.isVisible == "no"
                self.privacyButton.isHidden = privacy.isVisible == "no"
                self.rgpdLink = rgpd.path
                self.cookiesLink = cookies.path
                self.privacyLink = privacy.path
            })
            .disposed(by: self.disposeBag)
        
        rgpdButton.rx.tap.bind { [weak self] in
            if let url = URL(string: self?.rgpdLink ?? "") {
                UIApplication.shared.open(url)
            }
        }.disposed(by: disposeBag)
        
        cookiesButton.rx.tap.bind { [weak self] in
            if let url = URL(string: self?.cookiesLink ?? "") {
                UIApplication.shared.open(url)
            }
        }.disposed(by: disposeBag)
        
        privacyButton.rx.tap.bind { [weak self] in
            if let url = URL(string: self?.privacyLink ?? "") {
                UIApplication.shared.open(url)
            }
        }.disposed(by: disposeBag)
    }
    
    @IBAction func didTapEditProfile(_ sender: UIButton) {
        viewModel.isEditingProfiles.accept(true)
    }
    
    @IBAction func didTapEndingEditProfile(_ sender: UIButton) {
        viewModel.isEditingProfiles.accept(false)
    }
    
    @IBAction func didTapAddProfil(_ sender: Any) {
        if let maxProfilsCount = DefaultsHelper.maxProfilsInApplication,
           let currentProfilsCount = DefaultsHelper.currentprofilListCount,
           maxProfilsCount > currentProfilsCount {
            let vc = StoryboardScene.Profil.addEditProfileViewController.instantiate()
            vc.delegate = self
            vc.profil = nil
            vc.actionType = .add
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            ToastHelper.show(text: "Vous atteignez le nombre maximum de profils")
        }
    }
    
    @IBAction func didTapLogoutProfil(_ sender: Any) {
        DefaultsHelper.deleteAllDefaults()
        NavigationHelper.shared.startAppFromLogin()
    }
}
extension SelectProfileViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = 120
        if UIDevice.current.userInterfaceIdiom == .pad {
            width = 200
        }
        return CGSize(width: width, height: width+30)
    }
}
extension SelectProfileViewController : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.profils.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = viewModel.profils[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileCollectionViewCell.reuseIdentifier, for: indexPath) as! ProfileCollectionViewCell
        cell.viewModel = ProfileViewModel(profil: item, isEdit: viewModel.isEditingProfiles.value)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if viewModel.isEditingProfiles.value {
            let item = viewModel.profils[indexPath.row]
            let vc = StoryboardScene.Profil.addEditProfileViewController.instantiate()
            vc.delegate = self
            vc.profil = item
            vc.actionType = .edit
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if let path = viewModel.profils[indexPath.row].avatarPicturePath {
                DefaultsHelper.saveProfilImage(path)
            }
            if let nameToDisplayInSwitchProfileDefault = viewModel.profils[indexPath.row].nameToDisplayInSwitchProfileDefault {
                DefaultsHelper.saveNameToDisplayInSwitchProfileDefault(_nameToDisplayInSwitchProfileDefault: nameToDisplayInSwitchProfileDefault)
            }
            guard let id = viewModel.profils[indexPath.row].id else { return }
            DefaultsHelper.saveProfilId(id)
            NavigationHelper.shared.startAppFromMainTab()
        }
    }
}

extension SelectProfileViewController: AddEditProfileDelegate {
    func updatedUserInfos() {
        viewModel.isEditingProfiles.accept(false)
        viewModel.userInfos()
    }
}
