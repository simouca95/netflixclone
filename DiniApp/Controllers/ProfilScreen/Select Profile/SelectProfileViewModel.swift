//
//  SelectProfileViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 13/09/2021.
//

import RxSwift
import RxCocoa

class SelectProfileViewModel: BaseViewModel {
    
    var reloadData = BehaviorSubject<Void>(value: ())
    var profils: [Profil] = []
    var pathsList: [[String]] = []
    var profilsPathList: [(id: String, path: String?)] = []
    var isEditingProfiles: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    var isNotificationEnabled = "false"
    let isChecked = PublishSubject<Bool>()
    var showSessionPopup = BehaviorSubject<String>(value: "")
    var legalInfo = BehaviorSubject<UserLegalInfosResponse?>(value: nil)

    
    func userInfos() {
        if let id = DefaultsHelper.userId,
           let token = DefaultsHelper.userToken {
            isLoading.accept(true)
            ApiCall.Auth.userInfos(token: token, id: id, onSuccess: self.onSuccess, onError: self.onFail)
        }
    }
    
    func authorizeNotification(isNotificationAccepted: String) {
        isNotificationEnabled = isNotificationAccepted
        if let id = DefaultsHelper.userId, let token = DefaultsHelper.userToken,
           let deviceId = DefaultsHelper.deviceId {
            isLoading.accept(true)
            ApiCall.Auth.authorizeNotification(deviceId: deviceId,
                                               userId: id,
                                               token: token,
                                               isNotificationAccepted: isNotificationAccepted,
                                               onSuccess: self.onSuccess,
                                               onError: self.onFail)
        }
    }
    
    func restartSessionCounter() {
        if let token = DefaultsHelper.userToken,
           let id = DefaultsHelper.userId  {
            isLoading.accept(true)
            ApiCall.Auth.restartSessionCounter(token: token,
                                               userId: id,
                                               onSuccess: self.onSuccess,
                                               onError: self.onFail)
        }
    }
    
    func getUserLegalInfo() {
        if let token = DefaultsHelper.userToken {
            isLoading.accept(true)
            ApiCall.Auth.getUserLegalInfo(token: token, onSuccess: onSuccess, onError: onFail)
        }
    }
    
    func logout() {
        if let token = DefaultsHelper.userToken,
           let id = DefaultsHelper.userId {
            isLoading.accept(true)
            ApiCall.Auth.logout(token: token, userId: id, sessionToken: token, onSuccess: onSuccess, onError: onFail)
        }
    }
}

extension SelectProfileViewModel {
    // MARK: WS Callbacks
    private func onSuccess(_ response: UserResponse?) {
        guard let response = response, let profils = response.profils else {
            isLoading.accept(false)
            return }
        if let appDeviceManagement = response.appDeviceManagement {
            for device in appDeviceManagement {
                if DefaultsHelper.deviceId ?? "" == device.deviceId {
                    DefaultsHelper.saveNotificationStatus(device.isNotificationAccepted ?? true)
                    isChecked.onNext(device.isNotificationAccepted ?? false)
                    break
                }
            }
        }
        self.profils = profils
        reloadData.onNext(())
        isLoading.accept(false)
        
    }
    
    private func onSuccess(_ response: String?) {
        DefaultsHelper.saveNotificationStatus(Bool(isNotificationEnabled) ?? false)
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: SessionCounterResponse?) {
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: ResourceWatchResponse?) {
        isLoading.accept(false)
        if response?.isSessionNumberAttempt ?? false {
            if let message = response?.message {
                showSessionPopup.onNext(message)
            }
        }
    }
    
    private func onSuccess(_ response: UserLegalInfosResponse?) {
        guard let response = response else { return }
        legalInfo.onNext(response)
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
