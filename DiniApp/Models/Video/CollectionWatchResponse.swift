//
//  CollectionWatchResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 03/10/2021.
//

import Foundation

// MARK: - CollectionWatch
public struct CollectionWatchResponse: Codable {
    public let resourceID, media, duration, timeCode: String?
    public let title: String?

    enum CodingKeys: String, CodingKey {
        case resourceID = "resourceId"
        case media, duration, timeCode, title
    }
}

public struct ResourceWatchResponse: Codable {
    public let status: Bool?
    public let userSessionCounterLimit: Int?
    public let currentSessionCounter: Int?
    public let isSessionNumberAttempt: Bool?
    public let message: String?
}
