//
//  SignupViewModel.swift
//  netflixCloneApp
//
//  Created by sami hazel on 13/03/2022.
//

import Foundation
import RxSwift
import RxCocoa

class SignupViewModel: BaseViewModel {
    
    // MARK: Private Properties
    
    weak private var taskNavigator: SignupTaskNavigator!
    
    // MARK: Init Method

    init(taskNavigator : SignupTaskNavigator) {
        self.taskNavigator = taskNavigator
    }
    
    // MARK: Public Method

    func signup(email: String, firstName: String, lastname: String, phoneNumber: String) {
        self.isLoading.accept(true)
        ApiCall.Auth.signup(email: email,
                            firstname: firstName,
                            lastname: lastname,
                            phoneNumber: phoneNumber,
                            onSuccess: self.onSuccess,
                            onError: self.onFail)
    }
}

extension SignupViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: SignupResponse?) {
        self.isLoading.accept(false)
        if let response = response {
            if let status = response.status, status {
                ToastHelper.show(text: "User  created")

                self.taskNavigator.goToLoginScreen()
                return
            }
        }
        
        ToastHelper.show(text: "User already exist")
    }
    
    private func onFail(_ error: DiniError?) {
        self.isLoading.accept(false)
        ToastHelper.show(text: "User already exist")
    }
}
