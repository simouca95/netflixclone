//
//  EpisodeTableViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 24/02/2021.
//

import UIKit

class EpisodeTableViewCell: UITableViewCell {

    static let identifier = "EpisodeTableViewCell"

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var epLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.posterImageView.layer.borderWidth = 6
            self.posterImageView.layer.borderColor = UIColor.white.cgColor
        } else {
            self.posterImageView.layer.borderWidth = 0
            self.posterImageView.layer.borderColor = UIColor.clear.cgColor
        }
    }

}
