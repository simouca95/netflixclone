//
//  API+Islamic.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/03/2021.
//

import Foundation


import Alamofire

extension API {
    enum IslamicController {
        
        //https://api.pray.zone/v2/times/today.json        
        case prayHours(city: String)
        case surahs
        case ayahs(id: String, speaker: String)
        case ayahsFrench(id: String)
    }
}

extension API.IslamicController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .prayHours :
            return .get
        case .surahs :
            return .get
        case .ayahs :
            return .get
        case .ayahsFrench :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .prayHours:
            return ""
        case .surahs:
            return ""
        case .ayahs(let id, let speaker):
            return "/\(id)/\(speaker)"
        case .ayahsFrench(let id):
            return "/\(id)/fr.hamidullah"
        }
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .prayHours(let city):
            return ["city": city]
        case .surahs:
            return nil
        case .ayahs:
            return nil
        case .ayahsFrench:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        return Network.unsecureHeaders
    }
}
