//
//  Helper.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/12/2021.
//

import Foundation

struct Helper {
    
    static func concatinatePaths(path1: String?, path2: String?) -> URL? {
        if let baseURL = path1,
           let path = path2 {
            let urlString = "\(baseURL)/\(path)"
            return URL(string: urlString)
        }
        return nil
    }
}
