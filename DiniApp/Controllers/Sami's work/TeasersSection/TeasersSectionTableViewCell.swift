//
//  TeasersSectionTableViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import UIKit
import Reusable

class TeasersSectionTableViewCell: UITableViewCell, NibReusable {
    
    var section : Section!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sectionTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        registerCell()
    }
    
    func setupContent(section : Section)  {
        
        self.section = section
        
        self.sectionTitle.text = section.title
        
        self.collectionView.reloadData()
        
        
    }
    
    func registerCell() {
        
        self.collectionView.register(UINib(nibName: "TeasersCollectionViewCell", bundle: nil),
                                     forCellWithReuseIdentifier: TeasersCollectionViewCell.identifier)
        
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


extension TeasersSectionTableViewCell : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: self.collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


extension TeasersSectionTableViewCell : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.section.video.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TeasersCollectionViewCell.identifier, for: indexPath) as! TeasersCollectionViewCell
        
        cell.setupContent(video: self.section.video[indexPath.item])
        
        return cell
        
    }
    
    
}
