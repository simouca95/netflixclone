//
//  AllEpisodesViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/12/2021.
//

import UIKit
import DropDown
import RxCocoa

class AllEpisodesViewController: UIViewController {
    
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropDownTitleLabel: UILabel!
    @IBOutlet weak var dropDownViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dismissButton: UIButton!
    
    let dropDown = DropDown()
    var viewModel: AllEpisodesViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initViewModel()
    }
    
    func initView() {
        dismissButton.setTitle("", for: .normal)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.register(cellType: EpisodeCollectionViewCell.self)
        
        dropDownView.layer.cornerRadius = 4
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dropDownView.addGestureRecognizer(tap)
        dropDown.anchorView = dropDownView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        dropDown.show()
    }
    
    func initViewModel() {
        guard let viewModel = viewModel else { return }
        switch viewModel.type {
        case .episode, .season, .audio:
            dropDownView.isHidden = true
            dropDownViewHeightConstraint.constant = 0
            collectionView.reloadData()
        case .manySeasons:
            dropDownView.isHidden = false
            dropDownViewHeightConstraint.constant = 40
            guard let seasons = viewModel.seasons,
                  let currentSeasonIndex = viewModel.currentSeasonIndex
            else { return }
            
            dropDownTitleLabel.text = seasons[safe: currentSeasonIndex]?.name
            
            dropDown.dataSource = seasons.compactMap{$0.name}
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                dropDownTitleLabel.text = seasons[index].name
                viewModel.currentSeasonIndex = index
                collectionView.reloadData()

//                if let data = seasons[index].resources {
//                    //                            delegate?.didSeasonChanged(row: index)
//                    viewModel.resources = data
//                }
            }
        }
    }
    
    @IBAction func onDismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AllEpisodesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        switch viewModel.type {
        case .episode, .season, .audio:
            return viewModel.resources.count
        case .manySeasons:
            return viewModel.seasons?[safe: viewModel.currentSeasonIndex ?? 0]?.resources?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: EpisodeCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        guard let viewModel = viewModel else { return cell }
        switch viewModel.type {
        case .episode, .season, .audio:
            if let item = viewModel.resources[safe: indexPath.row] {
                cell.viewModel = EpisodeCollectionViewModel(title: item.title, posterPath: item.picture, duration: item.duration, progress: item.timeCodeFromViewed, isNewResource: item.isNewResource)
            }
        case .manySeasons:
            if let item = viewModel.seasons?[safe: viewModel.currentSeasonIndex ?? 0]?.resources?[safe: indexPath.row] {
                cell.viewModel = EpisodeCollectionViewModel(title: item.title, posterPath: item.picture, duration: item.duration, progress: item.timeCodeFromViewed, isNewResource: item.isNewResource)
            }
        }
        
        return cell
    }
}

extension AllEpisodesViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = 200
        let height = 210
        return CGSize(width: width, height:height)
    }
}

extension AllEpisodesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewModel = viewModel, let media = viewModel.resources[safe: indexPath.row]?.media {
            dismiss(animated: true, completion: {
                viewModel.delegate?.onSpecificEpTapped(media: media, seasonIndex: viewModel.currentSeasonIndex, resourceIndex: indexPath.row)
            })
        }
        
        guard let viewModel = viewModel else { return }
        switch viewModel.type {
        case .episode, .season, .audio:
            if let media = viewModel.resources[safe: indexPath.row]?.media {
                dismiss(animated: true, completion: {
                    viewModel.delegate?.onSpecificEpTapped(media: media, seasonIndex: viewModel.currentSeasonIndex, resourceIndex: indexPath.row)
                })
            }
        case .manySeasons:
            if let media = viewModel.seasons?[safe: viewModel.currentSeasonIndex ?? 0]?.resources?[safe: indexPath.row]?.media {
                dismiss(animated: true, completion: {
                    viewModel.delegate?.onSpecificEpTapped(media: media, seasonIndex: viewModel.currentSeasonIndex, resourceIndex: indexPath.row)
                })
            }
        }
    }
}
