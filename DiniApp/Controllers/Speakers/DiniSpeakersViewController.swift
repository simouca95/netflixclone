//
//  DiniSpeakersViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import UIKit
import RxSwift
import SVGKit

class DiniSpeakersViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileButton: UIButton!
    
    var disposeBag = DisposeBag()
    var viewModel: DiniSpeakersViewModel!
    
    internal static func instantiate(with viewModel: DiniSpeakersViewModel) -> DiniSpeakersViewController {
        let vc = StoryboardScene.Details.diniSpeakersViewController.instantiate()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initViewModel()
    }
    
    func initViews() {
        tableView.register(cellType: SpeakerIntroTableViewCell.self)
        tableView.register(cellType: SpeakerResourceTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
    }
    
    func initViewModel() {
        viewModel.reloadData.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe { [weak self] _ in
                guard let self = self else { return }
                self.tableView.reloadData()
            }
            .disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getSpeakerCollectionWatch()
    }
}

extension DiniSpeakersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.row]
        
        if let introTableViewModel = item as? SpeakerIntroViewModel {
            let cell: SpeakerIntroTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = introTableViewModel
            return cell
        } else if let resourceTableViewModel = item as? SpeakerResourceTableViewModel {
            let cell: SpeakerResourceTableViewCell = tableView.dequeueReusableCell(for: indexPath)
            cell.viewModel = resourceTableViewModel
            return cell
        }
        
        return UITableViewCell()
    }
}

extension DiniSpeakersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = viewModel.items[indexPath.row]
        
        if let introTableViewModel = item as? SpeakerIntroViewModel {
            return UITableView.automaticDimension
        } else if item is SpeakerResourceTableViewModel {
            return tableView.frame.width/1.77
        }
        
        return UITableView.automaticDimension
    }
}
