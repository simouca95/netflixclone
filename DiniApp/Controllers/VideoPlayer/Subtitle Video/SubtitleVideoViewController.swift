//
//  SubtitleVideoViewController.swift
//  DiniApp
//
//  Created by Rached Khoudi on 28/01/2022.
//

import UIKit

class SubtitleVideoViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: SubtitleVideoViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
    func initView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: QualityVideoTableViewCell.self)
        tableView.tintColor = .white
    }
    
    @IBAction func onDismissTapped(_ sender: Any) {
        dismiss(animated: true) {
            self.viewModel.delegate?.onSpecificSubtitleTapped(subtitleResourceId: self.viewModel.currentSubtitleId)
        }
    }
}

extension SubtitleVideoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.resourceSubtitles.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .black
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Sous-titrage"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = .white
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QualityVideoTableViewCell.reuseIdentifier) as! QualityVideoTableViewCell
        let item = viewModel.resourceSubtitles[indexPath.row]
        cell.titleLabel.text = viewModel.resourceSubtitles[indexPath.row].languageName
        cell.checkImageView.isHidden = item.id != viewModel.currentSubtitleId
        return cell
    }
}

extension SubtitleVideoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let id = viewModel.resourceSubtitles[indexPath.row].id else { return }
        viewModel.currentSubtitleId = id
        tableView.reloadData()
    }
}
