//
//  EpisodeTableViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/28/20.
//

import UIKit

class EpisodeTableViewCell: UITableViewCell {

    static let identifier = "episodeTableViewCell"

    @IBOutlet weak var episodeProgressBar: UIProgressView!
    @IBOutlet weak var episodeThumbnail: UIImageView!
    @IBOutlet weak var episodeDuration: UILabel!
    @IBOutlet weak var episodeTitle: UILabel!
    @IBOutlet weak var episodeResume: UILabel!
    
    var video : Video!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func playAction(_ sender: Any) {
    }
    
    @IBAction func downloadAction(_ sender: Any) {
    }
    
    
    func setupContent(video : Video)  {
        
        self.video = video
        
        self.episodeThumbnail.image = UIImage(named: video.image)
        
        
    }
}
