//
//  PersistenceHelper.swift
//  DiniApp
//
//  Created by Rached Khoudi on 16/02/2022.
//

import CoreData
import UIKit

class PersistenceHelper {
    
    static let shared = PersistenceHelper()
    private init() {}

    func retriveLocalMedia() -> [NSManagedObject]? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else { return nil }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "DiniVideo")
        
        do {
            let videos = try managedContext.fetch(fetchRequest)
            return videos
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func save(name: String, media: String, uri: String, picture: String?, duration: Int?, currentViewedDuration: Int?) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "DiniVideo",
                                   in: managedContext)!
        
        let diniVideo = NSManagedObject(entity: entity,
                                        insertInto: managedContext)
        
        diniVideo.setValue(name, forKeyPath: "name")
        diniVideo.setValue(media, forKeyPath: "media")
        diniVideo.setValue(picture, forKeyPath: "thumbnail")
        diniVideo.setValue(uri, forKeyPath: "uri")
        diniVideo.setValue(currentViewedDuration ?? 0, forKeyPath: "currentViewedDuration")
        diniVideo.setValue(duration ?? 0, forKeyPath: "duration")

        do {
            print(diniVideo)
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func isPersisted(media: String) -> Bool? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else { return nil }
        
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "DiniVideo")
        
        do {
            let videos = try managedContext.fetch(fetchRequest)
            if let videoItems = videos as? [DiniVideo] {
                return videoItems.compactMap({ $0.media }).contains(media)
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    func delete(media: String) -> Bool {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else { return false}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DiniVideo")
        fetchRequest.predicate = NSPredicate(format:"media == %@",media)

        do {
            let videos = try managedContext.fetch(fetchRequest)
            if let videoToDelete = videos.first {
                managedContext.delete(videoToDelete)
                try managedContext.save()
            }
            
            return videos.first != nil
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return false
        }
    }
}
