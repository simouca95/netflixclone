//
//  BaseCellViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import UIKit

class BaseCellViewModel {
    
    var cellHeight: CGFloat
    
    init(cellHeight: CGFloat) {
        self.cellHeight = cellHeight
    }
}
