//
//  SelectGenreCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/27/20.
//

import UIKit

class SelectGenreCell: UITableViewCell {

    static let identifier = "selectGenreCell"
    
    @IBOutlet weak var title: UILabel!
    
    var isCurrentSelectedGenre = false {
        didSet {
            self.title.font = isCurrentSelectedGenre ? .boldSystemFont(ofSize: 17) : .systemFont(ofSize: 16)
            self.title.textColor = isCurrentSelectedGenre ? .white : .gray
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
