//
//  InfoCollectionViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import UIKit
import Reusable

protocol InfoCollectionDelegate: AnyObject {
    func onClick(row: Int)
}

class InfoCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentNumView: UIView!
    @IBOutlet weak var contentNumLabel: UILabel!
    @IBOutlet weak var speakersKeyLabel: UILabel!
    @IBOutlet weak var speakersValueLabel: UILabel!
    @IBOutlet weak var categoryKeyLabel: UILabel!
    @IBOutlet weak var categoryValueLabel: UILabel!
    @IBOutlet weak var tagsKeyLabel: UILabel!
    @IBOutlet weak var tagsValueLabel: UILabel!
    
    var viewModel: InfoCollectionViewModel?
    weak var delegate: InfoCollectionDelegate?
    
    func setupContent(viewModel: InfoCollectionViewModel?) {
        
        self.viewModel = viewModel
        
        speakersKeyLabel.text = "Intervenants :"
        categoryKeyLabel.text = "Categories :"
        tagsKeyLabel.text = "Tags :"
        
        contentNumLabel.font = UIFont.boldSystemFont(ofSize: 30)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        speakersKeyLabel.font = UIFont.boldSystemFont(ofSize: 25)
        speakersValueLabel.font = UIFont.systemFont(ofSize: 25)
        categoryKeyLabel.font = UIFont.boldSystemFont(ofSize: 25)
        categoryValueLabel.font = UIFont.systemFont(ofSize: 25)
        tagsKeyLabel.font = UIFont.boldSystemFont(ofSize: 25)
        tagsValueLabel.font = UIFont.systemFont(ofSize: 25)
        
        contentNumLabel.textColor = .white
        titleLabel.textColor = .white
        speakersKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        speakersValueLabel.textColor = .white
        categoryKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        categoryValueLabel.textColor = .white
        tagsKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        tagsValueLabel.textColor = .white
        
        tableView.dataSource = self
        tableView.remembersLastFocusedIndexPath = true
        tableView.register(cellType: SimpleTvTableViewCell.self)
        
        guard let viewModel = viewModel else {
            return
        }
        
        titleLabel.text = viewModel.title
        speakersValueLabel.text = viewModel.speakers
        categoryValueLabel.text = viewModel.category
        tagsValueLabel.text = viewModel.tags
        
        switch viewModel.type {
        case .episode:
            contentNumView.isHidden = true
            contentNumLabel.text = ""
        case .season, .audio:
            contentNumView.isHidden = false
            contentNumLabel.text = "\(viewModel.contentNumber) Épisodes"
        case .manySeasons:
            contentNumView.isHidden = false
            contentNumLabel.text = "\(viewModel.contentNumber) Saisons"
            tableView.reloadData()
        }
    }
}

extension InfoCollectionViewCell: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.seasons.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SimpleTvTableViewCell = tableView.dequeueReusableCell(for: indexPath)

        let item = viewModel?.seasons[indexPath.row]
        cell.titleTvLabel.text = item?.name
        cell.delegate = self
        return cell
    }
    
//    func indexPathForPreferredFocusedView(in tableView: UITableView) -> IndexPath? {
//        return IndexPath(row: 0, section: 0)
//    }
}

extension InfoCollectionViewCell: SimpleTvDelegate {
    func onFocused(cell: SimpleTvTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            delegate?.onClick(row: indexPath.row)
        }
    }
}
