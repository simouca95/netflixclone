//
//  ApiCall+login.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Auth {
        
        public static func login(email: String,
                                 password: String,
                                 onSuccess: @escaping (_ response: LoginResponse?) -> Void,
                                 onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.auth(email: email, password: password)
                let classicUrlRequest = try api.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                var urlRequest = URLRequest(url: classicUrlRequest.url!)
                let json = "{\"email\" : \"abdelmonaim.boussenna@gmail.com\",\"password\" : \"Amir2012@\"}"

                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!

                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData

                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: LoginResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }

        
        public static func getLogoAndWalkthroughImages(onSuccess: @escaping (_ response: LogiAndWalkthroughResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.login
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: LogiAndWalkthroughResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func avatarPictureList(token: String, onSuccess: @escaping (_ response: [[String]]?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.avatarPictureList(token: token)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: [[String]] = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func userInfos(token: String, id: Int, onSuccess: @escaping (_ response: UserResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.userInfos(token: token, id: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: UserResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
    }
}
