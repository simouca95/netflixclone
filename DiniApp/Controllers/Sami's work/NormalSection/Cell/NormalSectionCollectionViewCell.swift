//
//  NormalSectionCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import UIKit

class NormalSectionCollectionViewCell: UICollectionViewCell {

    static let identifier = "normalSectionCell"

    @IBOutlet weak var movieImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var video : Video!
    
    func setupContent(video : Video)  {
        
        self.video = video
        
        self.movieImage.image = UIImage(named: video.image)
        
        
    }

}
