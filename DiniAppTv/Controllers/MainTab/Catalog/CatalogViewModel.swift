//
//  CatalogViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Foundation
import RxCocoa
import RxSwift

class CatalogViewModel: BaseViewModel {
    
    var items: [DiniSlider] = []
    var reloadData = BehaviorSubject<Void>(value: ())

    func getCollectionList() {
        guard let id = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Catalog.getCatalogs(token: token, profileId: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension CatalogViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CatalogsResponse?) {
        guard let response = response,
              let slider = response.collectionsList?.slider?.first
        else { return }
        
        items.removeAll()
        items.append(contentsOf: slider)
        reloadData.onNext(())
        isLoading.accept(false)
    }

    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
