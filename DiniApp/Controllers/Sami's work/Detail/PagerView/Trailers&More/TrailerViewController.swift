//
//  TrailerViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/28/20.
//

import UIKit
import XLPagerTabStrip

class TrailerViewController: UIViewController {

    @IBOutlet weak var trailerTableView: UITableView!
    var videos = DataMock.instance.videos2
    var parentDelegate : UpdateScrollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initView()
    }
    
    override func viewDidLayoutSubviews() {
        self.parentDelegate.updateScrollViewHeight(height: self.trailerTableView.contentSize.height)

    }
    func initView()  {
        self.registerTableCells()
    }
    
    func registerTableCells() {
        self.trailerTableView.register(UINib(nibName: "TrailerTableViewCell", bundle: nil), forCellReuseIdentifier: TrailerTableViewCell.identifier)
        
        self.trailerTableView.delegate = self
        self.trailerTableView.dataSource = self

    }

}
extension TrailerViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
}

extension TrailerViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: TrailerTableViewCell.identifier, for: indexPath) as! TrailerTableViewCell
        
        cell.setupContent(video: self.videos[indexPath.row])
        
        return cell
    }
    
    
}
extension TrailerViewController : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title:"TrailerViewController")
        
    }
    
    
}
