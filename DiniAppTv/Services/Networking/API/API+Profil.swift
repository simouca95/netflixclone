//
//  API+Profil.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 08/12/2021.
//

import Alamofire

extension API {
    enum ProfilController {
        case getProfilTypes(token: String)
        case addProfil(token: String, userId: String, typeId: String, name: String, avatar: String)
        case updateProfil(token: String, profileId: String, userId: String, typeId: String, name: String, avatar: String)
        case deleteProfil(token: String, profileId: String)
    }
}

extension API.ProfilController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getProfilTypes :
            return .get
        case .addProfil :
            return .post
        case .updateProfil :
            return .put
        case .deleteProfil :
            return .delete
        }
    }
    
    var path: String {
        switch self {
        case .getProfilTypes:
            return "/profile_types"
        case .addProfil:
            return "/addProfile"
        case .updateProfil(_, let profileId, _, _, _, _):
            return "/updateProfile/\(profileId)"
        case .deleteProfil(_, let profileId):
            return "/deleteProfile/\(profileId)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getProfilTypes, .addProfil, .updateProfil, .deleteProfil:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getProfilTypes(let token), .addProfil(let token,_,_,_,_), .updateProfil(let token,_,_,_,_,_), .deleteProfil(let token,_):
            return Network.secureHeaders(token: token)
        }
    }
}
