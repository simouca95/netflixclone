//
//  EpsCollectionViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 22/04/2022.
//

import UIKit
import Reusable

protocol EpsCollectionDelegate: AnyObject {
    func onClick(resources: [DiniResource], clickedIndex: Int)
}

class EpsCollectionViewCell: UICollectionViewCell, NibReusable, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: EpsCollectionViewModel?
    weak var delegate: EpsCollectionDelegate?
    
    func setupContent(viewModel: EpsCollectionViewModel?) {
        self.viewModel = viewModel

        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: EpsTableViewCell.self)
        tableView.remembersLastFocusedIndexPath = true
        tableView.reloadData()
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 6
            self.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.resources.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EpsTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        if let item = viewModel?.resources[safe: indexPath.row] {
            cell.viewModel = EpsTableViewModel(cellHeight: 140, title: item.title,
                                                    posterPath: item.picture, duration: item.duration,
                                                    progress: item.timeCodeFromViewed,
                                                    media: item.media)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func indexPathForPreferredFocusedView(in tableView: UITableView) -> IndexPath? {
        return IndexPath(row: 0, section: 0)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let resources = viewModel?.resources {
            delegate?.onClick(resources: resources, clickedIndex: indexPath.row)
        }
    }
}
