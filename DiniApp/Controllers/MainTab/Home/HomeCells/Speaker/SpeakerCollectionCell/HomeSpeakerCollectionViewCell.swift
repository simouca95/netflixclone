//
//  SpeakerCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable

class HomeSpeakerCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var speakerImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
