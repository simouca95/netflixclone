//
//  SliderTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable
import Kingfisher
import RxSwift

class SliderTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var collectionView: UICollectionView!
//    @IBOutlet weak var pageControl: UIPageControl!
    
    var viewModel: SliderTableViewModel!
    weak var delegate: HomeCellDelegate?
    private var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        initViews()
        initViewModel()
    }
    
    func initViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: SliderCollectionViewCell.self)
    }
    
    func initViewModel() {
        viewModel = SliderTableViewModel()
        viewModel.reloadSliderFavButton.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] isFav in
                guard let self = self else { return }
                if let cell = self.collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? SliderCollectionViewCell {
                    cell.updateFavBtn(isFav: isFav)
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    func setupContent(data : [CollectionListData], infos: [CollectionInfoState])  {
        viewModel.sliders = data
        viewModel.infos = infos
        self.collectionView.reloadData()
    }
}

extension SliderTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = viewModel.sliders[indexPath.row]
        let infos = viewModel.infos
        let cell: SliderCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        
        cell.setup(item: item, infoState: infos)
        cell.delegate = delegate
        cell.addFavButton.rx.tap
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.setInfoState(info: .fav)
            })
            .disposed(by: self.disposeBag)
        
        cell.playButton.rx.tap
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self,
                      let id = item.id
                else { return }
                self.viewModel.delegate?.didClickPlay(collectionId: id)
            })
            .disposed(by: self.disposeBag)
        
//        cell.infosButton.rx.tap
//            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
//            .subscribe (onNext: { [weak self] _ in
//                guard let self = self else { return }
//                self.delegate?.itemSelected(widgetRow: 0, row: 0)
//            })
//            .disposed(by: self.disposeBag)
        return cell
    }
}

extension SliderTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
