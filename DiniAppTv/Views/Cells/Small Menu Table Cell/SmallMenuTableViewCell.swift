//
//  SmallMenuTableViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 27/02/2021.
//

import UIKit
import Reusable

class SmallMenuTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var underlineView: UIView!
        
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        
//        print("Rachad - Tecst")
        
        if let cell = context.nextFocusedView as? SmallMenuTableViewCell {
            cell.underlineView.isHidden = false
        }
        
        if let cell = context.previouslyFocusedView as? SmallMenuTableViewCell{
            cell.underlineView.isHidden = true
        }
    }
}
