//
//  BaseCellViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit

class BaseCellViewModel {
    
    var cellHeight: CGFloat
    
    init(cellHeight: CGFloat) {
        self.cellHeight = cellHeight
    }
}
