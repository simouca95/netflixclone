//
//  HomeTableViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 22/02/2021.
//

import UIKit
import Reusable
import Kingfisher

protocol HomeTableViewCellDelegate: AnyObject {
    func didUpdateCellFocus(data: CollectionListData)
    func didSelectCell(vIndex: Int, hIndex: Int)
}

class HomeTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: HomeTableViewCellDelegate?
    var viewModel: HomeTableViewModel!
    
    private var dataType: HomeCellType?
    private var vIndex: Int?
    private var itemsViewedsInfos: [CollectionViewedsInfo] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.remembersLastFocusedIndexPath = true
        
        collectionView.register(cellType: HomeCollectionViewCell.self)
        collectionView.register(cellType: ProgressCollectionViewCell.self)
        collectionView.register(cellType: HomeSpeakerCollectionViewCell.self)
        viewModel = HomeTableViewModel()
    }
    
    func setupContent(vIndex: Int, data: CollectionList, itemsViewedsInfos: [CollectionViewedsInfo])  {
        self.vIndex = vIndex
        dataType = HomeCellType(rawValue: data.type ?? "") ?? .standard
        if dataType == .speaker {
            viewModel.sliders = data.speakerData ?? []
        } else {
            viewModel.sliders = data.collectionData ?? []
        }
        self.itemsViewedsInfos = itemsViewedsInfos
        self.collectionView.reloadData()
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 6
            self.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
}

extension HomeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = viewModel.sliders[indexPath.row]
        
        if dataType == .inProgress {
            let cell: ProgressCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)

            cell.setupContent(item: item, itemsViewedsInfos: itemsViewedsInfos)
            return cell
        } else if dataType == .speaker {
            let cell: HomeSpeakerCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.setupContent(item: item)
            return cell
        } else {
            let cell: HomeCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)

            cell.setupContent(item: item)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vIndex = self.vIndex else { return }
        self.delegate?.didSelectCell(vIndex: vIndex, hIndex: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let indexPath = context.nextFocusedIndexPath,
           let item = viewModel.sliders[safe: indexPath.row] {
            self.delegate?.didUpdateCellFocus(data: item)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width = (collectionView.frame.width-80-(10*5))/6
        //        let height = width*1.477
        let width = 250
        let height = 370
        return CGSize(width: width, height: height)
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        return IndexPath(row: 0, section: 0)
    }
}
