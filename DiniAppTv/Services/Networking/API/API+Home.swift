//
//  API+Home.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import Foundation
import Alamofire

extension API {
    enum HomeController {
        var controllerUrl: String { return "/homepage" }
        
        case getCollectionList(token: String, profileId: Int)
    }
}

extension API.HomeController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getCollectionList :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getCollectionList(_, let profileId):
            return self.controllerUrl + "?profileId=\(profileId)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getCollectionList:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getCollectionList(let token,_):
            return Network.secureHeaders(token: token)
        }
    }
}
