//
//  API+Catalog.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 08/12/2021.
//

import Alamofire

extension API {
    enum CatalogController {
        case getCatalogs(token: String, profilId: Int)
    }
}

extension API.CatalogController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getCatalogs :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getCatalogs(_, let profileId):
            return "/collectionCatalogueIndex?profileId=\(profileId)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getCatalogs:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getCatalogs(let token, _):
            return Network.secureHeaders(token: token)
        }
    }
}
