//
//  ATCWalkthroughViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/09/2021.
//

import UIKit

protocol ATCWalkthroughViewControllerDelegate: AnyObject {
  func walkthroughViewControllerDidFinishFlow(_ vc: ATCWalkthroughViewController)
}

class ATCWalkthroughViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
  @IBOutlet var pageControl: UIPageControl!
  @IBOutlet weak var skipButton: UIButton!
  @IBOutlet weak var logoImageView: UIImageView!
    
  weak var delegate: ATCWalkthroughViewControllerDelegate?
  
  let viewControllers: [UIViewController]
  var pageIndex = 0
  let pageController: UIPageViewController
  let fakeVC: UIViewController
  
  init(nibName nibNameOrNil: String?,
       bundle nibBundleOrNil: Bundle?,
       viewControllers: [UIViewController]) {
    self.viewControllers = viewControllers
    self.pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    self.fakeVC = UIViewController()
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    self.pageController.dataSource = self
    self.pageController.delegate = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    pageController.setViewControllers([viewControllers[0]], direction: .forward, animated: true, completion: nil)
    self.addChildViewControllerWithView(pageController)
    pageControl.numberOfPages = viewControllers.count
    self.view.bringSubviewToFront(pageControl)
    self.view.bringSubviewToFront(skipButton)
    self.view.bringSubviewToFront(logoImageView)
    if let logo = DefaultsHelper.logo {
        logoImageView.kf.setImage(with: URL(string: logo))
    }
    skipButton.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
    pageControl.currentPageIndicatorTintColor = UIColor(rgb: 0x7DA463)
    super.viewDidLoad()
  }
    
  @objc func handleSkip() {
      self.delegate?.walkthroughViewControllerDidFinishFlow(self)
  }
    
  public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    if let index = self.index(of: viewController) {
      if index == 0 {
        return nil
      }
      return viewControllers[index - 1]
    }
    return nil
  }
  
  public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    if let index = self.index(of: viewController) {
      if index + 1 >= viewControllers.count {
        return nil
      }
      return viewControllers[index + 1]
    }
    return nil
  }
  
  public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    if !completed {
      return
    }
    if let lastPushedVC = pageViewController.viewControllers?.last {
      if let index = index(of: lastPushedVC) {
        pageControl.currentPage = index
      }
    }
  }
  
  private func index(of viewController: UIViewController) -> Int? {
    for (index, vc) in viewControllers.enumerated() {
      if viewController == vc {
        return index
      }
    }
    return nil
  }
}
