//
//  StandardTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable
import SwiftUI
import Kingfisher

class StandardTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var collectionView: UICollectionView!

    
    var viewModel: StandardViewModel!
    weak var delegate: HomeCellDelegate? = nil
    var vIndex: Int? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        initViews()
        initViewModel()
    }
    
    func initViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: StandardCollectionViewCell.self)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 6
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

        collectionView.setCollectionViewLayout(layout, animated: false)
       
    }
    
    func initViewModel() {
        viewModel = StandardViewModel()
    }
    
    func setupContent(data : [CollectionListData], type: HomeCellType)  {
        viewModel = StandardViewModel(sliders: data, type: type)
        self.collectionView.reloadData()
    }
    
}

extension StandardTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel.type == .seasons {
            return viewModel.sliders.first?.seasons?.count ?? 0
        } else if viewModel.type == .reel {
            return viewModel.sliders.first?.resources?.count ?? 0
        }
        return viewModel.sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: StandardCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        var pictureUrl: URL?
   
        if viewModel.type == .reel {
            if  let reel =  viewModel.sliders.first?.resources?[indexPath.row] {
            cell.initView(using: reel)
            }
        } else {
            if let sliders = viewModel.sliders.first {
                cell.initView(using: sliders)
            }
        }
                    
        if viewModel.type == .seasons,
           let seasons = viewModel.sliders.first?.seasons {
            let item = seasons[indexPath.row]
            var path2: String?
            if let seasonPicUrl = item.portraitPicture {
                path2 = seasonPicUrl
            } else {
                path2 = viewModel.sliders.first?.portraitPicture
            }
            pictureUrl = Helper.concatinatePaths(path1: DefaultsHelper.portraitPictureSeasonsPath,
                                                 path2: path2)
        } else if viewModel.type == .reel {
            let item = viewModel.sliders.first?.resources?[indexPath.row]
            pictureUrl = URL(string: item?.pictureByDefaultUrl ?? "")

        } else {
            let item = viewModel.sliders[indexPath.row]
            pictureUrl = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                                 path2: item.portraitPicture)
        }

        if let url = pictureUrl {
            cell.imageView.kf.setImage(with: url)
        } else {
            cell.imageView.image = UIImage(named: "ItemEmpty")
        }

        cell.layer.cornerRadius = 6

        return cell
    }
}

extension StandardTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let height = collectionView.frame.height
            let width = height * 0.677
            return CGSize(width: width, height: height)
        }
        let width = collectionView.frame.width * 0.27
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vIndex = vIndex {
            if viewModel.type == .standard {
                delegate?.itemTapped(verticalIndex: vIndex, horizontalIndex: indexPath.row)
            } else if viewModel.type == .live {
                delegate?.itemLiveTapped(verticalIndex: vIndex, horizontalIndex: indexPath.row)
            } else if viewModel.type == .reel {
                delegate?.itemReelTapped(verticalIndex: vIndex, horizontalIndex: indexPath.row)
            } else if viewModel.type == .seasons {
                delegate?.itemHomeSeasonsTapped(verticalIndex: vIndex, horizontalIndex: indexPath.row)
            }
        }
    }
}
