//
//  DownloadTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/01/2022.
//

import UIKit
import Reusable

class DownloadTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        checkImageView.image = selected ? UIImage(named: "radio-on") : UIImage(named: "radio-off")
    }

}
