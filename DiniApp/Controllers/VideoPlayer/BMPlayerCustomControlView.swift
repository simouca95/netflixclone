//
//  BMPlayerCustomControlView.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 21/12/2021.
//

import UIKit
import BMPlayer
import Accelerate
import CocoaLumberjack
import MediaPlayer
import AVKit

enum BMPlayerCustomAction: Int {
    case close = 0
    case forward10   = 1
    case backward10   = 2
    case cast   = 3
}

protocol BMPlayerCustomControlViewDelegate: AnyObject {
    func didPressOnCloseButton()
    func didPressOnForward10Button(currentTime: Double)
    func didPressOnBackward10Button(currentTime: Double)
    func didPressOnNextEp()
    func didPressOnAllEps()
    func didPressQualityVideo()
    func didPressSubtitleVideo()
    func didPressCastVideo()
    func didPressLockScreen(lock: Bool)
}

class BMPlayerCustomControlView: BMPlayerControlView {
    
    var collectionType: CollectionType
    var subtitle: WebVttBMSubtitles?
    var currentTime: TimeInterval = 0.0
    var isPersisted: Bool
    var isScreenLocked: Bool = false

    var customBackButton = UIButton(type : UIButton.ButtonType.custom)
    var forward10Button = UIButton(type : UIButton.ButtonType.custom)
    var backward10Button = UIButton(type : UIButton.ButtonType.custom)
    var castButton = UIView()

    var brightnessProgressView = UIProgressView()
    var brightnessIcon = UIImageView()

    weak var customDelegate: BMPlayerCustomControlViewDelegate?
    open var centerMaskView = UIView()
    open var bottomButtonsView = UIStackView()
    open var lockedButtonView = UIStackView()

    // MARK: - Init
    init(frame: CGRect, collectionType: CollectionType, subtitle: WebVttBMSubtitles?, isPersisted: Bool) {
        self.collectionType = collectionType
        self.subtitle = subtitle
        self.isPersisted = isPersisted
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        self.collectionType = .season
        self.isPersisted = true
        super.init(coder: aDecoder)
    }
    
    override func customizeUIComponents() {
//        mainMaskView.backgroundColor = .red
//        maskImageView.backgroundColor = .darkGray
//        topMaskView.backgroundColor = .green
//        bottomMaskView.backgroundColor = .gray
//        bottomWrapperView.backgroundColor = .brown
//        topWrapperView.backgroundColor = .primary
        
        fullscreenButton.removeFromSuperview()
        replayButton.removeFromSuperview()
        backButton.removeFromSuperview()
        
        bottomMaskView.removeFromSuperview()
        mainMaskView.addSubview(bottomMaskView)
        bottomMaskView.snp.makeConstraints { [unowned self](make) in
            make.bottom.left.right.equalTo(self.mainMaskView)
            make.height.equalTo(80)
        }
        
        //centerMaskView
        mainMaskView.insertSubview(centerMaskView, belowSubview: topMaskView)
        centerMaskView.alpha = 0.0
        centerMaskView.snp.makeConstraints { [unowned self](make) in
            make.edges.equalTo(self.mainMaskView)
        }
        
        //bottomButtonsView
        bottomWrapperView.addSubview(bottomButtonsView)
//        bottomButtonsView.backgroundColor = .red
        bottomButtonsView.axis = .horizontal
        bottomButtonsView.distribution = .fillEqually
        bottomButtonsView.spacing = 20
        bottomButtonsView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.left.top.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        //lockedButtonView
        lockedButtonView.isHidden = true
        bottomMaskView.addSubview(lockedButtonView)
//        bottomButtonsView.backgroundColor = .red
        lockedButtonView.axis = .horizontal
        lockedButtonView.distribution = .fillEqually
        lockedButtonView.spacing = 20
        lockedButtonView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.left.top.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        let lockedButton = prepareBottomButton(title:"Déverrouiller", imageName: "lock")
        lockedButton.addTarget(self, action: #selector(handleLockScreen), for: .touchUpInside)
        lockedButtonView.addArrangedSubview(lockedButton)

        if !isPersisted {
            let qualityButton = prepareBottomButton(title:"Quality", imageName: "edit-video")
            qualityButton.addTarget(self, action: #selector(handleQuality), for: .touchUpInside)
            bottomButtonsView.addArrangedSubview(qualityButton)
        }

        let lockButton = prepareBottomButton(title:"Verrouiller", imageName: "unlock")
        lockButton.addTarget(self, action: #selector(handleLockScreen), for: .touchUpInside)
        bottomButtonsView.addArrangedSubview(lockButton)
        
        if collectionType != .episode, !isPersisted {
            let epsButton = prepareBottomButton(title:"Épisodes", imageName: "chapter")
            epsButton.addTarget(self, action: #selector(handleAllEps), for: .touchUpInside)
            bottomButtonsView.addArrangedSubview(epsButton)
            
            let nextEpButton = prepareBottomButton(title:"Ép. suivant", imageName: "skip")
            nextEpButton.addTarget(self, action: #selector(handleNextEp), for: .touchUpInside)
            bottomButtonsView.addArrangedSubview(nextEpButton)
        }

        
        let subtitleButton = prepareBottomButton(title:"sous-titres", imageName: "movie")
        subtitleButton.addTarget(self, action: #selector(handleSubtitle), for: .touchUpInside)
        bottomButtonsView.addArrangedSubview(subtitleButton)
        
        //Main Slider
//        let thumb = UIImage(systemName: "circle.fill")?.withTintColor(.redApp)
        timeSlider.minimumTrackTintColor = .redApp
        timeSlider.tintColor = .redApp
//        timeSlider.maximumTrackTintColor = .darkGray
//        timeSlider.setThumbImage(thumb, for: .normal)
        
        //brightness Slider
        brightnessProgressView.transform = CGAffineTransform(rotationAngle: -.pi / 2)
//        brightnessProgressView.minimumTrackTintColor = .white
//        brightnessSlider.maximumTrackTintColor = .darkGray
//        brightnessProgressView.thumbTintColor = .clear
        brightnessProgressView.layer.cornerRadius = 8
//        brightnessSlider.setMaximumTrackImage(UIImage(systemName: "sun.min"), for: .normal)
//        brightnessProgressView.addTarget(self, action:#selector(sliderValueDidChange(sender:)), for: .valueChanged)
        brightnessProgressView.setProgress(Float(UIScreen.main.brightness), animated: false)
        brightnessProgressView.tintColor = .white
        centerMaskView.addSubview(brightnessProgressView)
        brightnessProgressView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(40)
            make.centerY.equalToSuperview().offset(10)
            make.width.equalTo(160)
            make.height.equalTo(4)
        }
        
        //brightness Image
        centerMaskView.addSubview(brightnessIcon)
        brightnessIcon.image = Asset.bright.image
        brightnessIcon.tintColor = .white
        brightnessIcon.snp.makeConstraints { (make) in
            make.centerX.equalTo(brightnessProgressView.snp.centerX)
            make.centerY.equalTo(brightnessProgressView.snp.centerY).offset(-90)
            make.width.height.equalTo(14)
        }
        
        //castButton
        castButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let buttonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let routerPickerView = AVRoutePickerView(frame: buttonView.frame)
        buttonView.addSubview(routerPickerView)
        castButton.addSubview(buttonView)
        topWrapperView.addSubview(castButton)
        castButton.tag = BMPlayerCustomAction.cast.rawValue
//        castButton.setImage(Asset.screen.image.withColor(.white), for: .normal)
        castButton.tintColor = .white
//        castButton.addGestureRecognizer(UIGestureRecognizer(target: self, action: #selector(onCustomButtonPressed(_:))))
        castButton.snp.makeConstraints { (make) in
          make.width.height.equalTo(50)
          make.left.bottom.equalTo(20)
        }

        //titleLabel
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.width.bottom.centerX.equalToSuperview()
        }

        //customBackButton
        topWrapperView.addSubview(customBackButton)
        customBackButton.setImage(Asset.pictoClose.image, for: .normal)
        customBackButton.imageEdgeInsets = UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
        customBackButton.tag = BMPlayerCustomAction.close.rawValue
        customBackButton.addTarget(self, action: #selector(onCustomButtonPressed(_:)), for: .touchUpInside)
        customBackButton.snp.makeConstraints { (make) in
            make.width.height.equalTo(50)
            make.right.equalTo(topWrapperView)
            make.centerY.equalToSuperview()
        }
        
        //playButton
        playButton.removeFromSuperview()
        centerMaskView.addSubview(playButton)
        playButton.setImage(Asset.pictoPlay.image, for: .normal)
        playButton.setImage(Asset.pictoPause.image, for: .selected)
        playButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        playButton.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.height.width.equalTo(50)
        }
        
        //forward10Button
        forward10Button.tag = BMPlayerCustomAction.forward10.rawValue
        forward10Button.addTarget(self, action: #selector(onCustomButtonPressed(_:)), for: .touchUpInside)
        forward10Button.setImage(Asset.forward10.image, for: .normal)
        forward10Button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        centerMaskView.addSubview(forward10Button)
        forward10Button.snp.makeConstraints { (make) in
            make.left.equalTo(playButton.snp.right).offset(30)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(50)
        }
        
        //backward10Button
        backward10Button.tag = BMPlayerCustomAction.backward10.rawValue
        backward10Button.addTarget(self, action: #selector(onCustomButtonPressed(_:)), for: .touchUpInside)
        backward10Button.setImage(Asset.backward10.image, for: .normal)
        backward10Button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        centerMaskView.addSubview(backward10Button)
        backward10Button.snp.makeConstraints { (make) in
            make.right.equalTo(playButton.snp.left).offset(-30)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(50)
        }
        
        // currentTimeLabel
        currentTimeLabel.removeFromSuperview()
        bottomWrapperView.addSubview(currentTimeLabel)
        currentTimeLabel.snp.makeConstraints { [unowned self](make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(40)
            make.height.equalTo(30)
        }

        //timeSlider
        timeSlider.removeFromSuperview()
        bottomWrapperView.addSubview(timeSlider)
        timeSlider.snp.makeConstraints { [unowned self](make) in
            make.top.equalToSuperview()
            make.left.equalTo(self.currentTimeLabel.snp.right).offset(10).priority(750)
            make.height.equalTo(30)
        }
        
        //totalTimeLabel
        totalTimeLabel.removeFromSuperview()
        bottomWrapperView.addSubview(totalTimeLabel)
        totalTimeLabel.snp.makeConstraints { [unowned self](make) in
            make.top.equalToSuperview()
            make.left.equalTo(self.timeSlider.snp.right).offset(5)
            make.right.equalToSuperview()
            make.width.equalTo(40)
            make.height.equalTo(30)
        }

        //progressView
        progressView.snp.makeConstraints { [unowned self](make) in
            make.centerY.left.right.equalTo(self.timeSlider)
            make.height.equalTo(2)
        }
        
        //subtitle
        subtitleLabel.snp.makeConstraints { [unowned self](make) in
            make.left.equalTo(self.subtitleBackView.snp.left).offset(10)
            make.right.equalTo(self.subtitleBackView.snp.right).offset(-10)
            make.top.equalTo(self.subtitleBackView.snp.top).offset(2)
            make.bottom.equalTo(self.subtitleBackView.snp.bottom).offset(-15)
        }
        
        subtitleBackView.snp.makeConstraints { [unowned self](make) in
            make.bottom.equalTo(self.snp.bottom).offset(-20)
            make.centerX.equalTo(self.snp.centerX)
            make.width.lessThanOrEqualTo(self.snp.width).offset(-10).priority(750)
        }
    }
    
    @objc func handleQuality() {
        self.customDelegate?.didPressQualityVideo()
    }
    
    @objc func handleSubtitle() {
        self.customDelegate?.didPressSubtitleVideo()
    }
    
    @objc func handleNextEp() {
        self.customDelegate?.didPressOnNextEp()
    }
    
    @objc func handleAllEps() {
        self.customDelegate?.didPressOnAllEps()
    }

    @objc func handleLockScreen() {
        self.isScreenLocked = !self.isScreenLocked
        self.customDelegate?.didPressLockScreen(lock: self.isScreenLocked)
        self.centerMaskView.isHidden = self.isScreenLocked
        self.topWrapperView.isHidden = self.isScreenLocked
        self.bottomWrapperView.isHidden = self.isScreenLocked
        self.lockedButtonView.isHidden = !self.isScreenLocked
        
        self.doubleTapGesture.isEnabled = !self.isScreenLocked
    }

    override func onDoubleTapGestureRecognized(_ gesture: UITapGestureRecognizer) {
        
        let locationPoint = gesture.location(in: self)
        
        switch gesture.state {

        case UIGestureRecognizer.State.ended:
            let x = locationPoint.x
            if x > (self.frame.width / 2) {
                customDelegate?.didPressOnForward10Button(currentTime: self.currentTime)
            } else if x < (self.frame.width / 2) {
                customDelegate?.didPressOnBackward10Button(currentTime: self.currentTime)
            }
        default:
            break
        }
    }
    
    func prepareBottomButton(title: String, imageName: String) -> UIButton {
        let button = UIButton()
//        button.backgroundColor = .darkGray
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.titleLabel?.textColor = .white
        button.setImage(UIImage(named: imageName), for: .normal)
        button.tintColor = .white
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
    }
    
    @objc open func onCustomButtonPressed(_ button: UIButton) {
      autoFadeOutControlViewWithAnimation()
        switch button.tag {
        case BMPlayerCustomAction.close.rawValue:
            customDelegate?.didPressOnCloseButton()
        case BMPlayerCustomAction.forward10.rawValue:
            customDelegate?.didPressOnForward10Button(currentTime: self.currentTime)
        case BMPlayerCustomAction.backward10.rawValue:
            customDelegate?.didPressOnBackward10Button(currentTime: self.currentTime)
        case BMPlayerCustomAction.cast.rawValue:
            customDelegate?.didPressCastVideo()

        default:
            break
        }
    }
    
    override func controlViewAnimation(isShow: Bool) {
        let alpha: CGFloat = isShow ? 1.0 : 0.0
        self.isMaskShowing = isShow
        
        UIApplication.shared.setStatusBarHidden(!isShow, with: .fade)
        
        UIView.animate(withDuration: 0.3, animations: {[weak self] in
          guard let wSelf = self else { return }
          wSelf.topMaskView.alpha    = alpha
          wSelf.bottomMaskView.alpha = alpha
          wSelf.centerMaskView.alpha = alpha
          wSelf.mainMaskView.backgroundColor = UIColor(white: 0, alpha: isShow ? 0.4 : 0.0)

          if isShow {
              if wSelf.isFullscreen { wSelf.chooseDefinitionView.alpha = 1.0 }
          } else {
              wSelf.replayButton.isHidden = true
              wSelf.chooseDefinitionView.snp.updateConstraints { (make) in
                  make.height.equalTo(35)
              }
              wSelf.chooseDefinitionView.alpha = 0.0
          }
          wSelf.layoutIfNeeded()
        }) { [weak self](_) in
            if isShow {
                self?.autoFadeOutControlViewWithAnimation()
            }
        }
    }
    
    override func playTimeDidChange(currentTime: TimeInterval, totalTime: TimeInterval) {
        super.playTimeDidChange(currentTime: currentTime, totalTime: totalTime)
        self.currentTime = currentTime
        if let subtitle = subtitle {
            showVttSubtile(from: subtitle, at: currentTime)
        }
    }
    
    private func showVttSubtile(from subtitle: WebVttBMSubtitles, at time: TimeInterval) {
        if let group = subtitle.search(for: time) {
            subtitleBackView.isHidden = false
            subtitleLabel.attributedText = NSAttributedString(string: group.text,
                                                              attributes: subtileAttribute)
        } else {
            subtitleBackView.isHidden = true
        }
    }
}
