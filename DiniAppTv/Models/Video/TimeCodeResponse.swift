//
//  TimeCodeResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 15/02/2022.
//

import Foundation

// MARK: - JSONAnyCollectionResponse
public struct TimeCodeResponse: Codable {
    public let content: String?
    public let status: Bool?
}
