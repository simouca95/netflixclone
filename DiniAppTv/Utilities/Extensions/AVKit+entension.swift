//
//  AVKit+entension.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 01/02/2022.
//

import AVKit

extension AVPlayerViewController {
    /// Activity indicator contained nested inside the controller's view.
    var activityIndicator: UIActivityIndicatorView? {
        // Indicator is extracted by traversing the subviews of the controller's `view` property.
        // `AVPlayerViewController`'s view contains a private `AVLoadingIndicatorView` that
        // holds an instance of `UIActivityIndicatorView` as a subview.
        let nestedSubviews: [UIView] = view.subviews
            .flatMap { [$0] + $0.subviews }
            .flatMap { [$0] + $0.subviews }
            .flatMap { [$0] + $0.subviews }
        return nestedSubviews.filter { $0 is UIActivityIndicatorView }.first as? UIActivityIndicatorView
    }

    /// Indicating whether the built-in activity indicator is hidden or not.
    var isActivityIndicatorHidden: Bool {
        set {
            activityIndicator?.alpha = newValue ? 0 : 1
        }
        get {
            return activityIndicator?.alpha == 0
        }
    }
}
