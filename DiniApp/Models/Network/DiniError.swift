//
//  DiniError.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/03/2021.
//

public struct DiniError: Codable {
    
    // MARK: Public Properties
    
    private(set) var message: String!
    
    // MARK: Constructors
    
    private init() {
        fatalError("You can't use this constructor")
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = try container.decode(String.self, forKey: .message)
    }
}
