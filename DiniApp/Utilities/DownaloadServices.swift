//
//  DownaloadServices.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 02/01/2022.
//

import Foundation
import Photos
import OSLog
import UIKit
import CoreData

enum DownloadState {
    case none
    case downloading
}

class DownloadVideo {
    var name: String
    var media: String
    var duration: Int?
    var picture: String?
    var currentViewedDuration: Int
    var download: Download
    var dowloadState: DownloadState = .none
    var isDownloading: Bool = false
    var progress: Double = 0.0
    var sessionTask: URLSessionDownloadTask?


    init(name: String, media: String, download: Download, duration: Int?, picture: String?, currentViewedDuration: Int?) {
        self.media = media
        self.download = download
        self.name = name
        self.duration = duration
        self.picture = picture
        self.currentViewedDuration = currentViewedDuration ?? 0
    }
}

class DownaloadServices {

    static let shared = DownaloadServices()
    
    var urlSession: URLSession = URLSession(
        configuration: URLSessionConfiguration.background(withIdentifier: "com.networking.dini"),
        delegate: DownloaderDelegate(),
        delegateQueue: OperationQueue())
    var activeDownloads: [URL: DownloadVideo] = [:]
        
    func start(with download: Download, resource: DiniResource, downloadState: DownloadState) {

        guard let url = URL(string: download.link ?? "") else { return }
        
        let downloadVideo = DownloadVideo(name: resource.title ?? "unknown",
                                          media: resource.media ?? "unknown",
                                          download: download,
                                          duration: resource.duration,
                                          picture: resource.picture,
                                          currentViewedDuration: resource.timeCodeFromViewed)
        
        downloadVideo.sessionTask = urlSession.downloadTask(with: url)
        
        guard let task = downloadVideo.sessionTask else { return }
        
        task.resume()

        downloadVideo.dowloadState = downloadState
        downloadVideo.isDownloading = true

        if let url = URL(string: download.link ?? "")  {
            activeDownloads[url] = downloadVideo
        }
    }
    
    deinit {
        urlSession.invalidateAndCancel()
    }
    
    private class DownloaderDelegate : NSObject, URLSessionDownloadDelegate {
        func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
//            os_log("Rachad2")
        }
        
        func urlSession(_ session: URLSession,
                        downloadTask: URLSessionDownloadTask,
                        didFinishDownloadingTo location: URL) {
//            debugPrint("Download finished: \(location)")
//            os_log("Rachad1")

            guard let url = downloadTask.originalRequest?.url,
                  let download = DownaloadServices.shared.activeDownloads[url]
            else { return }
            
            DownaloadServices.shared.activeDownloads.removeValue(forKey: url)
            download.dowloadState = .none
            NotificationCenter.default.post(name: Notification.Name("DiniDownload"), object: nil, userInfo: ["download": download])
            do {
                let downloadedData = try Data(contentsOf: location)
                var fileName = "\(Int(Date().timeIntervalSinceReferenceDate))"
                
                if let ext = download.download.type {
                    if ext.contains("mp3") {
                        fileName.append(".mp3")
                    } else {
                        fileName.append(".mp4")
                    }
                } else {
                    fileName.append(".mp4")
                }
                
                let dir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                var dataPath = dir.appendingPathComponent("DownloadedData")
                
                if !FileManager.default.fileExists(atPath: dataPath.path) {
                    
                    // create folder if it doesn't exist
                    do {
                        try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)

                        // exclude downloaded files from backup
                        do {
                            var resourceValues = URLResourceValues()
                            resourceValues.isExcludedFromBackup = true
                            try dataPath.setResourceValues(resourceValues)

                        } catch let error as NSError {
                            print("Failed ExcludedFromBackup error : \(error.localizedDescription)")
                        }
                        
                    } catch let error as NSError {
                        print("Error creating directory: \(error.localizedDescription)")
                    }
                }

                let fileURL = dataPath.appendingPathComponent(fileName)
                
                let queue = OperationQueue()
                queue.maxConcurrentOperationCount = 1
                 
                let operation1 = BlockOperation(block: {
                    do {
                        try downloadedData.write(to: fileURL)

                    }
                    catch {
                        print(error.localizedDescription)
                    }
                })
                operation1.qualityOfService = .background
                operation1.completionBlock = {
                    print("hazelsa: writing video data Done")
                }
                let operation2 = BlockOperation(block: {
                    DispatchQueue.main.async {
                        PersistenceHelper.shared.save(name: download.name,
                                                      media: download.media,
                                                      uri: fileName,
                                                      picture: download.picture,
                                                      duration: download.duration,
                                                      currentViewedDuration: download.currentViewedDuration)
                    }
                })

                operation2.completionBlock = { [weak self] in
                    guard let self = self else {
                        return
                    }
                    self.displayfinishedAlert(name: download.name)
                }
                 
                operation2.addDependency(operation1)
                 
                queue.addOperation(operation1)
                queue.addOperation(operation2)

            } catch {
                print(error.localizedDescription)
            }
        }
        
        func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                        didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                        totalBytesExpectedToWrite: Int64) {
            
            guard let url = downloadTask.originalRequest?.url,
                  let download = DownaloadServices.shared.activeDownloads[url]
            else { return }
    
            download.progress = Double(Float(totalBytesWritten) / Float(totalBytesExpectedToWrite))
    
//            print("Rachad ??? \(download.progress)")
//            os_log("Rachad0 %f", download.progress)

            download.dowloadState = .downloading
            NotificationCenter.default.post(name: Notification.Name("DiniDownload"), object: nil, userInfo: ["download": download])
        }
        
        private func displayfinishedAlert(name: String) {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "Dini", message: "\"\(name)\" a été téléchargé avec succès", preferredStyle: .alert)
                
//                alertController.addAction(UIAlertAction(title: "Ouvrir Documents", style: .default, handler: {_ in
//                        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//                        let documentsDirectory = paths[0]
//                    let path = documentsDirectory.absoluteString.replacingOccurrences(of: "file://", with: "shareddocuments://")
//                    let url = URL(string: path)!
//
//                    UIApplication.shared.open(url)
//
//                }))
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel))
                alertController.show(animated: true, completion: nil)
            }
        }
    }
}
