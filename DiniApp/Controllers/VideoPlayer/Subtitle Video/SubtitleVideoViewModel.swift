//
//  SubtitleViewModel.swift
//  DiniApp
//
//  Created by Rached Khoudi on 28/01/2022.
//

import Foundation

protocol SubtitleVideoDelegate: AnyObject {
    func onSpecificSubtitleTapped(subtitleResourceId: Int)
}

class SubtitleVideoViewModel {
    
    var resourceSubtitles: [ResourceSubtitle]
    var currentSubtitleId: Int
    var delegate: SubtitleVideoDelegate?
    
    init(resourceSubtitles: [ResourceSubtitle], currentSubtitleId: Int) {
        var subtitles = resourceSubtitles
        let emptySubtitle = ResourceSubtitle(id: -1, resource: nil, title: nil, languageCode: nil, languageName: "Sans sous-titre", path: nil, completePath: nil)
        subtitles.insert(emptySubtitle, at: 0)
        self.resourceSubtitles = subtitles
        self.currentSubtitleId = currentSubtitleId
    }
}
