//
//  ViewController.swift
//  DiniTv
//
//  Created by Rached Khoudi on 04/02/2021.
//

import UIKit
import RxSwift
import SVGKit
import AVKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var showsTableView: UITableView!
    @IBOutlet weak var topShowView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    //Top show view outlets
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var hEffectView: UIView!
    @IBOutlet weak var vEffectView: UIView!
    @IBOutlet weak var lectureButton: UIButton!
    private var player: AVPlayer?
    private var sectionToRemember: Int?
    var task: DispatchWorkItem?
    
    var viewModel: HomeViewModel!
    private var disposeBag = DisposeBag()
    private lazy var spinnerView = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupViews()
        initViewModels()
        viewModel.getCollectionList()
    }
    
    private func setupViews() {
        showsTableView.dataSource = self
        showsTableView.delegate = self
        showsTableView.register(cellType: HomeTableViewCell.self)
        
        setupViewEffect()
        topShowView.isHidden = true
        
        
        if let baseURL = DefaultsHelper.profilesImagesBaseURL,
           let pitcture = DefaultsHelper.profilImage,
           let svg = URL(string: "\(baseURL)/\(pitcture)"),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            
            profileImageView.image = receivedimage.uiImage
        }
    }
    
    func initViewModels() {
        viewModel = HomeViewModel()
        viewModel.reloadData.asObservable()
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe (onNext:{ [weak self] value in
                guard let self = self else { return }
                self.showsTableView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] bool in
                guard let self = self else { return }
                if bool {
                    self.addChild(self.spinnerView)
                    self.spinnerView.view.frame = self.view.frame
                    self.view.addSubview(self.spinnerView.view)
                    self.spinnerView.didMove(toParent: self)
                } else {
                    self.spinnerView.willMove(toParent: nil)
                    self.spinnerView.view.removeFromSuperview()
                    self.spinnerView.removeFromParent()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.currentWatchVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                guard let self = self else { return }
                self.configurePlayer(video: video)
            })
            .disposed(by: self.disposeBag)
    }
    
    @IBAction func onLectureDidTapped(_ sender: Any) {
        if let id = viewModel.sliderCollectionList?.collectionData?.first?.id {
            viewModel.playHomeVideoType = .fromMain
            viewModel.getCollectionWatch(collectionId: id)
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
//        if let data = viewModel.sliderCollectionList?.collectionData?.first,
//           context.nextFocusedView == lectureButton {
//            task = DispatchWorkItem { self.setupVideoView(data: data) }
//            if let task = task {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: task)
//            }
//        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        task?.cancel()
        player?.pause()
        if let sectionToRemember = sectionToRemember {
            UserDefaults.standard.set(sectionToRemember, forKey: Constant.HomeSectionToRemember)
        }
        super.viewDidDisappear(animated)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let collectionData = viewModel.sliderCollectionList?.collectionData?.first {
            topShowView.isHidden = false
            setupContent(data: collectionData)
        }
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = viewModel.items[section]
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .clear
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = item.title
        label.font = .boldSystemFont(ofSize: 40)
        label.textColor = .white
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.section]
        let cell: HomeTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.delegate = self
        cell.focusStyle = UITableViewCell.FocusStyle.custom
        
        cell.setupContent(vIndex: indexPath.section, data: item, itemsViewedsInfos: viewModel.itemsViewedsInfos)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 390
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

//MARK: -- CollectionView Delegate
extension HomeViewController: HomeTableViewCellDelegate {
    
    func didUpdateCellFocus(data: CollectionListData) {
        topShowView.isHidden = false
        setupContent(data: data)
        
        task = DispatchWorkItem { self.setupVideoView(data: data) }
        if let task = task {
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: task)
        }
    }
    
    func didSelectCell(vIndex: Int, hIndex: Int) {
        let item = viewModel.items[vIndex]
        
        sectionToRemember = vIndex
        switch item.type {
        case HomeCellType.speaker.rawValue:
            guard let id = item.speakerData?[hIndex].id else { return }
            let vc = StoryboardScene.Details.diniSpeakersViewController.instantiate()
            vc.viewModel = DiniSpeakersViewModel(speakerId: id)
            navigationController?.pushViewController(vc, animated: true)
        case HomeCellType.inProgress.rawValue:
            guard let id = item.collectionData?[hIndex].id else { return }
            viewModel.playHomeVideoType = .fromInProgress
            viewModel.currentHomeVideoVerticalItemIndex = vIndex
            viewModel.currentHomeVideoHorizontalItemIndex = hIndex
            viewModel.getCollectionWatch(collectionId: id)
        default:
            guard let id = item.collectionData?[hIndex].id else { return }
            let vc = StoryboardScene.Details.detailsViewController.instantiate()
            vc.viewModel = DetailsViewModel(collectionId: id, infoState: [])
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//Top Show View Methods
extension HomeViewController {
    func setupContent(data: CollectionListData) {
        task?.cancel()
        titleLabel.text = data.title
        
        if let tags = data.tags {
            let tagsNames = tags.compactMap{ $0.name }.joined(separator: " • ")
            self.tagsLabel.text = tagsNames
        }
        
        imageView.isHidden = false
        player?.pause()
        player = nil
        
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                             path2: data.sliderPicture) {
            imageView.kf.setImage(with: url)
        } else {
            imageView.image = UIImage(named: "ItemEmpty")
        }
        
        
        //        let imageView = UIImageView()
        //        imageView.contentMode = .scaleAspectFill
        //        if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
        //                                             path2: data.sliderPicture) {
        //            imageView.kf.setImage(with: url)
        //        } else {
        //            imageView.image = UIImage(named: "ItemEmpty")
        //        }
        //
        //        mediaView.addSubview(imageView)
        //        imageView.match(parent: mediaView)
    }
    
    func setupViewEffect() {
        let gradientMaskLayer0 = CAGradientLayer()
        gradientMaskLayer0.frame = vEffectView.bounds
        gradientMaskLayer0.colors = [UIColor.white.cgColor, UIColor.clear.cgColor]
        gradientMaskLayer0.startPoint = CGPoint(x: 0, y: 0.5)
        gradientMaskLayer0.endPoint = CGPoint(x:1.0, y:0.5)
        vEffectView.layer.mask = gradientMaskLayer0
        
        let gradientMaskLayer1 = CAGradientLayer()
        gradientMaskLayer1.frame = hEffectView.bounds
        gradientMaskLayer1.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
        gradientMaskLayer1.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientMaskLayer1.endPoint = CGPoint(x:0.5, y:1.0)
        hEffectView.layer.mask = gradientMaskLayer1
    }
    
    func setupVideoView(data: CollectionListData) {
        guard let base = DefaultsHelper.trailerBaseURL,
              let path = data.trailer,
              let url = URL(string: "\(base)/\(path)")
        else { return }
        
        self.player = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        //        controller.isActivityIndicatorHidden = true
        controller.player = self.player
        controller.videoGravity = AVLayerVideoGravity.resizeAspectFill
        controller.isActivityIndicatorHidden = true
        self.mediaView.addSubview(controller.view)
        controller.view.match(parent: self.mediaView)
        
        imageView.isHidden = true
        player?.play()
    }
}
