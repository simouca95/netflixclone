//
//  ApiCall+Query.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 09/02/2022.
//

import Alamofire

extension ApiCall {
    
    public struct Query {
        
        static func getCatalogCollectionList(token: String, profilId: Int, userId: Int, onSuccess: @escaping (_ response: QueryCollectionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.QueryController.getCatalogCollectionList(token: token, profileId: profilId, id: userId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: QueryCollectionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func getSpeakerCollectionList(token: String, profileid: Int, id: Int, onSuccess: @escaping (_ response: QueryCollectionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.QueryController.getSpeakerCollectionList(token: token, profileId: profileid, id: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: QueryCollectionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
    }
}
