//
//  SpeakerIntroTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import UIKit
import Reusable
import Kingfisher

class SpeakerIntroTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var viewModel: SpeakerIntroViewModel! {
        didSet {
            if let url = Helper.concatinatePaths(path1: DefaultsHelper.speakerImagesBaseURL,
                                                 path2: viewModel.imagePath) {
                speakerImageView.kf.setImage(with: url)
            } else {
                speakerImageView.image = UIImage(named: "ItemEmpty")
            }
            titleLabel.text = viewModel.title
            descriptionLabel.text = viewModel.description.html2String
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        speakerImageView.makeCircular()
        speakerImageView.layer.borderWidth = 1
        speakerImageView.layer.borderColor = UIColor.white.cgColor
        speakerImageView.layer.masksToBounds = true
    }
}
