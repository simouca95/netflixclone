//
//  SimpleTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/10/2021.
//

import UIKit
import Reusable

class SimpleTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var leftConstraintConstant: NSLayoutConstraint!
    @IBOutlet weak var posterImageView: UIImageView!
    
    override func awakeFromNib() {
    }
}
