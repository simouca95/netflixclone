//
//  NotificationHelper.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 04/02/2022.
//

import Foundation

enum NotificationHelper {
    
    // MARK: Public Static Properties
    
    static let onUserIsUnathorizedNotificationName = "onUserIsUnathorizedNotification"
    
    // MARK: Public Static Methods
    
    static func notifyUserIsUnathorized() {
        DefaultsHelper.deleteAllDefaults()
        NavigationHelper.shared.startAppFromLogin()
    }
}
