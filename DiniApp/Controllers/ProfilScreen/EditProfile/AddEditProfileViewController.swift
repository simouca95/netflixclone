//
//  AddEditProfileViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 1/19/21.
//

import UIKit
import DropDown
import SVGKit
import RxSwift

class AddEditProfileViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var sexDropDownView: UIView!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var deleteProfilButton: UIButton!
    @IBOutlet weak var addEditProfilButton: UIButton!
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var profileButton: UIButton!

    private var disposeBag = DisposeBag()
    let dropDown = DropDown()
    var actionType: ProfilActionType?
    var profil: Profil?
    weak var delegate : AddEditProfileDelegate?

    var viewModel: AddEditProfileViewModel! {
        didSet {
            updateView(actionType: viewModel.actionType, profil: viewModel.profil)
        }
    }
    
    private var selectedTypeId = ""
    private var selectedAvatarPath = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        initDropDown()
        initView()
        initViewModel()
    }
    
    func initView() {
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))

        imageView.layer.cornerRadius = 10
        imageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.didTapImageView(_:)))
        imageView.addGestureRecognizer(tap)
        
        nameTextField.placeholder = "Nom"
        sexLabel.text = "Type"
        imageView.layer.cornerRadius = 10
        imageView.layer.borderWidth = 4
        imageView.layer.borderColor = UIColor.white.cgColor
        
        deleteProfilButton.backgroundColor = .white
        deleteProfilButton.setTitleColor(.black, for: .normal)
        deleteProfilButton.setTitle("Supprimer Profil", for: .normal)
        
        addEditProfilButton.backgroundColor = .primary
        addEditProfilButton.setTitleColor(.white, for: .normal)
        
        nameView.layer.borderWidth = 2
        nameView.layer.borderColor = UIColor.white.cgColor
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            }
        }
    }
    
    @objc func didTapImageView(_ sender: UITapGestureRecognizer?) {
        let vc = StoryboardScene.Profil.avatarsViewController.instantiate()
        vc.viewModel = AvatarsViewModel()
        vc.viewModel.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func initDropDown() {
        dropDown.anchorView = sexDropDownView
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.sexLabel.text = viewModel.sexDataSource[index].name
            self.selectedTypeId = "\(viewModel.sexDataSource[index].id ?? 1)"
        }
        
        sexDropDownView.layer.borderWidth = 2
        sexDropDownView.layer.borderColor = UIColor.white.cgColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        sexDropDownView.addGestureRecognizer(tap)
    }
    
    func initViewModel() {
        guard let actionType = self.actionType else { return }
        
        viewModel = AddEditProfileViewModel(actionType: actionType, profil: profil)
        viewModel.getProfilTypes()
        viewModel.reloadData.asObservable()
            .skip(1)
            .subscribe (onNext:{[weak self] bool in
                guard let self = self else { return }
                if bool {
                    self.delegate?.updatedUserInfos()
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.dropDown.dataSource = self.viewModel.sexDataSource.compactMap({ $0.name })
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
    }
    
    func updateView(actionType: ProfilActionType, profil: Profil?) {
        switch actionType {
        case .add:
            imageLabel.text = "AJOUTER"
//            imageView.image = UIImage.init(systemName: "plus")
            deleteProfilButton.isHidden = true
            addEditProfilButton.setTitle("+ Ajouter Profil", for: .normal)

        case .edit:
            guard  let profil = profil else { return }
            imageLabel.text = "MODIFIER"
            addEditProfilButton.setTitle("Modifier Profil", for: .normal)

            if let data = viewModel.getPictureData(picturePath: profil.picture), UIImage(data: data) == nil {
                let receivedimage: SVGKImage = SVGKImage(data: data)
                imageView.image = receivedimage.uiImage
            }
            
            nameTextField.text = profil.name
            sexLabel.text = profil.type?.name
            selectedTypeId = "\(profil.type?.id ?? 0)"
            selectedAvatarPath = profil.picture ?? ""
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        dropDown.show()
    }
    
    @IBAction func didTappedDelete(_ sender: UIButton) {
        viewModel.deleteProfil(userId: "")
    }
    
    @IBAction func didTappedAddEdit(_ sender: UIButton) {
        switch actionType {
        case .add:
            viewModel.addProfil(userId: "\(DefaultsHelper.userId ?? 0)", profileTypeId: selectedTypeId, name: nameTextField.text ?? "", picture: selectedAvatarPath)
        case .edit:
            viewModel.editProfil(userId: "\(DefaultsHelper.userId ?? 0)", profileTypeId: selectedTypeId, name: nameTextField.text ?? "", picture: selectedAvatarPath)
        default:
            break
        }
    }
}

extension AddEditProfileViewController: AvatarsDelegate {
    func updateImage(imagePath: String) {
        selectedAvatarPath = imagePath
        if let data = viewModel.getPictureData(picturePath: imagePath) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            imageView.image = receivedimage.uiImage
        }
    }
}
