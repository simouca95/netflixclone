//
//  ProgressHelper.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import SVProgressHUD

struct ProgressHelper {

    // MARK: Public Static Methods

    static func show() {
        SVProgressHUD.show()
    }

    static func dismiss() {
        SVProgressHUD.dismiss()
    }
}
