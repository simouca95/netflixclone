//
//  SimpleAvatarCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/12/2021.
//

import UIKit
import SVGKit
import Reusable

class SimpleAvatarCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.image = nil
        avatarImageView.layer.cornerRadius = 10
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
    }

    func setupView(path: String) {
        if path.contains(".svg"), let svg = URL(string: path) {
            avatarImageView.downloadedsvg(from: svg)
        } else if let url = URL(string: path){
            avatarImageView.kf.setImage(with: url,
                                        placeholder: UIImage(named: "ItemEmpty"),
                                        options: [.cacheOriginalImage])
        }
    }
}
