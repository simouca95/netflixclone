//
//  FavoriteViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import RxCocoa
import RxSwift

class FavoriteViewModel: BaseViewModel {
    
    var items: [DiniSlider] = []
    var reloadData = BehaviorSubject<Void>(value: ())

    func getCollectionList() {
        guard let id = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }
        isLoading.accept(true)
        ApiCall.Favorite.getFavoriteCollectionList(token: token, id: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension FavoriteViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: FavoritesResponse?) {
        guard let response = response,
              let slider = response.collectionsList?.favorites?.first
        else { return }
        
        items.removeAll()
        items.append(contentsOf: slider)
        reloadData.onNext(())
        isLoading.accept(false)
    }

    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
