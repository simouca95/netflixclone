//
//  LoginViewController.swift
//  DiniTv
//
//  Created by Rached Khoudi on 20/02/2021.
//

import UIKit
import SideNavigationController
import RxSwift

class LoginViewController: UIViewController {
    
    var signInButtonIsEnabled = false {
        didSet {
            self.loginButton.isEnabled = signInButtonIsEnabled
            if signInButtonIsEnabled == false {
                self.loginButton.backgroundColor = .black
            } else {
                self.loginButton.backgroundColor = UIColor(rgb: AppConstants.Colors.primary)
            }
        }
    }
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailtextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    private var viewModel: LoginViewModel!
    private var disposeBag = DisposeBag()
    private lazy var spinnerView = SpinnerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupContentView()
        initViewModel()
    }
    
    func setupContentView(){
        emailtextField.addTarget(self, action: #selector(self.textFieldDidChange(sender:)), for: .editingChanged)
        passwordtextField.addTarget(self, action: #selector(self.textFieldDidChange(sender:)), for: .editingChanged)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    func initViewModel() {
        viewModel = LoginViewModel()
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] bool in
                guard let self = self else { return }
                if bool {
                    self.addChild(self.spinnerView)
                    self.spinnerView.view.frame = self.view.frame
                    self.view.addSubview(self.spinnerView.view)
                    self.spinnerView.didMove(toParent: self)
                } else {
                    self.spinnerView.willMove(toParent: nil)
                    self.spinnerView.view.removeFromSuperview()
                    self.spinnerView.removeFromParent()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoginSucceed.asObservable()
            .filter({ $0 })
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { _ in
//            .subscribe (onNext: { [weak self] _ in
//                guard let self = self else { return }
                
//                let options = SideNavigationController.Options(
//                    widthPercent: 1,
//                    overlayColor: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.4),
//                    shadowColor: UIColor.clear,
//                    scale: 1, position: .front)
//                let sideNavigationController = SideNavigationController(mainViewController: UINavigationController(rootViewController: HomeViewController()))
//                sideNavigationController.leftSide(viewController: SideMenuViewController(), options: options)
//                let window = UIWindow(frame: UIScreen.main.bounds)
//                window.rootViewController = sideNavigationController
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.window = window
//                window.makeKeyAndVisible()
                
                NavigationHelper.shared.startAppFromProfiles()
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        guard let email = emailtextField.text,
                let password = passwordtextField.text
        else { return }
        
        viewModel.login(email: email, password: password)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
    }
}

extension LoginViewController : UITextFieldDelegate{
    
    @objc func textFieldDidChange(sender : UITextField){
        if let passwordText = passwordtextField.text , let emailText = emailtextField.text {
            if passwordText.count > 0 , emailText.count > 0  {
                self.signInButtonIsEnabled = true
                self.loginButton.setNeedsFocusUpdate()
                self.setNeedsFocusUpdate()
            } else {
                self.signInButtonIsEnabled = false
            }
        }
    }
}
