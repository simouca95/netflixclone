//
//  EpsTableViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 22/04/2022.
//

import UIKit
import Reusable

class EpsTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var posterView: UIView!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!

    var progressValue: Float = 0.0

    override func prepareForReuse() {
        progressView.setProgress(0.0, animated: false)
    }
    
    var viewModel: EpsTableViewModel! {
        didSet {
            if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                 path2: viewModel.posterPath) {
                posterImage.kf.setImage(with: url,
                                        placeholder: UIImage(named: "ItemEmpty"),
                                        options: [.cacheOriginalImage])
            } else {
                posterImage.image = UIImage(named: "ItemEmpty")
            }
            
            infoLabel.text = viewModel.title
            durationLabel.text = "\(viewModel.durationInMinutes) min"
            if let progress = viewModel.progress {
                self.progressView.setProgress(progress, animated: false)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        posterView.layer.cornerRadius = 6
        posterView.clipsToBounds = true
        progressView.progressTintColor = .red
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            posterView.layer.borderWidth = 6
            posterView.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            posterView.layer.borderWidth = 0
            posterView.layer.borderColor = UIColor.clear.cgColor
        }
    }

}
