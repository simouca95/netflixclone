//
//  User.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

public struct LogiAndWalkthroughResponse: Codable {
    public let images: [[String]]?
    public let logo: [[String]]?
}

// MARK: - User
public struct LoginResponse: Codable {
    public let code: Int?
    public let message: String?
    public let token: String?
    public let vimeoToken: String?
    public let userInfos: UserInfos?
    public let appInfos: AppInfos?

    enum CodingKeys: String, CodingKey {
        case code
        case message
        case token
        case vimeoToken
        case userInfos = "UserInfos"
        case appInfos = "AppInfos"
    }
}

// MARK: - AppInfos
public struct AppInfos: Codable {
    public let imagesPath: ImagesPath?
    public let profils: AppInfosProfils?
    public let preferencesParameters: PreferencesParameters?
    public let podcastPath: PodcastPath?
    public let trailerPath: TrailerPath?
}

// MARK: - ImagesPath
public struct ImagesPath: Codable {
    public let imagesPath1, imagesPath2, imagesPath3, imagesPath4: String?
    public let imagesPath5, imagesPath6, imagesPath7, imagesPath8: String?
    public let imagesPath9, imagesPath10, imagesPath11, imagesPath12: String?
    public let portraitPicturePath: String?
    public let portraitPictureResourcesPath: String?
    public let portraitPictureSeasonsPath: String?
    public let subTitleResourcesPath: String?

    enum CodingKeys: String, CodingKey {
        case imagesPath1 = "imagesPath_1"
        case imagesPath2 = "imagesPath_2"
        case imagesPath3 = "imagesPath_3"
        case imagesPath4 = "imagesPath_4"
        case imagesPath5 = "imagesPath_5"
        case imagesPath6 = "imagesPath_6"
        case imagesPath7 = "imagesPath_7"
        case imagesPath8 = "imagesPath_8"
        case imagesPath9 = "imagesPath_9"
        case imagesPath10 = "imagesPath_10"
        case imagesPath11 = "imagesPath_11"
        case imagesPath12 = "imagesPath_12"
        case portraitPicturePath = "portraitPicturePath"
        case portraitPictureResourcesPath = "portraitPictureResourcesPath"
        case portraitPictureSeasonsPath = "portraitPictureSeasonsPath"
        case subTitleResourcesPath = "subTitleResourcesPath"
    }
}

// MARK: - AppInfosProfils
public struct AppInfosProfils: Codable {
    public let maxProfilsInApplication: Int?
}

public struct PodcastPath: Codable {
    public let podcastPath: String?
}

public struct TrailerPath: Codable {
    public let trailerPath: String?
}
// MARK: - PreferencesParameters
public struct PreferencesParameters: Codable {
    public let isLikeUnLikeVisible: Bool?
}

// MARK: - UserInfos
public struct UserInfos: Codable {
    public let userID: Int?
    public let userRole: [String]?
    public let userEmail, userFirstName, userLastName, userIdentifier: String?
    public let subscription: Subscription?
    public let profils: UserInfosProfils?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case userRole, userEmail, userFirstName, userLastName, userIdentifier, subscription, profils
    }
}

// MARK: - UserInfosProfils
public struct UserInfosProfils: Codable {
    public let currentprofilListCount: Int?
}

// MARK: - Subscription
public struct Subscription: Codable {
    public let isEnabled: Bool
    public let createAt, endAt, updateAt, canceledAt: String?
}
