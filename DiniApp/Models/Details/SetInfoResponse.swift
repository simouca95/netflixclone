//
//  SetInfoResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/10/2021.
//

import Foundation

public struct SetInfoResponse: Codable {
    let status: Bool?
    let result: SetInfoResult?
    
    enum SetInfoResult: String, Codable {
        case delete = "delete"
        case add = "add"
    }
}

