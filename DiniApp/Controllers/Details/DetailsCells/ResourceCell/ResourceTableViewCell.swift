//
//  ResourceTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import Reusable
import Photos
import SnapKit

class ResourceTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var posterView: UIView!
    @IBOutlet weak var circleProgressView: CircularProgressView!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var newResourceImage: UIImageView!
    
    var progressValue: Float = 0.0
    let downloadService = DownaloadServices.shared
    var onDownloadTapped: (() -> Void) = {  }
    var onCancelTapped: (() -> Void) = {  }
    var onDeleteTapped: (() -> Void) = {  }

    override func prepareForReuse() {
        downloadButton.isHidden = false
        circleProgressView.isHidden = true
        progressView.setProgress(0.0, animated: false)
        newResourceImage.isHidden = false
    }
    
    var viewModel: ResourceTableViewModel! {
        didSet {            
            if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                 path2: viewModel.posterPath) {
                posterImage.kf.setImage(with: url,
                                        placeholder: UIImage(named: "ItemEmpty"),
                                        options: [.cacheOriginalImage])
            } else {
                posterImage.image = UIImage(named: "ItemEmpty")
            }
            
            if let media = viewModel?.media,
               let bool = PersistenceHelper.shared.isPersisted(media: media), bool {
                downloadButton.setImage(UIImage(named: "delete"), for: .normal)
                // downloadView.isUserInteractionEnabled = false
            } else {
                downloadButton.setImage(UIImage(named: "download-circular-button"), for: .normal)
                downloadView.isUserInteractionEnabled = true
            }
            if let isNewResource = viewModel?.isNewResource {
                newResourceImage.isHidden = !isNewResource
            }
            newResourceImage.kf.setImage(with: URL(string: DefaultsHelper.newCollectionPicture ?? ""))
            infoLabel.text = viewModel.title
            durationLabel.text = "\(viewModel.durationInMinutes) min"
            if let progress = viewModel.progress {
                self.progressView.setProgress(progress, animated: false)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        posterView.layer.cornerRadius = 6
        posterView.clipsToBounds = true
        downloadView.isUserInteractionEnabled = true
        downloadButton.setTitle("", for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapDownloadView(_:)))
        downloadView.addGestureRecognizer(tap)
    }
    
    @objc func handleTapDownloadView(_ sender: UITapGestureRecognizer?) {
        switch viewModel.downloadState {
        case .none:
            if let media = viewModel?.media,
               let bool = PersistenceHelper.shared.isPersisted(media: media), bool {
                self.onDeleteTapped()

            } else {
                self.onDownloadTapped()
            }
        case .downloading:
            self.onCancelTapped()
        }
    }
    
    func updateDisplay(progress: Double) {
        downloadButton.isHidden = true
        circleProgressView.isHidden = false
//        self.downloadButton.setTitle("\(String(format: "%.f", progress*100))%", for: .normal)
        circleProgressView.setprogress(progress, UIColor.primary)
    }
    
    func makeDownloadDone() {
        downloadButton.isHidden = false
        circleProgressView.isHidden = true
        downloadButton.setImage(UIImage(named: "delete"), for: .normal)
        //downloadButton.isUserInteractionEnabled = false
//        downloadButton.setTitle("", for: .normal)
    }
    func resetCell() {
        downloadButton.isHidden = false
        downloadButton.setImage(UIImage(named: "download-circular-button"), for: .normal)
        downloadView.isUserInteractionEnabled = true
        circleProgressView.isHidden = true
        //downloadButton.isUserInteractionEnabled = false
//        downloadButton.setTitle("", for: .normal)
    }
}
