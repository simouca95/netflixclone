// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum Details: StoryboardType {
    internal static let storyboardName = "Details"

    internal static let detailsAudioViewController = SceneType<DetailsAudioViewController>(storyboard: Details.self, identifier: "DetailsAudioViewController")

    internal static let detailsViewController = SceneType<DetailsViewController>(storyboard: Details.self, identifier: "DetailsViewController")

    internal static let diniSpeakersViewController = SceneType<DiniSpeakersViewController>(storyboard: Details.self, identifier: "DiniSpeakersViewController")

    internal static let downloadAlertViewController = SceneType<DownloadAlertViewController>(storyboard: Details.self, identifier: "DownloadAlertViewController")
  }
  internal enum Islamic: StoryboardType {
    internal static let storyboardName = "Islamic"

    internal static let mushafViewController = SceneType<MushafViewController>(storyboard: Islamic.self, identifier: "MushafViewController")

    internal static let prayViewController = SceneType<PrayViewController>(storyboard: Islamic.self, identifier: "PrayViewController")

    internal static let qiblaViewController = SceneType<QiblaViewController>(storyboard: Islamic.self, identifier: "QiblaViewController")

    internal static let speakersViewController = SceneType<SpeakersViewController>(storyboard: Islamic.self, identifier: "SpeakersViewController")

    internal static let surahViewController = SceneType<SurahViewController>(storyboard: Islamic.self, identifier: "SurahViewController")
  }
  internal enum Landing: StoryboardType {
    internal static let storyboardName = "Landing"

    internal static let initialScene = InitialSceneType<SplashScreenViewController>(storyboard: Landing.self)

    internal static let loginViewController = SceneType<LoginViewController>(storyboard: Landing.self, identifier: "LoginViewController")

    internal static let menuViewController = SceneType<MenuViewController>(storyboard: Landing.self, identifier: "MenuViewController")

    internal static let onboardingViewController = SceneType<WalkthroughViewController>(storyboard: Landing.self, identifier: "OnboardingViewController")

    internal static let signupViewController = SceneType<SignupViewController>(storyboard: Landing.self, identifier: "SignupViewController")

    internal static let splashScreenViewController = SceneType<SplashScreenViewController>(storyboard: Landing.self, identifier: "SplashScreenViewController")

    internal static let landingNVC = SceneType<UIKit.UINavigationController>(storyboard: Landing.self, identifier: "landingNVC")

    internal static let loginNVC = SceneType<UIKit.UINavigationController>(storyboard: Landing.self, identifier: "loginNVC")

    internal static let menuNVC = SceneType<UIKit.UINavigationController>(storyboard: Landing.self, identifier: "menuNVC")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Main: StoryboardType {
    internal static let storyboardName = "Main"

    internal static let catalogViewController = SceneType<CatalogViewController>(storyboard: Main.self, identifier: "CatalogViewController")

    internal static let favoriteViewController = SceneType<FavoriteViewController>(storyboard: Main.self, identifier: "FavoriteViewController")

    internal static let homeViewController = SceneType<HomeViewController>(storyboard: Main.self, identifier: "HomeViewController")

    internal static let mainNVC = SceneType<UIKit.UINavigationController>(storyboard: Main.self, identifier: "MainNVC")

    internal static let mainTab = SceneType<UIKit.UITabBarController>(storyboard: Main.self, identifier: "MainTab")

    internal static let offlineVideoListViewController = SceneType<OfflineVideoListViewController>(storyboard: Main.self, identifier: "OfflineVideoListViewController")
  }
  internal enum Profil: StoryboardType {
    internal static let storyboardName = "Profil"

    internal static let addEditProfileViewController = SceneType<AddEditProfileViewController>(storyboard: Profil.self, identifier: "AddEditProfileViewController")

    internal static let avatarsViewController = SceneType<AvatarsViewController>(storyboard: Profil.self, identifier: "AvatarsViewController")

    internal static let mainNVC = SceneType<UIKit.UINavigationController>(storyboard: Profil.self, identifier: "MainNVC")

    internal static let selectProfileViewController = SceneType<SelectProfileViewController>(storyboard: Profil.self, identifier: "SelectProfileViewController")
  }
  internal enum Video: StoryboardType {
    internal static let storyboardName = "Video"

    internal static let allEpisodesViewController = SceneType<AllEpisodesViewController>(storyboard: Video.self, identifier: "AllEpisodesViewController")

    internal static let nextEpisodeLoaderViewController = SceneType<NextEpisodeLoaderViewController>(storyboard: Video.self, identifier: "NextEpisodeLoaderViewController")

    internal static let qualityVideoViewController = SceneType<QualityVideoViewController>(storyboard: Video.self, identifier: "QualityVideoViewController")

    internal static let subtitleVideoViewController = SceneType<SubtitleVideoViewController>(storyboard: Video.self, identifier: "SubtitleVideoViewController")

    internal static let videoPlayViewController = SceneType<VideoPlayViewController>(storyboard: Video.self, identifier: "VideoPlayViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
