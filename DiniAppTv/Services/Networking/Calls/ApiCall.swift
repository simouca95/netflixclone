//
//  ApiCall.swift
//  PlaceStation
//
//  Created by Rached Khoudi on 17/10/2020.
//

import Alamofire

public struct ApiCall {
    
    // MARK: Private Static Methods
    
    private static func decodeResponse<T: Decodable>(data: Data?) -> T? {
        if  let data = data {
            let decoder = JSONDecoder()
            return try? decoder.decode(T.self, from: data)
        }
        return nil
    }
    
    // MARK: Public Static Methods
    
    static func decodeResponse<T: Decodable>(response: DataResponse<Any>) -> T? {
        return ApiCall.decodeResponse(data: response.data)
    }
    
    static func decodeError<T: Any>(response: DataResponse<T>) -> DiniError? {
        if let statusCode = response.response?.statusCode, statusCode == 401 {
            NotificationHelper.notifyUserIsUnathorized()
        }
        return ApiCall.decodeResponse(data: response.data)
    }
}
