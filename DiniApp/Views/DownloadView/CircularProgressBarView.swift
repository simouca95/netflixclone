//
//  CircularProgressBarView.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/01/2022.
//

import UIKit

//class CircularProgress: UIView {
//
//    fileprivate var progressLayer = CAShapeLayer()
//    fileprivate var tracklayer = CAShapeLayer()
//
//    /*
//     // Only override draw() if you perform custom drawing.
//     // An empty implementation adversely affects performance during animation.
//     override func draw(_ rect: CGRect) {
//     // Drawing code
//     }
//     */
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        createCircularPath()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        createCircularPath()
//    }
//
//    var progressColor:UIColor = UIColor.red {
//        didSet {
//            progressLayer.strokeColor = progressColor.cgColor
//        }
//    }
//
//    var trackColor:UIColor = UIColor.white {
//        didSet {
//            tracklayer.strokeColor = trackColor.cgColor
//        }
//    }
//
//    fileprivate func createCircularPath() {
//        self.backgroundColor = UIColor.clear
//        self.layer.cornerRadius = self.frame.size.width/2.0
//        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0),
//                                      radius: (frame.size.width - 1.5)/2, startAngle: CGFloat(-0.5 * Double.pi),
//                                      endAngle: CGFloat(1.5 * Double.pi), clockwise: true)
//
//        tracklayer.path = circlePath.cgPath
//        tracklayer.fillColor = UIColor.clear.cgColor
//        tracklayer.strokeColor = trackColor.cgColor
//        tracklayer.lineWidth = 10.0;
//        tracklayer.strokeEnd = 1.0
//        layer.addSublayer(tracklayer)
//
//        progressLayer.path = circlePath.cgPath
//        progressLayer.fillColor = UIColor.clear.cgColor
//        progressLayer.strokeColor = progressColor.cgColor
//        progressLayer.lineWidth = 10.0;
//        progressLayer.strokeEnd = 0.0
//        layer.addSublayer(progressLayer)
//
//    }
//
//    func setProgressWithAnimation(duration: TimeInterval, value: Float) {
//        let animation = CABasicAnimation(keyPath: "strokeEnd")
//        animation.duration = duration
//        // Animate from 0 (no circle) to 1 (full circle)
//        animation.fromValue = 0
//        animation.toValue = value
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
//        progressLayer.strokeEnd = CGFloat(value)
//        progressLayer.add(animation, forKey: "animateCircle")
//    }
//
////    func setProgress(value: Float) {
////        let animation = CABasicAnimation(keyPath: "strokeEnd")
////        animation.duration = 0
////        // Animate from 0 (no circle) to 1 (full circle)
////        animation.fromValue = 0
////        animation.toValue = value
////        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
////        progressLayer.strokeEnd = CGFloat(value)
////        progressLayer.add(animation, forKey: "animateCircle")
////    }
//
//}

class CircularProgressView: UIView {
    private let labelLayer = CATextLayer()
    private let centerText = UILabel()
    private let foregroundLayer = CAShapeLayer()
    private let backgroundLayer = CAShapeLayer()
    
    private var startPoint = CGFloat(-Double.pi / 2)
    private var endPoint = CGFloat(3 * Double.pi / 2)
    private var pathCenter: CGPoint{ get{ return self.convert(self.center, from:self.superview) } }
    
    /// circle line width
    var lineWidth: Double  = 4
    /// Text inside the circle line 1
    var title : String = ""
    /// Text inside the circle line 2
    var subtitle : String = ""
    /// Circle foregroundColor
    var color: UIColor = UIColor.primary
    /// Circle backgroundColor
    var Backgroundcolor: UIColor = UIColor.white
    ///0.0 - 1.0
    var progress: Double = 0.5
    /// circle radius
    var radius: Int = 10
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .black
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.backgroundColor = .black
    }
    
    private func createCircularPath() {
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2, y: frame.size.height / 2), radius: CGFloat(self.radius), startAngle: startPoint, endAngle: endPoint, clockwise: true)
        
        // Create background layer
        backgroundLayer.path = circularPath.cgPath
        backgroundLayer.fillColor = UIColor.clear.cgColor
        backgroundLayer.lineCap = .round
        backgroundLayer.lineWidth = self.lineWidth
        backgroundLayer.strokeEnd = 1.0
        backgroundLayer.strokeColor = Backgroundcolor.cgColor
        layer.addSublayer(backgroundLayer)
        
        // Create foreground layer
        foregroundLayer.path = circularPath.cgPath
        foregroundLayer.fillColor = UIColor.clear.cgColor
        foregroundLayer.lineCap = .round
        foregroundLayer.lineWidth = self.lineWidth
        foregroundLayer.strokeEnd = self.progress
        foregroundLayer.strokeColor = self.color.cgColor
        layer.addSublayer(foregroundLayer)
    }
    
    
    private func createLabels(){
        centerText.sizeToFit()
        centerText.textAlignment = .center
        centerText.numberOfLines = 2
        centerText.frame = self.frame
        // center textfield in view
        centerText.center = CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0)
        
        //Text Formatting
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        let firstAttributes: [NSAttributedString.Key: Any] = [ .font: UIFont.systemFont(ofSize: 10.0), .foregroundColor: self.color, .paragraphStyle: titleParagraphStyle]
        let secondAttributes = [ NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10.0), NSAttributedString.Key.foregroundColor: self.color, .paragraphStyle: titleParagraphStyle]
        
        let firstString = NSMutableAttributedString(string: self.title, attributes: firstAttributes)
        let secondString = NSMutableAttributedString(string: self.subtitle, attributes: secondAttributes)//
        
        firstString.append(secondString)
        centerText.attributedText = firstString
        
        self.addSubview(centerText)
    }
    
    
    func setprogress(_ progress: Double = 0.5, _ color: UIColor = UIColor.blue, _ text: String = "", _ text2: String = ""){
        self.progress =  progress
        self.color = color
        self.title = text
        self.subtitle = "\n" + text2
        createLabels()
        createCircularPath()
    }
    
    func animate(_ value: Double, duration: TimeInterval = 2 ) {
        let circularProgressAnimation = CABasicAnimation(keyPath: "strokeEnd")
        circularProgressAnimation.duration = duration
        circularProgressAnimation.toValue = value
        circularProgressAnimation.fillMode = .forwards
        circularProgressAnimation.isRemovedOnCompletion = false
        foregroundLayer.add(circularProgressAnimation, forKey: "progressAnim")
    }
}
