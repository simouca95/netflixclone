//
//  AppDelegate.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/12/20.
//

import UIKit
import AVKit
import CoreData
import OSLog
import Siren
import Firebase
import FirebaseMessaging



@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    let gcmMessageIDKey = "gcm.message_id"
    var ressourceId = 0
    var launchFromNotification = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UITabBar.appearance().tintColor = .primary
        
        let session = AVAudioSession.sharedInstance()
        do {
            // Configure the audio session for movie playback
            try session.setCategory(AVAudioSession.Category.playback,
                                    mode: AVAudioSession.Mode.moviePlayback,
                                    options: [])
        } catch let error as NSError {
            debugPrint("Failed to set the audio session category and mode: \(error.localizedDescription)")
        }
        
        do {
            try session.setActive(true)
        } catch let error as NSError {
            debugPrint("Failed to activate the audio session with the set category: \(error.localizedDescription)")
        }
        
        let siren = Siren.shared
        siren.rulesManager = RulesManager(globalRules: .annoying)
        Siren.shared.presentationManager = PresentationManager(alertTitle: "Une nouvelle version est disponible", alertMessage: "Une nouvelle version est disponible. Veuillez mettre l’application à jour pour profiter d’une meilleure expérience utilisateur.", updateButtonTitle: "Mettre à jour", nextTimeButtonTitle: "La prochaine fois", forceLanguageLocalization: .french)
        
        siren.wail() // Line 2
        ///Firebase
        FirebaseApp.configure()
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        registerNotification()
        return true
    }
    
    func registerNotification() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
            Messaging.messaging().isAutoInitEnabled = true
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult)
                     -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DiniApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate {
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
}

extension AppDelegate {
    func application(_: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions)
                                -> Void) {
        completionHandler([[.alert, .sound]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let data = response.notification.request.content.userInfo
        let type = data["type"] as? String
        switch type {
        case "resource":
            if let ressourceId = data["resourceId"] as? String, let collectionId = data["collectionId"] as? String {
                PushNotificationHandler.shared.didReceiveNotification = .resource(resourceId: Int(ressourceId) ?? 0, collectionId: Int(collectionId) ?? 0)
                PushNotificationHandler.shared.clickNotfication.onNext(.resource(resourceId: Int(ressourceId) ?? 0, collectionId: Int(collectionId) ?? 0))
            }
        case "collection":
            if let collectionId = data["collectionId"] as? String {
                PushNotificationHandler.shared.didReceiveNotification = .collection(collectionId: Int(collectionId) ?? 0)
                PushNotificationHandler.shared.clickNotfication.onNext(.collection(collectionId: Int(collectionId) ?? 0))
            }
        default:
            let aps = data["aps"] as? [String: Any]
            let alert = aps?["alert"] as? [String: String]
            let title = alert?["title"]
            let body = alert?["body"]
            if let title = title, let body = body {
            PushNotificationHandler.shared.clickNotfication.onNext(.simpleNotification(title: title, body: body))
            PushNotificationHandler.shared.didReceiveNotification = .simpleNotification(title: title, body: body)
            }
        }
        
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        DefaultsHelper.saveDeviceId(fcmToken ?? "")
        let dataDict: [String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(
            name: Notification.Name("FCMToken"),
            object: nil,
            userInfo: dataDict
        )
    }
    
}
