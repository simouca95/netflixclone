//
//  HomeViewController+Video.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 15/02/2022.
//

import AVKit

extension HomeViewController {
    
    func configurePlayer(video: VideoResponse) {
        
        let vc = VideoPlayerViewController()

        switch viewModel.playHomeVideoType {
        case .fromMain:
            
            if let typeIndex = viewModel.sliderCollectionList?.collectionData?.first?.type,
                     let type = CollectionType(rawValue: typeIndex),
                     let resourceId = self.viewModel.currentHomeVideoId,
               let resources = viewModel.sliderCollectionList?.collectionData?.first?.resources {
               
                let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.sliderCollectionList?.collectionData?.first?.seasons)
               vm.delegate = self
               vc.viewModel = vm
           }
            
        case .fromInProgress:
            
            if let verticalIndex = viewModel.currentHomeVideoVerticalItemIndex,
               let horizontalIndex = viewModel.currentHomeVideoHorizontalItemIndex,
               let typeIndex = viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.type,
               let type = CollectionType(rawValue: typeIndex),
               let resourceId = self.viewModel.currentHomeVideoId,
               let resources = viewModel.getAvailableResources(verticalIndex: verticalIndex, horizontalIndex: horizontalIndex) {
                
                let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.seasons)
                vm.delegate = self
                vc.viewModel = vm
            }
            
        case .fromHomeResources:
            
            if let verticalIndex = viewModel.currentHomeVideoVerticalItemIndex,
               let typeIndex = viewModel.items[verticalIndex].collectionData?[safe: 0]?.type,
               let type = CollectionType(rawValue: typeIndex),
               let resourceId = self.viewModel.currentHomeVideoId,
               let resources = viewModel.getAvailableResources(verticalIndex: verticalIndex, horizontalIndex: 0) {
                
                let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.items[safe: verticalIndex]?.collectionData?[safe: 0]?.seasons)
                vm.delegate = self
                vc.viewModel = vm
            }
            
        default:
            break
        }
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension HomeViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("anotherViewControllerYouWantToObserve was dismissed")
        self.viewModel.getCollectionList()
        return nil
    }
}

extension HomeViewController: AVPlayerDelegate {
    func onSuccessTimeCodeSend() {
        viewModel.getCollectionList()
    }
    
    func onFailTimeCodeSend() {
        print("CustomAVPlayerDelegate NOT WORKING")
    }
}
