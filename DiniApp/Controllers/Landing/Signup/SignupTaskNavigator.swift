//
//  SignupTaskNavigator.swift
//  netflixCloneApp
//
//  Created by sami hazel on 13/03/2022.
//

import Foundation
protocol SignupTaskNavigator: AnyObject {
    func goToLoginScreen()
}
