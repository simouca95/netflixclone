// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let bg = ImageAsset(name: "Bg")
  internal static let seasonalGiftCard1 = ImageAsset(name: "SeasonalGiftCard1")
  internal static let seasonalGiftCard10 = ImageAsset(name: "SeasonalGiftCard10")
  internal static let seasonalGiftCard2 = ImageAsset(name: "SeasonalGiftCard2")
  internal static let seasonalGiftCard3 = ImageAsset(name: "SeasonalGiftCard3")
  internal static let seasonalGiftCard4 = ImageAsset(name: "SeasonalGiftCard4")
  internal static let seasonalGiftCard5 = ImageAsset(name: "SeasonalGiftCard5")
  internal static let seasonalGiftCard6 = ImageAsset(name: "SeasonalGiftCard6")
  internal static let seasonalGiftCard7 = ImageAsset(name: "SeasonalGiftCard7")
  internal static let seasonalGiftCard8 = ImageAsset(name: "SeasonalGiftCard8")
  internal static let seasonalGiftCard9 = ImageAsset(name: "SeasonalGiftCard9")
  internal static let thankYouGiftCard1 = ImageAsset(name: "ThankYouGiftCard1")
  internal static let thankYouGiftCard2 = ImageAsset(name: "ThankYouGiftCard2")
  internal static let thankYouGiftCard3 = ImageAsset(name: "ThankYouGiftCard3")
  internal static let thankYouGiftCard4 = ImageAsset(name: "ThankYouGiftCard4")
  internal static let itemEmpty = ImageAsset(name: "ItemEmpty")
  internal static let belleBete = ImageAsset(name: "belle_bete")
  internal static let img1 = ImageAsset(name: "img_1")
  internal static let img10 = ImageAsset(name: "img_10")
  internal static let img11 = ImageAsset(name: "img_11")
  internal static let img12 = ImageAsset(name: "img_12")
  internal static let img13 = ImageAsset(name: "img_13")
  internal static let img14 = ImageAsset(name: "img_14")
  internal static let img15 = ImageAsset(name: "img_15")
  internal static let img16 = ImageAsset(name: "img_16")
  internal static let img17 = ImageAsset(name: "img_17")
  internal static let img18 = ImageAsset(name: "img_18")
  internal static let img19 = ImageAsset(name: "img_19")
  internal static let img2 = ImageAsset(name: "img_2")
  internal static let img20 = ImageAsset(name: "img_20")
  internal static let img21 = ImageAsset(name: "img_21")
  internal static let img22 = ImageAsset(name: "img_22")
  internal static let img23 = ImageAsset(name: "img_23")
  internal static let img24 = ImageAsset(name: "img_24")
  internal static let img25 = ImageAsset(name: "img_25")
  internal static let img26 = ImageAsset(name: "img_26")
  internal static let img27 = ImageAsset(name: "img_27")
  internal static let img28 = ImageAsset(name: "img_28")
  internal static let img29 = ImageAsset(name: "img_29")
  internal static let img3 = ImageAsset(name: "img_3")
  internal static let img30 = ImageAsset(name: "img_30")
  internal static let img31 = ImageAsset(name: "img_31")
  internal static let img32 = ImageAsset(name: "img_32")
  internal static let img33 = ImageAsset(name: "img_33")
  internal static let img34 = ImageAsset(name: "img_34")
  internal static let img35 = ImageAsset(name: "img_35")
  internal static let img36 = ImageAsset(name: "img_36")
  internal static let img37 = ImageAsset(name: "img_37")
  internal static let img38 = ImageAsset(name: "img_38")
  internal static let img39 = ImageAsset(name: "img_39")
  internal static let img4 = ImageAsset(name: "img_4")
  internal static let img40 = ImageAsset(name: "img_40")
  internal static let img41 = ImageAsset(name: "img_41")
  internal static let img42 = ImageAsset(name: "img_42")
  internal static let img43 = ImageAsset(name: "img_43")
  internal static let img44 = ImageAsset(name: "img_44")
  internal static let img45 = ImageAsset(name: "img_45")
  internal static let img46 = ImageAsset(name: "img_46")
  internal static let img47 = ImageAsset(name: "img_47")
  internal static let img5 = ImageAsset(name: "img_5")
  internal static let img6 = ImageAsset(name: "img_6")
  internal static let img7 = ImageAsset(name: "img_7")
  internal static let img8 = ImageAsset(name: "img_8")
  internal static let img9 = ImageAsset(name: "img_9")
  internal static let pagerFour = ImageAsset(name: "pager_four")
  internal static let pagerOne = ImageAsset(name: "pager_one")
  internal static let pagerThree = ImageAsset(name: "pager_three")
  internal static let pagerTwo = ImageAsset(name: "pager_two")
  internal static let playVideoIcon = ImageAsset(name: "play_video_icon")
  internal static let rosace = ImageAsset(name: "Rosace")
  internal static let screen = ImageAsset(name: "Screen")
  internal static let cards = ImageAsset(name: "Cards")
  internal static let gift = ImageAsset(name: "Gift")
  internal static let home = ImageAsset(name: "Home")
  internal static let order = ImageAsset(name: "Order")
  internal static let stores = ImageAsset(name: "Stores")
  internal static let back = ImageAsset(name: "back")
  internal static let bright = ImageAsset(name: "bright")
  internal static let chapter = ImageAsset(name: "chapter")
  internal static let compass = ImageAsset(name: "compass")
  internal static let delete = ImageAsset(name: "delete")
  internal static let diniIcon = ImageAsset(name: "dini_icon")
  internal static let diniLogo = ImageAsset(name: "dini_logo_")
  internal static let down = ImageAsset(name: "down")
  internal static let downloadCircularButton = ImageAsset(name: "download-circular-button")
  internal static let editVideo = ImageAsset(name: "edit-video")
  internal static let hdSign = ImageAsset(name: "hd-sign")
  internal static let iconDiniNew = ImageAsset(name: "icon_dini_new")
  internal static let iconeSourate = ImageAsset(name: "icone-Sourate")
  internal static let iconeCoran = ImageAsset(name: "icone-coran")
  internal static let iconeEcran = ImageAsset(name: "icone-ecran")
  internal static let iconeMosquee = ImageAsset(name: "icone-mosquee")
  internal static let iconeQibla = ImageAsset(name: "icone-qibla")
  internal static let iconeTraduction = ImageAsset(name: "icone-traduction")
  internal static let backward10 = ImageAsset(name: "backward10")
  internal static let forward10 = ImageAsset(name: "forward10")
  internal static let pauseCircle = ImageAsset(name: "pause-circle")
  internal static let play = ImageAsset(name: "play")
  internal static let info = ImageAsset(name: "info")
  internal static let information = ImageAsset(name: "information")
  internal static let informationB = ImageAsset(name: "informationB")
  internal static let kaaba = ImageAsset(name: "kaaba")
  internal static let like = ImageAsset(name: "like")
  internal static let lock = ImageAsset(name: "lock")
  internal static let logo = ImageAsset(name: "logo")
  internal static let mainLogo = ImageAsset(name: "main_logo")
  internal static let menu = ImageAsset(name: "menu")
  internal static let menuside = ImageAsset(name: "menuside")
  internal static let moreBB = ImageAsset(name: "moreBB")
  internal static let mosque = ImageAsset(name: "mosque")
  internal static let movie = ImageAsset(name: "movie")
  internal static let netflix = ImageAsset(name: "netflix")
  internal static let netflixProfilImage = ImageAsset(name: "netflix_profil_image")
  internal static let next = ImageAsset(name: "next")
  internal static let paperPlane = ImageAsset(name: "paper-plane")
  internal static let pause = ImageAsset(name: "pause")
  internal static let pictoBackward10 = ImageAsset(name: "picto_backward_10")
  internal static let pictoClose = ImageAsset(name: "picto_close")
  internal static let pictoForward10 = ImageAsset(name: "picto_forward_10")
  internal static let pictoPause = ImageAsset(name: "picto_pause")
  internal static let pictoPlay = ImageAsset(name: "picto_play")
  internal static let placeholderImage = ImageAsset(name: "placeholder-image")
  internal static let playButtonCircle = ImageAsset(name: "play-button-circle")
  internal static let playButtonFill = ImageAsset(name: "play-button-fill")
  internal static let plus = ImageAsset(name: "plus")
  internal static let posterTest = ImageAsset(name: "poster test")
  internal static let quran = ImageAsset(name: "quran")
  internal static let radioOff = ImageAsset(name: "radio-off")
  internal static let radioOn = ImageAsset(name: "radio-on")
  internal static let separator = ImageAsset(name: "separator")
  internal static let settings = ImageAsset(name: "settings")
  internal static let skip = ImageAsset(name: "skip")
  internal static let television = ImageAsset(name: "television")
  internal static let thumb = ImageAsset(name: "thumb")
  internal static let tick = ImageAsset(name: "tick")
  internal static let unlock = ImageAsset(name: "unlock")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
