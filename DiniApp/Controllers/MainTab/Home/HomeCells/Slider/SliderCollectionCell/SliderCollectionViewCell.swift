//
//  SliderCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable

class SliderCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var addFavButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var infosButton: UIButton!
    
    weak var delegate: HomeCellDelegate?
    
    lazy var topGradient = CAGradientLayer()
    lazy var bottomGradient = CAGradientLayer()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addFavButton.alignImageAndTitleVertically()
        playButton.layer.cornerRadius = 6
        infosButton.alignImageAndTitleVertically()
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.updateFavBtn(isFav: false)
    }
    private func addTransparentView() {
        topGradient.removeFromSuperlayer()
        bottomGradient.removeFromSuperlayer()
        
        topGradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        let topGradientFrame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 100)
        topGradient.frame = topGradientFrame
        mainView.layer.addSublayer(topGradient)

        bottomGradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        let y = self.frame.height - 150
        let bottomGradientFrame = CGRect(x: self.frame.origin.x, y: y, width: self.frame.width, height: 150)
        bottomGradient.frame = bottomGradientFrame
        
        mainView.layer.addSublayer(bottomGradient)
    }
    
    func setup(item: CollectionListData, infoState: [CollectionInfoState]) {
        addTransparentView()
        
        var sliderImageURL = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                                     path2: item.portraitPicture)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            sliderImageURL = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                  path2: item.sliderPicture)
        }
        if let url = sliderImageURL {
            self.posterImageView.kf.setImage(with: url)
        } else {
            self.posterImageView.image = UIImage(named: "ItemEmpty")
        }

        if let tags = item.tags {
            let tagsNames = tags.compactMap{ $0.name }.joined(separator: " • ")
            self.tagsLabel.text = tagsNames
        }
        let isFav = infoState.contains(.fav)
        self.updateFavBtn(isFav: isFav)
    }
    
    func updateFavBtn(isFav: Bool) {

        let favBtnColor: UIColor = isFav ? .primary : .white
        let favBtnImage = isFav ? Asset.tick.image.withRenderingMode(.alwaysTemplate) : Asset.plus.image.withRenderingMode(.alwaysTemplate)
        self.addFavButton.tintColor = favBtnColor
        self.addFavButton.setTitleColor(favBtnColor, for: .normal)
        self.addFavButton.setImage(favBtnImage, for: .normal)

    }
    
    @IBAction func didTapInfos(_ sender: UIButton) {
        delegate?.itemTapped(verticalIndex: 0, horizontalIndex: 0)
    }
}
