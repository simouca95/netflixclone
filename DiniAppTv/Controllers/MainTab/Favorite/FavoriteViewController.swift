//
//  FavoriteViewController.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import UIKit
import RxSwift
import SVGKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profileImageView: UIImageView!

//    @IBOutlet weak var headerBackground: UIView!
//    @IBOutlet weak var profileButton: UIButton!
    private lazy var spinnerView = SpinnerViewController()

    var viewModel: FavoriteViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = FavoriteViewModel()
        collectionView.remembersLastFocusedIndexPath = true
        
        UserDefaults.standard.removeObject(forKey: Constant.HomeSectionToRemember)

        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            }
        }
        
        initViewModels()
    }
    
    func initViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: SimpleCollectionViewCell.self)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        collectionView.setCollectionViewLayout(layout, animated: false)
        
        
//        headerBackground.alpha = 0.8
        
//        if let baseURL = DefaultsHelper.profilesImagesBaseURL,
//           let pitcture = DefaultsHelper.profilImage,
//           let svg = URL(string: "\(baseURL)/\(pitcture)"),
//           let data = try? Data(contentsOf: svg) {
//            let receivedimage: SVGKImage = SVGKImage(data: data)
//
//            profileButton.setImage(receivedimage.uiImage, for: .normal)
//        }
    }
    
    func initViewModels() {
        viewModel.getCollectionList()
        viewModel.reloadData.asObservable()
            .subscribe (onNext:{ [weak self] value in
                guard let self = self else { return }
                self.collectionView.reloadData()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] bool in
                guard let self = self else { return }
                if bool {
                    self.addChild(self.spinnerView)
                    self.spinnerView.view.frame = self.view.frame
                    self.view.addSubview(self.spinnerView.view)
                    self.spinnerView.didMove(toParent: self)
                } else {
                    self.spinnerView.willMove(toParent: nil)
                    self.spinnerView.view.removeFromSuperview()
                    self.spinnerView.removeFromParent()
                }
            })
            .disposed(by: disposeBag)
    }
}

extension FavoriteViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SimpleCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.setupContent(item: viewModel.items[indexPath.row])
        return cell
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        return IndexPath(row: 0, section: 0)
    }
}

extension FavoriteViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (collectionView.frame.width-(10*5))/6
        let height = width*1.477
        return CGSize(width: width, height: height)
    }
}

extension FavoriteViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let id = viewModel.items[indexPath.row].id else { return }
        let vc = StoryboardScene.Details.detailsViewController.instantiate()
        vc.viewModel = DetailsViewModel(collectionId: id, infoState: [])
        navigationController?.pushViewController(vc, animated: true)
    }
}
