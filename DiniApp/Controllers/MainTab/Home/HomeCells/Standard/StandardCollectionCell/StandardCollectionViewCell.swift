//
//  StandardCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable

class StandardCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var newCollectionImageView: UIImageView!
    @IBOutlet weak var newResourceImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        newCollectionImageView.isHidden = false
        newResourceImageView.isHidden = false
        dateLabel.isHidden = true
    }

    func initView(using diniResource: CollectionListData) {
        newCollectionImageView.kf.setImage(with: URL(string: DefaultsHelper.newCollectionPicture ?? ""))
        newResourceImageView.kf.setImage(with: URL(string: DefaultsHelper.newResourcePicturePath ?? ""))
        let isNewCollection = diniResource.isNewCollection ?? false
        let isNewResourceIntoThisCollection = diniResource.isNewResourcesIntoThisCollection ?? false
        newCollectionImageView.isHidden = !isNewCollection
        newResourceImageView.isHidden = !isNewResourceIntoThisCollection
        if isNewCollection && isNewResourceIntoThisCollection {
            newResourceImageView.isHidden = true
        }
    }
    
    ///for reels
    func initView(using resource: DiniResource) {
        newCollectionImageView.isHidden = true
        newResourceImageView.isHidden = true
        dateLabel.isHidden = false
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from:resource.publishedAt ?? "")
        dateLabel.text = date?.timeAgoDisplay()
    }
}

extension Date {
    func timeAgoDisplay() -> String {
 
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "Il y a \(diff) secondes"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "Il y a \(diff) minutes"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "Il y a \(diff) heures"
        }
        let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
        return "Il y a \(diff) jours"
    }
}
