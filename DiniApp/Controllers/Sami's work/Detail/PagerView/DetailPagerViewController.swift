//
//  DetailPagerViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/29/20.
//

import UIKit
import XLPagerTabStrip


class DetailPagerViewController : ButtonBarPagerTabStripViewController{

    var parentDelegate : UpdateScrollView!
    

    override func viewDidLoad() {
        
        setupPager()

        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = StoryboardScene.Sami.episodesViewController.instantiate()
        child_1.parentDelegate = self.parentDelegate
        
        let child_2 = StoryboardScene.Sami.moreLikeViewController.instantiate()
        child_2.parentDelegate = self.parentDelegate

        let child_3 = StoryboardScene.Sami.trailerViewController.instantiate()
        child_3.parentDelegate = self.parentDelegate

        return [child_1, child_2,child_3]
    }

    override func moveToViewController(at index: Int, animated: Bool = true) {
        super.moveToViewController(at: index, animated: false)
    }
    
    override func moveTo(viewController: UIViewController, animated: Bool = true) {
        super.moveTo(viewController: viewController, animated: false)
        
    }
    
}

// MARK: XLPagerTabStrip methods
extension DetailPagerViewController{
    
    
    fileprivate func setupPager() {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .black
        settings.style.buttonBarItemBackgroundColor = .black
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .systemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .red
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .lightGray
            newCell?.label.textColor = .white
        }
        
    }
    

}
