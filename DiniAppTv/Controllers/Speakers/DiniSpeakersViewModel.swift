//
//  DiniSpeakersViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 09/02/2022.
//

import RxSwift

class DiniSpeakersViewModel: BaseViewModel {
    
    var cellData: CollectionListData?
    var speakerId: Int
    var reloadData = BehaviorSubject<Void>(value: ())
    
    
    var speaker: CollectionsListSpeaker?
    var resources: [DiniSlider] = []
    
    init(speakerId: Int) {
        self.speakerId = speakerId
    }
    
    func updateItems(speaker: CollectionsListSpeaker, resources: [DiniSlider]) {
        self.speaker = speaker
        self.resources = resources
        reloadData.onNext(())
    }
    
    func getSpeakerCollectionWatch() {
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }
        isLoading.accept(true)
        ApiCall.Query.getSpeakerCollectionList(token: token, profileid: profileId, id: speakerId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DiniSpeakersViewModel {
    private func onSuccess(_ response: QueryCollectionResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        if let speaker = response.collectionsInfos?.speakerList?.first?.filter({ $0.id == speakerId }).first,
           let resources = response.collectionsList?.slider?.first {
            updateItems(speaker: speaker, resources: resources)
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
