//
//  ProfileCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 1/19/21.
//

import UIKit
import SVGKit
import Reusable

class ProfileCollectionViewCell: UICollectionViewCell, NibReusable {
    
//    @IBOutlet weak var editProfileView: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileAvatar: UIImageView!
    
    var viewModel: ProfileViewModel! {
        didSet {
            updateView(name: viewModel.profil.name, pictureData: viewModel.getPictureData())
            updateState(isSelected: viewModel.isSelected)
            updateState(isEdit: viewModel.isEdit)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            profileName.font = UIFont.boldSystemFont(ofSize: 30)
            profileName.textColor = .gray
            self.profileAvatar.layer.cornerRadius = 45
            self.profileAvatar.layer.borderWidth = 10
            self.profileAvatar.layer.borderColor = UIColor.gray.cgColor
        } else {
            profileName.font = UIFont.systemFont(ofSize: 30)
            profileName.textColor = .white
            self.profileAvatar.layer.borderWidth = 0
            self.profileAvatar.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func updateView(name: String?, pictureData: Data?) {
        if let data = pictureData {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileAvatar.image = receivedimage.uiImage
        }
        
        profileName.text = name
    }
    
    func updateState(isSelected: Bool) {
//        if isSelected {
//            profileAvatar.layer.cornerRadius = 20
//            profileAvatar.layer.borderWidth = 4
//            profileAvatar.layer.borderColor = UIColor.white.cgColor
//            profileName.font = UIFont.boldSystemFont(ofSize: 30)
//        } else {
//            profileAvatar.layer.borderWidth = 0
//            profileAvatar.layer.borderColor = UIColor.clear.cgColor
//            profileName.font = UIFont.systemFont(ofSize: 30)
//        }
    }
    
    func updateState(isEdit: Bool) {
//        editProfileView.isHidden = !isEdit
    }
}
