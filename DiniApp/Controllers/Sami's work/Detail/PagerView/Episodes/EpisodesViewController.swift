//
//  EpisodesViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/28/20.
//

import UIKit

import XLPagerTabStrip

class EpisodesViewController: UIViewController {

    @IBOutlet weak var episodesTableView: UITableView!

    
    var episodes = DataMock.instance.videos0
    
    var parentDelegate : UpdateScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initView()
    }
    
    override func viewDidLayoutSubviews() {
        self.parentDelegate.updateScrollViewHeight(height: self.episodesTableView.contentSize.height + 50)

    }
    
    func initView()  {
        self.registerTableCells()
    }
    
    func registerTableCells() {
        self.episodesTableView.register(UINib(nibName: "EpisodeTableViewCell", bundle: nil), forCellReuseIdentifier: EpisodeTableViewCell.identifier)
        
        self.episodesTableView.delegate = self
        self.episodesTableView.dataSource = self
    }
}

extension EpisodesViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 170
    }
    
}

extension EpisodesViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.episodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: EpisodeTableViewCell.identifier, for: indexPath) as! EpisodeTableViewCell
        cell.setupContent(video: self.episodes[indexPath.row])
        return cell
    }
    
    
}

extension EpisodesViewController : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title:"EpisodesViewController")
        
    }
    
    
}
