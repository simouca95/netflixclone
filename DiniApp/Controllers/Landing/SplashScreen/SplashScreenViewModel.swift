//
//  SplashScreenViewModel.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import Foundation
import RxRelay
import RxSwift

class SplashScreenViewModel : BaseViewModel{
    
    var images:[String] = []
    var isNewVersionRelay = BehaviorRelay<Bool?>(value: nil)
    
    // MARK: Public Method
    
    func getLogoAndWalkthroughImages() {
        super.isLoading.accept(true)
        ApiCall.Auth.getLogoAndWalkthroughImages(onSuccess: self.onSuccess, onError: self.onFail)
    }

}

extension SplashScreenViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: LogiAndWalkthroughResponse?) {
        if let response = response, let responseImages = response.images {
            images = responseImages.flatMap{ $0 }
        }
        
        if let response = response, let logo = response.logo?[safe: 0]?.first {
            DefaultsHelper.saveLogo(logo)
        }
        if let response = response, let logo = response.logo?[safe: 1]?.first {
            DefaultsHelper.saveChildLogo(logo)
        }
        self.isLoading.accept(false)
        
        if images.count > 0, DefaultsHelper.isWalkthrought ?? true {
            NavigationHelper.shared.startAppFromWalkthrought(images: images)
        } else if let _ = DefaultsHelper.userId,
                  let token = DefaultsHelper.userToken, token.count > 0,
                  let _ = DefaultsHelper.profilId {
                NavigationHelper.shared.startAppFromMainTab()
        } else if let _ = DefaultsHelper.userId,
                  let token = DefaultsHelper.userToken, token.count > 0 {
            NavigationHelper.shared.startAppFromProfiles()
        } else {
            NavigationHelper.shared.startAppFromLogin()
        }
       
    }
    
    private func onFail(_ error: DiniError?) {
        self.isLoading.accept(false)
    }
}
