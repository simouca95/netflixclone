//
//  ATCWalkthroughModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/09/2021.
//

public class ATCWalkthroughModel {
    public var title: String
    public var subtitle: String
    public var icon: String

    public init(title: String, subtitle: String, icon: String) {
        self.title = title
        self.subtitle = subtitle
        self.icon = icon
    }
}
