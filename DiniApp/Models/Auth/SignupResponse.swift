//
//  SignupResponse.swift
//  netflixCloneApp
//
//  Created by sami hazel on 13/03/2022.
//

import Foundation

// MARK: - User
public struct SignupResponse: Codable {
    public let message: String?
    public let status: Bool?


    enum CodingKeys: String, CodingKey {
        case status
        case message
    }
}
