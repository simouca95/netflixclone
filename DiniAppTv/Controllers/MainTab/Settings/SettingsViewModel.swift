//
//  SettingsViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import RxSwift
import RxCocoa

class SettingsViewModel: BaseViewModel {
    
    var reloadData = BehaviorSubject<Void>(value: ())
    var profils: [Profil] = []
    var pathsList: [[String]] = []
    var profilsPathList: [(id: String, path: String?)] = []
    var isEditingProfiles: BehaviorRelay<Bool> = BehaviorRelay(value: false)

    
    func userInfos() {
        if let id = DefaultsHelper.userId,
           let token = DefaultsHelper.userToken {
            isLoading.accept(true)
            ApiCall.Auth.userInfos(token: token, id: id, onSuccess: self.onSuccess, onError: self.onFail)
        }
    }
}

extension SettingsViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: UserResponse?) {
        guard let response = response, let profils = response.profils else {
            isLoading.accept(false)
            return }
        
        self.profils = profils
        reloadData.onNext(())
        isLoading.accept(false)
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
