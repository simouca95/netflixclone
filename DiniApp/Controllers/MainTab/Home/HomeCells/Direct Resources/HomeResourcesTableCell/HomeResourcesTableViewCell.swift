//
//  HomeResourcesTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 11/01/2022.
//

import UIKit
import Reusable

class HomeResourcesTableViewCell: UITableViewCell, NibReusable {

    var section : Section!
    var vIndex: Int? = nil
    var delegate: HomeCellDelegate? = nil


    @IBOutlet weak var collectionView: UICollectionView!

    var viewModel: HomeResourcesTableViewModel!
    var itemsViewedsInfos: [CollectionViewedsInfo] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        initViews()
        initViewModel()
    }
    
    func initViews() {
        self.collectionView.register(cellType: HomeResourcesCollectionCell.self)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.showsHorizontalScrollIndicator = false
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 6
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

        collectionView.setCollectionViewLayout(layout, animated: false)
    }

    func initViewModel() {
        viewModel = HomeResourcesTableViewModel()
    }
    
    func setupContent(data : [DiniResource])  {
        viewModel.sliders = data
        self.collectionView.reloadData()
    }
}
extension HomeResourcesTableViewCell : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            let height = collectionView.frame.height
            let width = height * 0.677
            return CGSize(width: width, height: height)
        }
        let width = collectionView.frame.width * 0.27
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
}

extension HomeResourcesTableViewCell : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = viewModel.sliders[indexPath.row]
        let cell: HomeResourcesCollectionCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.vIndex = vIndex
        cell.hIndex = indexPath.row
        cell.delegate = delegate
        cell.setupContent(item: item)
        return cell
    }
}
