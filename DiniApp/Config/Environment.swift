//
//  Environment.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/03/2021.
//

import UIKit

enum Environment {
    
    // MARK: Api
    
    enum Api {

//        static let baseURL: String = fromInfo(forKey: "API_BASE_URL")
//        static let version = "app_dev.php/api/v1"

        #if DEVELOPMENT
        static let basePath: String = "https://preprod.tvdini.fr/api"
        #else
        static let basePath: String = "https://dini.tv/api"
        #endif
        static let vimeoPath: String = "https://api.vimeo.com/videos"
        static let prayPath: String = "https://api.pray.zone/v2/times/today.json"
        
#if DEVELOPMENT
        static let signupBasePath: String = "https://preprod.tvdini.fr"
#else
        static let signupBasePath: String = "https://dini.tv"
#endif

        static let signupToken: String = "jEn9pjkuUYGH6PDBUKDjd7PUZx3lb99Q878olv0LYOzEeB92UsntBuUYGo4k"
    }
    
    enum Bundles {
        
        static var MainBundle: Bundle {
            var bundle = Bundle.main
            if bundle.bundleURL.pathExtension == "appex" {
                // Peel off two directory levels - MY_APP.app/PlugIns/MY_APP_EXTENSION.appex
                let url = bundle.bundleURL.deletingLastPathComponent().deletingLastPathComponent()
                if let otherBundle = Bundle(url: url) {
                    bundle = otherBundle
                }
            }
            return bundle
        }
    }
}

private func fromInfo(forKey key: String) -> String {
    return (Environment.Bundles.MainBundle.object(forInfoDictionaryKey: key) as? String ?? "")
}
