//
//  ProfileCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by sami hazel on 1/19/21.
//

import UIKit
import SVGKit
import Reusable

class ProfileCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var editProfileView: UIView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileAvatar: UIImageView!
    
    var viewModel: ProfileViewModel! {
        didSet {
            updateView(name: viewModel.profil.name,
                       pictureData: viewModel.getPictureData(),
                       imagePng: viewModel.profil.avatarPicturePath ?? "")
            updateState(isSelected: viewModel.isSelected)
            updateState(isEdit: viewModel.isEdit)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateView(name: String?, pictureData: Data?, imagePng: String) {
        if let data = pictureData, UIImage(data: data) == nil {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileAvatar.image = receivedimage.uiImage
        } else {
            profileAvatar.kf.setImage(with: URL(string: imagePng))
        }
        
        profileName.text = name
    }
    
    func updateState(isSelected: Bool) {
        if isSelected {
            profileAvatar.layer.cornerRadius = 20
            profileAvatar.layer.borderWidth = 4
            profileAvatar.layer.borderColor = UIColor.white.cgColor
            profileName.font = UIFont.boldSystemFont(ofSize: 20)
        } else {
            profileAvatar.layer.borderWidth = 0
            profileAvatar.layer.borderColor = UIColor.clear.cgColor
            profileName.font = UIFont.systemFont(ofSize: 20)
        }
    }
    
    func updateState(isEdit: Bool) {
        editProfileView.isHidden = !isEdit
    }
}
