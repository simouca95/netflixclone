//
//  CollectionResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import Foundation

// MARK: - CollectionResponse
public struct CollectionResponse: Codable {
    public let collectionList: [CollectionList]?
    public let collectionInfos: [CollectionInfo]?
    public let collectionViewedsInfos: [CollectionViewedsInfo]?
    public let profileTypeInfos: ProfileTypeInfos?
    public let usersInfos: UserInfos?
}

// MARK: - CollectionInfo
public struct CollectionInfo: Codable {
//    let order: Int?
//    let title: String?
//    let name: String?
    public let data: [Int]?
}

// MARK: - CollectionList
public struct CollectionList: Codable {
    public let order: Float?
    public let name: String?
    public let type: String?
    public let title: String?
    public let collectionData: [CollectionListData]?
    public let speakerData: [CollectionListData]?
}

// MARK: - CollectionListDatum
public struct CollectionListData: Codable {
    public let id: Int?
    public let title: String?
//    let summary: String?
//    let tagline: String?
    let trailer: String?
    public let picture: String?
//    let picture2, picture3: JSONNull?
//    let background: String?
//    let isInFront: Bool?
    public let category: Category?
    public let tags: [Category]?
    public let resources: [DiniResource]?
    public let type: Int?
    public let seasons: [Season]?
    public let enabled, isSafe: Bool?
    public let sliderPicture: String?
    public let portraitPicture: String?
    public let datumDescription: String?
    public let name: String?
    public let isNewCollection: Bool?
    public let isNewResourcesIntoThisCollection: Bool?
//    let keywords: JSONNull?
    
    enum CodingKeys: String, CodingKey {
        case id, title, picture, category, tags, resources, type,
             seasons, enabled, isSafe, sliderPicture, name, portraitPicture, trailer, isNewCollection,isNewResourcesIntoThisCollection
        case datumDescription = "description"
    }
}

// MARK: - Category
public struct Category: Codable {
    public let id: Int?
    public let name: String?
}

// MARK: - DiniResource
public struct DiniResource: Codable {
    public let id: Int?
    public let title: String?
//    let summary: String?
    public let media: String?
    public let duration: Int?
    public let picture: String?
    public let speakers: [SpeakerElement]?
    public let portraitPicture: String?
//    let ordering: Int?
//    let collection: String?
    public let enabled: Bool?
    public let publishedAt: String?
    public let timeCodeFromViewed: Int?
//    let videoLinkForLive: String?
    public let webPathToLive: String?
    public let startAtForLive: String?
    public let subTitle: String?
    let resourceSubtitles: [ResourceSubtitle]?
    public let pictureByDefaultUrl: String?
    public let isNewResource: Bool?
}

// MARK: - ResourceSubtitle
struct ResourceSubtitle: Codable {
    let id: Int?
    let resource, title, languageCode, languageName: String?
    let path: String?
//    let subtitleFile: JSONNull?
//    let createdAt: Date?
//    let updatedAt: JSONNull?
    let completePath: String?
}

// MARK: - SpeakerElement
public struct SpeakerElement: Codable {
    public let id: Int?
    public let name: String?
    public let title: String?
    public let picture, datumDescription: String?

    enum CodingKeys: String, CodingKey {
        case id, name, title, picture
        case datumDescription = "description"
    }
}

// MARK: - JSONAnySeason
public struct Season: Codable {
    public let id: Int?
    public let name: String?
    public let ordering: Int?
    public let collection: String?
    public let picture: String?
    public let portraitPicture: String?
    public let resources: [DiniResource]?
}

// MARK: - CollectionViewedsInfo
public struct CollectionViewedsInfo: Codable {
    public let collectionID, percentProgression: Int?

    enum CodingKeys: String, CodingKey {
        case collectionID = "collection_id"
        case percentProgression
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}

public struct SwitchProfileResponse: Codable {
    public let status: Bool?
    public let profileTypeBeforeSwitch: String?
    public let profileTypeAfterSwitch: String?
    public let nameToDisplayInSwitchProfileMenu: String?
    public let logoToDisplay: String?
}
//MARK: -ProfileTypeInfos
public struct ProfileTypeInfos: Codable {
    public let isSwitchProfileVisible: Bool?
    public let nameToDisplayInSwitchProfile: String?
    
}
