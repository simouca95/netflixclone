//
//  CatalogViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/10/2021.
//

import Foundation
import RxCocoa
import RxSwift

class CatalogViewModel: BaseViewModel {
    
    var items: [DiniSlider] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    var collectionInfos: CollectionsInfos? = nil

    func getCollectionList() {
        guard let id = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Catalog.getCatalogs(token: token, profileId: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getCollectionInfoState(collectionId: Int) -> [CollectionInfoState] {
        var infos:[CollectionInfoState] = []
        
        if let favData = collectionInfos?.collectionIDFavorites?.first, favData.contains(collectionId) {
                infos.append(.fav)
        }
        
        if let likedData = collectionInfos?.collectionIDRatingLike?.first, likedData.contains(collectionId) {
                infos.append(.liked)
        }
        
        if let favData = collectionInfos?.collectionIDRatingUnLike?.first, favData.contains(collectionId) {
                infos.append(.disliked)
        }
        
        return infos
    }
}

extension CatalogViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CatalogsResponse?) {
        guard let response = response,
              let slider = response.collectionsList?.slider?.first,
              let collectionInfos = response.collectionsInfos
        else { return }
        
        self.collectionInfos = collectionInfos

        items.removeAll()
        items.append(contentsOf: slider)
        reloadData.onNext(())
        isLoading.accept(false)
    }

    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
