//
//  Network+Url.swift
//  PlaceStation
//
//  Created by Rached Khoudi on 17/10/2020.
//

import Alamofire

protocol NetworkUrl: URLRequestConvertible {
    // MARK: URLRequestConvertible
    
    var method: HTTPMethod { get }
    var path: String { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders { get }
}


extension NetworkUrl {
    
    func asURLRequest() throws -> URLRequest {
        //let urlString = Environment.Api.baseURL + Environment.Api.version + self.path
        let urlString = Environment.Api.basePath + self.path
        guard
            let url = URL(string: urlString),
            let urlRequest = try? URLRequest(url: url, method: self.method, headers: self.headers),
            let encodedURLRequest = try? self.encoding.encode(urlRequest, with: self.parameters)
            else {
                throw Network.Error.encodingError
        }
        return encodedURLRequest
    }
    
    func asURLVimeoRequest() throws -> URLRequest {
        //let urlString = Environment.Api.baseURL + Environment.Api.version + self.path
        let urlString = Environment.Api.vimeoPath + self.path
        guard
            let url = URL(string: urlString),
            let urlRequest = try? URLRequest(url: url, method: self.method, headers: self.headers),
            let encodedURLRequest = try? self.encoding.encode(urlRequest, with: self.parameters)
            else {
                throw Network.Error.encodingError
        }
        return encodedURLRequest
    }
    
    func asURLPrayRequest() throws -> URLRequest {
        let urlString = "https://api.pray.zone/v2/times/today.json" + self.path
        guard
            let url = URL(string: urlString),
            let urlRequest = try? URLRequest(url: url, method: self.method, headers: self.headers),
            let encodedURLRequest = try? self.encoding.encode(urlRequest, with: self.parameters)
            else {
                throw Network.Error.encodingError
        }
        return encodedURLRequest
    }
    
    func asURLSurahsRequest() throws -> URLRequest {
        let urlString = "http://api.alquran.cloud/v1/surah" + self.path
        guard
            let url = URL(string: urlString),
            let urlRequest = try? URLRequest(url: url, method: self.method, headers: self.headers),
            let encodedURLRequest = try? self.encoding.encode(urlRequest, with: self.parameters)
            else {
                throw Network.Error.encodingError
        }
        return encodedURLRequest
    }
}
