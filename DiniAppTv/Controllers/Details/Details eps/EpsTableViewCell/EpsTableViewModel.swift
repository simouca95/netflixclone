//
//  EpsTableViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 22/04/2022.
//

import UIKit

class EpsTableViewModel: BaseCellViewModel {
    
    var title: String?
    var posterPath: String?
    var duration: Int?
    var progress: Float?
    var media: String?
    
    var durationInMinutes = 0
    
    var isOffline: Bool = false
//    var delegate: ResourcesDelegate?
//    var download: Download
//    var media: String

    init(cellHeight: CGFloat, title: String?,
         posterPath: String?, duration: Int?,
         progress: Int?, media: String?, isOffline: Bool? = nil) {
        
        self.title = title
        self.posterPath = posterPath
        self.duration = duration
        self.media = media

        self.isOffline = isOffline ?? false

        if let d = duration {
            self.durationInMinutes = d/60
            
            if let p = progress {
                self.progress = Float(p)/Float(d)
            }
        }
        
        super.init(cellHeight: cellHeight)
    }
}

