//
//  DetailsAudioViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 17/12/2021.
//

import Foundation

class DetailsAudioViewModel: BaseViewModel {
    
    weak var delegate: AVPlayerDelegate?

    func timeCode(resourceId: Int, timeCode: Int) {
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken
        else { return }
        ApiCall.Video.timeCode(token: token, resourceId: resourceId, profileId: profileId, timeCode: timeCode, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DetailsAudioViewModel {
    private func onSuccess(_ response: TimeCodeResponse?) {
        guard let _ = response else { return }
        delegate?.onSuccessTimeCodeSend()
    }
    
    private func onFail(_ error: DiniError?) {
        delegate?.onFailTimeCodeSend()
    }
}

