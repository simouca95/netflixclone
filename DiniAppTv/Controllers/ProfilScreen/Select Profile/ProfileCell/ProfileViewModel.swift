//
//  ProfileViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 04/12/2021.
//

import Foundation

class ProfileViewModel {
    
    var profil: Profil
    var isSelected: Bool
    var isEdit: Bool
    
    init(profil: Profil, isEdit: Bool) {
        self.profil = profil
        self.isEdit = isEdit
        self.isSelected = DefaultsHelper.profilId == profil.id
    }
    
    func getPictureData() -> Data? {
        if let baseURL = DefaultsHelper.profilesImagesBaseURL,
           let pitcture = profil.picture,
           let svg = URL(string: "\(baseURL)/\(pitcture)"),
           let data = try? Data(contentsOf: svg) {
            return data
        }
        return nil
    }
    
}
