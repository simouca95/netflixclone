//
//  LoginViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

import Foundation
import RxSwift
import RxCocoa

class LoginViewModel: BaseViewModel {
    
    // MARK: Private Properties
    
    weak private var taskNavigator: LoginTaskNavigator!
    
    // MARK: Init Method

    init(taskNavigator : LoginTaskNavigator) {
        self.taskNavigator = taskNavigator
    }
    
    // MARK: Public Method

    func login(email: String, password: String, deviceId: String) {
        self.isLoading.accept(true)
        ApiCall.Auth.login(email: email, password: password, deviceId: deviceId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension LoginViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: LoginResponse?) {
        self.isLoading.accept(false)
        if let response = response {
            if let _ = response.code, let _ = response.message {
                ToastHelper.show(text: "Email ou mot de passe invalide")
                return
            } else if let token = response.token,
                      let vimeoToken = response.vimeoToken,
                      let id = response.userInfos?.userID,
                      let subscription = response.userInfos?.subscription, subscription.isEnabled {
                
                DefaultsHelper.saveUserToken(token)
                DefaultsHelper.saveViemoToken(vimeoToken)
                DefaultsHelper.saveUserId(id)
                
                if let appInfos = response.appInfos,
                   let dataResourcesBaseURL = appInfos.imagesPath?.imagesPath2 {
                    DefaultsHelper.saveDataResourcesBaseURL(dataResourcesBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let profileImageBaseURL = appInfos.imagesPath?.imagesPath6 {
                    DefaultsHelper.saveProfilesImagesBaseURL(profileImageBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let speakerImageBaseURL = appInfos.imagesPath?.imagesPath4 {
                    DefaultsHelper.saveSpeakerImagesBaseURL(speakerImageBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let isLikeUnLikeVisible = appInfos.preferencesParameters?.isLikeUnLikeVisible {
                    DefaultsHelper.saveIsLikeUnLikeVisible(isLikeUnLikeVisible)
                }
                
                if let maxProfilsInApplication = response.appInfos?.profils?.maxProfilsInApplication {
                    DefaultsHelper.saveMaxProfilsInApplication(maxProfilsInApplication)
                }
                
                if let maxProfilsInApplication = response.appInfos?.profils?.maxProfilsInApplication {
                    DefaultsHelper.saveMaxProfilsInApplication(maxProfilsInApplication)
                }
                
                if let currentprofilListCount = response.userInfos?.profils?.currentprofilListCount {
                    DefaultsHelper.saveCurrentprofilListCount(currentprofilListCount)
                }
                
                if let portraitPicturePath = response.appInfos?.imagesPath?.portraitPicturePath {
                    DefaultsHelper.savePortraitPicturePath(portraitPicturePath)
                }
                
                if let portraitPictureResourcesPath = response.appInfos?.imagesPath?.portraitPictureResourcesPath {
                    DefaultsHelper.savePortraitPictureResourcesPath(portraitPictureResourcesPath)
                }
                
                if let portraitPictureSeasonsPath = response.appInfos?.imagesPath?.portraitPictureSeasonsPath {
                    DefaultsHelper.savePortraitPictureSeasonsPath(portraitPictureSeasonsPath)
                }
                
                if let podcastPath = response.appInfos?.podcastPath?.podcastPath {
                    DefaultsHelper.savePodcastBaseURL(podcastPath)
                }
                
                if let trailerPath = response.appInfos?.trailerPath?.trailerPath {
                    DefaultsHelper.saveTrailerBaseURL(trailerPath)
                }
                
                if let subTitleResourcesPath = response.appInfos?.imagesPath?.subTitleResourcesPath {
                    DefaultsHelper.saveSubTitleResourcesPath(subTitleResourcesPath)
                }
                
                if let newCollectionPicture = response.appInfos?.imagesPath?.newCollectionPicture {
                    DefaultsHelper.saveNewCollectionPicture(newCollectionPicture)
                }
                
                if let newResourcePicturePath = response.appInfos?.imagesPath?.newResourcePicturePath {
                    DefaultsHelper.saveNewResourcePicturePath(newResourcePicturePath)
                }
                if let newResourcePicturePath = response.appInfos?.imagesPath?.newResourcePicturePath {
                    DefaultsHelper.saveNewResourcePicturePath(newResourcePicturePath)
                }
                if let newResourcePicturePath = response.appInfos?.imagesPath?.newResourcePicturePath {
                    DefaultsHelper.saveNewResourcePicturePath(newResourcePicturePath)
                }
                
                self.taskNavigator.goToMenuScreen()
                return
            }
        }
        
        ToastHelper.show(text: "Email ou mot de passe invalide")
    }
    
    private func onFail(_ error: DiniError?) {
        self.isLoading.accept(false)
        ToastHelper.show(text: "Email ou mot de passe invalide")
    }
}
