//
//  SimpleCollectionViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 13/02/2022.
//

import Foundation

class SimpleCollectionViewModel: BaseCellViewModel {
    
    var resource: DiniResource
    
    init(resource: DiniResource) {
        self.resource = resource
        super.init(cellHeight: 0)
    }
}
