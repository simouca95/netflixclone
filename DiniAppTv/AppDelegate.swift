//
//  AppDelegate.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 24/01/2022.
//

import UIKit
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .moviePlayback)
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
//        let window = UIWindow(frame: UIScreen.main.bounds)
//        window.rootViewController = LoginViewController()
//        self.window = window
//        window.makeKeyAndVisible()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        UserDefaults.standard.removeObject(forKey: Constant.HomeSectionToRemember)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UserDefaults.standard.removeObject(forKey: Constant.HomeSectionToRemember)
    }

}
