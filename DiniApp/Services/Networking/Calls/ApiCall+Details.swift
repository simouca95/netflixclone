//
//  ApiCall+Details.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 03/10/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Details {
//        static func getCollection(id: Int, onSuccess: @escaping (_ response: CollectionListData?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
//            do {
//                let api = API.DetailsController.getCollection(id: id)
//                let urlRequest = try api.asURLRequest()
//                
//                (Network.manager as SessionManager)
//                    .request(urlRequest)
//                    .validate()
//                    .responseJSON(completionHandler: { (response) in
//                        switch response.result {
//                        case .success:
//                            if let result: CollectionListData = ApiCall.decodeResponse(response: response) {
//                                onSuccess(result)
//                            } else {
//                                onSuccess(nil)
//                            }
//                        case .failure:
//                            onError(ApiCall.decodeError(response: response))
//                        }
//                    })
//            } catch _ {
//                onError(nil)
//            }
//        }
        
        public static func getCollectionViewsInfo(token: String, profileId: Int, id: Int, onSuccess: @escaping (_ response: CollectionListData?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.DetailsController.getCollectionViewsInfo(token: token, profileId: profileId, id: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: CollectionInfoResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result.first)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func setUnsetInfo(token: String, infoState: Int, collectionId: Int, profileId: Int, onSuccess: @escaping (_ response: SetInfoResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                
                var api: API.DetailsController?
                switch infoState {
                case 0:
                    api = API.DetailsController.setLikeState(token: token, collectionId: collectionId, profileId: profileId)
                case 1:
                    api = API.DetailsController.setDislikeState(token: token, collectionId: collectionId, profileId: profileId)
                case 2:
                    api = API.DetailsController.setFavState(token: token, collectionId: collectionId, profileId: profileId)
                default:
                    break
                }

                let classicUrlRequest = try api?.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                guard let classic = classicUrlRequest, let url = classic.url else { return }
                var urlRequest = URLRequest(url: url)
                let json = "{\"collectionId\" : \(collectionId), \"profileId\" : \(profileId)}"

                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!

                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                urlRequest.allHTTPHeaderFields = Network.secureHeaders(token: token)

                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: SetInfoResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
    }
}
