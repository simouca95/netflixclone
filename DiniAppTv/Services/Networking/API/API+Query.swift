//
//  API+Query.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 09/02/2022.
//

import Foundation
import Alamofire

extension API {
    enum QueryController {
        var controllerUrl: String { return "/collectionCatalogueSearch" }
        
        case getSpeakerCollectionList(token: String, profileId: Int, id: Int)
        case getCatalogCollectionList(token: String, profileId: Int, id: Int)
        case getTagCollectionList(token: String, profileId: Int, id: Int)
    }
}

extension API.QueryController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        default :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getSpeakerCollectionList(_,let profileId, let id):
            return self.controllerUrl + "?profileId=\(profileId)&speakerQuery=\(id)"
        case .getCatalogCollectionList(_,let profileId, let id):
            return self.controllerUrl + "?profileId=\(profileId)&categoryQuery=\(id)"
        case .getTagCollectionList(_,let profileId, let id):
            return self.controllerUrl + "?profileId=\(profileId)&tagQuery=\(id)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        default:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getSpeakerCollectionList(let token,_,_), .getCatalogCollectionList(let token,_,_), .getTagCollectionList(let token,_,_) :
            return Network.secureHeaders(token: token)
        }
    }
}
