//
//  DetailViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/27/20.
//

import UIKit

protocol UpdateScrollView{
    func updateScrollViewHeight(height : CGFloat)
}


class DetailViewController: BaseViewController {
    
    @IBOutlet weak var pagerView: UIView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var pageViewHeightConstraint: NSLayoutConstraint!
    
//    var detailPagerView = StoryboardScene.Main.detailPagerViewController.instantiate()
//    
//    var player : BMPlayer?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        configurePagerView()
//        configurePlayer()
//        
//    }
//    
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        self.player?.prepareToDealloc()
//    }
//    
//    
//    fileprivate func configurePagerView() {
//        // Do any additional setup after loading the view.
//        detailPagerView.willMove(toParent: self)
//        
//        addChild(detailPagerView)
//        
//        detailPagerView.parentDelegate = self
//        
//        detailPagerView.view.frame = CGRect(x: 0, y: 0 ,
//                                            width: pagerView.bounds.width,
//                                            height: detailPagerView.view.frame.height)
//        
//        pagerView.addSubview(detailPagerView.view)
//        
//        detailPagerView.didMove(toParent: self)
//    }
//    
//    func configurePlayer() {
//        BMPlayerConf.topBarShowInCase = .horizantalOnly
//        BMPlayerConf.shouldAutoPlay = true
//        
//        let controller = BMPlayerCustomControlView()
//
//        self.player = BMPlayer(customControlView: controller)
//        self.player?.delegate = self
//        
//        self.playerView.addSubview(player!)
//        
//        self.player!.snp.makeConstraints { (make) in
//            make.top.equalTo(playerView.snp.top)
//            make.left.equalTo(playerView.snp.left)
//            make.right.equalTo(playerView.snp.right)
//            make.height.equalTo(playerView.snp.width).multipliedBy(9.0/16.0).priority(500)
//        }
//        
//        let videoUrl = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")
//        let vttURL = URL(string: "https://bitdash-a.akamaihd.net/content/sintel/subtitles/subtitles_fr.vtt")!
//        
//        let asset = BMPlayerResource(name: "Test Test",
//                                     definitions: [BMPlayerResourceDefinition(url: videoUrl!, definition: "480p"/*, options: options*/)],
//                                     cover: nil,
//                                     subtitles: BMSubtitles(url: vttURL))
//        
//        player!.setVideo(resource: asset)
//
//    }
//    
//    deinit {
//        // If use the slide to back, remember to call this method
//        // 使用手势返回的时候，调用下面方法手动销毁
//        player!.prepareToDealloc()
//        print("VideoPlayViewController Deinit")
//    }
//    
//    
//}
//extension DetailViewController : UpdateScrollView{
//    func updateScrollViewHeight(height: CGFloat) {
//        self.pageViewHeightConstraint.constant = height + 50
//        
//    }
//}
//// MARK:- BMPlayerDelegate example
//extension DetailViewController: BMPlayerDelegate {
//    // Call when player orinet changed
//    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
////        player.snp.remakeConstraints { (make) in
////            make.top.equalTo(view.snp.top)
////            make.left.equalTo(view.snp.left)
////            make.right.equalTo(view.snp.right)
////            if isFullscreen {
////                make.bottom.equalTo(view.snp.bottom)
////            } else {
////                make.height.equalTo(view.snp.width).multipliedBy(9.0/16.0).priority(500)
////            }
////        }
//    }
//    
//    // Call back when playing state changed, use to detect is playing or not
//    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
//        //print("| BMPlayerDelegate | playerIsPlaying | playing - \(playing)")
//    }
//    
//    // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
//    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
//        //print("| BMPlayerDelegate | playerStateDidChange | state - \(state)")
//    }
//    
//    // Call back when play time change
//    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
//        //        print("| BMPlayerDelegate | playTimeDidChange | \(currentTime) of \(totalTime)")
//    }
//    
//    // Call back when the video loaded duration changed
//    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
//        //        print("| BMPlayerDelegate | loadedTimeDidChange | \(loadedDuration) of \(totalDuration)")
//    }
}
