//
//  QualityVideoViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 30/12/2021.
//

import UIKit

class QualityVideoViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: QualityVideoViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func initView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(cellType: QualityVideoTableViewCell.self)
        tableView.tintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }
    
    @IBAction func onDismissTapped(_ sender: Any) {
        dismiss(animated: true) {
            self.viewModel.delegate?.onSpecificQualityTapped(newFileIndex: self.viewModel.currentFileIndex)
        }
    }
}

extension QualityVideoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.files.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .black
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Qualité"
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = .white
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: QualityVideoTableViewCell.reuseIdentifier) as! QualityVideoTableViewCell
        cell.titleLabel.text = viewModel.files[indexPath.row].publicName
        cell.checkImageView.isHidden = indexPath.row != viewModel.currentFileIndex
        return cell
    }
}

extension QualityVideoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.currentFileIndex = indexPath.row
        tableView.reloadData()
    }
}
