//
//  ApiCall+Notif.swift
//  DiniApp
//
//  Created by Wafa Labidi on 2/7/2022.
//

import Alamofire

extension ApiCall {
    
    public struct Notif {
        
        public static func getNotificationList(token: String, onSuccess: @escaping (_ response: NotificationResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.NotificationController.getNotificationList(token: token)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: NotificationResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
    }
    
}
