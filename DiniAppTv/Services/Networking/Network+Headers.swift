//
//  Network+Headers.swift
//  PlaceStation
//
//  Created by Rached Khoudi on 17/10/2020.
//

import Alamofire

extension Network {
    
    static func secureHeaders(token: String) -> HTTPHeaders {
        var header = [ "Content-Type": "application/json" ]
        
//        if let token = DiniDefaultsHelper.userToken {
            header["Authorization"] = "Bearer \(token)"
//        }
        
        return header
    }
    
    static var unsecureHeaders: HTTPHeaders {
        let header = [ "Content-Type": "application/json" ]
        return header
    }
    
    static func vimeoHeaders(token: String) -> HTTPHeaders {
        var header = [ "Content-Type": "application/json" ]
        
//        if let token = DiniDefaultsHelper.vimeoToken {
            header["Authorization"] = "Bearer \(token)"
//        }
        
        return header
    }
}
