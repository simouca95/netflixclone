//
//  ApiCall+Video.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/09/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Video {
        
        public static func getVideoData(tokenVimeo: String, media: String, onSuccess: @escaping (_ response: VideoResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.VideoController.getVideoData(tokenVimeo: tokenVimeo, media: media)
                let urlRequest = try api.asURLVimeoRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: VideoResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func timeCode(token: String, resourceId: Int, profileId: Int, timeCode: Int, onSuccess: @escaping (_ response: TimeCodeResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.VideoController.timeCode(token: token, resourceId: resourceId, profileId: profileId, timeCode: timeCode)
                let classicUrlRequest = try api.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                var urlRequest = URLRequest(url: classicUrlRequest.url!)
                let json = "{\"resourceId\" : \(resourceId),\"profileId\" : \(profileId),\"timecode\" : \(timeCode)}"

                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!

                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                urlRequest.allHTTPHeaderFields = Network.secureHeaders(token: token)

                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: TimeCodeResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func getCollectionWatch(token: String, profileId: Int, collectionId: Int, onSuccess: @escaping (_ response: CollectionWatchResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.VideoController.getCollectionWatch(token: token, profileId: profileId, collectionId: collectionId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: CollectionWatchResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func getNumberOfUserWatching(token: String, profileId: Int, userId: Int, onSuccess: @escaping (_ response: ResourceWatchResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.VideoController.getResourceWatch(token: token, profileId: profileId, userId: userId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ResourceWatchResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }

    }
    
}
