//
//  DefaultsHelper.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import SwiftyUserDefaults

extension DefaultsKeys {
    // MARK: User DefaultsKeys
    
    //URL
    static let profilesImagesBaseURL = DefaultsKey<String?>(DefaultsHelper.profilesImagesBaseURLKey)
    static let dataResourcesBaseURL = DefaultsKey<String?>(DefaultsHelper.dataResourcesBaseURLKey)
    static let speakerImagesBaseURL = DefaultsKey<String?>(DefaultsHelper.speakerImagesBaseURLKey)
    static let podcastBaseURL = DefaultsKey<String?>(DefaultsHelper.podcastBaseURLKey)
    static let trailerBaseURL = DefaultsKey<String?>(DefaultsHelper.trailerBaseURLKey)

    //ID & Tokens
    static let userToken = DefaultsKey<String?>(DefaultsHelper.userTokenKey)
    static let userId = DefaultsKey<Int?>(DefaultsHelper.userIdKey)
    static let profilId = DefaultsKey<Int?>(DefaultsHelper.profilIdKey)
    static let vimeoToken = DefaultsKey<String?>(DefaultsHelper.vimeoTokenKey)
    
    //IS_REMEMBER
    static let isWalkthrought = DefaultsKey<Bool?>(DefaultsHelper.isWalkthroughtKey)
    static let isLikeUnLikeVisible = DefaultsKey<Bool?>(DefaultsHelper.isLikeUnLikeVisibleKey)
    static let maxProfilsInApplication = DefaultsKey<Int?>(DefaultsHelper.maxProfilsInApplicationKey)
    static let currentprofilListCount = DefaultsKey<Int?>(DefaultsHelper.currentprofilListCountKey)

    //IMAGES
    static let profilImage = DefaultsKey<String?>(DefaultsHelper.profilImageKey)
    static let logo = DefaultsKey<String?>(DefaultsHelper.logoKey)
    static let portraitPicturePath = DefaultsKey<String?>(DefaultsHelper.portraitPicturePathKey)
    static let portraitPictureResourcesPath = DefaultsKey<String?>(DefaultsHelper.portraitPictureResourcesPathKey)
    static let portraitPictureSeasonsPath = DefaultsKey<String?>(DefaultsHelper.portraitPictureSeasonsPathKey)
    static let subTitleResourcesPath = DefaultsKey<String?>(DefaultsHelper.subTitleResourcesPathKey)
}

enum DefaultsHelper {
    // MARK: Public Static Properties
    
    //URL
    static let profilesImagesBaseURLKey = "profilesImagesBaseURL"
    static let dataResourcesBaseURLKey = "dataResourcesBaseURL"
    static let speakerImagesBaseURLKey = "speakerImagesBaseURL"
    static let podcastBaseURLKey = "podcastBaseURL"
    static let trailerBaseURLKey = "trailerBaseURL"

    //ID & Tokens
    static let userTokenKey = "userToken"
    static let userIdKey = "userId"
    static let profilIdKey = "profilId"
    static let vimeoTokenKey = "vimeoToken"

    //IS_REMEMBER
    static let isWalkthroughtKey = "isWalkthrought"
    static let isLikeUnLikeVisibleKey = "isLikeUnLikeVisible"
    static let maxProfilsInApplicationKey = "maxProfilsInApplication"
    static let currentprofilListCountKey = "currentprofilListCount"

    //IMAGES
    static let profilImageKey = "profilImage"
    static let logoKey = "logo"
    static let portraitPicturePathKey = "portraitPicturePath"
    static let portraitPictureResourcesPathKey = "portraitPictureResourcesPath"
    static let portraitPictureSeasonsPathKey = "portraitPictureSeasonsPath"
    static let subTitleResourcesPathKey = "subTitleResourcesPath"
}

extension DefaultsHelper {
    //URL
    static var profilesImagesBaseURL: String? { return Defaults[.profilesImagesBaseURL] }
    static var dataResourcesBaseURL: String? { return Defaults[.dataResourcesBaseURL] }
    static var speakerImagesBaseURL: String? { return Defaults[.speakerImagesBaseURL] }
    static var podcastBaseURL: String? { return Defaults[.podcastBaseURL] }
    static var trailerBaseURL: String? { return Defaults[.trailerBaseURL] }

    //ID & Tokens
    static var userToken: String? { return Defaults[.userToken] }
    static var userId: Int? { return Defaults[.userId] }
    static var profilId: Int? { return Defaults[.profilId] }
    static var vimeoToken: String? { return Defaults[.vimeoToken] }

    //IS_REMEMBER
    static var isUserLogged: Bool { return !(DefaultsHelper.userToken?.isEmpty ?? true) }
    static var isWalkthrought: Bool? { return Defaults[.isWalkthrought] }
    static var isLikeUnLikeVisible: Bool? { return Defaults[.isLikeUnLikeVisible]}
    static var maxProfilsInApplication: Int? { return Defaults[.maxProfilsInApplication]}
    static var currentprofilListCount: Int? { return Defaults[.currentprofilListCount]}

    //IMAGES
    static var profilImage: String? { return Defaults[.profilImage] }
    static var logo: String? { return Defaults[.logo] }
    static var portraitPicturePath: String? { return Defaults[.portraitPicturePath] }
    static var portraitPictureResourcesPath: String? { return Defaults[.portraitPictureResourcesPath] }
    static var portraitPictureSeasonsPath: String? { return Defaults[.portraitPictureSeasonsPath] }
    static var subTitleResourcesPath: String? { return Defaults[.subTitleResourcesPath] }
}

extension DefaultsHelper {
    //URL
    static func saveProfilesImagesBaseURL(_ url: String) { Defaults[.profilesImagesBaseURL] = url }
    static func saveDataResourcesBaseURL(_ url: String) { Defaults[.dataResourcesBaseURL] = url }
    static func saveSpeakerImagesBaseURL(_ url: String) { Defaults[.speakerImagesBaseURL] = url }
    static func savePodcastBaseURL(_ url: String) { Defaults[.podcastBaseURL] = url }
    static func saveTrailerBaseURL(_ url: String) { Defaults[.trailerBaseURL] = url }

    //ID & Tokens
    static func saveUserToken(_ userToken: String) { Defaults[.userToken] = userToken }
    static func saveUserId(_ id: Int) { Defaults[.userId] = id }
    static func saveProfilId(_ id: Int) { Defaults[.profilId] = id }
    static func saveViemoToken(_ token: String) { Defaults[.vimeoToken] = token }

    //IS_REMEMBER
    static func saveIsWalkthrought(_ isWalkthrought: Bool)
    { Defaults[.isWalkthrought] = isWalkthrought }
    static func saveIsLikeUnLikeVisible(_ isLikeUnLikeVisible: Bool)
    { Defaults[.isLikeUnLikeVisible] = isLikeUnLikeVisible }
    static func saveMaxProfilsInApplication(_ maxProfilsInApplication: Int)
    { Defaults[.maxProfilsInApplication] = maxProfilsInApplication }
    static func saveCurrentprofilListCount(_ currentprofilListCount: Int)
    { Defaults[.currentprofilListCount] = currentprofilListCount }
    
    //IMAGES
    static func saveProfilImage(_ path: String) { Defaults[.profilImage] = path }
    static func saveLogo(_ path: String) { Defaults[.logo] = path }
    static func savePortraitPicturePath(_ path: String) { Defaults[.portraitPicturePath] = path }
    static func savePortraitPictureResourcesPath(_ path: String) { Defaults[.portraitPictureResourcesPath] = path }
    static func savePortraitPictureSeasonsPath(_ path: String) { Defaults[.portraitPictureSeasonsPath] = path }
    static func saveSubTitleResourcesPath(_ path: String) { Defaults[.subTitleResourcesPath] = path }

    //DELETE
    static func deleteAllDefaults() {
        Defaults.removeAll()
        DefaultsHelper.saveIsWalkthrought(false)
    }
}
