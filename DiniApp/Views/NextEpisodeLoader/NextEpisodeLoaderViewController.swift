//
//  NextEpisodeLoaderViewController.swift
//  DiniApp
//
//  Created by sami hazel on 31/03/2022.
//

import UIKit

protocol NextEpisodeLoaderDelegate: AnyObject {
    func didPressNext()
    func didPressCancel()
}

class NextEpisodeLoaderViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextEpButton: UIButton!
    @IBOutlet weak var cancelButtonn: UIButton!
    @IBOutlet weak var closeButtonn: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var delegate: NextEpisodeLoaderDelegate?
    private var timer: Timer?
    private var autoLoadCount = 6
    private let text = "Episode dans: "

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        nextEpButton.layer.cornerRadius = 5
        cancelButtonn.layer.cornerRadius = 5
        cancelButtonn.layer.borderColor = UIColor.white.cgColor
        cancelButtonn.layer.borderWidth = 2
        
        self.titleLabel.text = "\(text) \(autoLoadCount) secondes"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loader.startAnimating()
        self.startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.timer?.invalidate()
        self.loader.stopAnimating()
    }
    
    private func startTimer() {
        autoLoadCount -= 1
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
    }

    @objc private func updateTimerLabel() {

        var aux = text + String(autoLoadCount) + " seconde"
        if autoLoadCount > 1 {
            aux.append("s")
        }
        DispatchQueue.main.async {
            self.titleLabel.text = aux
        }
        if autoLoadCount == 0 {
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    self.delegate?.didPressNext()
                }
            }

        } else {
            autoLoadCount -= 1

        }
        

    }
    
    
    @IBAction func loadNextEpisode(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.didPressNext()
        }
    }
    
    @IBAction func cancelLoader(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.didPressCancel()
        }
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.didPressCancel()
        }
    }
}
