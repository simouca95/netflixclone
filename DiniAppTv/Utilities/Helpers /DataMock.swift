//
//  DataMock.swift
//  DiniTv
//
//  Created by Rached Khoudi on 18/02/2021.
//

import Foundation

final class Constant {
    static let HomeSectionToRemember = "HOME_SECTION"
}

final internal class DataMock {
    
    private init() {}
    
    static let instance = DataMock()
    
    static let genresAccueil = ["Action" , "Drama" , "Comedie", "Drames"]
    static let genresSeries = ["Top 5" , "Horror" , "Drames", "Comedie"]
    
    let series = [("Season 1","22 épisdoes"),
                  ("Season 2","19 épisdoes"),
                  ("Season 3","20 épisdoes"),
                  ("Season 4","23 épisdoes"),
                  ("Season 5","17 épisdoes")]
    
    let videos0 : [Video] = [Video(title: "CROWN", image:"img_1" ),
                             Video(title: "MINDHUNTER", image:"img_2" ),
                             Video(title: "DARK", image:"img_3" ),
                             Video(title: "LOCKE KEY", image:"img_4" ),
                             Video(title: "CURSED", image:"img_5" ),
                             Video(title: "THE WITCHER", image:"img_6" ),
                             Video(title: "CROWN", image:"img_1" ),
                             Video(title: "MINDHUNTER", image:"img_2" ),
                             Video(title: "DARK", image:"img_3" ),
                             Video(title: "LOCKE KEY", image:"img_4" ),
                             Video(title: "CURSED", image:"img_5" ),
                             Video(title: "THE WITCHER", image:"img_6" )]
    let videos1 : [Video] = [Video(title: "THE LETTER FOR THE KING", image:"img_7" ),
                             Video(title: "HEAVEN", image:"img_8" ),
                             Video(title: "WITCHER", image:"img_9" ),
                             Video(title: "13 REASONS WHY", image:"img_10" ),
                             Video(title: "BLACK MIRROR", image:"img_11" ),
                             Video(title: "PROTECTED", image:"img_12" ),
                             Video(title: "Test", image:"img_7" ),
                             Video(title: "Test", image:"img_8" ),
                             Video(title: "WITCHER", image:"img_9" ),
                             Video(title: "13 REASONS WHY", image:"img_10" ),
                             Video(title: "BLACK MIRROR", image:"img_11" ),
                             Video(title: "PROTECTED", image:"img_12" )]
    let videos2 : [Video] = [Video(title: "HEAVEN", image:"img_13" ),
                             Video(title: "UNFORTUNATE EVENTS", image:"img_14" ),
                             Video(title: "STRANGER THINGS", image:"img_15" ),
                             Video(title: "ANNE E", image:"img_16" ),
                             Video(title: "GILMORE GIRLS", image:"img_17" ),
                             Video(title: "TYPEWRITER", image:"img_18" ),
                             Video(title: "HEAVEN", image:"img_13" ),
                             Video(title: "UNFORTUNATE EVENTS", image:"img_14" ),
                             Video(title: "STRANGER THINGS", image:"img_15" ),
                             Video(title: "ANNE E", image:"img_16" ),
                             Video(title: "GILMORE GIRLS", image:"img_17" ),
                             Video(title: "TYPEWRITER", image:"img_18" )]
    let videos3 : [Video] = [Video(title: "LUCIFER", image:"img_19" ),
                             Video(title: "DAREDIVEL", image:"img_20" ),
                             Video(title: "SHE", image:"img_21" ),
                             Video(title: "MARIANNE", image:"img_22" ),
                             Video(title: "RATEHED", image:"img_23" ),
                             Video(title: "ATYPICAL", image:"img_24" ),
                             Video(title: "LUCIFER", image:"img_19" ),
                             Video(title: "DAREDIVEL", image:"img_20" ),
                             Video(title: "SHE", image:"img_21" ),
                             Video(title: "MARIANNE", image:"img_22" ),
                             Video(title: "RATEHED", image:"img_23" ),
                             Video(title: "ATYPICAL", image:"img_24" )]
    
    static let sectionsAccueil : [Section] =
        [Section(title: "Action", video: DataMock.instance.videos0, type: .teasers),
         Section(title: "Drama", video: DataMock.instance.videos1 ,type: .watching),
         Section(title: "Comedie", video: DataMock.instance.videos3, type: .normal),
         Section(title: "Horror", video: DataMock.instance.videos2, type: .normal)]
    
    static let sectionsSeries : [Section] =
        [Section(title: "Top 5", video: DataMock.instance.videos3, type: .normal),
         Section(title: "Comedie", video: DataMock.instance.videos2, type: .teasers),
         Section(title: "Drama", video: DataMock.instance.videos1 ,type: .watching),
         Section(title: "Action", video: DataMock.instance.videos0, type: .normal)]
    
    static let sideMenu = [("Accueil","house"),
                           ("Lecture aléatoire","shuffle"),
                           ("Les plus regardés","megaphone"),
                           ("Séries TV","tv"),
                           ("Films","video"),
                           ("Ma list","heart")]
}

struct Video {
    var title : String
    var image : String
    var type : VideoType = .movie
    
}

struct Section{
    var title : String
    var video : [Video]
    var type : SectionType = .normal
}

enum SectionType {
    case top
    case teasers
    case watching
    case normal
}
enum VideoType{
    case movie
    case serie
}
