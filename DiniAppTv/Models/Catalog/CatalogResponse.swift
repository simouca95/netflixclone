//
//  CatalogResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Foundation

// MARK: - CollectionsListCollectionResponse
public struct QueryCollectionResponse: Codable {
    public let collectionsList: CollectionsList?
    public let collectionsInfos: CollectionsInfos?
}

// MARK: - JSONAnyCollectionsListCollectionsInfos
public struct CollectionsInfos: Codable {
    public let categoryList, tagList: [[CollectionListCategoryList]]?
    public let speakerList: [[CollectionsListSpeaker]]?
    public let collectionIDFavorites, collectionIDRatingLike, collectionIDRatingUnLike: [[Int]]?

    enum CodingKeys: String, CodingKey {
        case categoryList, tagList, speakerList
        case collectionIDFavorites = "collectionId_favorites"
        case collectionIDRatingLike = "collectionId_ratingLike"
        case collectionIDRatingUnLike = "collectionId_ratingUnLike"
    }
}

// MARK: - JSONAnyCollectionsListCategoryList
public struct CollectionListCategoryList: Codable {
    public let id: Int?
    public let name: String?
}

// MARK: - JSONAnyCollectionsListSpeaker
public struct CollectionsListSpeaker: Codable {
    public let id: Int?
    public let name: String?
    public let title: String?
    public let picture: String?
    public let speakerDescription: String?

    enum CodingKeys: String, CodingKey {
        case id, name, title, picture
        case speakerDescription = "description"
    }
}

// MARK: - JSONAnyCollectionsListCollectionsList
public struct CollectionsList: Codable {
    public let slider: [[DiniSlider]]?
}

// MARK: - JSONAnyCollectionsListSlider
public struct DiniSlider: Codable {
    public let id: Int?
    public let title, summary, tagline: String?
    public let trailer: String?
    public let picture, background: String?
    public let isInFront: Bool?
    public let category: CollectionListCategoryList?
    public let tags: [CollectionListCategoryList]?
    public let resources: [CollectionsListResource]?
    public let type: Int?
    public let seasons: [CollectionsListSeason]?
    public let enabled, isSafe: Bool?
    public let sliderPicture: String?
    public let portraitPicture: String?
//    let keywords: JSONNull?
}

//// MARK: - JSONAnyCollectionsListResource
public struct CollectionsListResource: Codable {
    public let id: Int?
    public let title, summary: String?
    public let media: String?
    public let duration: Int?
    public let picture: String?
    public let speakers: [CollectionsListSpeaker]?
    public let ordering: Int?
    public let collection: String?
    public let enabled: Bool?
//    let publishedAt: Date?
    public let isCompletedFromViewed: Bool?
    public let timeCodeFromViewed: Int?
}

//// MARK: - CollectionsListSeason
public struct CollectionsListSeason: Codable {
    public let id: Int?
    public let name: String?
    public let ordering: Int?
    public let collection: String?
    public let resources: [CollectionsListResource]?
    public let picture: String?
    public let portraitPicture: String?
}
