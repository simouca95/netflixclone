//
//  LoginViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import Foundation
import RxSwift

class LoginViewModel: BaseViewModel {
    
    var isLoginSucceed = BehaviorSubject<Bool>(value: false)

    func login(email: String, password: String) {
        isLoading.accept(true)
        ApiCall.Auth.login(email: email, password: password, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension LoginViewModel {
    private func onSuccess(_ response: LoginResponse?) {
        isLoading.accept(false)
        if let response = response {
            if let _ = response.code, let _ = response.message {
//                ToastHelper.show(text: "Email ou mot de passe invalide")
                return
            } else if let token = response.token,
                      let vimeoToken = response.vimeoToken,
                      let id = response.userInfos?.userID,
                      let subscription = response.userInfos?.subscription, subscription.isEnabled {
                
                DefaultsHelper.saveUserToken(token)
                DefaultsHelper.saveViemoToken(vimeoToken)
                DefaultsHelper.saveUserId(id)
                
                if let appInfos = response.appInfos,
                   let dataResourcesBaseURL = appInfos.imagesPath?.imagesPath2 {
                    DefaultsHelper.saveDataResourcesBaseURL(dataResourcesBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let profileImageBaseURL = appInfos.imagesPath?.imagesPath6 {
                    DefaultsHelper.saveProfilesImagesBaseURL(profileImageBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let speakerImageBaseURL = appInfos.imagesPath?.imagesPath4 {
                    DefaultsHelper.saveSpeakerImagesBaseURL(speakerImageBaseURL)
                }
                
                if let appInfos = response.appInfos,
                   let isLikeUnLikeVisible = appInfos.preferencesParameters?.isLikeUnLikeVisible {
                    DefaultsHelper.saveIsLikeUnLikeVisible(isLikeUnLikeVisible)
                }
                
                if let maxProfilsInApplication = response.appInfos?.profils?.maxProfilsInApplication {
                    DefaultsHelper.saveMaxProfilsInApplication(maxProfilsInApplication)
                }
                
                if let maxProfilsInApplication = response.appInfos?.profils?.maxProfilsInApplication {
                    DefaultsHelper.saveMaxProfilsInApplication(maxProfilsInApplication)
                }
                
                if let currentprofilListCount = response.userInfos?.profils?.currentprofilListCount {
                    DefaultsHelper.saveCurrentprofilListCount(currentprofilListCount)
                }
                
                if let portraitPicturePath = response.appInfos?.imagesPath?.portraitPicturePath {
                    DefaultsHelper.savePortraitPicturePath(portraitPicturePath)
                }
                
                if let portraitPictureResourcesPath = response.appInfos?.imagesPath?.portraitPictureResourcesPath {
                    DefaultsHelper.savePortraitPictureResourcesPath(portraitPictureResourcesPath)
                }
                
                if let podcastPath = response.appInfos?.podcastPath?.podcastPath {
                    DefaultsHelper.savePodcastBaseURL(podcastPath)
                }
                
                if let subTitleResourcesPath = response.appInfos?.imagesPath?.subTitleResourcesPath {
                    DefaultsHelper.saveSubTitleResourcesPath(subTitleResourcesPath)
                }
                
                if let trailerPath = response.appInfos?.trailerPath?.trailerPath {
                    DefaultsHelper.saveTrailerBaseURL(trailerPath)
                }
                
                isLoginSucceed.onNext(true)
            }
        }
        
//        ToastHelper.show(text: "Email ou mot de passe invalide")
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
//        ToastHelper.show(text: "Email ou mot de passe invalide")
    }
}
