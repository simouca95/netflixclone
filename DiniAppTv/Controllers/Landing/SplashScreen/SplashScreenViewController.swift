//
//  SplashScreenViewController.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import UIKit
import RxSwift
import RxCocoa
import SideNavigationController

class SplashScreenViewController: UIViewController {
    
    // MARK: Private Properties
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let token = DefaultsHelper.userToken, token.count > 0,
                  let _ = DefaultsHelper.profilId {
            NavigationHelper.shared.startAppFromHome()
        } else if let _ = DefaultsHelper.userId {
            NavigationHelper.shared.startAppFromProfiles()
        } else {
            NavigationHelper.shared.startAppFromLogin()
        }
    }
    
}
