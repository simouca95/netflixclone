//
//  Network+Error.swift
//  PlaceStation
//
//  Created by Rached Khoudi on 17/10/2020.
//

import Alamofire

extension Network {
    
    enum Error: Swift.Error {
        
        case unknown (response: DataResponse<Any>)
        case unauthorized
        case encodingError
        case url
        case jsonDecoder(underlying: Swift.Error)
        
        var localizedDescription: String {
            switch self {
            case .jsonDecoder(let underlying):
                return underlying.localizedDescription
            case .unknown(let response):
                return response.debugDescription
            default:
                return "An error was occured"
            }
        }
    }
}
