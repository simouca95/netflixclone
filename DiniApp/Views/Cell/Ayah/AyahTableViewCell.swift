//
//  AyahTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/03/2021.
//

import UIKit

class AyahTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ayahNumberLabel: UILabel!
    @IBOutlet weak var arabicLabel: UILabel!
    @IBOutlet weak var frenchLabel: UILabel!
    
}
