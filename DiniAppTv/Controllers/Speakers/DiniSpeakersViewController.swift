//
//  DiniSpeakersViewController.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 09/02/2022.
//

import UIKit
import RxSwift

class DiniSpeakersViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: DiniSpeakersViewModel!
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initViewModel()
    }

    func initView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.register(cellType: SimpleCollectionViewCell.self)
        
        imageView.makeCircular()
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.white.cgColor
    }
    
    func initViewModel() {
        viewModel.getSpeakerCollectionWatch()
        viewModel.reloadData.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self,
                      let picture = self.viewModel.speaker?.picture,
                      let description = self.viewModel.speaker?.speakerDescription
                else { return }
                
                if let url = Helper.concatinatePaths(path1: DefaultsHelper.speakerImagesBaseURL,
                                                     path2: picture) {
                    self.imageView.kf.setImage(with: url)
                } else {
                    self.imageView.image = UIImage(named: "ItemEmpty")
                }

                self.descriptionLabel.text = description.html2String
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
    }

}
 
extension DiniSpeakersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.resources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SimpleCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.setupContent(item: viewModel.resources[indexPath.row])
        return cell
    }
    
}

extension DiniSpeakersViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width-(10*3))/4
        let height = width*1.477
        return CGSize(width: width, height: height)
    }
}
