//
//  CustomBMSubtitles.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 15/01/2022.
//

import Foundation
import BMPlayer

public class WebVttBMSubtitles {
    public var groups: [Group] = []
    public var url: String = ""
    
    public struct Group: CustomStringConvertible {
        var index: Int
        var start: TimeInterval
        var end  : TimeInterval
        var text : String
        
        init(_ index: Int, _ start: NSString, _ end: NSString, _ text: NSString) {
            self.index = index
            self.start = Group.parseDuration(start as String)
            self.end   = Group.parseDuration(end as String)
            self.text  = text as String
        }
        
        init(index: Int, start: String, end: String, text: String) {
            self.index = index
            self.start = Group.parseDuration(start)
            self.end   = Group.parseDuration(end)
            self.text  = text
        }
        
        static func parseDuration(_ fromStr:String) -> TimeInterval {
            var h: TimeInterval = 0.0, m: TimeInterval = 0.0, s: TimeInterval = 0.0, c: TimeInterval = 0.0
            let scanner = Scanner(string: fromStr)
            scanner.scanDouble(&h)
            scanner.scanString(":", into: nil)
            scanner.scanDouble(&m)
            scanner.scanString(":", into: nil)
            scanner.scanDouble(&s)
            scanner.scanString(",", into: nil)
            scanner.scanDouble(&c)
            return (h * 3600.0) + (m * 60.0) + s + (c / 1000.0)
        }
        
        public var description: String {
            return "Subtile Group ==========\nindex : \(index),\nstart : \(start)\nend   :\(end)\ntext  :\(text)"
        }
    }
    
    public convenience init(string: String, encoding: String.Encoding? = nil) {
        self.init(url: URL(string: string)!, encoding: encoding)
        self.url = string
    }
    
    public init(url: URL, encoding: String.Encoding? = nil) {
        DispatchQueue.global(qos: .background).async {
            do {
                let string: String
                if let encoding = encoding {
                    string = try String(contentsOf: url, encoding: encoding)
                } else {
                    string = try String(contentsOf: url)
                }
                
                self.groups = Self.parseSubRip(string) ?? []
            } catch {
                print("| BMPlayer | [Error] failed to load \(url.absoluteString) \(error.localizedDescription)")
            }
        }
    }
    
    /**
     Search for target group for time
     
     - parameter time: target time
     
     - returns: result group or nil
     */
    public func search(for time: TimeInterval) -> Group? {
        let result = groups.first(where: { group -> Bool in
            if group.start <= time && group.end >= time {
                return true
            }
            return false
        })
        return result
    }
    
    /**
     Parse str string into Group Array
     
     - parameter payload: target string
     
     - returns: result group
     */
    fileprivate static func parseSubRip(_ payload: String) -> [Group]? {
        var groups: [Group] = []
        let scanner = Scanner(string: payload)
        var index = 0
        while !scanner.isAtEnd {
            var line: NSString?
            scanner.scanUpToCharacters(from: .newlines, into: &line)
            
            guard let timeline = line else {continue}
            if !timeline.contains(" --> ") {continue}
            
            let times = String(timeline).components(separatedBy: " --> ")
            if times.count != 2 {continue}
            
            var textString: NSString?
            scanner.scanUpToCharacters(from: .newlines, into: &textString)
            guard let text = textString else {continue}
            
            index += 1
            let group = Group(index: index, start: times[0], end: times[1], text: String(text))
            
            groups.append(group)
        }
        return groups
    }
}
