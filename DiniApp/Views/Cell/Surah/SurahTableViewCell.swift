//
//  SurahTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/03/2021.
//

import UIKit

class SurahTableViewCell: UITableViewCell {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var surahNameLabel: UILabel!
    @IBOutlet weak var surahNumberLabel: UILabel!
    @IBOutlet weak var surahEnglishNameLabel: UILabel!
    @IBOutlet weak var surahArabicNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.parentView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
