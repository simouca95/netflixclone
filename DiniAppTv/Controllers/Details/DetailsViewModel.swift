//
//  DetailsViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import RxSwift

enum CollectionType: Int {
    case episode = 0
    case season = 1
    case manySeasons = 2
    case audio = 4
}

protocol DetailsDelegate: AnyObject {
    func onInfoStateChanged()
}

class DetailsViewModel: BaseViewModel {
    
    var cellData: CollectionListData?
    var collectionId: Int
    var items:[BaseCellViewModel] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    var reloadEpsData = BehaviorSubject<Void>(value: ())
    var currentVideo = BehaviorSubject<VideoResponse?>(value: nil)

    var seasonSelected = 0
    var infoState:[CollectionInfoState] = []
    var viewdInfos: [CollectionViewedsInfo] = []

    //TO DO enhance logic
    var infoStateToBeUpdatedNow: CollectionInfoState? = nil
    
    
    private var resourcesFirstId: String?
//    weak var delegate: DetailsDelegate?
    
    //When user click an item, we pass this data to VideoPlayVC
    var currentClickedIndex:Int?
    var currentResources:[DiniResource] = []
    
    var currentResourceId: Int?
    var availableResources: [DiniResource]? {
        return getAvailableResources(resources: cellData?.resources) ?? nil
    }

    var mediaToBeDownloaded: String?
    
    init(collectionId: Int, infoState: [CollectionInfoState]) {//, delegate: DetailsDelegate) {
        self.collectionId = collectionId
        self.infoState = infoState
//        self.delegate = delegate
    }
    
    func updateItems() {
        guard let cellData = cellData,
              let typeIndex = cellData.type,
              let type = CollectionType(rawValue: typeIndex)
        else { return }

        items.removeAll()
        
        var infosTitle = ""
        var infosSpeakers = ""
        var infosCategory = ""
        var infosTags = ""
        var contentNumber: Int = 0
        var portraitPicture = ""
        var bigPicture = ""
        
        if let title = cellData.title { infosTitle = title }
        
        if let resources = availableResources {
            let resourcesSpeakers = resources.compactMap{$0.speakers}.flatMap{$0}
            let speakersName = Set(resourcesSpeakers.compactMap{$0.name})
            infosSpeakers = speakersName.joined(separator: ",")
        }
        
        if let category = cellData.category?.name { infosCategory = category }

        if let tags = cellData.tags {
            let tagsName = tags.compactMap{$0.name}
            infosTags = tagsName.joined(separator: ",")
        }
        
        if let portrait = cellData.portraitPicture {
            portraitPicture = portrait
        }
        
        if let big = cellData.sliderPicture {
            bigPicture = big
        }
        
        switch type {
        case CollectionType.episode, CollectionType.season:
            if let resources = availableResources?.count {
                contentNumber = resources
            }
        case CollectionType.manySeasons:
            if let seasons = cellData.seasons?.count {
                contentNumber = seasons
            }
        case CollectionType.audio:
            if let resources = availableResources?.count {
                contentNumber = resources
            }
        }
        
        let infoCellViewModel = InfoCollectionViewModel(
            type: type,
            title: infosTitle,
            speakers: infosSpeakers,
            category: infosCategory,
            tags: infosTags,
            contentNumber: contentNumber,
            portraitPicture: portraitPicture,
            bigPicture: bigPicture,
            seasons: cellData.seasons ?? [])

        items.append(infoCellViewModel)
        
        switch type {
        case CollectionType.episode, CollectionType.season, CollectionType.audio:
            if let resources = cellData.resources,
               let availableResources = getAvailableResources(resources: resources) {
                let simpleCellViewModel = EpsCollectionViewModel(cellHeight: 0, type: type, resources: availableResources)
                items.append(simpleCellViewModel)
            }
        case CollectionType.manySeasons:
            if let firstSeason = cellData.seasons?.first,
               let availableResources = getAvailableResources(resources: firstSeason.resources) {
                let simpleCellViewModel = EpsCollectionViewModel(cellHeight: 0, type: .manySeasons, resources: availableResources)
                items.append(simpleCellViewModel)
            }
        }

        reloadData.onNext(())
    }
    
    func updateEps(row: Int) {
        guard let cellData = cellData,
              let typeIndex = cellData.type,
              let type = CollectionType(rawValue: typeIndex)
        else { return }

        items.removeLast()
        
        switch type {
        case CollectionType.episode, CollectionType.season, CollectionType.audio:
            if let resources = cellData.resources,
               let availableResources = getAvailableResources(resources: resources) {
                let simpleCellViewModel = EpsCollectionViewModel(cellHeight: 0, type: type, resources: availableResources)
                items.append(simpleCellViewModel)
            }
        case CollectionType.manySeasons:
            if let firstSeason = cellData.seasons?[safe: row],
               let availableResources = getAvailableResources(resources: firstSeason.resources) {
                let simpleCellViewModel = EpsCollectionViewModel(cellHeight: 0, type: .manySeasons, resources: availableResources)
                items.append(simpleCellViewModel)
            }
        }

        reloadEpsData.onNext(())
    }
    
    private func getAvailableResources(resources: [DiniResource]?) -> [DiniResource]? {
        let resources = resources?.filter({ resource in
            let dateFormatter = ISO8601DateFormatter()
            if let publishedAtString = resource.publishedAt,
               let date = dateFormatter.date(from:publishedAtString),
               let isEnabled = resource.enabled {
                if date < Date(), isEnabled {
                    return true
                }
                return false
            }
            return true
        })
        return resources
    }

    func getCollectionDetails() {
        isLoading.accept(true)
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }

        ApiCall.Details.getCollectionViewsInfo(token: token, profileId: profileId, id: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getCollectionWatch(collectionId id: Int) {
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getCollectionWatch(token: token, profileId: profileId, collectionId: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func setInfoState(info: CollectionInfoState) {
//        guard let profileId = DefaultsHelper.profilId,
//        let token = DefaultsHelper.userToken
//        else { return }
//        isLoading.accept(true)
//        infoStateToBeUpdatedNow = info
//        ApiCall.Details.setUnsetInfo(token: token, infoState: info.rawValue, collectionId: collectionId, profileId: profileId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DetailsViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CollectionListData?) {
        isLoading.accept(false)
        guard let response = response else { return }
        cellData = response
        updateItems()
    }

    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        currentVideo.onNext(response)
    }
    
    private func onSuccess(_ response: CollectionWatchResponse?) {
        if let media = response?.media {
            isLoading.accept(true)
            currentResourceId = Int(response?.resourceID ?? "")
            getMedia(media: media)
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
