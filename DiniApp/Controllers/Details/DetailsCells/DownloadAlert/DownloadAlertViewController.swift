//
//  DownloadAlertViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/01/2022.
//

import UIKit

class DownloadAlertViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var downloadButton: UIButton!
    
    var viewModel: DownloadAlertViewModel!
    private var indexSelected = 0
    
    internal static func instantiate(with viewModel: DownloadAlertViewModel) -> DownloadAlertViewController {
        let vc = StoryboardScene.Details.downloadAlertViewController.instantiate()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    func initView() {
        titleLabel.text = viewModel.resource.title ?? "unknown"
        tableView.dataSource = self
        tableView.delegate = self
        downloadButton.isEnabled = false
    }
    
    @IBAction func onDismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onDownloadTapped(_ sender: Any) {
        self.dismiss(animated: true) {
            DownaloadServices.shared.start(with: self.viewModel.files[self.indexSelected],
                                           resource: self.viewModel.resource,
                                           downloadState: .downloading)
        }
    }
}

extension DownloadAlertViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.files.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.files[indexPath.row]
        let cell: DownloadTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.titleLabel.text = item.publicName
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .black
        let label = UILabel()
        label.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Choisir la qualité d'image"
        label.font = .systemFont(ofSize: 17)
        label.textColor = .white
        
        headerView.addSubview(label)
        
        return headerView
    }
}

extension DownloadAlertViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        downloadButton.isEnabled = true
        indexSelected = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
