//
//  ApiCall+Profil.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 08/12/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Profil {
        
        public static func getProfilTypes(token: String, onSuccess: @escaping (_ response: ProfilTypes?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.ProfilController.getProfilTypes(token: token)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ProfilTypes = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        
        public static func updateProfil(token: String, profileId: String, userId: String, typeId: String, name: String, avatar: String, onSuccess: @escaping (_ response: ProfilActionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.ProfilController.updateProfil(token: token, profileId: profileId, userId: userId, typeId: typeId, name: name, avatar: avatar)
                let classicUrlRequest = try api.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                var urlRequest = URLRequest(url: classicUrlRequest.url!)
                let json = "{\"userId\" : \"\(userId)\",\"type\" : \"\(typeId)\", \"name\" : \"\(name)\", \"picture\" : \"\(avatar)\"}"
                
                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
                
                urlRequest.httpMethod = HTTPMethod.put.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                urlRequest.allHTTPHeaderFields = Network.secureHeaders(token: token)
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ProfilActionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func addProfil(token: String, userId: String, typeId: String, name: String, avatar: String, onSuccess: @escaping (_ response: ProfilActionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.ProfilController.addProfil(token: token, userId: userId, typeId: typeId, name: name, avatar: avatar)
                let classicUrlRequest = try api.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                var urlRequest = URLRequest(url: classicUrlRequest.url!)
                let json = "{\"userId\" : \"\(userId)\",\"profileTypeId\" : \"\(typeId)\", \"name\" : \"\(name)\", \"picture\" : \"\(avatar)\"}"
                
                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
                
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                urlRequest.allHTTPHeaderFields = Network.secureHeaders(token: token)
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ProfilActionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func deleteProfil(token: String, profilId: String, onSuccess: @escaping (_ response: ProfilActionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.ProfilController.deleteProfil(token: token, profileId: profilId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ProfilActionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
    }
}
