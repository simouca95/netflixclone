//
//  Ayah.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/03/2021.
//

import Foundation

// MARK: - Surah
public struct SurahResponse: Codable {
    public let code: Int
    public let status: String
    public let data: [Surah]
}

// MARK: - Datum
public struct Surah: Codable {
    public let number: Int
    public let name, englishName, englishNameTranslation: String
    public let numberOfAyahs: Int
    let revelationType: RevelationType
}

enum RevelationType: String, Codable {
    case meccan = "Meccan"
    case medinan = "Medinan"
}
