//
//  SimpleCollectionViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import UIKit
import Reusable
import Kingfisher

class SimpleCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 8
            self.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func setupContent(item : DiniSlider) {
        
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
    
    }
    
    func setupContent(item : DiniResource) {
        
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
    
    }
    
}
