//
//  DetailsViewModel.swift
//  DiniApp
//
//  Created by Rached Khoudi on 18/02/2022.
//

import RxSwift

class DetailsReelViewModel: BaseViewModel {
    
    var items: [DiniResource] = []
    var collectionId: Int
    var id: Int
    var collectionListData: CollectionListData?

    var reloadData = BehaviorSubject<Void>(value: ())
    var currentVideo = BehaviorSubject<VideoResponse?>(value: nil)

    init(collectionId: Int, id: Int) {
        self.collectionId = collectionId
        self.id = id
    }
    
    func updateItems() {
        self.items.removeAll()
        
        guard let collectionListData = collectionListData,
              let items = collectionListData.resources
        else { return }
        
        self.items.append(contentsOf: items)
        
        reloadData.onNext(())
    }
    
    func getCollectionDetails() {
        isLoading.accept(true)
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }

        ApiCall.Details.getCollectionViewsInfo(token: token, profileId: profileId, id: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DetailsReelViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CollectionListData?) {
        isLoading.accept(false)
        guard let response = response else { return }
        collectionListData = response
        updateItems()
    }
    
    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        currentVideo.onNext(response)
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
