//
//  VideoPlayerViewController.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 14/02/2022.
//

import AVFoundation
import AVKit
import UIKit
import RxSwift

class VideoPlayerViewController: UIViewController {
    var playPauseButton: VideoCustomView!
    var viewModel: VideoPlayViewModel?
//    var player: AVPlayer?
    let playerViewController = AVPlayerViewController()
    private var disposeBag = DisposeBag()


    override func viewDidLoad() {
        super.viewDidLoad()

//        guard let url = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4") else { return }

//        let player = AVPlayer(url: url)
//        player.rate = 1 //auto play
        let playerFrame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
//        playerViewController.player = player
        playerViewController.view.frame = playerFrame
        playerViewController.showsPlaybackControls = true

        addChild(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.didMove(toParent: self)
//        playerViewController.player?.play()

        playPauseButton = VideoCustomView()
//        playPauseButton.avPlayer = player
        view.addSubview(playPauseButton)
        playPauseButton.setup(in: self)
        
        initViewModel()
        
        guard let viewModel = self.viewModel else { return }
        viewModel.currentVideo.accept(viewModel.video)
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        playPauseButton.updateUI()
    }
    
    func initViewModel() {
        guard let viewModel = self.viewModel else { return }

        viewModel.currentVideo.asObservable()
            .subscribe (onNext:{ [weak self] video in
                guard let self = self,
                      let videoUrl = URL(string: video?.files?[safe: viewModel.currentFileIndex]?.link ?? "")
                else { return }
                
                self.playerViewController.player = AVPlayer(url: videoUrl)
                self.playerViewController.player?.play()

//                if let timeToSeek = viewModel.timeCodeFromViewed {
//                    player.seek(TimeInterval(timeToSeek))
//                }
            })
            .disposed(by: self.disposeBag)
    }
}
