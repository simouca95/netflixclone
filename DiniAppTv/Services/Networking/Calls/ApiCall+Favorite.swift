//
//  ApiCall+Favorite.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Alamofire

extension ApiCall {
    
    public struct Favorite {
        
        public static func getFavoriteCollectionList(token: String, id: Int, onSuccess: @escaping (_ response: FavoritesResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.FavoriteController.getCollectionList(token: token, profileId: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: FavoritesResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
    }
}
