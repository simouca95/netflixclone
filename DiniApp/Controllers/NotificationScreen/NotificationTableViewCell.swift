//
//  NotificationTableViewCell.swift
//  DiniApp
//
//  Created by Wafa Labidi on 3/7/2022.
//

import UIKit
import RxSwift

protocol NotificationTableViewCellDelegate: AnyObject {
    func didTapCollectionButton(idCollection: Int)
    func didTapResourceButton(idResource: Int, idCollection: Int)
}

class NotificationTableViewCell: UITableViewCell {
    
    //MARK: OUTLETS
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var containertView: UIView!
    @IBOutlet weak var playView: UIView!
    
    //MARK: Propreties
    class var identifier: String {
        return String(describing: self)
    }
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    private let disposeBag = DisposeBag()
    private var isCollection = false
    private var idCollection = 0
    private var idResource = 0
    weak var delegate: NotificationTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    func initView(using notification: NotificationData) {
        newLabel.isHidden = !(notification.isNewNotification ?? false)
        newLabel.clipsToBounds = true
        newLabel.layer.cornerRadius = 4
        titleLabel.text = notification.title
        contentLabel.text = notification.content
        guard let date = notification.sendingAt else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFromString = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let datenew = dateFormatter.string(from: dateFromString ?? Date())
        dateLabel.text = datenew
        if let collectionId = notification.collectionId {
            typeLabel.isHidden = false
            playView.isHidden = false
            typeLabel.text = notification.collectionTitle
            isCollection = true
            idCollection = collectionId
        } else if let resourceId = notification.resourceId,
                  let collectionId = notification.collectionIdIfNotificationTypeResource {
            typeLabel.isHidden = false
            playView.isHidden = false
            typeLabel.text = notification.resourceTitle
            isCollection = false
            idCollection = collectionId
            idResource = resourceId
        } else {
            typeLabel.isHidden = true
            playView.isHidden = true
        }
    }
    
    private func setupUI() {
        dateLabel.textColor = .white
        dateLabel.font = .boldSystemFont(ofSize: 11)
        containertView.layer.cornerRadius = 4
        containertView.layer.borderWidth = 1
        containertView.layer.borderColor = UIColor.white.cgColor
        typeLabel.isHidden = true
        playView.layer.cornerRadius = 10
        playView.tintColor = .white
        titleLabel.font = .boldSystemFont(ofSize: 16)
        contentLabel.font = .systemFont(ofSize: 15)
        typeLabel.font = .systemFont(ofSize: 13)
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(checkAction))
        containertView.addGestureRecognizer(gesture)
        
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        if isCollection {
            delegate?.didTapCollectionButton(idCollection: idCollection)
        } else {
            delegate?.didTapResourceButton(idResource: idResource, idCollection: idCollection)
        }
    }
}

