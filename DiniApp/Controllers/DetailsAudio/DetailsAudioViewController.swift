//
//  DetailsViewController+Audio.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/12/2021.
//

import UIKit
import AVFoundation
import SwiftAudioPlayer
import Kingfisher

struct LocalAudio {
    let localURL: URL
    let picture: String
    let name: String
    let timeCodeFromViewed: Int
}
class DetailsAudioViewController: BaseViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var audioNameLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var playerSlider: UISlider!
    @IBOutlet weak var leftPlaterLabel: UILabel!
    @IBOutlet weak var rightPlaterLabel: UILabel!
    
    var resource: DiniResource?
    var currentPosition: Double = 0
    
    var beingSeeked: Bool = false
    var duration: Double = 0.0
    var playbackStatus: SAPlayingStatus = .paused
    var isPlayable: Bool = false
    var localAudio: LocalAudio?
    var isDragging = false
    private var audioPlayer: AVAudioPlayer? = nil
    var viewModel = DetailsAudioViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let local = localAudio {
            
            audioNameLabel.text = local.name
            
            if !local.picture.isEmpty, let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                                         path2: local.picture) {
                posterImageView.kf.setImage(with: url)
            } else {
                posterImageView.image = UIImage(named: "ItemEmpty")
            }
            SAPlayer.shared.startSavedAudio(withSavedUrl: local.localURL)
            
            SAPlayer.shared.seekTo(seconds: Double(local.timeCodeFromViewed))
            SAPlayer.shared.play()
            
        } else if let url = Helper.concatinatePaths(path1: DefaultsHelper.podcastBaseURL, path2: resource?.media) {
            audioNameLabel.text = resource?.title
            if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                                 path2: resource?.picture) {
                posterImageView.kf.setImage(with: url)
            } else {
                posterImageView.image = UIImage(named: "ItemEmpty")
            }
            SAPlayer.shared.startRemoteAudio(withRemoteUrl: url)
            
            if let time = resource?.timeCodeFromViewed {
                SAPlayer.shared.seekTo(seconds: Double(time))
                SAPlayer.shared.play()
            } else {
                SAPlayer.shared.play()
            }
            
        }
        
        isPlayable = false
        
        subscribeToChanges()
    }
    
    func subscribeToChanges() {
        _ = SAPlayer.Updates.Duration.subscribe { [weak self] (duration) in
            guard let self = self else { return }
            self.rightPlaterLabel.text = SAPlayer.prettifyTimestamp(duration).substring(from: String.Index(encodedOffset: 2))
            self.duration = duration
        }
        
        _ = SAPlayer.Updates.ElapsedTime.subscribe { [weak self] (position) in
            guard let self = self else { return }
            self.currentPosition = position
            self.leftPlaterLabel.text = SAPlayer.prettifyTimestamp(position).substring(from: String.Index(encodedOffset: 2))
            guard self.duration != 0 else { return }
            if !self.beingSeeked {
                self.playerSlider.value = Float(position/self.duration)
            }
        }
        
        _ = SAPlayer.Updates.PlayingStatus.subscribe { [weak self] (playing) in
            guard let self = self else { return }
            
            self.playbackStatus = playing
            
            switch playing {
            case .playing:
                self.isPlayable = true
                self.playButton.setImage(UIImage(named: "pause"), for: .normal)
                return
            case .paused:
                self.isPlayable = true
                self.playButton.setImage(UIImage(named: "play-button-fill"), for: .normal)
                return
            case .buffering:
                self.isPlayable = false
                //                self.playButton.setTitle("Loading", for: .normal)
                return
            case .ended:
                self.isPlayable = false
                self.playButton.setImage(UIImage(named: "play-button-fill"), for: .normal)
                return
            }
        }
    }
    
    @IBAction func playPauseTouched(_ sender: Any) {
        SAPlayer.shared.togglePlayAndPause()
    }
    
    @IBAction func scrubberStartedSeeking(_ sender: UISlider) {
        beingSeeked = true
    }
    
    @IBAction func scrubberSeeked(_ sender: Any) {
        let value = Double(playerSlider.value) * duration
        SAPlayer.shared.seekTo(seconds: value)
        beingSeeked = false
    }
    
    @IBAction func skipBackwardTouched(_ sender: Any) {
        SAPlayer.shared.seekTo(seconds: currentPosition-10)
    }
    
    @IBAction func skipForwardTouched(_ sender: Any) {
        SAPlayer.shared.seekTo(seconds: currentPosition+10)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        guard let resourceId = resource?.id
        else {
            SAPlayer.shared.clear()
            print("timeCode not working")
            return }
        viewModel.timeCode(resourceId: resourceId, timeCode: Int(self.currentPosition))
        SAPlayer.shared.clear()
    }
}
