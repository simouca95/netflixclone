//
//  SeriesTableViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 24/02/2021.
//

import UIKit

class SeriesTableViewCell: UITableViewCell {
    
    static let identifier = "SeriesTableViewCell"
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var episodesLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        
        if let nextCell = context.nextFocusedView as? SeriesTableViewCell,
           let previousCell = context.previouslyFocusedView as? SeriesTableViewCell {
            nextCell.parentView.layer.borderWidth = 5
            nextCell.parentView.layer.borderColor = UIColor.white.cgColor
            previousCell.parentView.layer.borderWidth = 0
            previousCell.parentView.layer.borderColor = UIColor.clear.cgColor
        } else if let _ = context.nextFocusedView as? SeriesTableViewCell,
                  let previousCell = context.previouslyFocusedView as? SeriesTableViewCell {
            previousCell.parentView.layer.borderWidth = 5
            previousCell.parentView.layer.borderColor = UIColor.white.cgColor
        } else if let nextCell = context.nextFocusedView as? SeriesTableViewCell {
            nextCell.parentView.layer.borderWidth = 5
            nextCell.parentView.layer.borderColor = UIColor.white.cgColor
        }
    }
    
}
