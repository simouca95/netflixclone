//
//  AvatarsViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/12/2021.
//

import RxCocoa
import SVGKit
import RxSwift

protocol AvatarsDelegate: AnyObject {
    func updateImage(imagePath: String)
}

class AvatarsViewModel: BaseViewModel {
    
    var reloadData = BehaviorSubject<Void>(value: ())
    var pictures: [String] = []
    weak var delegate: AvatarsDelegate?
    
    func avatarPictureList() {
        guard let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Auth.avatarPictureList(token: token, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension AvatarsViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: [[String]]?) {
        if let response = response {
            
            pictures = response.map({ stringArray in
                if let path = stringArray.first,
                   path.contains(".svg") {
                    return path
                }
                return nil
            }).compactMap{ $0 }
            
            isLoading.accept(false)
            reloadData.onNext(())
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
