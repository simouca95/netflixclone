//
//  PushnotificationHandler.swift
//  DiniApp
//
//  Created by Wafa Labidi on 17/6/2022.
//

import Foundation
import RxSwift
import CloudKit

enum NotificationType {
    case resource(resourceId: Int, collectionId: Int)
    case collection(collectionId: Int)
    case none
    case simpleNotification(title: String, body: String)
}

class PushNotificationHandler {
    
    static let shared = PushNotificationHandler()
    
    var didReceiveNotification: NotificationType = .none
    var clickNotfication = PublishSubject<NotificationType>()
    
    func reset() {
        didReceiveNotification = .none
        clickNotfication.onNext(.none)
    }
    
}
