//
//  DownloadView.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 31/12/2021.
//

//import UIKit
//import SnapKit
//
//enum CustomDownloadViewState {
//    case prepare
//    case donwload(Double)
//    case nothing
//}
//
//class CustomDownloadView: UIView {
//
//    let progressCircle = UILabel()
//    let indicator = UIActivityIndicatorView()
//    let downloadButton = UIButton()
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setView(state: .nothing)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setView(state: .nothing)
//    }
//
//    func setView(state: CustomDownloadViewState) {
//        switch state {
//        case .prepare:
//            addActivityIndicator()
//        case .donwload(let progress):
//            addCircularProgress(value: progress)
//        case .nothing:
//            addDownloadButton()
//        }
//    }
//
//    private func addActivityIndicator() {
//        reset()
//        indicator.frame.size = CGSize(width: 20, height: 20)
//        indicator.style = UIActivityIndicatorView.Style.white
//        self.addSubview(indicator)
//        indicator.snp.makeConstraints { make in
//            make.centerX.centerY.equalToSuperview()
//        }
//        indicator.startAnimating()
//    }
//
//    private func addDownloadButton() {
//        reset()
//        downloadButton.frame.size = CGSize(width: 20, height: 20)
//        downloadButton.setImage(UIImage(systemName: "arrow.down.to.line"), for: .normal)
//        downloadButton.tintColor = .white
//        self.addSubview(downloadButton)
//        downloadButton.snp.makeConstraints { make in
//            make.centerX.centerY.equalToSuperview()
//        }
//    }
//
//    private func addCircularProgress(value: Double) {
//        reset()
//        progressCircle.frame.size = CGSize(width: 20, height: 20)
//        progressCircle.text = "\(value)"
//        progressCircle.textColor = .white
//        self.addSubview(progressCircle)
//        progressCircle.snp.makeConstraints { make in
//            make.centerX.centerY.equalToSuperview()
//        }
//    }
//
////    func animateCircle(circleToValue: CGFloat) {
////        let fifths:CGFloat = circleToValue / 5
////        let animation = CABasicAnimation(keyPath: "strokeEnd")
////        animation.duration = 0.25
////        animation.fromValue = fifths
////        animation.byValue = fifths
////        animation.fillMode = CAMediaTimingFillMode.both
////        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
////        progressCircle.strokeEnd = fifths
////
////        // Create the animation.
////        progressCircle.add(animation, forKey: "strokeEnd")
////    }
//
//    func reset() {
//        downloadButton.removeFromSuperview()
//        progressCircle.removeFromSuperview()
//        indicator.removeFromSuperview()
//    }
//
//}
