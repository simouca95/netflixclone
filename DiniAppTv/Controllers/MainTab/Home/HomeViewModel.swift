//
//  HomeViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import RxSwift
import RxCocoa

enum HomeCellType: String {
    case main = ""
    case speaker = "5"
    case inProgress = "0"
    case homeResources = "11"
    case live = "7"
    case standard = "990"
}

enum CollectionInfoState: Int {
    case liked = 0
    case disliked = 1
    case fav = 2
}

enum PlayHomeVideoType {
    case fromMain
    case fromInProgress
    case fromHomeResources
    case none
}

class HomeViewModel: BaseViewModel {
    
    var items: [CollectionList] = []
    var itemsViewedsInfos: [CollectionViewedsInfo] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    var collectionInfos: [CollectionInfo]?
    var collectionViewdInfos: [CollectionViewedsInfo]?

    var currentWatchVideo = BehaviorRelay<VideoResponse?>(value: nil)
    var playHomeVideoType: PlayHomeVideoType = .none
    var currentHomeVideoVerticalItemIndex: Int?
    var currentHomeVideoHorizontalItemIndex: Int?
    var currentHomeVideoId: Int?
    
    var sliderCollectionList: CollectionList?

    func getCollectionList() {
        guard let id = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }
        isLoading.accept(true)
        ApiCall.Home.getHomeCollectionList(token: token, id: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getCollectionWatch(collectionId: Int) {
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getCollectionWatch(token: token, profileId: profileId, collectionId: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getAvailableResources(verticalIndex: Int, horizontalIndex: Int) -> [DiniResource]? {
        return items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.resources
    }
    
    private func getAvailableResources(resources: [DiniResource]?) -> [DiniResource]? {
        let resources = resources?.filter({ resource in
            let dateFormatter = ISO8601DateFormatter()
            if let publishedAtString = resource.publishedAt,
               let date = dateFormatter.date(from:publishedAtString),
               let isEnabled = resource.enabled {
                if date < Date(), isEnabled {
                    return true
                }
                return false
            }
            return true
        })
        return resources
    }
}

extension HomeViewModel {
    private func onSuccess(_ response: CollectionResponse?) {
        guard let response = response,
              let collectionList = response.collectionList,
              let collectionInfos = response.collectionInfos,
              let collectionViewdInfos = response.collectionViewedsInfos
        else {
            isLoading.accept(false)
            return
        }
        
        items.removeAll()

        self.collectionInfos = collectionInfos
        self.collectionViewdInfos = collectionViewdInfos

        var collectionListOrdered = collectionList
            .compactMap({$0})
            .sorted(by: { $0.order ?? 0 < $1.order ?? 0 })
        
        sliderCollectionList = collectionListOrdered.filter({ $0.type == HomeCellType.main.rawValue }).first
        collectionListOrdered = collectionListOrdered.filter({ $0.type != HomeCellType.main.rawValue })
        
        items.append(contentsOf: collectionListOrdered)
        
        if let collectionViewedsInfos = response.collectionViewedsInfos {
            itemsViewedsInfos.removeAll()
            itemsViewedsInfos.append(contentsOf: collectionViewedsInfos)
        }
        
        reloadData.onNext(())
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: CollectionWatchResponse?) {
        if let media = response?.media {
            isLoading.accept(true)
            currentHomeVideoId = Int(response?.resourceID ?? "")
            getMedia(media: media)
        }
    }
    
    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        currentWatchVideo.accept(response)
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
