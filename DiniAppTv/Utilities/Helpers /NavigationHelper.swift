//
//  NavigationHelper.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 04/02/2022.
//

import UIKit

struct NavigationHelper {
    static let shared = NavigationHelper()
    
    func startAppFromLogin() {
        let loginViewController = StoryboardScene.Landing.loginViewController.instantiate()
        UIApplication.shared.windows.first?.rootViewController = loginViewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func startAppFromProfiles() {
        let selectProfileViewController = StoryboardScene.Profile.selectProfileViewController.instantiate()
        UIApplication.shared.windows.first?.rootViewController = selectProfileViewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func startAppFromHome() {
        let tabBarController = StoryboardScene.Main.mainNVC.instantiate()
        UIApplication.shared.windows.first?.rootViewController = tabBarController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
