//
//  MenuViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 17/03/2021.
//

import UIKit

class MenuViewController: BaseViewController {


    @IBOutlet weak var logoButton: UIButton!
    @IBOutlet weak var diniTvButton: UIButton!
    @IBOutlet weak var prayButton: UIButton!
    @IBOutlet weak var qiblaButton: UIButton!
    @IBOutlet weak var mushafButton: UIButton!
    
    
    @IBOutlet weak var DiniTvView: UIView!
    @IBOutlet weak var QiblaView: UIView!
    @IBOutlet weak var prayView: UIView!
    @IBOutlet weak var mushafView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.DiniTvView.makeCircular()
        self.DiniTvView.layer.borderWidth = 1
        self.DiniTvView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.QiblaView.makeCircular()
        self.QiblaView.layer.borderWidth = 1
        self.QiblaView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.prayView.makeCircular()
        self.prayView.layer.borderWidth = 1
        self.prayView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.mushafView.makeCircular()
        self.mushafView.layer.borderWidth = 1
        self.mushafView.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        
        self.prayButton.layer.cornerRadius = self.prayButton.frame.height / 2
        self.prayButton.layer.borderWidth = 1
        self.prayButton.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.qiblaButton.layer.cornerRadius = self.qiblaButton.frame.height / 2
        self.qiblaButton.layer.borderWidth = 1
        self.qiblaButton.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        
        self.mushafButton.layer.cornerRadius = self.mushafButton.frame.height / 2
        self.mushafButton.layer.borderWidth = 1
        self.mushafButton.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor

        if let logo = DefaultsHelper.logo {
            logoButton.kf.setImage(with: URL(string: logo), for: .normal)
        }
    }
    
    
    @IBAction func didSelectDiniTv(_ sender: UIButton) {
        let selectProfileVc = StoryboardScene.Profil.selectProfileViewController.instantiate()
        self.navigationController?.pushViewController(selectProfileVc, animated: true)
    }
    
    @IBAction func didSelectPray(_ sender: UIButton) {
        let prayVc = StoryboardScene.Islamic.prayViewController.instantiate()
        self.navigationController?.pushViewController(prayVc, animated: true)
    }
    
    @IBAction func didSelectQibla(_ sender: UIButton) {
        let qiblaVc = StoryboardScene.Islamic.qiblaViewController.instantiate()
        self.navigationController?.pushViewController(qiblaVc, animated: true)
    }
    
    @IBAction func didSelectMushaf(_ sender: UIButton) {
        let mushafVc = StoryboardScene.Islamic.mushafViewController.instantiate()
        self.navigationController?.pushViewController(mushafVc, animated: true)
    }
}
