//
//  LoginViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import RxSwift
import SkyFloatingLabelTextField
import SafariServices

class LoginViewController: BaseViewController {
    
    // MARK: Public Static Methods
    
    static func newViewController() -> UIViewController {
        let viewController = StoryboardScene.Landing.loginViewController.instantiate()
        return viewController
    }
    
    // MARK: Outlets
    @IBOutlet weak var logoButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!

    @IBOutlet weak var rememberMeButton: UIButton!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailtextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordtextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var attributedLabel: UILabel!

    // MARK: Private Properties
    
    private var viewModel: LoginViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel = LoginViewModel(taskNavigator: self)
        self.initViewModel()
    }
    
    // MARK: Private Methods
    
    private func initViews() {
        if let logo = DefaultsHelper.logo {
            logoButton.kf.setImage(with: URL(string: logo), for: .normal)
        }
        
        rememberMeButton.isSelected = true
        
        emailtextField.placeholder = "E-mail ou numéro de téléphone"
        emailtextField.placeholderFont = UIFont.systemFont(ofSize: 18)
        emailtextField.titleFormatter = { $0 }
        emailtextField.font = UIFont.systemFont(ofSize: 18)
        
        passwordtextField.placeholder = "Mot de passe"
        passwordtextField.placeholderFont = UIFont.systemFont(ofSize: 18)
        passwordtextField.titleFormatter = { $0 }
        passwordtextField.font = UIFont.systemFont(ofSize: 18)
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        loginButton.backgroundColor = .primary
        loginButton.layer.cornerRadius = 4
        emailView.layer.cornerRadius = 4
        passwordView.layer.cornerRadius = 4
        
        // Tap gesture recognizers
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction))
        
        // Gesture recognizer Label
        attributedLabel.isUserInteractionEnabled = true
        attributedLabel.addGestureRecognizer(tapLabel)
        attributedLabel.isHidden = true
        let plainAttributedString = NSMutableAttributedString(string: "Pas encore inscrit sur Dini ? RDV sur ", attributes: [.foregroundColor: UIColor.lightGray])
        let string = "dini.tv"
        let attributedLinkString = NSMutableAttributedString(string: string, attributes:[.link: URL(string: "http://www.dini.tv")!, .foregroundColor: UIColor.systemBlue])
        
        let fullAttributedString = NSMutableAttributedString()
        fullAttributedString.append(plainAttributedString)
        fullAttributedString.append(attributedLinkString)
        attributedLabel.attributedText = fullAttributedString
        
        if let email = DefaultsHelper.userEmail,
           let password = DefaultsHelper.userPassword {
            emailtextField.text = email
            passwordtextField.text = password
        }
    }

    private func initViewModel() {
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func openHelp(_ sender: Any) {
    }
    
    @IBAction func rememberMeTapped(_ sender: UIButton) {
        
        sender.isSelected.toggle()
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        let linkRange = (attributedLabel.text! as NSString).range(of: "dini.tv")

        if sender.didTapAttributedTextInLabel(label: attributedLabel, inRange: linkRange) {
            if let url = URL(string: "http://www.dini.tv") {
                let config = SFSafariViewController.Configuration()

                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        }


    }
    
    @IBAction func login(_ sender: UIButton) {
        guard let email = emailtextField.text, email.count > 0,
              let password = passwordtextField.text, password.count > 0
        else {
            ToastHelper.show(text: "Email ou mot de passe invalide")
            return
        }
        
        if rememberMeButton.isSelected {
            DefaultsHelper.saveUserEmail(email)
            DefaultsHelper.saveUserPassword(password)
        } else {
            UserDefaults.standard.removeObject(forKey: DefaultsHelper.userEmailKey)
            UserDefaults.standard.removeObject(forKey: DefaultsHelper.userEmailKey)
        }
        viewModel.login(email: email, password: password, deviceId: DefaultsHelper.deviceId ?? "")
    }
    
    
    
    @IBAction func goToSignup(_ sender: UIButton) {
        self.goToSignupScreen()
    }
    
    
    @IBAction func showHidePassword(_ sender: UIButton) {
        sender.isSelected.toggle()
        passwordtextField.isSecureTextEntry = !sender.isSelected
    }
}

extension LoginViewController: LoginTaskNavigator {
    func goToSignupScreen() {
        let vc = StoryboardScene.Landing.signupViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func displaySimpleAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToMenuScreen() {
        NavigationHelper.shared.startAppFromProfiles()
    }
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
