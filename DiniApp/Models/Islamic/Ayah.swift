//
//  Surah.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/03/2021.
//

import Foundation

// MARK: - Surah
public struct AyahResponse: Codable {
    public let code: Int
    public let status: String
    public let data: DataClass
}

// MARK: - DataClass
public struct DataClass: Codable {
    public let number: Int
    public let name, englishName, englishNameTranslation, revelationType: String
    public let numberOfAyahs: Int
    public let ayahs: [Ayah]
    let edition: Edition
}

// MARK: - Ayah
public struct Ayah: Codable {
    public let number: Int
    public let audio: String?
    public let audioSecondary: [String]?
    public let text: String
    public let numberInSurah, juz, manzil, page: Int
    public let ruku, hizbQuarter: Int
    public let sajda: Bool
    public var isPlaying: Bool?
}

// MARK: - Edition
struct Edition: Codable {
    let identifier, language, name, englishName: String
    let format, type: String
    let direction: JSONNull?
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

// MARK: - AyahFrench
public struct AyahFrenchResponse: Codable {
    public let code: Int
    public let status: String
    public let data: DataClassFrench
}

// MARK: - DataClass
public struct DataClassFrench: Codable {
    public let number: Int
    public let name, englishName, englishNameTranslation, revelationType: String
    public let numberOfAyahs: Int
    public let ayahs: [AyahFrench]
    public let edition: EditionFrench
}

// MARK: - Ayah
public struct AyahFrench: Codable {
    public let number: Int
    public let text: String
    public let numberInSurah, juz, manzil, page: Int
    public let ruku, hizbQuarter: Int
    public let sajda: Bool
}
// MARK: - Edition
public struct EditionFrench: Codable {
    public let identifier, language, name, englishName: String
    public let format, type, direction: String
}
