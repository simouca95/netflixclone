//
//  DetailsViewController.swift
//  DiniTv
//
//  Created by Rached Khoudi on 17/02/2021.
//

import UIKit
import RxSwift

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var viewModel: DetailsViewModel!
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        initViewModel()
    }
    
    func initView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.setCollectionViewLayout(layout, animated: false)
        collectionView.register(cellType: EpsCollectionViewCell.self)
        collectionView.register(cellType: InfoCollectionViewCell.self)
        collectionView.allowsSelection = false
    }
    
    func initViewModel() {
        viewModel.getCollectionDetails()
        viewModel.reloadData.asObserver()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.collectionView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.reloadEpsData.asObserver()
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.collectionView.reloadItems(at: [IndexPath(row: 1, section: 0)])
            })
            .disposed(by: disposeBag)
        
        viewModel.currentVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                guard let self = self else { return }
                self.configurePlayer(video: video)
            })
            .disposed(by: self.disposeBag)
    }
    
}

extension DetailsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let item = viewModel.items[indexPath.row] as? InfoCollectionViewModel {
            let cell: InfoCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.delegate = self
            cell.setupContent(viewModel: item)
            return cell
        } else if let item = viewModel.items[indexPath.row] as? EpsCollectionViewModel {
            let cell: EpsCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
            cell.delegate = self
            cell.setupContent(viewModel: item)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}

extension DetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if viewModel.items[indexPath.row] is InfoCollectionViewModel {
            let width: CGFloat = 400
            let height = CGFloat(collectionView.frame.height)
            return CGSize(width: width, height: height)
        } else if viewModel.items[indexPath.row] is EpsCollectionViewModel {
            let width: CGFloat = (collectionView.frame.width - 400)
            let height = CGFloat(collectionView.frame.height)
            return CGSize(width: width, height: height)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
}

extension DetailsViewController: InfoCollectionDelegate {
    func onClick(row: Int) {
        viewModel.updateEps(row: row)
    }
}

extension DetailsViewController: EpsCollectionDelegate {
    func onClick(resources: [DiniResource], clickedIndex: Int) {
        viewModel.currentResources = resources
        viewModel.currentClickedIndex = clickedIndex
        let resource = resources[clickedIndex]
        if let media = resource.media {
            if media.contains(".mp3") {
            } else {
                viewModel.currentResourceId = resource.id
                viewModel.getMedia(media: media)
            }
        }
    }
}
