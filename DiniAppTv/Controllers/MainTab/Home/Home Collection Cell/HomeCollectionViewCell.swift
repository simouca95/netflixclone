//
//  HomeCollectionViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 16/02/2021.
//

import UIKit
import Reusable

class HomeCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var posterImg: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 6
            self.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func setupContent(item : CollectionListData) {
        
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            self.posterImg.kf.setImage(with: url)
        } else {
            self.posterImg.image = UIImage(named: "ItemEmpty")
        }
    
    }
    
}
