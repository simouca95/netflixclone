//
//  QualityVideoViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 30/12/2021.
//

import Foundation

protocol QualityVideoDelegate: AnyObject {
    func onSpecificQualityTapped(newFileIndex: Int)
}

class QualityVideoViewModel {
    
    var files: [Download]
    var currentFileIndex: Int
    var delegate: QualityVideoDelegate?
    
    init(files: [Download], currentFileIndex: Int) {
        self.files = files
        self.currentFileIndex = currentFileIndex
    }
}
