//
//  AddEditProfileViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 05/12/2021.
//

import RxCocoa

protocol AddEditProfileDelegate: AnyObject {
    func updatedUserInfos()
}

enum ProfilActionType {
    case add
    case edit
}

class AddEditProfileViewModel: BaseViewModel {
    var reloadData = BehaviorRelay<Bool>(value: false)
    var actionType: ProfilActionType
    var profil: Profil?
    var sexDataSource: [ProfilType] = []
    
    init(actionType: ProfilActionType, profil: Profil?) {
        self.actionType = actionType
        self.profil = profil
    }
    
    func getPictureData(picturePath: String?) -> Data? {
        if let baseURL = DefaultsHelper.profilesImagesBaseURL,
           let pitcture = picturePath,
           let svg = URL(string: "\(baseURL)/\(pitcture)"),
           let data = try? Data(contentsOf: svg) {
            return data
        }
        return nil
    }
    
    func getProfilTypes() {
        guard let token = DefaultsHelper.userToken
        else { return }
            isLoading.accept(true)
        ApiCall.Profil.getProfilTypes(token: token, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func addProfil(userId: String, profileTypeId: String, name: String, picture: String) {
        guard let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Profil.addProfil(token: token, userId: userId, typeId: profileTypeId, name: name, avatar: picture, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func editProfil(userId: String, profileTypeId: String, name: String, picture: String) {
        guard let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Profil.updateProfil(token: token, profileId: "\(profil?.id ?? 0)", userId: userId, typeId: profileTypeId, name: name, avatar: picture, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func deleteProfil(userId: String) {
        guard let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Profil.deleteProfil(token: token, profilId: "\(profil?.id ?? 0)", onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension AddEditProfileViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: ProfilTypes?) {
        guard let dataSource = response else { return }
        sexDataSource = dataSource
        reloadData.accept(false)
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: ProfilActionResponse?) {
        guard let _ = response else { return }
        reloadData.accept(true)
        isLoading.accept(false)
    }
    
    
    private func onFail(_ error: DiniError?) {
        print("onFail")
    }
}
