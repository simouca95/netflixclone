//
//  DetailsReelCollectionViewCell.swift
//  DiniApp
//
//  Created by Rached Khoudi on 18/02/2022.
//

import UIKit
import AVKit
import Reusable

protocol feedDataCellDelegate {
    func hideWhenLongPressBegan()
    func showWhenLongPressEnded()
}

class DetailsReelCollectionViewCell: UICollectionViewCell, NibReusable {
    
    // MARK:- PROPERTIES
    
    var data: DiniResource?{
        didSet{
            manageData()
        }
    }
    
    var avQueuePlayer: AVQueuePlayer?
    var avPlayerLayer: AVPlayerLayer?
    var delegate: feedDataCellDelegate?
    
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var detailText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        setUpViews()
        // setUpConstraints()
    }
    
    // MARK:- FUNCTIONS

    
    func setUpConstraints(){
        NSLayoutConstraint.activate([
            playerView.leadingAnchor.constraint(equalTo: playerView.leadingAnchor),
            playerView.trailingAnchor.constraint(equalTo: playerView.trailingAnchor),
            playerView.topAnchor.constraint(equalTo: playerView.topAnchor),
            playerView.bottomAnchor.constraint(equalTo: playerView.bottomAnchor),
        ])
    }
    
    func addPlayer(for url: URL , bounds: CGRect) {
        avQueuePlayer = AVQueuePlayer(url: url)
        avPlayerLayer = AVPlayerLayer(player: self.avQueuePlayer!)
        avPlayerLayer?.frame = bounds
        avPlayerLayer?.fillMode = .both
        avPlayerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.addSublayer(self.avPlayerLayer!)
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer , _ index: IndexPath){
        if gesture.state == .began {
            avQueuePlayer?.pause()
            delegate?.hideWhenLongPressBegan()
//            UIView.animate(withDuration: 0.3) {
//                self.detailView.alpha = 0
//            }
        }
        if gesture.state == .ended {
            avQueuePlayer?.play()
            delegate?.showWhenLongPressEnded()
//            UIView.animate(withDuration: 0.3) {
//                self.detailView.alpha = 1
//            }
        }
    }
    
    func manageData(){
        guard let data = data else {return}
        self.detailText.text = "\(data.speakers?.first?.name ?? "") \(data.title ?? "")"
    }
    
}
