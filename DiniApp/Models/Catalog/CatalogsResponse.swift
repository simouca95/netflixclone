//
//  CatalogsResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 08/12/2021.
//

import Foundation

// MARK: - CatalogsResponse
public struct CatalogsResponse: Codable {
    public let collectionsList: CollectionsList?
    public let collectionsInfos: CollectionsInfos?
}
