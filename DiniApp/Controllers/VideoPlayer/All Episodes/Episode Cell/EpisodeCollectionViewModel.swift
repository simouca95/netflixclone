//
//  EpisodeCollectionViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 28/12/2021.
//

import Foundation

class EpisodeCollectionViewModel {
    
    var title: String?
    var posterPath: String?
    var duration: Int?
    var progress: Float?
    var isNewResource: Bool?
    
    var durationInMinutes = 0
    
    init(title: String?, posterPath: String?, duration: Int?, progress: Int?, isNewResource: Bool?) {
        self.title = title
        self.posterPath = posterPath
        self.duration = duration
        self.isNewResource = isNewResource

        if let d = duration {
            self.durationInMinutes = d/60
            
            if let p = progress {
                self.progress = Float(p)/Float(d)
            }
        }
    }
    
    func getPosterImageURL() -> URL? {
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                             path2: posterPath) {
            return url
        } else {
            return nil
        }
    }
}
