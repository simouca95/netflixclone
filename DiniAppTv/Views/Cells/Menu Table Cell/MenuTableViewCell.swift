//
//  MenuTableViewCell.swift
//  DiniTv
//
//  Created by Rached Khoudi on 16/02/2021.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var underlineView: UIView!
    
    static let identifier = "MenuTableViewCell"
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        
        if let cell = context.nextFocusedView as? MenuTableViewCell {
            cell.titleLbl.font = UIFont.boldSystemFont(ofSize: 30)
            cell.titleLbl.layer.shadowColor = UIColor.white.cgColor
            cell.titleLbl.layer.shadowOffset = .zero
            cell.titleLbl.layer.shadowRadius = 2.0
            cell.titleLbl.layer.shadowOpacity = 1.0
            cell.titleLbl.layer.masksToBounds = false
            cell.titleLbl.layer.shouldRasterize = true
            cell.underlineView.isHidden = false
        }
        
        if let cell = context.previouslyFocusedView as? MenuTableViewCell{
            cell.titleLbl.font = UIFont.systemFont(ofSize: 30)
            cell.titleLbl.layer.shadowColor = UIColor.clear.cgColor
            cell.underlineView.isHidden = true
        }
        
    }
}

