//
//  SpeakersViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 04/04/2021.
//

import UIKit

class SpeakersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var currentSpeaker = UserDefaults.standard.string(forKey: Constants.defaultSpeaker) ?? DataMock.speakers[4].path

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "SpeakerTableViewCell", bundle: nil), forCellReuseIdentifier: "SpeakerTableViewCell")
    }
    
}

extension SpeakersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataMock.speakers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpeakerTableViewCell") as! SpeakerTableViewCell
        cell.speakerNumberLabel.text = "\(indexPath.row + 1)"
        cell.speakerNameLabel.text = DataMock.speakers[indexPath.row].name
        
        if DataMock.speakers[indexPath.row].path == self.currentSpeaker {
            cell.setSelected(true, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.setValue(DataMock.speakers[indexPath.row].path, forKey: Constants.defaultSpeaker)
        self.navigationController?.popViewController(animated: true)
    }
}
