//
//  PrayHours.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/03/2021.
//

import Foundation

// MARK: - ResponsePrayHours
public struct PrayHoursResponse: Codable {
    public let code: Int
    public let status: String
    public let results: Results
}

// MARK: - Results
public struct Results: Codable {
    public let datetime: [Datetime]
    public let location: Location
    public let settings: Settings
}

// MARK: - Datetime
public struct Datetime: Codable {
    public let times: Times
    public let date: DateClass
}

// MARK: - DateClass
public struct DateClass: Codable {
    public let timestamp: Int
    public let gregorian, hijri: String
}

// MARK: - Times
public struct Times: Codable {
    public let imsak, sunrise, fajr, dhuhr: String
    public let asr, sunset, maghrib, isha: String
    public let midnight: String

    enum CodingKeys: String, CodingKey {
        case imsak = "Imsak"
        case sunrise = "Sunrise"
        case fajr = "Fajr"
        case dhuhr = "Dhuhr"
        case asr = "Asr"
        case sunset = "Sunset"
        case maghrib = "Maghrib"
        case isha = "Isha"
        case midnight = "Midnight"
    }
}

// MARK: - Location
public struct Location: Codable {
    public let latitude, longitude: Double
    public let elevation: Int
    public let city, country, countryCode, timezone: String
    public let localOffset: Int

    enum CodingKeys: String, CodingKey {
        case latitude, longitude, elevation, city, country
        case countryCode = "country_code"
        case timezone
        case localOffset = "local_offset"
    }
}

// MARK: - Settings
public struct Settings: Codable {
    public let timeformat, school, juristic, highlat: String
    public let fajrAngle, ishaAngle: Int

    enum CodingKeys: String, CodingKey {
        case timeformat, school, juristic, highlat
        case fajrAngle = "fajr_angle"
        case ishaAngle = "isha_angle"
    }
}
