//
//  DetailsViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import RxSwift

enum CollectionType: Int {
    case episode = 0
    case season = 1
    case manySeasons = 2
    case audio = 4
}

protocol DetailsDelegate: AnyObject {
    func onInfoStateChanged()
}

class DetailsViewModel: BaseViewModel {
    
    var cellData: CollectionListData?
    var collectionId: Int
    var seasonId: Int?
    var items:[BaseCellViewModel] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    var currentVideo = BehaviorSubject<VideoResponse?>(value: nil)
    var backToHome = BehaviorSubject<Void>(value: ())

    
    var seasonSelected = 0
    var infoState:[CollectionInfoState] = []
    var viewdInfos: [CollectionViewedsInfo] = []
    var isMedia = false
    //TO DO enhance logic
    var infoStateToBeUpdatedNow: CollectionInfoState? = nil
    
    
    private var resourcesFirstId: String?
    weak var delegate: DetailsDelegate?
    
    //When user click an item, we pass this data to VideoPlayVC
    var currentClickedIndex:Int?
    var currentResources:[DiniResource] = []
    
    var currentResourceId: Int?
    var availableResources: [DiniResource]? {
        return getAvailableResources(resources: cellData?.resources) ?? nil
    }
    
    var mediaToBeDownloaded: String?
    var media = ""
    
    init(collectionId: Int, seasonId: Int? = nil, infoState: [CollectionInfoState], delegate: DetailsDelegate?) {
        self.collectionId = collectionId
        self.seasonId = seasonId
        self.infoState = infoState
        self.delegate = delegate
    }
    
    func updateItems() {
        guard let cellData = cellData,
              let typeIndex = cellData.type,
              var type = CollectionType(rawValue: typeIndex)
        else { return }
        
        items.removeAll()
        
        //ShowTableViewModel
        let availableResources = getAvailableResources(resources: cellData.resources)
        if let id = cellData.id {
            let show = ShowTableViewModel(cellHeight: 330,
                                          trailerPath: cellData.trailer,
                                          posterPath: cellData.picture,
                                          collectionId: id,
                                          resource: availableResources?.first,
                                          infoState: infoState)
            items.append(show)
        }
        
        //InfosTableViewModel
        var infoCellHeight: CGFloat
        var infosTitle = ""
        var infosSpeakers = ""
        var infosCategory = ""
        var infosTags = ""
        var contentNumber: Int = 0
        
        if let seasonId = seasonId,
           let seasons = cellData.seasons,
           let index = seasons.firstIndex(where: {$0.id == seasonId}),
           let season = seasons[safe: index],
           let resources = getAvailableResources(resources: season.resources) {
            type = .season
            infoCellHeight = 75 + 40 + 40
            contentNumber = resources.count
        } else  {
            switch type {
            case CollectionType.episode, CollectionType.season:
                infoCellHeight = 75 + 40 + 40
                if let resources = availableResources?.count {
                    contentNumber = resources
                }
            case CollectionType.manySeasons:
                infoCellHeight = 75 + 40 + 40
                if let seasons = cellData.seasons?.count {
                    contentNumber = seasons
                }
            case CollectionType.audio:
                infoCellHeight = 75 + 40 + 40
                if let resources = availableResources?.count {
                    contentNumber = resources
                }
            }
        }
        
        if let title = cellData.title { infosTitle = title }
        
        if let resources = availableResources {
            let resourcesSpeakers = resources.compactMap{$0.speakers}.flatMap{$0}
            let speakersName = Set(resourcesSpeakers.compactMap{$0.name})
            infosSpeakers = speakersName.joined(separator: ",")
        }
        
        if let category = cellData.category?.name { infosCategory = category }

        if let tags = cellData.tags {
            let tagsName = tags.compactMap{$0.name}
            infosTags = tagsName.joined(separator: ",")
        }
        
        let infos = InfosTableViewModel(cellHeight: infoCellHeight, type: type, title: infosTitle,
                                        speakers: infosSpeakers, category: infosCategory,
                                        tags: infosTags, contentNumber: contentNumber)
        items.append(infos)
        
        //ResourcesTableViewModel

        switch type {
        case CollectionType.episode, CollectionType.season:
            // get season index for seaons home cell type
            var resources: [DiniResource]?
            if let seasonId = seasonId, let seasons = cellData.seasons, let index = seasons.firstIndex(where: {$0.id == seasonId}), let season = seasons[safe: index] {
                resources = season.resources
                
            } else {
                resources = cellData.resources
            }
            
            if let availableResources = getAvailableResources(resources: resources) {
                let resourcesVM = ResourcesTableViewModel(
                    cellHeight: CGFloat(140 * availableResources.count),
                    type: type,
                    resources: availableResources)
                items.append(resourcesVM)
            }
        case CollectionType.manySeasons:
            if let seasons = cellData.seasons,
               let season = seasons[safe: seasonSelected],
               let resources = getAvailableResources(resources: season.resources) {
                let resources = ResourcesTableViewModel(
                    cellHeight: CGFloat(140 * resources.count) + 70,
                    type: type,
                    resources: resources,
                    seasons: seasons,
                    selectedSeason: seasonSelected)
                items.append(resources)
            }
        case CollectionType.audio:
            if let resources = getAvailableResources(resources:cellData.resources) {
                let resources = ResourcesTableViewModel(
                    cellHeight: CGFloat(140 * resources.count),
                    type: type,
                    resources: resources)
                items.append(resources)
            }
        }
        
        reloadData.onNext(())
    }
    
    private func getAvailableResources(resources: [DiniResource]?) -> [DiniResource]? {
        let resources = resources?.filter({ resource in
            let dateFormatter = ISO8601DateFormatter()
            if let publishedAtString = resource.publishedAt,
               let date = dateFormatter.date(from:publishedAtString),
               let isEnabled = resource.enabled {
                if date < Date(), isEnabled {
                    return true
                }
                return false
            }
            return true
        })
        return resources
    }

    func getCollectionDetails() {
        isLoading.accept(true)
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken else { return }
        ApiCall.Details.getCollectionViewsInfo(token: token, profileId: profileId, id: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getMedia(media: String) {
        self.media = media
        checkNumberOfuserWatching()
        isMedia = true
    }
    
    func checkNumberOfuserWatching() {
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken,
              let userId = DefaultsHelper.userId
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getNumberOfUserWatching(token: token, profileId: profileId, userId: userId, onSuccess: self.onSuccess, onError: self.onFail)
    }

    func getCollectionWatch(collectionId id: Int) {
        isLoading.accept(true)
        checkNumberOfuserWatching()
    }
    
    func setInfoState(info: CollectionInfoState) {
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        infoStateToBeUpdatedNow = info
        ApiCall.Details.setUnsetInfo(token: token, infoState: info.rawValue, collectionId: collectionId, profileId: profileId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DetailsViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CollectionListData?) {
        isLoading.accept(false)
        guard let response = response else { return }
        cellData = response
        updateItems()
    }

    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        currentVideo.onNext(response)
    }
    
    private func onSuccess(_ response: CollectionWatchResponse?) {
        if let media = response?.media {
            isLoading.accept(true)
            currentResourceId = Int(response?.resourceID ?? "")
            getMedia(media: media)
        }
    }
    
    private func onSuccess(_ response: SetInfoResponse?) {
        if let response = response,
           let status = response.status, status,
           let result = response.result,
           let showViewModel = items[exist: 0] as? ShowTableViewModel,
           let updateDelegate = showViewModel.updateShowDelegate {
            //add means Fav, delete means not Fav
            switch infoStateToBeUpdatedNow {
            case .fav:
                updateDelegate.updateFav(isFav: result == .add)
            case .liked:
                updateDelegate.updateLiked(isLiked: result == .add)
            case .disliked:
                updateDelegate.updateDisliked(isDisliked: result == .add)
            case .none:
                print("nothing happens")
            }
        }
    }
    
    private func onSuccess(_ response: ResourceWatchResponse?) {
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken,
        let tokenVimeo = DefaultsHelper.vimeoToken else { return }
        if !(response?.isSessionNumberAttempt ?? false)  {
            if isMedia {
                ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
            } else {
                ApiCall.Video.getCollectionWatch(token: token, profileId: profileId, collectionId: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
            }
        } else {
            backToHome.onNext(())
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
