//
//  CollectionInfoResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import Foundation

public typealias CollectionInfoResponse = [CollectionListData]
