//
//  MyListViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/26/20.
//

//import UIKit
//
//class MyListViewController: BaseViewController {
//
//    @IBOutlet weak var myListcollectionView: UICollectionView!
//
//    
//    var myList = [Video]()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        
//        registerCell()
//
//        myList = DataMock.instance.videos0
//    }
//    
//
//    func registerCell() {
//    
//        self.myListcollectionView.register(UINib(nibName: "NormalSectionCollectionViewCell", bundle: nil),
//                                     forCellWithReuseIdentifier: NormalSectionCollectionViewCell.identifier)
//        
//        self.myListcollectionView.delegate = self
//        self.myListcollectionView.dataSource = self
//    }
//
//}
//extension MyListViewController : UICollectionViewDelegateFlowLayout{
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 100, height: 130)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
//}
//
//extension MyListViewController : UICollectionViewDataSource{
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.myList.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        
//        
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NormalSectionCollectionViewCell.identifier, for: indexPath) as! NormalSectionCollectionViewCell
//        
//        cell.setupContent(video: self.myList[indexPath.item])
//        
//        return cell
//        
//    }
//    
//    
//}
