//
//  ViewController.swift
//  BookItUp
//
//  Created by sami hazel on 11/9/20.
//  Copyright © 2020 Barsha Technology. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var loadingHelper: LoadingHelper!
    var baseViewModel = BaseViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingHelper = LoadingHelper(self)
        self.view.bringSubviewToFront(loadingHelper.loading)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func back(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoTapped(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func profilTapped(_ sender: UIButton) {
        
        let vc = StoryboardScene.Profil.selectProfileViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func moviesSectionTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func seriesSectionTapped(_ sender: UIButton) {
    }
    
    @IBAction func categoryTapped(_ sender: UIButton) {
    }
    
    @IBAction func subCategoryTapped(_ sender: UIButton) {
    }
}


