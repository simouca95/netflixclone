//
//  PrayViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 17/03/2021.
//

import UIKit
import Alamofire
import CoreLocation
import SVProgressHUD
import DropDown

class PrayViewController: BaseViewController {

    @IBOutlet weak var fajrLabel: UILabel!
    @IBOutlet weak var dhuhrLabel: UILabel!
    @IBOutlet weak var asrLabel: UILabel!
    @IBOutlet weak var maghrebLabel: UILabel!
    @IBOutlet weak var ishaLabel: UILabel!
    @IBOutlet weak var yearGregorianLabel: UILabel!
    @IBOutlet weak var yearHijriLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var hoursView: UIView!
    @IBOutlet weak var cityDropDownView: UIView!
    @IBOutlet weak var cityDropDownLabel: UILabel!
    
    private let manager = CLLocationManager()
    private var cityDropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.manager.delegate = self
        self.manager.requestWhenInUseAuthorization()
        self.manager.requestLocation()
        self.hoursView.isHidden = true
        
        setupDropDown()
    }
    
    func setupDropDown() {
        self.cityDropDown.anchorView = self.cityDropDownView
        self.cityDropDown.dataSource = DataMock.cities.map{ $0.name }
        self.cityDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.cityDropDownLabel.text = item
            if let path = DataMock.cities.filter({ $0.name == item }).first?.path {
                ApiCall.Islamic.prayHours(city: path, onSuccess: self.onSuccess, onError: self.onFail)
                SVProgressHUD.show()
            }
        }
        
        let cityTap = UITapGestureRecognizer(target: self, action: #selector(self.cityHandleTap(_:)))
        self.cityDropDownView.addGestureRecognizer(cityTap)
    }

    @objc func cityHandleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.cityDropDown.show()
    }
}


extension PrayViewController: CLLocationManagerDelegate {
    
    func locationManager( _ manager: CLLocationManager, didUpdateLocations locations: [CLLocation] ) {
        if let _ = locations.first {
//            let longitude = location.coordinate.longitude
//            let latitude = location.coordinate.latitude

//            print("long : \(location.coordinate.longitude) | lat : \(location.coordinate.latitude)")
            self.cityDropDownLabel.text = DataMock.cities.first?.path ?? ""
            ApiCall.Islamic.prayHours(city: DataMock.cities.first?.path ?? "", onSuccess: self.onSuccess, onError: self.onFail)
            SVProgressHUD.show()
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}


extension PrayViewController {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: PrayHoursResponse?) {
        hoursView.isHidden = false
        SVProgressHUD.dismiss()
        if let response = response, response.code == 200,
           let times = response.results.datetime.first?.times {
            self.fajrLabel.text = times.fajr
            self.dhuhrLabel.text = times.dhuhr
            self.asrLabel.text = times.asr
            self.maghrebLabel.text = times.maghrib
            self.ishaLabel.text = times.isha
        }
        
        if let response = response, response.code == 200,
           let gregorian = response.results.datetime.first?.date.gregorian,
           let hijri = response.results.datetime.first?.date.hijri {
            self.yearGregorianLabel.text = gregorian
            self.yearHijriLabel.text = hijri
        }
        
        if let response = response, response.code == 200 {
            self.cityLabel.text = response.results.location.city
        }
    }
    
    private func onFail(_ error: DiniError?) {
        SVProgressHUD.dismiss()
    }
}
