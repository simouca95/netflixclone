//
//  SpeakerTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import UIKit

class SpeakerIntroViewModel: BaseCellViewModel {
    var title: String
    var description: String
    var imagePath: String
    
    init(cellHeight: CGFloat, title:String, description: String, imagePath: String) {
        self.title = title
        self.description = description
        self.imagePath = imagePath
        super.init(cellHeight: cellHeight)
    }
}
