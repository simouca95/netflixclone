//
//  OfflineVideoListViewController.swift
//  DiniApp
//
//  Created by sami hazel on 25/02/2022.
//

import UIKit
import RxSwift
import SVGKit
import CoreData

class OfflineVideoListViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var headerBackground: UIView!
    @IBOutlet weak var logoButton: UIButton!
    
    var disposeBag = DisposeBag()
    var viewModel: OfflineVideoListViewModel!
    
    var currentResourceTitle = ""
    var currentResourceId: Int?
    var timeCodeFromViewed: Int?

    internal static func instantiate(with viewModel: OfflineVideoListViewModel) -> OfflineVideoListViewController {
        let vc = StoryboardScene.Main.offlineVideoListViewController.instantiate()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initViewModel()
    }
    
    private func manageLogo() {
        if DefaultsHelper.nameToDisplayInSwitchProfileDefault == "Section tout public" {
            if let logoPath = URL(string: DefaultsHelper.logoChild ?? "") {
                logoButton.getURL2(url: logoPath)
            }
        } else {
            if let logoPath = URL(string: DefaultsHelper.logo ?? "") {
                logoButton.getURL2(url: logoPath)
            }
        }
    }
    
    
    func initViews() {

        tableView.register(cellType: ResourceTableViewCell.self)
        
        self.headerBackground.alpha = 0.7

        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
    }
    
    func initViewModel() {
        viewModel = OfflineVideoListViewModel()
        viewModel.getCollectionList()
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.reloadData.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext: { [weak self] _ in
                guard let self = self else { return }
                self.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getCollectionList()
        manageLogo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    private func configurePersistedPlayer(video: VideoResponse) {
        let vc = StoryboardScene.Video.videoPlayViewController.instantiate()
        let vm = VideoPlayViewModel(video: video, type: .episode, media: video.media, subtitles: nil)
        vm.delegate = self
        vc.viewModel = vm
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func configurePersistedAudioPlayer(audio: VideoResponse) {
        guard let uri = audio.uri
        else { return }
        
        let dir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        let dataPath = dir.appendingPathComponent("DownloadedData")
        let destinationURLForFile = dataPath.appendingPathComponent(uri)
        let localResource = LocalAudio(localURL: destinationURLForFile,
                                       picture: audio.localPicture ?? "",
                                       name: audio.name ?? "",
                                       timeCodeFromViewed: 0)
        let vc = StoryboardScene.Details.detailsAudioViewController.instantiate()
        vc.localAudio = localResource
        vc.viewModel.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }

    private func didSelectItemToDelete(resource: VideoResponse) {
        
        let title = resource.name ?? "Untitled"
        let alert = UIAlertController(title: "Dini", message: "Voulez-vous supprimer l'épisode : \(title)", preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "OUI", style: .default) { _ in
            
            
            if let media = resource.media {
                
                if let videos = PersistenceHelper.shared.retriveLocalMedia(),
                   let videoItems = videos as? [DiniVideo],
                   let videoItem = videoItems.filter({ $0.media == media }).first,
                   let uri = videoItem.uri {
                    print(videoItem)
                                        
                    let dir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                    let dataPath = dir.appendingPathComponent("DownloadedData")
                    let destinationURLForFile = dataPath.appendingPathComponent(uri)
                    
                    do {
                        let fileNames = try FileManager.default.contentsOfDirectory(atPath: "\(dataPath.path)")
                        print("all files in cache: \(fileNames)")
                        for fileName in fileNames {
                            
                            if (fileName == uri) {
                                let filePathName = "\(destinationURLForFile.path)"
                                if PersistenceHelper.shared.delete(media: media) {
                                    try FileManager.default.removeItem(atPath: filePathName)
                                }
                                DispatchQueue.main.async {
                                    self.viewModel.getCollectionList()
                                }
                            }
                        }
                        
                        let files = try FileManager.default.contentsOfDirectory(atPath: "\(dataPath.path)")
                        print("all files in cache after deleting images: \(files)")
                        
                        
                    } catch {
                        print("Could not clear temp folder: \(error)")
                    }
                }
                
            }
            
            
        }
        
        alert.addAction(action)
        alert.addAction(UIAlertAction(title: "Non", style: .cancel))
        self.present(alert, animated: true, completion: nil)
    }
}


extension OfflineVideoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.items[indexPath.row]
        let resourceTableViewModel = ResourceTableViewModel(cellHeight: 140,
                                                             title: item.name ?? "unknown",
                                                            posterPath: item.localPicture,
                                                            duration: item.localDuration,
                                                             progress: nil,
                                                            media: item.media,
                                                            isOffline: true, isNewResource: false)
        let cell: ResourceTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.viewModel = resourceTableViewModel
        
        cell.onDeleteTapped = { [weak self] in
            guard let self = self else { return }
            self.didSelectItemToDelete(resource: item)
        }
        return cell
        
    }
}

extension OfflineVideoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 140
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.items[indexPath.row]
        
        if let uri = item.uri, uri.contains("mp3") {
            self.configurePersistedAudioPlayer(audio: item)
        } else {
            self.configurePersistedPlayer(video: item)
        }
    }
}


extension OfflineVideoListViewController: AVPlayerDelegate {
    func onSuccessTimeCodeSend() {
        viewModel.getCollectionList()
    }
    
    func onFailTimeCodeSend() {
        print("CustomAVPlayerDelegate NOT WORKING")
    }
}

