//
//  API+Login.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

import Foundation
import Alamofire

extension API {
    enum AuthController {
        case auth(email: String, password: String, deviceId: String)
        case signup(email: String, firstname: String, lastname: String, phoneNumber: String)
        case login
        case avatarPictureList(token: String)
        case userInfos(token: String, id: Int)
        case authorizeNotification(token: String, deviceId: String, userId: Int, isNotificationAccepted: String)
        case getAppVersion
        case restartSessionCounter(token: String, userId: Int)
        case logout(token:String, userId: Int, sessionToken: String)
        case getUserLegalInfo(token: String)
    }
}

extension API.AuthController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .auth:
            return .post
        case .login:
            return .get
        case .avatarPictureList:
            return .get
        case .userInfos:
            return .get
        case .signup:
            return .post
        case .authorizeNotification:
            return .post
        case .getAppVersion:
            return .get
        case .restartSessionCounter:
            return .get
        case .logout:
            return .post
        case .getUserLegalInfo :
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .auth:
            return "/auth"
        case .login:
            return "/login"
        case .avatarPictureList :
            return "/avatarPictureList"
        case .userInfos(_, let id) :
            return "/users/\(id)"
        case .signup:
            return "/registermobile?token=\(Environment.Api.signupToken)"
        case .authorizeNotification:
            return "/updateNotificationAcceptation"
        case .getAppVersion:
            return "/controlVersion"
        case .restartSessionCounter(_, let userId):
            return "/restartSessionCounter?userId=\(userId)"
        case .logout:
            return "/logout"
        case .getUserLegalInfo:
            return "/getUserLegalInfos"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .auth(let email, let password, let deviceId):
            return ["email": email, "password": password,"deviceId": deviceId]
        case .login, .avatarPictureList, .userInfos:
            return nil
        case .authorizeNotification(_,let deviceId, let userId, let isNotificationAccepted):
            return ["deviceId": deviceId, "userId": userId, "isNotificationAccepted": isNotificationAccepted]
        case .signup(let email, let firstname, let lastname, let phoneNumber):
            return ["email": email, "firstname": firstname, "lastname": lastname, "phone": phoneNumber]
        case .getAppVersion:
            return nil
        case .restartSessionCounter:
            return nil
        case .logout(_, userId: let userId, sessionToken: let sessionToken):
            return ["userId": userId, "sessionToken": sessionToken]
        case .getUserLegalInfo(_):
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .auth, .login, .signup, .getAppVersion:
            return Network.unsecureHeaders
        case .avatarPictureList(let token), .userInfos(let token, _), .authorizeNotification(let token,_,_,_), .restartSessionCounter(let token,_), .getUserLegalInfo(let token) :
            return Network.secureHeaders(token: token)
        case .logout(token: let token,_,_):
            return Network.secureHeaders(token: token)
        }
    }
}
