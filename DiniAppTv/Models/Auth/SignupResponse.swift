//
//  SignupResponse.swift
//  netflixCloneApp
//
//  Created by sami hazel on 13/03/2022.
//

import Foundation

// MARK: - User
public struct SignupResponse: Codable {
    public let message: String?
    public let status: Bool?


    enum CodingKeys: String, CodingKey {
        case status
        case message
    }
}

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
