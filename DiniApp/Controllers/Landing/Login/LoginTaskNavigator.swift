//
//  LoginTaskNavigator.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 13/09/2021.
//

protocol LoginTaskNavigator: AnyObject {
    func displaySimpleAlert(title: String, message: String)
    func goToMenuScreen()
    func goToSignupScreen()
}
