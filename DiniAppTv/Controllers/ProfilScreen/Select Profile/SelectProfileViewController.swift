//
//  SelectProfileViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import RxSwift
import SVGKit
import SideNavigationController

class SelectProfileViewController: UIViewController {
    
    @IBOutlet weak var profilesCollectionView: UICollectionView!
    @IBOutlet weak var collectionWidthConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var editProfileButton: UIButton!
    //    @IBOutlet weak var finishEditingButton: UIButton!
    //    @IBOutlet weak var backButton: UIButton!
    //    @IBOutlet weak var addProfilButton: UIButton!
    //    @IBOutlet weak var logoutButton: UIButton!
    
    var viewModel: SelectProfileViewModel!
    private var disposeBag = DisposeBag()
    private lazy var spinnerView = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initViews()
        self.initViewModels()
    }
    
    func initViews() {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical
//        layout.minimumLineSpacing = 10
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//
//        profilesCollectionView.setCollectionViewLayout(layout, animated: false)
        
        profilesCollectionView.register(cellType: ProfileCollectionViewCell.self)
        //        logoutButton.backgroundColor = .primary
        //        if let _ = DefaultsHelper.profilId {
        //            backButton.isHidden = true
        //            logoutButton.isHidden = false
        //        } else {
        //            backButton.isHidden = false
        //            logoutButton.isHidden = true
        //        }
    }
    
    func initViewModels() {
        viewModel = SelectProfileViewModel()
        viewModel.userInfos()
        viewModel.reloadData.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe (onNext:{[weak self] value in
                guard let self = self else { return }
                let itemsWidth = CGFloat(self.viewModel.profils.count * 285)
                let spaceWidth = CGFloat((self.viewModel.profils.count-1) * 30)
                self.collectionWidthConstraint.constant = itemsWidth + spaceWidth
                self.profilesCollectionView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .compactMap({ $0 })
            .subscribe(onNext: { [weak self] bool in
                guard let self = self else { return }
                if bool {
                    self.addChild(self.spinnerView)
                    self.spinnerView.view.frame = self.view.frame
                    self.view.addSubview(self.spinnerView.view)
                    self.spinnerView.didMove(toParent: self)
                } else {
                    self.spinnerView.willMove(toParent: nil)
                    self.spinnerView.view.removeFromSuperview()
                    self.spinnerView.removeFromParent()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.isEditingProfiles
            .asObservable()
            .skip(1)
            .compactMap{ $0 }
            .distinctUntilChanged()
            .subscribe (onNext: { [weak self] bool in
                guard let self = self else { return }
                //                if DefaultsHelper.profilId == nil {
                //                    self.backButton.isHidden = bool
                //                }
                //                self.finishEditingButton.isHidden = !bool
                //                self.addProfilButton.isHidden = !bool
                //                self.editProfileButton.isHidden = bool
                self.profilesCollectionView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    @IBAction func didTapEditProfile(_ sender: UIButton) {
        //        viewModel.isEditingProfiles.accept(true)
    }
    
    @IBAction func didTapEndingEditProfile(_ sender: UIButton) {
        //        viewModel.isEditingProfiles.accept(false)
    }
    
    @IBAction func didTapAddProfil(_ sender: Any) {
        //        if let maxProfilsCount = DefaultsHelper.maxProfilsInApplication,
        //           let currentProfilsCount = DefaultsHelper.currentprofilListCount,
        //           maxProfilsCount > currentProfilsCount {
        //            let vc = StoryboardScene.Profil.addEditProfileViewController.instantiate()
        //            vc.delegate = self
        //            vc.profil = nil
        //            vc.actionType = .add
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        } else {
        //            ToastHelper.show(text: "Vous atteignez le nombre maximum de profils")
        //        }
    }
    
    @IBAction func didTapLogoutProfil(_ sender: Any) {
        DefaultsHelper.deleteAllDefaults()
        //        NavigationHelper.shared.startAppFromLogin()
    }
}
extension SelectProfileViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = 250
        return CGSize(width: width, height: width+30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
extension SelectProfileViewController : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.profils.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = viewModel.profils[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileCollectionViewCell.reuseIdentifier, for: indexPath) as! ProfileCollectionViewCell
        cell.viewModel = ProfileViewModel(profil: item, isEdit: viewModel.isEditingProfiles.value)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //        if viewModel.isEditingProfiles.value {
        //            let item = viewModel.profils[indexPath.row]
        //            let vc = StoryboardScene.Profil.addEditProfileViewController.instantiate()
        //            vc.delegate = self
        //            vc.profil = item
        //            vc.actionType = .edit
        //            self.navigationController?.pushViewController(vc, animated: true)
        //        } else {
        if let path = viewModel.profils[indexPath.row].picture {
            DefaultsHelper.saveProfilImage(path)
        }
        guard let id = viewModel.profils[indexPath.row].id else { return }
        DefaultsHelper.saveProfilId(id)
        NavigationHelper.shared.startAppFromHome()
        //        }
    }
}

//extension SelectProfileViewController: AddEditProfileDelegate {
//    func updatedUserInfos() {
//        viewModel.isEditingProfiles.accept(false)
//        viewModel.userInfos()
//    }
//}
