//
//  CatalogViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/10/2021.
//

import UIKit
import RxSwift
import Kingfisher
import SVGKit

class CatalogViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerBackground: UIView!
    @IBOutlet weak var profileButton: UIButton!

    var viewModel: CatalogViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        initViewModels()
    }
    
    func initViews() {
        tableView.separatorStyle = .none
        tableView.register(cellType: SimpleTableViewCell.self)
        headerBackground.alpha = 0.8
        
        if let pitcture = DefaultsHelper.profilImage,
           pitcture.contains(".svg"),
           let svg = URL(string: pitcture),
           let data = try? Data(contentsOf: svg) {
            let receivedimage: SVGKImage = SVGKImage(data: data)
            profileButton.setImage(receivedimage.uiImage, for: .normal)
        } else {
            if let avartarPath = URL(string: DefaultsHelper.profilImage ?? "") {
                profileButton.getURL2(url: avartarPath)
            } else {
                profileButton.setImage(UIImage(named: "netflix_profil_image"), for: .normal)
            }
        }
    }
    
    func initViewModels() {
        viewModel = CatalogViewModel()
        viewModel.getCollectionList()
        viewModel.reloadData.asObservable()
            .subscribe (onNext:{ [weak self] value in
                guard let self = self else { return }
                self.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
        
        viewModel.isLoading.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
    }
}

extension CatalogViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SimpleTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        let item = viewModel.items[indexPath.row]
        
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.dataResourcesBaseURL,
                                             path2: item.sliderPicture) {
            cell.posterImageView.kf.setImage(with: url)
        } else {
            cell.posterImageView.image = UIImage(named: "ItemEmpty")
        }
        
        cell.posterImageView.layer.cornerRadius = 10
        cell.posterImageView.clipsToBounds = true
        cell.posterImageView.layer.masksToBounds = true
        return cell
    }
    
}

extension CatalogViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = viewModel.items[indexPath.row]
        
        if let id = item.id {
            let infoState = self.viewModel.getCollectionInfoState(collectionId: id)
            let vm = DetailsViewModel(collectionId: id, infoState: infoState, delegate: self)
            let vc = DetailsViewController.instantiate(with: vm)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.width/1.77
    }
}

extension CatalogViewController: DetailsDelegate {
    func onInfoStateChanged() {
        viewModel.getCollectionList()
    }
}
