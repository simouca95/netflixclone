//
//  ApiCall+Home.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Home {
        
        public static func getHomeCollectionList(token: String, id: Int, onSuccess: @escaping (_ response: CollectionResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.HomeController.getCollectionList(token: token, profileId: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: CollectionResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
    }

}
