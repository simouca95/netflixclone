//
//  SpeakersViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import RxSwift

class DiniSpeakersViewModel: BaseViewModel {
    
    var cellData: CollectionListData?
    var speakerId: Int
    var items:[BaseCellViewModel] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    
    init(speakerId: Int) {
        self.speakerId = speakerId
    }
    
    func updateItems(speaker: CollectionsListSpeaker, resources: [DiniSlider]) {
        items.removeAll()
        if let title = speaker.title,
           let description = speaker.speakerDescription,
           let imagePath = speaker.picture {
            items.append(SpeakerIntroViewModel(cellHeight: 500, title: title, description: description, imagePath: imagePath))
        }
        
        for resource in resources {
            var isNewResource = false
            for res in resource.resources ?? [] {
                if res.isNewResource == true {
                    isNewResource = true
                    break
                }
            }
            
            if let picture = resource.picture {
                items.append(SpeakerResourceTableViewModel(cellHeight: 220, path: picture, isNewResource: isNewResource))
            }
        }

        reloadData.onNext(())
    }
    
    func getSpeakerCollectionWatch() {
        guard let profileId = DefaultsHelper.profilId,
        let token = DefaultsHelper.userToken else { return }
        isLoading.accept(true)
        ApiCall.Query.getSpeakerCollectionList(token: token, profileid: profileId, id: speakerId, onSuccess: self.onSuccess, onError: self.onFail)
    }
}

extension DiniSpeakersViewModel {
    private func onSuccess(_ response: QueryCollectionResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        if let speaker = response.collectionsInfos?.speakerList?.first?.filter({ $0.id == speakerId }).first,
           let resources = response.collectionsList?.slider?.first{
            updateItems(speaker: speaker, resources: resources)
        }
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
