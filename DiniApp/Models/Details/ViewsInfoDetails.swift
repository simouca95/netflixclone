//
//  ViewsInfoDetails.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 10/11/2021.
//

import Foundation

public typealias CollectionInfoResponse = [CollectionListData]
