//
//  LoadingHelper.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 19/09/2021.
//

import UIKit
import NVActivityIndicatorView

struct LoadingHelper {
    
    var loading: NVActivityIndicatorView!
    
    init(_ viewController: UIViewController) {
        loading = NVActivityIndicatorView(frame: .zero, type: .circleStrokeSpin, color: UIColor(rgb: 0x7DA463), padding: 0)
        viewController.view.addSubview(loading)
        loading.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loading.widthAnchor.constraint(equalToConstant: 40),
            loading.heightAnchor.constraint(equalToConstant: 40),
            loading.centerYAnchor.constraint(equalTo: viewController.view.centerYAnchor),
            loading.centerXAnchor.constraint(equalTo: viewController.view.centerXAnchor)
        ])
    }
    
    func start() {
        loading.startAnimating()
    }
    
    func stop() {
        loading.stopAnimating()
    }
}
