//
//  DetailsViewController+Video.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/09/2021.
//

import AVKit
import Photos

extension DetailsViewController {

    func configureNetworkedPlayer(video: VideoResponse) {
        guard let typeIndex = viewModel?.cellData?.type,
              let type = CollectionType(rawValue: typeIndex),
              let resourceId = self.viewModel?.currentResourceId,
              let resources = viewModel?.availableResources
        else { return }
        
        let vc = StoryboardScene.Video.videoPlayViewController.instantiate()
        let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel?.cellData?.seasons)
        vm.delegate = self
        vc.viewModel = vm
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func configurePersistedPlayer(video: VideoResponse, media: String) {
        guard let typeIndex = viewModel?.cellData?.type,
              let type = CollectionType(rawValue: typeIndex),
              let resource = viewModel.availableResources?.filter({ $0.media == media }).first,
              let subtitles = resource.resourceSubtitles
        else { return }
        
        let vc = StoryboardScene.Video.videoPlayViewController.instantiate()
        let vm = VideoPlayViewModel(video: video, type: type, media: media, subtitles: subtitles)
        vm.delegate = self
        vc.viewModel = vm
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
}

extension DetailsViewController: AVPlayerDelegate {
    func onSuccessTimeCodeSend() {
        viewModel.getCollectionDetails()
    }
    
    func onFailTimeCodeSend() {
        print("CustomAVPlayerDelegate NOT WORKING")
    }
}
