//
//  SelectProfileViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 13/09/2021.
//

import RxSwift
import RxCocoa

class SelectProfileViewModel: BaseViewModel {
    
    var reloadData = BehaviorSubject<Void>(value: ())
    var profils: [Profil] = []
    var pathsList: [[String]] = []
    var profilsPathList: [(id: String, path: String?)] = []
    var isEditingProfiles: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    
    func userInfos() {
        if let id = DefaultsHelper.userId,
           let token = DefaultsHelper.userToken {
            isLoading.accept(true)
            ApiCall.Auth.userInfos(token: token, id: id, onSuccess: self.onSuccess, onError: self.onFail)
        }
    }
}

extension SelectProfileViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: UserResponse?) {
        guard let response = response, let profils = response.profils else {
            isLoading.accept(false)
            return }
        
        self.profils = profils
        response.avatarPicturePath
        reloadData.onNext(())
        isLoading.accept(false)
    }
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
}
