//
//  API+Details.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import Alamofire

extension API {
    enum DetailsController {
        case getCollection(token: String, id: Int)
        case getCollectionViewsInfo(token: String, profileId: Int,id: Int)
        case setFavState(token: String, collectionId: Int, profileId: Int)
        case setLikeState(token: String, collectionId: Int, profileId: Int)
        case setDislikeState(token: String, collectionId: Int, profileId: Int)

    }
}


extension API.DetailsController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getCollection, .getCollectionViewsInfo :
            return .get
        case .setFavState, .setLikeState, .setDislikeState :
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getCollection(_, let id):
            return "/collections/\(id)"
        case .getCollectionViewsInfo(_, let profileId, let id):
            return "/resourcesViewedInCollection?profileId=\(profileId)&collectionId=\(id)"
        case .setFavState :
            return "/setUnsetFavorite"
        case .setLikeState :
            return "/likeUnlike"
        case .setDislikeState :
            return "/dislikeUndislike"
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        case .getCollectionViewsInfo:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }

    }
    
    var parameters: Parameters? {
        switch self {
        case .getCollection, .getCollectionViewsInfo:
            return nil
        case .setFavState(_, let collectionId, let profileId),
             .setLikeState(_, let collectionId, let profileId),
             .setDislikeState(_, let collectionId, let profileId) :
            return ["collectionId": collectionId, "profileId": profileId]
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getCollection(let token,_), .getCollectionViewsInfo(let token,_,_), .setFavState(let token,_,_), .setLikeState(let token,_,_), .setDislikeState(let token,_,_):
            return Network.secureHeaders(token: token)
        }
    }
}
