//
//  MoreActionsBottomSheet.swift
//  DiniApp
//
//  Created by sami hazel on 26/02/2022.
//

import UIKit


enum MoreAction: Int {
    case none = 0
    case info = 1
    case download = 2
}

protocol MoreActionsDelegete: AnyObject {
    func itemProgressMoreActionsTapped(action: MoreAction, verticalIndex: Int, horizontalIndex: Int)
}
class MoreActionsBottomSheet: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!

    weak var delegate: MoreActionsDelegete?
    var vIndex: Int?
    var hIndex: Int?
    var dataTitle: String?

    internal static func instantiate(with title: String?, vIndex: Int, hIndex: Int) -> MoreActionsBottomSheet {
        let vc = MoreActionsBottomSheet(nibName: "MoreActionsBottomSheet", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.title = title
        vc.hIndex = hIndex
        vc.vIndex = vIndex
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.titleLabel.text = title
    }
    
    @IBAction func didTapAction(_ sender: UIButton) {
        guard let vIndex = vIndex , let hIndex = hIndex else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let action = sender.tag
        self.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.delegate?.itemProgressMoreActionsTapped(action: MoreAction(rawValue: action) ?? .none,
                                                         verticalIndex: vIndex,
                                                         horizontalIndex: hIndex)
        })
    }
}
