//
//  AllEpisodesViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/12/2021.
//

import Foundation

protocol AllEpisodesDelegate: AnyObject {
    func onSpecificEpTapped(media: String, seasonIndex: Int?, resourceIndex: Int)
}

class AllEpisodesViewModel {
    
    var type: CollectionType
    var resources: [DiniResource] = []
    var currentResourceIndex: Int
    var seasons: [Season]?
    var currentSeasonIndex: Int?
    
    weak var delegate: AllEpisodesDelegate?
    
    init(type: CollectionType, resources: [DiniResource], currentResourceIndex: Int) {
        self.type = type
        self.resources = resources
        self.currentResourceIndex = currentResourceIndex
    }
    
    init(type: CollectionType, resources: [DiniResource], currentResourceIndex: Int, seasons: [Season]?, currentSeasonIndex: Int) {
        self.type = type
        self.resources = resources
        self.currentResourceIndex = currentResourceIndex
        self.seasons = seasons
        self.currentSeasonIndex = currentSeasonIndex
    }
}
