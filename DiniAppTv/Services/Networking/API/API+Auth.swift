//
//  API+Login.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

import Foundation
import Alamofire

extension API {
    enum AuthController {
        case auth(email: String, password: String)
        case login
        case avatarPictureList(token: String)
        case userInfos(token: String, id: Int)
    }
}

extension API.AuthController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .auth:
            return .post
        case .login:
            return .get
        case .avatarPictureList:
            return .get
        case .userInfos:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .auth:
            return "/auth"
        case .login:
            return "/login"
        case .avatarPictureList :
            return "/avatarPictureList"
        case .userInfos(_, let id) :
            return "/users/\(id)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .auth(let email, let password):
            return ["email": email, "password": password]
        case .login, .avatarPictureList, .userInfos:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .auth, .login:
            return Network.unsecureHeaders
        case .avatarPictureList(let token), .userInfos(let token, _) :
            return Network.secureHeaders(token: token)
        }
    }
}
