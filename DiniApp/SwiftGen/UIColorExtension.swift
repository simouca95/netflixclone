// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen


// MARK: - Color catalogs
import UIKit

@objc public extension UIColor {

  static let primary = UIColor.color(named: "primary")
  static let redApp = UIColor.color(named: "redApp")
  static let allColors: [UIColor] = [
    primary,
    redApp,
  ]

  static let allNames: [String] = [
    "primary",
    "redApp",
  ]

  private static func color(named: String) -> UIColor {
      return UIColor(named: named)!
  }
}
