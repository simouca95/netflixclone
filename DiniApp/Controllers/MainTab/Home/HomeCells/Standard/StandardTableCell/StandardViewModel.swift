//
//  StandardViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import Foundation

struct StandardViewModel {
    
    var sliders: [CollectionListData] = []
    var type:HomeCellType = .standard
}

