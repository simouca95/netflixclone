//
//  API+Video.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 15/02/2022.
//

import Foundation
import Alamofire

extension API {
    enum VideoController {
        case getVideoData(tokenVimeo: String, media: String)
        case timeCode(token: String, resourceId: Int, profileId: Int, timeCode: Int)
        case getCollectionWatch(token: String, profileId: Int, collectionId: Int)
    }
}

extension API.VideoController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getVideoData, .getCollectionWatch:
            return .get
        case .timeCode:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getVideoData(_, let media):
            return "/\(media)"
        case .timeCode:
            return "/resourceVieweds"
        case .getCollectionWatch(_, let profileId, let collectionId):
            return"/collectionWatch?profileId=\(profileId)&collectionId=\(collectionId)"
        }
    }

    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getVideoData, .getCollectionWatch:
            return nil
        case .timeCode(_,let resourceId, let profileId, let timeCode):
            return ["resourceId": resourceId, "profileId": profileId, "timecode": timeCode]
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getVideoData(let tokenVimeo,_):
            return Network.vimeoHeaders(token: tokenVimeo)
        case .timeCode(let token,_,_,_), .getCollectionWatch(let token,_, _):
            return Network.secureHeaders(token: token)
        }
    }
}
