//
//  OnboardingViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit

class WalkthroughViewController: BaseViewController,  ATCWalkthroughViewControllerDelegate {
    
    var images: [String] = []
    var walkthroughs: [ATCWalkthroughModel] = []
    
    var titles: [String] = ["Dini c'est quoi ?",
                            "Les thèmes ? Les enseignants ?",
                            "Comment ça marche ?",
                            "Continuity : La fonctionnalité qui vous facilitera le suivi.",
                            "Les thèmes ? Les enseignants ?"]

    var subtitles: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in images.indices {
            walkthroughs.append(ATCWalkthroughModel(title: "", subtitle: "", icon: images[i]))
        }
        
        let walkthroughVC = self.walkthroughVC()
        walkthroughVC.delegate = self
        self.addChildViewControllerWithView(walkthroughVC)
    }
    
    func walkthroughViewControllerDidFinishFlow(_ vc: ATCWalkthroughViewController) {
        DefaultsHelper.saveIsWalkthrought(false)
        let loginViewController = LoginViewController.newViewController()
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
    fileprivate func walkthroughVC() -> ATCWalkthroughViewController {
      let viewControllers = walkthroughs.map { ATCClassicWalkthroughViewController(model: $0, nibName: "ATCClassicWalkthroughViewController", bundle: nil) }
      return ATCWalkthroughViewController(nibName: "ATCWalkthroughViewController",
                                          bundle: nil,
                                          viewControllers: viewControllers)
    }
}
