//
//  NotificationViewController.swift
//  DiniApp
//
//  Created by Wafa Labidi on 2/7/2022.
//

import UIKit
import RxSwift

class NotificationViewController: BaseViewController, DetailsDelegate {
    
    func onInfoStateChanged() {
        
    }
    
    
    //MARK: OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoButton: UIButton!
    
    //MARK: Propreties
    private let disposeBag = DisposeBag()
    var viewModel: NotificationViewModel!
    
    internal static func instantiate(with viewModel: NotificationViewModel, and notificationData: [NotificationData]) -> NotificationViewController {
        let vc = NotificationViewController()
        vc.viewModel = viewModel
        vc.viewModel.tableData = notificationData
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
        setupTableView()
    }
    
    //MARK: Functions
    private func setupBinding() {
        viewModel.launchVideo.asObservable()
            .compactMap({ $0 })
            .subscribe (onNext:{ [weak self] video in
                let vc = StoryboardScene.Video.videoPlayViewController.instantiate()
                let vm = VideoPlayViewModel(video: video,
                                            resourceId: self?.viewModel.resourceId ?? 0,
                                            type: .episode,
                                            resources: self?.viewModel.resources ?? [DiniResource](),
                                            seasons: nil)
//                vm.delegate = self
                vc.viewModel = vm
                vc.modalPresentationStyle = .fullScreen
                self?.navigationController?.present(vc, animated: true, completion: nil)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupTableView() {
        tableView.register(NotificationTableViewCell.nib, forCellReuseIdentifier: NotificationTableViewCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = .clear
        tableView.separatorInset = UIEdgeInsets.zero
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier) as! NotificationTableViewCell
        cell.initView(using: viewModel.tableData[indexPath.row])
        cell.delegate = self
        return cell
    }
}

extension NotificationViewController: NotificationTableViewCellDelegate {
    func didTapCollectionButton(idCollection: Int) {
        let viewmodel = DetailsViewModel(collectionId: idCollection , infoState: [], delegate: self)
        let detailsViewController = DetailsViewController.instantiate(with: viewmodel)
        self.navigationController?.pushViewController(detailsViewController, animated:true)
    }
    
    func didTapResourceButton(idResource: Int, idCollection: Int) {
        viewModel.getCollection(collectionId: idCollection, resourceId: idResource)
    }
        
}
