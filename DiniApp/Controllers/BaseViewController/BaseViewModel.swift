//
//  BaseViewModel.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import RxSwift
import RxCocoa

class BaseViewModel {
    
    
    // MARK: Public Properties

    var isLoading = BehaviorRelay<Bool?>(value: nil)
    
    let error = BehaviorRelay<String?>(value: nil)
    
    // MARK: Public Methods
    
    func viewDidLoad() {
    }
    
    func viewWillAppear() {
    }
    
    func viewDidAppear() {
    }
    
}
