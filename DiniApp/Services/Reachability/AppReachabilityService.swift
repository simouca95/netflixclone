//
//  AppReachabilityService.swift
//  PlaceStation
//
//  Created by Rached Khoudi on 15/10/2020.
//

import Reachability
import RxReachability
import RxSwift
import Alamofire

public protocol ReachabilityService {
    var isReachable: Observable<Bool> { get }
}

public final class AppReachabilityService: ReachabilityService {
    
    private let reachability = Reachability()
    private let disposeBag = DisposeBag()
    
    static let instance = AppReachabilityService()
    
    private let reachabilitySubject = ReplaySubject<Bool>.create(bufferSize: 1)
    
    public var isReachable: Observable<Bool> {
        return reachabilitySubject.asObservable()
    }
    
    // MARK: - LifeCycle
    
    private init() {
        guard let reachability = reachability else { return }
        
        reachabilitySubject.onNext(reachability.connection != .none)
        
        reachability.rx.isReachable
            .subscribe(onNext: { [weak self] isReach in
                self?.reachabilitySubject.onNext(isReach)
            })
            .disposed(by: disposeBag)
        
        try? reachability.startNotifier()
    }
    
    deinit {
        guard let reachability = reachability else { return }
        reachability.stopNotifier()
    }
}

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}
