//
//  VideoPlayViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 22/12/2021.
//

import UIKit
import BMPlayer
import NVActivityIndicatorView
import RxSwift
import AVKit

class VideoPlayViewController: UIViewController {
    

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var player: BMPlayer?
    var subtitles: WebVttBMSubtitles?
    var viewModel: VideoPlayViewModel?
    var controller: BMPlayerCustomControlView? = nil
    var disposeBag = DisposeBag()
    var customPanGesture: UIPanGestureRecognizer!
    var panDirection: Int = 0
    var isVolume = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViewModel()
        setupPlayerManager()
        preparePlayer()
        guard let viewModel = self.viewModel else { return }
        
        if let _ = viewModel.persistedMedia {
            viewModel.currentPersistedVideo.accept(viewModel.video)
        } else {
            viewModel.currentNetworkedVideo.accept(viewModel.video)
        }
        
//        NotificationCenter.default.addObserver(self,
//                               selector: #selector(brightnessDidChange),
//                               name: UIScreen.brightnessDidChangeNotification,
//                               object: nil)
        
        self.player?.panGesture.isEnabled = false
        customPanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panDirection(_:)))
        self.player?.addGestureRecognizer(customPanGesture)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc fileprivate func panDirection(_ pan: UIPanGestureRecognizer) {
        guard let player = self.player else { return }

        let locationPoint = pan.location(in: player)
        let velocityPoint = pan.velocity(in: player)
        
        switch pan.state {
        case UIGestureRecognizer.State.began:
            let x = abs(velocityPoint.x)
            let y = abs(velocityPoint.y)
            
            if x > y {
            } else {
                self.panDirection = 1
                if locationPoint.x > player.bounds.size.width / 2 {
                    self.isVolume = true
                } else {
                    self.isVolume = false
                }
            }
            
        case UIGestureRecognizer.State.changed:
            switch self.panDirection {
            case 1:
                self.verticalMoved(velocityPoint.y)
            default:
                break
            }
            
        case UIGestureRecognizer.State.ended:
            switch (self.panDirection) {
            case 1:
                self.isVolume = false
            default:
                break
            }
        default:
            break
        }
    }
    
    fileprivate func verticalMoved(_ value: CGFloat) {
        if self.isVolume{
//            self.volumeViewSlider.value -= Float(value / 10000)
            self.player?.addVolume(step: Float(value / 10000))
        } else {
            UIScreen.main.brightness -= value / 10000
            self.controller?.brightnessProgressView.progress = Float(UIScreen.main.brightness)
        }
    }
    
    func initViewModel() {
        guard let viewModel = self.viewModel else { return }
        
        viewModel.currentNetworkedVideo.asObservable()
            .subscribe (onNext:{ [weak self] video in
                guard let self = self,
                      let player = self.player
                else { return }
                
                guard let file = video?.files?[safe: viewModel.currentFileIndex],
                      let link = file.link,
                      let videoUrl = URL(string: link)
                else { return }
                
                let res = BMPlayerResourceDefinition(url: videoUrl, definition: "")
                let asset = BMPlayerResource(name: viewModel.videoTitle, definitions: [res], cover: nil, subtitles: nil)
                player.setVideo(resource: asset)
                if let timeToSeek = viewModel.timeCodeFromViewed {
                    player.seek(TimeInterval(timeToSeek))
                }
            })
            .disposed(by: self.disposeBag)
        
        viewModel.currentPersistedVideo.asObservable()
            .subscribe (onNext:{ [weak self] video in
                guard let self = self,
                      let player = self.player
                else { return }
                
                guard let uri = video?.uri
                else { return }

                    let dir = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                    let dataPath = dir.appendingPathComponent("DownloadedData")
                    let destinationURLForFile = dataPath.appendingPathComponent(uri)
                    
                    let res = BMPlayerResourceDefinition(url: destinationURLForFile, definition: "")
                    let asset = BMPlayerResource(name: viewModel.videoTitle, definitions: [res], cover: nil, subtitles: nil)
                    player.setVideo(resource: asset)
                    if let timeToSeek = viewModel.timeCodeFromViewed {
                        player.seek(TimeInterval(timeToSeek))
                    }
            })
            .disposed(by: self.disposeBag)
    }
    
    func setupPlayerManager() {
        BMPlayerConf.allowLog = false
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .always
        BMPlayerConf.loaderType  = NVActivityIndicatorType.ballRotateChase
        BMPlayerConf.enablePlayControlGestures = true
    }
    
    func preparePlayer() {
        controller = BMPlayerCustomControlView(frame: .zero, collectionType: viewModel?.type ?? .season, subtitle: subtitles, isPersisted: PersistenceHelper.shared.isPersisted(media: viewModel?.persistedMedia ?? "") ?? false)
        controller?.customDelegate = self
    
        controller?.isFullscreen = true
        player = BMPlayer(customControlView: controller)
        
        guard let player = self.player else { return }

        player.avPlayer?.allowsExternalPlayback = true
        view.addSubview(player)
        
        player.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
        }
        
        player.delegate = self
        player.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen {
                return
            } else {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
        self.view.layoutIfNeeded()
    }
    
//    func setupPlayerResource() {
//        let asset = self.preparePlayerItem()
//        player.setVideo(resource: asset)
//    }
//
//    func preparePlayerItem() -> BMPlayerResource {
//        let res = BMPlayerResourceDefinition(url: viewModel.videoURL, definition: "")
//        let asset = BMPlayerResource(name: viewModel.videoTitle, definitions: [res], cover: nil)
//        return asset
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        guard let player = self.player, let viewModel = self.viewModel else { return }
        
        if let timeToSeek = viewModel.timeCodeFromViewed {
            player.seek(TimeInterval(timeToSeek))
        }
        player.autoPlay()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        guard let player = self.player, let viewModel = self.viewModel else { return }

        guard let time = self.player?.avPlayer?.currentTime().seconds,
              let profileId = DefaultsHelper.profilId,
              let resourceId = viewModel.currentResourceId
        else {
            print("timeCode not working")
            return }
        
        viewModel.timeCode(resourceId: resourceId, profileId: profileId, timeCode: Int(time))
        player.pause(allowAutoPlay: true)
    }
    
    deinit {
        guard let player = self.player else { return }
        player.prepareToDealloc()
    }
    
    private func showAutoloadView() {
        
        self.controller?.controlViewAnimation(isShow: false)
        let vc = StoryboardScene.Video.nextEpisodeLoaderViewController.instantiate()
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve

        self.present(vc, animated: true, completion: nil)
    }
//    @objc func brightnessDidChange() {
//        DispatchQueue.main.async {
//            self.controller?.brightnessProgressView.setProgress(Float(UIScreen.main.brightness), animated: true)
//        }
//    }
}

// MARK:- BMPlayerDelegate example
extension VideoPlayViewController: BMPlayerDelegate {
    // Call when player orinet changed
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        player.snp.remakeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
        }
    }
    
    // Call back when playing state changed, use to detect is playing or not
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
//        print("| BMPlayerDelegate | playerIsPlaying | playing - \(playing)")
    }
    
    // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
//        print("| BMPlayerDelegate | playerStateDidChange | state - \(state)")
        if state == .playedToTheEnd {
            guard let viewModel = self.viewModel, viewModel.hasNext() else { return }
            self.showAutoloadView()
        }
    }
    
    // Call back when play time change
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
//        print("| BMPlayerDelegate | playTimeDidChange | \(currentTime) of \(totalTime)")
    }
    
    // Call back when the video loaded duration changed
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        //        print("| BMPlayerDelegate | loadedTimeDidChange | \(loadedDuration) of \(totalDuration)")
    }
}

extension VideoPlayViewController: BMPlayerCustomControlViewDelegate {
    func didPressLockScreen(lock: Bool) {
    }
    
    @available(iOS 11.0, *)
    open func createAirPlayView() -> AVRoutePickerView {
        let routerPickerView = AVRoutePickerView()
        
        
        routerPickerView.tintColor = .gray
        routerPickerView.activeTintColor = .white

        if #available(iOS 13.0, *) {
            routerPickerView.prioritizesVideoDevices = true
        }
                
        return routerPickerView
    }
    func didPressCastVideo() {
//        DispatchQueue.main.async {
//            let castview = self.createAirPlayView()
//            self.view.addSubview(castview)
//        }

    }
    
    func didPressOnCloseButton() {
        dismiss(animated: true, completion: {
            guard let player = self.player else { return }
            player.playerLayer?.prepareToDeinit()
        })
    }
    
    func didPressOnForward10Button(currentTime: Double) {
        self.player?.seek(currentTime+10)
    }
    
    func didPressOnBackward10Button(currentTime: Double) {
        self.player?.seek(currentTime-10)
    }
    
    func didPressOnNextEp() {
        guard let viewModel = self.viewModel else { return }
        viewModel.didPressOnNextEp()
    }
    
    func didPressOnAllEps() {
        guard let player = self.player, let viewModel = self.viewModel else { return }

        //player.pause()
        
        let vc = StoryboardScene.Video.allEpisodesViewController.instantiate()

        switch viewModel.type {
        case .episode, .season, .audio:
            let vm = AllEpisodesViewModel(type: viewModel.type, resources: viewModel.resources, currentResourceIndex: viewModel.currentResourceIndex)
            vc.viewModel = vm
            vm.delegate = self
        case .manySeasons:
            guard let currentSeasonIndex = viewModel.currentSeasonIndex else { return }
            let vm = AllEpisodesViewModel(type: viewModel.type, resources: viewModel.resources, currentResourceIndex: viewModel.currentResourceIndex, seasons: viewModel.seasons, currentSeasonIndex: currentSeasonIndex)
            vc.viewModel = vm
            vm.delegate = self
        }
        
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressQualityVideo() {
        let vc = StoryboardScene.Video.qualityVideoViewController.instantiate()
        guard let viewModel = self.viewModel else { return }
        guard let files = viewModel.currentNetworkedVideo.value?.files else { return }
        vc.viewModel = QualityVideoViewModel(files: files, currentFileIndex: viewModel.currentFileIndex)
        vc.viewModel.delegate = self
        
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func didPressSubtitleVideo() {
        let vc = StoryboardScene.Video.subtitleVideoViewController.instantiate()
        guard let viewModel = self.viewModel
        else { return }
        
        if let media = viewModel.persistedMedia,
           let bool = PersistenceHelper.shared.isPersisted(media: media),
           bool {
            vc.viewModel = SubtitleVideoViewModel(resourceSubtitles: viewModel.persistedSubtitles ?? [], currentSubtitleId: viewModel.currentSubtitleResourceId)
        } else if let resourceSubtitles = viewModel.currentResource?.resourceSubtitles {
            vc.viewModel = SubtitleVideoViewModel(resourceSubtitles: resourceSubtitles, currentSubtitleId: viewModel.currentSubtitleResourceId)
        }

        vc.viewModel.delegate = self
        
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}

extension VideoPlayViewController: AllEpisodesDelegate {
    func onSpecificEpTapped(media: String, seasonIndex: Int?, resourceIndex: Int) {
        guard let viewModel = self.viewModel else { return }

        viewModel.onSpecificEpTapped(media: media, seasonIndex: seasonIndex, resourceIndex: resourceIndex)
    }
}

extension VideoPlayViewController: SubtitleVideoDelegate {
    func onSpecificSubtitleTapped(subtitleResourceId: Int) {
        guard let baseURL = DefaultsHelper.subTitleResourcesPath,
              let subtitle = viewModel?.currentResource?.resourceSubtitles?.filter({ $0.id == subtitleResourceId }).first,
              let path = subtitle.completePath
        else {
            controller?.subtitle = nil
            return
        }
        
        viewModel?.currentSubtitleResourceId = subtitleResourceId
        controller?.subtitle = WebVttBMSubtitles(string: "\(baseURL)/\(path)", encoding: .utf8)
    }
}

extension VideoPlayViewController: QualityVideoDelegate {
    func onSpecificQualityTapped(newFileIndex: Int) {
        guard let viewModel = self.viewModel else { return }

        if viewModel.currentFileIndex != newFileIndex {
            viewModel.currentFileIndex = newFileIndex
            viewModel.currentNetworkedVideo.accept(viewModel.currentNetworkedVideo.value)
        }
    }
}

// MARK:- VideoPlayViewController example
extension VideoPlayViewController: NextEpisodeLoaderDelegate {
    func didPressNext() {
        guard let viewModel = self.viewModel else { return }
        viewModel.didPressOnNextEp()
    }
    
    func didPressCancel() {
        self.controller?.controlViewAnimation(isShow: true)

    }
    
    
}
