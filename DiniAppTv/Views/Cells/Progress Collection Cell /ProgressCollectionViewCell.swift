//
//  ProgressCollectionViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import UIKit
import Reusable

class ProgressCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var progressBarView: UIProgressView!
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 6
            self.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func setupContent(item : CollectionListData, itemsViewedsInfos: [CollectionViewedsInfo]) {
        
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        progressBarView.progressTintColor = .red
        
        if let itemViewedsInfos = itemsViewedsInfos.filter({$0.collectionID == item.id}).first,
           let progress = itemViewedsInfos.percentProgression {
            progressBarView.setProgress(Float(progress)/Float(100), animated: false)
        }
        
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.portraitPicturePath,
                                             path2: item.portraitPicture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
    
    }
}
