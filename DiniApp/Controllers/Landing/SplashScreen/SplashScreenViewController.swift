//
//  SplashScreenViewController.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/18/20.
//

import UIKit
import RxSwift
import RxCocoa

class SplashScreenViewController: BaseViewController {
    
    // MARK: Private Properties
    
    private let disposeBag = DisposeBag()
    private var viewModel: SplashScreenViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.viewWillAppear()
    }
    
    private func initViews() {
    }
    
    private func initViewModel() {
        viewModel = SplashScreenViewModel()
        viewModel.getLogoAndWalkthroughImages()
        
        viewModel.isLoading.asObservable()
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if let isLoading = isLoading, isLoading {
                    self.loadingHelper.start()
                } else {
                    self.loadingHelper.stop()
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.error.asObservable()
            .subscribe(onNext: { error in
                if let error = error {
                    ToastHelper.show(text: error)
                }
            })
            .disposed(by: disposeBag)
    }
}
