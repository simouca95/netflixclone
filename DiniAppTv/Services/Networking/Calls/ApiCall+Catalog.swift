//
//  ApiCall+Catalog.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Alamofire

extension ApiCall {
    public struct Catalog {
        
        public static func getCatalogs(token: String, profileId: Int, onSuccess: @escaping (_ response: CatalogsResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.CatalogController.getCatalogs(token: token,profilId: profileId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: CatalogsResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
    }
}
