//
//  ShowTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import Reusable
import Kingfisher
import AVKit
import BMPlayer
import NVActivityIndicatorView

protocol ShowDelegate: AnyObject {
    func didClickPlay(resource: DiniResource)
    func didClickPlay(collectionId: Int)
    func didSelectFav()
    func didSelectLiked()
    func didSelectDisliked()
    func unmuteVolume(isMuted: Bool)
}

class ShowTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var trailerContainerView: UIView!
    @IBOutlet weak var LikeView: UIView!
    @IBOutlet weak var unlikeView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var unmuteButton: UIButton!
    
    var player: BMPlayer?
    weak var delegate: ShowDelegate?
    private var isMuted = true
    
    var viewModel: ShowTableViewModel! {
        didSet {
            viewModel.updateShowDelegate = self
            
            if let trailerPath = viewModel.trailerPath,
               let baseURL = DefaultsHelper.trailerBaseURL,
               let fullUrl = Helper.concatinatePaths(path1: baseURL, path2: trailerPath) {
                
                self.setupTrailerView(trailerURL: fullUrl)
                
            } else if let posterPath = viewModel.posterPath,
                      let baseURL = DefaultsHelper.dataResourcesBaseURL,
                      let posterPathFullUrl = Helper.concatinatePaths(path1: baseURL, path2: posterPath) {
                
                self.posterImageView.kf.setImage(with: posterPathFullUrl)
                
            } else {
                posterImageView.image = UIImage(named: "ItemEmpty")
            }
            
            let isFav = self.viewModel.infoState.contains(.fav)
            let isLike = self.viewModel.infoState.contains(.liked)
            let isDislike = self.viewModel.infoState.contains(.disliked)
            
            DispatchQueue.main.async {
                self.favButton.tintColor = isFav ? .primary : .white
                self.favButton.titleLabel?.textColor = isFav ? .primary : .white
                
                self.likeButton.tintColor = isLike ? .primary : .white
                self.likeButton.titleLabel?.textColor = isLike ? .primary : .white
                
                self.dislikeButton.tintColor = isDislike ? .primary : .white
                self.dislikeButton.titleLabel?.textColor = isDislike ? .primary : .white
            }
            
        }
    }
    
    private func resetPlayerManager() {
        BMPlayerConf.allowLog = false
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .none
        BMPlayerConf.loaderType  = NVActivityIndicatorType.ballRotateChase
    }
    
    private func setupTrailerView(trailerURL: URL) {
        self.trailerContainerView.isHidden = false
        
        let customControlView = BMPlayerControlView()
        customControlView.fullscreenButton.isHidden = true
        customControlView.backButton.removeFromSuperview()
        player = BMPlayer(customControlView: customControlView)
        trailerContainerView.addSubview(player!)
        
        player?.snp.makeConstraints { (make) in
            make.top.equalTo(trailerContainerView.snp.top)
            make.left.equalTo(trailerContainerView.snp.left)
            make.right.equalTo(trailerContainerView.snp.right)
            make.height.equalTo(trailerContainerView.snp.width).multipliedBy(9.0/16.0).priority(500)
        }
        
        let asset = BMPlayerResource(name: "",
                                     definitions: [BMPlayerResourceDefinition(url: trailerURL, definition: "")],
                                     cover: nil,
                                     subtitles: nil)
        player?.setVideo(resource: asset)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        favButton.alignImageAndTitleVertically()
        likeButton.alignImageAndTitleVertically()
        dislikeButton.alignImageAndTitleVertically()
        
        self.resetPlayerManager()
        
        if let isLikeUnLikeVisible = DefaultsHelper.isLikeUnLikeVisible, isLikeUnLikeVisible {
            LikeView.isHidden = false
            unlikeView.isHidden = false
        } else {
            LikeView.isHidden = true
            unlikeView.isHidden = true
        }
        unmuteButton.tintColor = .white
        let image = UIImage(named: "volume-mute")?.withRenderingMode(.alwaysTemplate)
        unmuteButton.setImage(image, for: .normal)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        LikeView.isHidden = true
        unlikeView.isHidden = true
        trailerContainerView.isHidden = true
        self.player?.pause()
        self.player = nil
    }
    
    @IBAction func favButtonDidSelected(_ sender: UIButton) {
        delegate?.didSelectFav()
    }
    
    @IBAction func likeButtonDiDSelected(_ sender: UIButton) {
        delegate?.didSelectLiked()
    }
    
    @IBAction func dislikeButtonDidSelected(_ sender: UIButton) {
        delegate?.didSelectDisliked()
    }
    
    @IBAction func playButtonDidSelected(_ sender: UIButton) {
        if let resource = viewModel.resource {
            if resource.media?.contains(".mp3") ?? false {
                delegate?.didClickPlay(resource: resource)
            } else {
                delegate?.didClickPlay(collectionId: viewModel.collectionId)
            }
        }
    }
    
    @IBAction func unmuteButtonAction(_ sender: Any) {
        isMuted = !isMuted
        let image = UIImage(named: isMuted ? "volume-mute" : "volume")?.withRenderingMode(.alwaysTemplate)
        unmuteButton.setImage(image, for: .normal)
        delegate?.unmuteVolume(isMuted: isMuted)
    }
}

extension ShowTableViewCell: UpdateShowDelegate {
    func updateFav(isFav: Bool) {
        favButton.tintColor = isFav ? .primary : .white
        favButton.titleLabel?.textColor = isFav ? .primary : .white
    }
    func updateLiked(isLiked: Bool) {
        likeButton.tintColor = isLiked ? .primary : .white
        likeButton.titleLabel?.textColor = isLiked ? .primary : .white
    }
    func updateDisliked(isDisliked: Bool) {
        dislikeButton.tintColor = isDisliked ? .primary : .white
        dislikeButton.titleLabel?.textColor = isDisliked ? .primary : .white
    }
    
}
