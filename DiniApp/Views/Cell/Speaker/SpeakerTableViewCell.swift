//
//  SpeakerTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 04/04/2021.
//

import UIKit

class SpeakerTableViewCell: UITableViewCell {

    @IBOutlet weak var speakerNumberLabel: UILabel!
    @IBOutlet weak var speakerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)

         if selected {
             contentView.backgroundColor = UIColor(rgb: 0xEFE8DD)
         } else {
             contentView.backgroundColor = UIColor.white
         }
     }
    
}
