//
//  ApiCall+login.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 12/09/2021.
//

import Alamofire

extension ApiCall {
    
    public struct Auth {
        
        
        public static func signup(email: String,
                                  firstname: String,
                                  lastname: String,
                                  phoneNumber: String,
                                  onSuccess: @escaping (_ response: SignupResponse?) -> Void,
                                  onError: @escaping (_ error: DiniError?) -> Void) {
            
            let api = API.AuthController.signup(email: email,
                                                firstname: firstname,
                                                lastname: lastname,
                                                phoneNumber: phoneNumber)
            
            //TO DO: Adapt networking logic to this ws
            let urlString = Environment.Api.signupBasePath + api.path
            guard let url = URL(string: urlString) else { return }
            var urlRequest = URLRequest(url: url)
            let json = "{\"email\" : \"\(email)\",\"firstname\" : \"\(firstname)\",\"lastname\" : \"\(lastname)\",\"phone\" : \"\(phoneNumber)\"}"
            
            let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
            
            urlRequest.httpMethod = HTTPMethod.post.rawValue
            urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = jsonData
            
            (Network.manager as SessionManager)
                .request(urlRequest)
                .validate()
                .responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        if let result: SignupResponse = ApiCall.decodeResponse(response: response) {
                            onSuccess(result)
                        } else {
                            onSuccess(nil)
                        }
                    case .failure:
                        onError(ApiCall.decodeError(response: response))
                    }
                })
        }
        
        public static func login(email: String,
                                 password: String,
                                 deviceId: String,
                                 onSuccess: @escaping (_ response: LoginResponse?) -> Void,
                                 onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.auth(email: email, password: password, deviceId: deviceId)
                let classicUrlRequest = try api.asURLRequest()
                
                //TO DO: Adapt networking logic to this ws
                var urlRequest = URLRequest(url: classicUrlRequest.url!)
                let json = "{\"email\" : \"\(email)\",\"password\" : \"\(password)\",\"deviceId\" : \"\(deviceId)\"}"
                
                let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
                
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                urlRequest.httpBody = jsonData
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: LoginResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func getLogoAndWalkthroughImages(onSuccess: @escaping (_ response: LogiAndWalkthroughResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.login
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: LogiAndWalkthroughResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func avatarPictureList(token: String, onSuccess: @escaping (_ response: [[String]]?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.avatarPictureList(token: token)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: [[String]] = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func userInfos(token: String, id: Int, onSuccess: @escaping (_ response: UserResponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.userInfos(token: token, id: id)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: UserResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        
        public static func authorizeNotification(deviceId: String,
                                                 userId: Int,
                                                 token: String,
                                                 isNotificationAccepted: String,
                                                 onSuccess: @escaping (_ response: String?) -> Void,
                                                 onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.authorizeNotification(token: token,
                                                                   deviceId: deviceId,
                                                                   userId: userId,
                                                                   isNotificationAccepted: isNotificationAccepted)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: String = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func getAppVersion(onSuccess: @escaping (_ response: ControlVersionReponse?) -> Void, onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.getAppVersion
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ControlVersionReponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        public static func restartSessionCounter(token: String,
                                                 userId: Int,
                                                 onSuccess: @escaping (_ response: SessionCounterResponse?) -> Void,
                                                 onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.restartSessionCounter(token: token, userId: userId)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: SessionCounterResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        
        public static func logout(token: String,
                                  userId: Int,
                                  sessionToken: String,
                                  onSuccess: @escaping (_ response: ResourceWatchResponse?) -> Void,
                                  onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.logout(token: token, userId: userId, sessionToken: sessionToken)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: ResourceWatchResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
        
        
        public static func getUserLegalInfo(token: String,
                                  onSuccess: @escaping (_ response: UserLegalInfosResponse?) -> Void,
                                  onError: @escaping (_ error: DiniError?) -> Void) {
            do {
                let api = API.AuthController.getUserLegalInfo(token: token)
                let urlRequest = try api.asURLRequest()
                
                (Network.manager as SessionManager)
                    .request(urlRequest)
                    .validate()
                    .responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .success:
                            if let result: UserLegalInfosResponse = ApiCall.decodeResponse(response: response) {
                                onSuccess(result)
                            } else {
                                onSuccess(nil)
                            }
                        case .failure:
                            onError(ApiCall.decodeError(response: response))
                        }
                    })
            } catch _ {
                onError(nil)
            }
        }
    }
}
