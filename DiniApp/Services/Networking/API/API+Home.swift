//
//  API+Home.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import Foundation
import Alamofire

extension API {
    enum HomeController {
        var controllerUrl: String { return "/homepage" }
        var url: String { return "/switchProfileType/"}
        
        case getCollectionList(token: String, profileId: Int, deviceId: String)
        case switchProfile(token: String, userId: Int, profileId: Int)
    }
}

extension API.HomeController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getCollectionList :
            return .get
        case .switchProfile:
            return .put
        }
    }
    
    var path: String {
        switch self {
        case .getCollectionList(_, let profileId, let deviceId):
            return self.controllerUrl + "?profileId=\(profileId)" + "&deviceId=\(deviceId)"
        case .switchProfile(_,_,let profilId):
            return "/switchProfileType/\(profilId)"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getCollectionList:
            return nil
        case .switchProfile(_,let userId,_):
            return ["userId" : userId]
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .switchProfile(let token,_,_):
            return Network.secureHeaders(token: token)
        case .getCollectionList(let token,_,_):
            return Network.secureHeaders(token: token)
        }
    }
}
