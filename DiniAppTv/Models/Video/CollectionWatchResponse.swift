//
//  CollectionWatchResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 15/02/2022.
//

import Foundation

// MARK: - CollectionWatch
public struct CollectionWatchResponse: Codable {
    public let resourceID, media, duration, timeCode: String?
    public let title: String?

    enum CodingKeys: String, CodingKey {
        case resourceID = "resourceId"
        case media, duration, timeCode, title
    }
}
