//
//  SpeakerResourceTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 09/11/2021.
//

import UIKit

class SpeakerResourceTableViewModel: BaseCellViewModel {
    
    var posterPath: String
    var isNewResource: Bool

    
    init(cellHeight: CGFloat, path: String, isNewResource: Bool) {
        self.posterPath = path
        self.isNewResource = isNewResource
        super.init(cellHeight: cellHeight)
    }
}

