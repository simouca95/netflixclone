//
//  HomeSpeakerCollectionViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 25/01/2022.
//

import UIKit
import Reusable

class HomeSpeakerCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            imageView.layer.borderWidth = 6
            imageView.layer.borderColor = UIColor(rgb: 0x63AD65).cgColor
        } else {
            imageView.layer.borderWidth = 0
            imageView.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func setupContent(item : CollectionListData) {
        
        imageView.clipsToBounds = true
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.speakerImagesBaseURL,
                                             path2: item.picture) {
            self.imageView.kf.setImage(with: url)
        } else {
            self.imageView.image = UIImage(named: "ItemEmpty")
        }
        imageView.contentMode = .scaleAspectFill
        imageView.makeCircular()
    }
}
