//
//  ResourcesTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit

class ResourcesTableViewModel: BaseCellViewModel {
    
    var type: CollectionType
    var resources: [DiniResource] = []
    var seasons: [Season]?
    var selectedSeason: Int = 0
    
    init(cellHeight: CGFloat, type: CollectionType, resources: [DiniResource]) {
        self.type = type
        self.resources = resources
        super.init(cellHeight: cellHeight)
    }
    
    init(cellHeight: CGFloat, type: CollectionType, resources: [DiniResource], seasons: [Season]?, selectedSeason: Int) {
        self.type = type
        self.resources = resources
        self.seasons = seasons
        self.selectedSeason = selectedSeason
        super.init(cellHeight: cellHeight)
    }
}
