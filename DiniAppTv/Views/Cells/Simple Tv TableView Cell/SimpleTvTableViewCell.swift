//
//  SimpleTvTableViewCell.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 23/04/2022.
//

import UIKit
import Reusable

protocol SimpleTvDelegate: AnyObject {
    func onFocused(cell: SimpleTvTableViewCell)
}

class SimpleTvTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var titleTvLabel: UILabel!
    weak var delegate: SimpleTvDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func didUpdateFocus(in context: UIFocusUpdateContext,
                                 with coordinator: UIFocusAnimationCoordinator) {
        if isFocused {
            self.layer.borderWidth = 6
            self.layer.borderColor = UIColor.white.cgColor
            if let cell = context.nextFocusedView as? SimpleTvTableViewCell {
                delegate?.onFocused(cell: cell)
            }
        } else {
            self.layer.borderWidth = 0
            self.layer.borderColor = UIColor.clear.cgColor
        }
    }

}
