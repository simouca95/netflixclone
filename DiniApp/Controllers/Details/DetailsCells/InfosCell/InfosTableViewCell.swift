//
//  InfosTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit
import Reusable

class InfosTableViewCell: UITableViewCell, NibReusable {


    @IBOutlet weak var titleLabel: UILabel!
  
    @IBOutlet weak var contentNumView: UIView!
    @IBOutlet weak var contentNumLabel: UILabel!
    
    @IBOutlet weak var speakersKeyLabel: UILabel!
    @IBOutlet weak var speakersValueLabel: UILabel!
    @IBOutlet weak var categoryKeyLabel: UILabel!
    @IBOutlet weak var categoryValueLabel: UILabel!
    @IBOutlet weak var tagsKeyLabel: UILabel!
    @IBOutlet weak var tagsValueLabel: UILabel!
    
    var viewModel: InfosTableViewModel! {
        didSet {
            titleLabel.text = viewModel.title
            speakersValueLabel.text = viewModel.speakers
            categoryValueLabel.text = viewModel.category
            tagsValueLabel.text = viewModel.tags
            
            switch viewModel.type {
            case .episode:
                contentNumView.isHidden = true
                contentNumLabel.text = ""
            case .season, .audio:
                contentNumView.isHidden = false
                contentNumLabel.text = "\(viewModel.contentNumber) Épisodes"
            case .manySeasons:
                contentNumView.isHidden = false
                contentNumLabel.text = "\(viewModel.contentNumber) Saisons"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none

        speakersKeyLabel.text = "Intervenants :"
        categoryKeyLabel.text = "Categories :"
        tagsKeyLabel.text = "Tags :"
        
        contentNumLabel.font = UIFont.boldSystemFont(ofSize: 15)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        speakersKeyLabel.font = UIFont.boldSystemFont(ofSize: 13)
        speakersValueLabel.font = UIFont.systemFont(ofSize: 13)
        categoryKeyLabel.font = UIFont.boldSystemFont(ofSize: 13)
        categoryValueLabel.font = UIFont.systemFont(ofSize: 13)
        tagsKeyLabel.font = UIFont.boldSystemFont(ofSize: 13)
        tagsValueLabel.font = UIFont.systemFont(ofSize: 13)
        
        contentNumLabel.textColor = .white
        titleLabel.textColor = .white
        speakersKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        speakersValueLabel.textColor = .white
        categoryKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        categoryValueLabel.textColor = .white
        tagsKeyLabel.textColor = UIColor(rgb: 0x7DA463)
        tagsValueLabel.textColor = .white
    }
    
}
