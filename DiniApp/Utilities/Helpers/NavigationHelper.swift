//
//  NavigationHelper.swift
//  Onwani
//
//  Created by sami hazel on 11/18/20.
//  Copyright © 2020 Barsha Technology. All rights reserved.
//

import Foundation
import UIKit
import UIWindowTransitions

struct NavigationHelper {
    
    static let shared = NavigationHelper()
    
    func presentVcOverRoot(viewController : UIViewController, animation : UIView.AnimationOptions){
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let options = UIWindow.TransitionOptions(direction: .fade, style: .easeIn)
            appDelegate.window?.set(rootViewController: viewController, options: options, nil)
        }
    }
    
    func startAppFromWalkthrought(images: [String]) {
        let navigationViewController = StoryboardScene.Landing.landingNVC.instantiate()
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
           let onBoardingVC = navigationViewController.children.first as? WalkthroughViewController {
            onBoardingVC.images = images
            let options = UIWindow.TransitionOptions(direction: .fade, style: .easeIn)
            appDelegate.window?.set(rootViewController: navigationViewController, options: options, nil)
        }
    }
    
    func startAppFromLogin(){
        NavigationHelper.shared.presentVcOverRoot(viewController: StoryboardScene.Landing.loginNVC.instantiate(),
                                                  animation: .curveEaseIn)
    }
    
    func startAppFromMainTab(){
        NavigationHelper.shared.presentVcOverRoot(viewController: StoryboardScene.Main.mainNVC.instantiate(),
                                                  animation: .curveEaseIn)
    }
    
    func startAppFromProfiles(){
        NavigationHelper.shared.presentVcOverRoot(viewController: StoryboardScene.Profil.mainNVC.instantiate(),animation: .curveEaseIn)
    }
}


