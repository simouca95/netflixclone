//
//  NotificationSheet.swift
//  DiniApp
//
//  Created by Wafa Labidi on 13/07/2022.
//

import UIKit

protocol NotificationSheetDelegete: AnyObject {
    func showNotification()
}

class NotificationSheet: UIViewController {

    //MARK: OUTLETS
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var contentStack: UIStackView!
    @IBOutlet weak var notificationButton: UIButton!
    
    //MARK: Propreties
    weak var delegate: NotificationSheetDelegete?
    var viewtitle: String?
    var body: String?

    internal static func instantiate(with title: String, body: String) -> NotificationSheet {
        let vc = NotificationSheet(nibName: "NotificationSheet", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.viewtitle = title
        vc.body = body
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        contentStack.tag = 100
        notificationButton.layer.cornerRadius = 5
    }
    
    //MARK: FUNCTIONS
    func setupView() {
        titleLabel.text = viewtitle
        bodyLabel.text = body
    }
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true)
    }
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view?.tag != 100 {
            dismiss(animated: true)
        }
    }

    @IBAction func notificationButtonAction(_ sender: Any) {
        dismiss(animated: true)
        delegate?.showNotification()
    }
    
}
