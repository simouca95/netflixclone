//
//  QiblaViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 17/03/2021.
//

import UIKit
import Adhan
import CoreLocation

class QiblaViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    private let manager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
}


extension QiblaViewController: CLLocationManagerDelegate {
    
    func locationManager( _ manager: CLLocationManager, didUpdateLocations locations: [CLLocation] ) {
        if let location = locations.first {
            let longitude = location.coordinate.longitude
            let latitude = location.coordinate.latitude

            print("\(longitude) && \(latitude)")
            let qibla = Qibla(coordinates: Coordinates(latitude: latitude, longitude: longitude)).direction
            let radians = qibla * (.pi/180)
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat(radians))
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
}
