//
//  DetailsViewController+Video.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 24/04/2022.
//

import AVKit
import Photos

extension DetailsViewController {

    func configurePlayer(video: VideoResponse) {
        guard let typeIndex = viewModel?.cellData?.type,
              let type = CollectionType(rawValue: typeIndex),
              let resourceId = self.viewModel?.currentResourceId,
              let resources = viewModel?.availableResources
        else { return }
        
        let vc = VideoPlayerViewController()
        let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel?.cellData?.seasons)
        vm.delegate = self
        vc.viewModel = vm
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension DetailsViewController: AVPlayerDelegate {
    func onSuccessTimeCodeSend() {
        viewModel.getCollectionDetails()
    }
    
    func onFailTimeCodeSend() {
        print("CustomAVPlayerDelegate NOT WORKING")
    }
}
