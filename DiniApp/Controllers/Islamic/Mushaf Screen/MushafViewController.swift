//
//  MushafViewController.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 17/03/2021.
//

import UIKit
import RxSwift
import SVProgressHUD

class MushafViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var surahTextField: UITextField!
    
    var data: [Surah] = []
    var surahs: [Surah] = []
    let disposeBag = DisposeBag()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "SurahTableViewCell", bundle: nil), forCellReuseIdentifier: "SurahTableViewCell")
        self.surahTextField.attributedPlaceholder = NSAttributedString(string: "Recherche", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        ApiCall.Islamic.surahs(onSuccess: self.onSuccess, onError: self.onFail)
        SVProgressHUD.show()
        setupSearchView()
    }
    
    func setupSearchView() {
        self.searchView.layer.cornerRadius = 15
        self.surahTextField.rx.text
            .asObservable()
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .compactMap{ $0 }
            .filter({ (elemnt) -> Bool in
                if elemnt.count > 0 {
                    return true
                } else {
                    self.surahs = self.data
                    self.tableView.reloadData()
                    return false
                }
            })
            .subscribe(onNext: { element in
                print(element)
                self.surahs = self.data
                self.surahs = self.surahs.filter { $0.englishName.lowercased().contains(element.lowercased()) }
                self.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
}


extension MushafViewController {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: SurahResponse?) {
        SVProgressHUD.dismiss()
        if let response = response, response.code == 200 {
            self.data = response.data
            self.surahs = self.data
            tableView.reloadData()
        }
    }
    
    private func onFail(_ error: DiniError?) {
        SVProgressHUD.dismiss()
    }
}


extension MushafViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.surahs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurahTableViewCell") as! SurahTableViewCell
        cell.surahNumberLabel.text = "\(self.surahs[indexPath.row].number)."
        cell.surahNameLabel.text = self.surahs[indexPath.row].englishName
        cell.surahEnglishNameLabel.text = "\(self.surahs[indexPath.row].englishNameTranslation) (\(self.surahs[indexPath.row].numberOfAyahs))"
        cell.surahArabicNameLabel.text = self.surahs[indexPath.row].name.replacingOccurrences(of: String(self.surahs[indexPath.row].name.prefix(5)), with: "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let surahVc = StoryboardScene.Islamic.surahViewController.instantiate()
        surahVc.surahId = "\(self.surahs[indexPath.row].number)"
        self.navigationController?.pushViewController(surahVc, animated: true)
    }
}
 
