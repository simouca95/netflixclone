//
//  HomeViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 16/09/2021.
//

import RxCocoa
import RxSwift

enum HomeCellType: String {
    case main = ""
    case speaker = "5"
    case inProgress = "0"
    case homeResources = "11"
    case homeAudioResources = "13"
    case seasons = "14"
    case live = "7"
    case reel = "12"
    case standard = "990"
}

enum CollectionInfoState: Int {
    case liked = 0
    case disliked = 1
    case fav = 2
}

enum PlayHomeVideoType {
    case fromMain
    case fromInProgress
    case fromHomeResources
    case none
}

protocol HomeCellDelegate: AnyObject {
    func itemTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemSpeakerTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemProgressTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemProgressInfoTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemProgressMoreActionsTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemHomeResourcesTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemHomeSeasonsTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemLiveTapped(verticalIndex: Int, horizontalIndex: Int)
    func itemReelTapped(verticalIndex: Int, horizontalIndex: Int)
}

class HomeViewModel: BaseViewModel {
    
    var items: [CollectionList] = []
    var itemsViewedsInfos: [CollectionViewedsInfo] = []
    var reloadData = BehaviorSubject<Void>(value: ())
    var collectionInfos: [CollectionInfo]?
    var collectionViewdInfos: [CollectionViewedsInfo]?
    var currentWatchVideo = BehaviorRelay<VideoResponse?>(value: nil)
    var playHomeVideoType: PlayHomeVideoType = .none
    var currentHomeVideoVerticalItemIndex: Int?
    var currentHomeVideoHorizontalItemIndex: Int?
    var currentHomeVideoId: Int?
    var isFromNotification = false
    var launchVideo = BehaviorRelay<VideoResponse?>(value: nil)
    var resources = [DiniResource]()
    var resourceId = 0
    var collectionId = 0
    var notificationData = [NotificationData]()
    var isNewNotification = BehaviorRelay<Bool?>(value: false)
    var isExpiredAccount = PublishSubject<Void>()
    
    func getCollectionList() {
        guard let id = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken else { return }
        isLoading.accept(true)
        ApiCall.Home.getHomeCollectionList(token: token,
                                           id: id,
                                           deviceId: DefaultsHelper.deviceId ?? "",
                                           onSuccess: self.onSuccess,
                                           onError: self.onFail)
    }
    
    func switchProfile() {
        guard let id = DefaultsHelper.profilId,
              let userId = DefaultsHelper.userId,
              let token = DefaultsHelper.userToken else { return }
        ApiCall.Home.switchProfileType(token: token,
                                       profileId: id,
                                       userId: userId,
                                       onSuccess: self.onSuccess,
                                       onError: self.onFail)
        
    }
    
    func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getCollectionWatch(collectionId id: Int) {
        collectionId = id
        guard let profileId = DefaultsHelper.profilId,
              let userId = DefaultsHelper.userId,
              let token = DefaultsHelper.userToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getNumberOfUserWatching(token: token, profileId: profileId, userId: userId, onSuccess: self.onSuccess, onError: self.onFail)
    }
    func getCollectionFromNotification(connectionId id: Int) {
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken else { return }
        ApiCall.Details.getCollectionViewsInfo(token: token, profileId: profileId, id: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    func getCollectionInfoState(collectionId: Int) -> [CollectionInfoState] {
        var infos:[CollectionInfoState] = []
        
        if let favData = collectionInfos?[exist: 0]?.data, favData.contains(collectionId) {
            infos.append(.fav)
        }
        
        if let likedData = collectionInfos?[exist: 1]?.data, likedData.contains(collectionId) {
            infos.append(.liked)
        }
        
        if let favData = collectionInfos?[exist: 2]?.data, favData.contains(collectionId) {
            infos.append(.disliked)
        }
        
        return infos
    }
    
    func getAvailableResources(verticalIndex: Int, horizontalIndex: Int) -> [DiniResource]? {
        return items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.resources
    }
    
    private func getAvailableResources(resources: [DiniResource]?) -> [DiniResource]? {
        let resources = resources?.filter({ resource in
            let dateFormatter = ISO8601DateFormatter()
            if let publishedAtString = resource.publishedAt,
               let date = dateFormatter.date(from:publishedAtString),
               let isEnabled = resource.enabled {
                if date < Date(), isEnabled {
                    return true
                }
                return false
            }
            return true
        })
        return resources
    }
    
    func getNotification() {
        isLoading.accept(true)
        guard let token = DefaultsHelper.userToken
        else { return }
        ApiCall.Notif.getNotificationList(token: token,
                                          onSuccess: onSuccess,
                                          onError: onFail)
    }
}

extension HomeViewModel {
    // MARK: WS Callbacks
    
    private func onSuccess(_ response: CollectionResponse?) {
        guard let response = response,
              let collectionList = response.collectionList,
              let collectionInfos = response.collectionInfos,
              let collectionViewdInfos = response.collectionViewedsInfos,
              let userInfo = response.usersInfos?.subscription
        else {
            isLoading.accept(false)
            return
        }
        items.removeAll()
        if userInfo.isEnabled {
            self.collectionInfos = collectionInfos
            self.collectionViewdInfos = collectionViewdInfos
            let collectionListOrdered = collectionList
                .compactMap({$0})
            items.append(contentsOf: collectionListOrdered)
            
            if let profileInfo = response.profileTypeInfos {
                DefaultsHelper.saveNameToDisplayInSwitchProfileDefault(_nameToDisplayInSwitchProfileDefault: profileInfo.nameToDisplayInSwitchProfile ?? "" )
            }

            if let collectionViewedsInfos = response.collectionViewedsInfos {
                itemsViewedsInfos.removeAll()
                itemsViewedsInfos.append(contentsOf: collectionViewedsInfos)
            }
            reloadData.onNext(())}
        else {
            isExpiredAccount.onNext(Void())
        }
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: CollectionWatchResponse?) {
        if let media = response?.media {
            isLoading.accept(true)
            currentHomeVideoId = Int(response?.resourceID ?? "")
            getMedia(media: media)
        }
    }
    
    private func onSuccess(_ response: CollectionListData?) {
        let resource = response?.resources?.filter({ $0.id == resourceId }).first
        self.resources = response?.resources ?? [DiniResource]()
        getMedia(media: resource?.media ?? "")
    }
    
    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        if isFromNotification {
            launchVideo.accept(response)
        } else {
            currentWatchVideo.accept(response)
        }
    }
    
    private func onSuccess(_ response: SwitchProfileResponse?) {
        DefaultsHelper.saveNameToDisplayInSwitchProfileDefault(_nameToDisplayInSwitchProfileDefault: response?.nameToDisplayInSwitchProfileMenu ?? "")
        getCollectionList()
    }
    
    private func onSuccess(_ response: ResourceWatchResponse?) {
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken else { return }
        if !(response?.isSessionNumberAttempt ?? false)  {
            ApiCall.Video.getCollectionWatch(token: token, profileId: profileId, collectionId: collectionId, onSuccess: self.onSuccess, onError: self.onFail)
        }
    }
    
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: NotificationResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        notificationData = response.notification
        isNewNotification.accept(response.displaySpotIsNewNotification)
    }
}
