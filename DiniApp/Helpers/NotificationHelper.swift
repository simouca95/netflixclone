//
//  NotificationHelper.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/03/2021.
//

import Foundation

enum NotificationHelper {
    
    // MARK: Public Static Properties
    
    static let onUserIsUnathorizedNotificationName = "onUserIsUnathorizedNotification"
    
    // MARK: Public Static Methods
    
    static func notifyUserIsUnathorized() {
        DefaultsHelper.deleteAllDefaults()
        NavigationHelper.shared.startAppFromLogin()
    }
}
