//
//  CatalogsResponse.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 31/01/2022.
//

import Foundation

// MARK: - CatalogsResponse
public struct CatalogsResponse: Codable {
    public let collectionsList: CollectionsList?
    public let collectionsInfos: CollectionsInfos?
}
