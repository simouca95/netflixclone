//
//  SelectGenrePopup.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/27/20.
//

import UIKit


protocol DidSelectGenreProtocol : class{
    
    func didSelectGenre()

}
class SelectGenrePopup: UIViewController {

    @IBOutlet weak var categoriesTableView: UITableView!
    
    weak var delegate : DidSelectGenreProtocol!
    
    var datasource = DataMock.instance.genres
    
    var selectedGenre = ""
    var selectedGenreIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension SelectGenrePopup : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension SelectGenrePopup : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.datasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectGenreCell.identifier, for: indexPath) as! SelectGenreCell
        
        cell.title.text = datasource[indexPath.row]
        
        cell.isCurrentSelectedGenre = datasource[indexPath.row] == selectedGenre
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedGenreIndex != -1 ,
           let visible = tableView.indexPathsForVisibleRows,
           visible.contains(IndexPath(row: selectedGenreIndex, section: 0)){
            
            let oldCell = tableView.cellForRow(at: IndexPath(row: selectedGenreIndex, section: 0)) as! SelectGenreCell
            oldCell.isCurrentSelectedGenre = false
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! SelectGenreCell
        cell.isCurrentSelectedGenre = true
        
        self.selectedGenre = datasource[indexPath.row]
        self.selectedGenreIndex = indexPath.row
        
        self.delegate.didSelectGenre()
    }
    
    
}
