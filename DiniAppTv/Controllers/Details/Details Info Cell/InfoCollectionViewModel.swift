//
//  InfoCollectionViewModel.swift
//  DiniAppTv
//
//  Created by Rached Khoudi on 11/02/2022.
//

import Foundation

class InfoCollectionViewModel: BaseCellViewModel {
    var type: CollectionType
    var title: String
    var speakers: String
    var category: String
    var tags: String
    var contentNumber: Int
    var portraitPicture: String
    var bigPicture: String
    var seasons: [Season] = []
    

    init(type: CollectionType,
         title: String, speakers: String,
         category: String, tags: String, contentNumber: Int,
         portraitPicture: String, bigPicture: String, seasons: [Season]) {
        self.type = type
        self.title = title
        self.speakers = speakers
        self.category = category
        self.tags = tags
        self.contentNumber = contentNumber
        self.portraitPicture = portraitPicture
        self.bigPicture = bigPicture
        self.seasons = seasons
         
        super.init(cellHeight: 0)
    }
}
