//
//  DataMock.swift
//  netflixCloneApp
//
//  Created by sami hazel on 12/19/20.
//

import Foundation


final internal class DataMock {


    private init() {}


    static let instance = DataMock()


    let genres = ["Tous les genres", "action" , "Drama" , "Comedie", "Drames"]

    let videos0 : [Video] = [Video(id: 0, title: "Test", image:"img_1" ),
                             Video(id: 1, title: "Test", image:"img_2" ),
                             Video(id: 2, title: "Test", image:"img_3" ),
                             Video(id: 3, title: "Test", image:"img_4" ),
                             Video(id: 4, title: "Test", image:"img_5" ),
                             Video(id: 5, title: "Test", image:"img_6" )]

    let videos1 : [Video] = [Video(id: 6, title: "Test", image:"img_7" ),
                             Video(id: 7, title: "Test", image:"img_8" ),
                             Video(id: 8, title: "Test", image:"img_9" ),
                             Video(id: 9, title: "Test", image:"img_10" ),
                             Video(id: 10, title: "Test", image:"img_11" ),
                             Video(id: 11, title: "Test", image:"img_12" )]

    let videos2 : [Video] = [Video(id: 12, title: "Test", image:"img_13" ),
                             Video(id: 13, title: "Test", image:"img_14" ),
                             Video(id: 14, title: "Test", image:"img_15" ),
                             Video(id: 15, title: "Test", image:"img_16" ),
                             Video(id: 16, title: "Test", image:"img_17" ),
                             Video(id: 17, title: "Test", image:"img_18" )]

    let videos3 : [Video] = [Video(id: 18, title: "Test", image:"img_19" ),
                             Video(id: 19, title: "Test", image:"img_20" ),
                             Video(id: 20, title: "Test", image:"img_21" ),
                             Video(id: 21, title: "Test", image:"img_22" ),
                             Video(id: 22, title: "Test", image:"img_23" ),
                             Video(id: 23, title: "Test", image:"img_24" )]

    static let sections : [Section] = [Section(title: "Mini-teasers", video: [Video(id: 24, title: "Belle Bete", image: "belle_bete")], type: .top),
                    Section(title: "Mini-teasers", video: DataMock.instance.videos0, type: .teasers),
                    Section(title: "Reprendre Avec le profil X", video: DataMock.instance.videos1 ,type: .watching),
                    Section(title: "Horror", video: DataMock.instance.videos2, type: .normal),
                    Section(title: "Ma liste", video: DataMock.instance.videos3, type: .normal),
                    Section(title: "Drama", video: DataMock.instance.videos0, type: .normal),
                    Section(title: "Top 5", video: DataMock.instance.videos1, type: .normal)]

    static let speakers: [(id: Int, name: String, path: String)] =
        [(id: 0, name: "Abdul Basit", path: "ar.abdulbasitmurattal"),
         (id: 1, name: "Abdurrahmaan As-Sudais", path: "ar.abdurrahmaansudais"),
         (id: 2, name: "Abdul Samad", path: "ar.abdulsamad"),
         (id: 3, name: "Abu Bakr Ash-Shaatree", path: "ar.shaatree"),
         (id: 4, name: "Alafasy", path: "ar.alafasy"),
         (id: 5, name: "Hani Rifai", path: "ar.hanirifai"),
         (id: 6, name: "Husary", path: "ar.husary"),
         (id: 7, name: "Husary (Mujawwad)", path: "ar.husarymujawwad"),
         (id: 8, name: "Hudhaify", path: "ar.hudhaify"),
         (id: 9, name: "Ibrahim Akhdar", path: "ar.ibrahimakhbar"),
         (id: 10, name: "Maher Al Muaiqly", path: "ar.mahermuaiqly"),
         (id: 11, name: "Minshawi", path: "ar.minshawi"),
         (id: 12, name: "Minshawy (Mujawwad)", path: "ar.minshawimujawwad"),
         (id: 13, name: "Muhammad Ayyoub", path: "ar.muhammadayyoub"),
         (id: 14, name: "Muhammad Jibreel", path: "ar.muhammadjibreel"),
         (id: 15, name: "Saood bin Ibraaheem Ash-Shuraym", path: "ar.saoodshuraym"),
         (id: 16, name: "Parhizgar", path: "ar.parhizgar"),
         (id: 17, name: "Ayman Sowaid", path: "ar.aymanswoaid"),
         (id: 18, name: "Abdullah Basfar", path: "ar.abdullahbasfar"),
         (id: 19, name: "Ahmed ibn Ali al-Ajamy", path: "ar.ahmedajamy")]

    static let cities: [(name: String, path: String)] =
        [(name: "Paris", path: "Paris"),
         (name: "Marseille", path: "Marseille"),
         (name: "Nantes", path: "Nantes"),
         (name: "Montpellier", path: "Montpellier"),
         (name: "Strasbourg", path: "Strasbourg"),
         (name: "Bordeaux", path: "Bordeaux"),
         (name: "Rennes", path: "Rennes"),
         (name: "Reims", path: "Reims"),
         (name: "Saint-Étienne", path: "saint-etienne"),
         (name: "Le Havre", path: "le-havre"),
         (name: "Grenoble", path: "Grenoble"),
         (name: "Dijon", path: "Dijon"),
         (name: "Angers", path: "Angers"),
         (name: "Villeurbanne", path: "Villeurbanne"),
         (name: "Saint-Denis", path: "saint-denis-fr"),
         (name: "Nîmes", path: "nimes"),
         (name: "Clermont-Ferrand", path: "Clermont-Ferrand"),
         (name: "Le Mans", path: "le-mans"),
         (name: "Aix-en-Provence", path: "Aix-en-Provence"),
         (name: "Brest", path: "Brest"),
         (name: "Tours", path: "Tours"),
         (name: "Amiens", path: "Amiens"),
         (name: "Limoges", path: "Limoges"),
         (name: "Boulogne-Billancourt", path: "Boulogne-Billancourt"),
         (name: "Perpignan", path: "Perpignan"),
         (name: "Besançon", path: "besancon"),
         (name: "Metz", path: "Metz"),
         (name: "Rouen", path: "Rouen"),
         (name: "Argenteuil", path: "Argenteuil"),
         (name: "Montreuil", path: "Montreuil"),
         (name: "Caen", path: "Caen")]


//    func getSectionDataMock() -> [Section]{
//
//        return self.sections
//    }


}

struct Video {
    var id: Int
    var title : String
    var image : String
    var type : VideoType = .movie

}

struct Section {
    var title : String
    var video : [Video]
    var type : SectionType = .normal
}

enum SectionType {
    case top
    case teasers
    case watching
    case normal
}

enum VideoType {
    case movie
    case serie
}
