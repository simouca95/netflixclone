//
//  API+Notif.swift
//  DiniApp
//
//  Created by Wafa Labidi on 2/7/2022.
//

import Foundation
import Alamofire

extension API {
    enum NotificationController{
        case getNotificationList(token: String)
    }
}

extension API.NotificationController: NetworkUrl {
    
    var method: HTTPMethod {
        switch self {
        case .getNotificationList:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getNotificationList(_):
            return "/notificationRender"
        }
    }
    
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var parameters: Parameters? {
        switch self {
        case .getNotificationList:
            return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getNotificationList(let token):
            return Network.secureHeaders(token: token)
        }
    }
    
    
}
