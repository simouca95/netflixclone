//
//  SpeakerTableViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 18/09/2021.
//

import UIKit
import Reusable

class HomeSpeakerTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var collectionView: UICollectionView!

    var viewModel: HomeSpeakerTableViewModel!
    weak var delegate: HomeCellDelegate?
    var vIndex: Int? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        initViews()
        initViewModel()
    }
    
    func initViews() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellType: HomeSpeakerCollectionViewCell.self)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)

        collectionView.setCollectionViewLayout(layout, animated: false)
    }
    
    func initViewModel() {
        viewModel = HomeSpeakerTableViewModel()
    }
    
    func setupContent(data : [CollectionListData])  {
        viewModel.sliders = data
        self.collectionView.reloadData()
    }
    
}

extension HomeSpeakerTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item = viewModel.sliders[indexPath.row]
        let cell: HomeSpeakerCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        
        if let url = Helper.concatinatePaths(path1: DefaultsHelper.speakerImagesBaseURL,
                                             path2: item.picture) {
            cell.speakerImageView.kf.setImage(with: url)
        } else {
            cell.speakerImageView.image = UIImage(named: "ItemEmpty")
        }

        cell.makeCircular()
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.gray.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let vIndex = vIndex {
            delegate?.itemSpeakerTapped(verticalIndex: vIndex, horizontalIndex: indexPath.row)
        }
    }
}

extension HomeSpeakerTableViewCell: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            let height = collectionView.frame.height - 30
            let width = height
            return CGSize(width: width, height: height)
        }
        let width = collectionView.frame.width * 0.27
        let height = width
        return CGSize(width: width, height: height)
    }
}
