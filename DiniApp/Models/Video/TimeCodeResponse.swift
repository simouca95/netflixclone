//
//  TimeCodeResponse.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 01/10/2021.
//

import Foundation

// MARK: - JSONAnyCollectionResponse
public struct TimeCodeResponse: Codable {
    public let content: String?
    public let status: Bool?
}
