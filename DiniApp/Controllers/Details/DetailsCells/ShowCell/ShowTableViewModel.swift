//
//  ShowTableViewModel.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 24/09/2021.
//

import UIKit

protocol UpdateShowDelegate: AnyObject {
    func updateFav(isFav: Bool)
    func updateLiked(isLiked: Bool)
    func updateDisliked(isDisliked: Bool)
}

class ShowTableViewModel: BaseCellViewModel {
    
    var posterPath: String?
    var trailerPath: String?
    var collectionId: Int
    var resource: DiniResource?
    var infoState:[CollectionInfoState]
    weak var updateShowDelegate: UpdateShowDelegate?

    init(cellHeight: CGFloat, trailerPath: String?, posterPath: String?, collectionId: Int, resource: DiniResource?, infoState: [CollectionInfoState]) {
        self.trailerPath = trailerPath
        self.posterPath = posterPath
        self.resource = resource
        self.collectionId = collectionId
        self.infoState = infoState
        super.init(cellHeight: cellHeight)
    }
}
