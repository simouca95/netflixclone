//
//  HomeViewController+Video.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 03/10/2021.
//

import AVKit

extension HomeViewController {
    
    func configureAudioPlayer(resource: DiniResource) {
        let vc = StoryboardScene.Details.detailsAudioViewController.instantiate()
        vc.resource = resource
        vc.viewModel.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func configurePlayer(video: VideoResponse) {
        let vc = StoryboardScene.Video.videoPlayViewController.instantiate()

        switch viewModel.playHomeVideoType {
        case .fromMain:
            
            if let typeIndex = viewModel.items.filter({ $0.type == HomeCellType.main.rawValue }).first?.collectionData?.first?.type,
                     let type = CollectionType(rawValue: typeIndex),
                     let resourceId = self.viewModel.currentHomeVideoId,
                     let resources = viewModel.items.filter({ $0.type == HomeCellType.main.rawValue }).first?.collectionData?.first?.resources {
               
               let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.items.filter({$0.type == HomeCellType.main.rawValue}).first?.collectionData?.first?.seasons)
               vm.delegate = self
               vc.viewModel = vm
           }
            
        case .fromInProgress:
            
            if let verticalIndex = viewModel.currentHomeVideoVerticalItemIndex,
               let horizontalIndex = viewModel.currentHomeVideoHorizontalItemIndex,
               let typeIndex = viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.type,
               let type = CollectionType(rawValue: typeIndex),
               let resourceId = self.viewModel.currentHomeVideoId,
               let resources = viewModel.getAvailableResources(verticalIndex: verticalIndex, horizontalIndex: horizontalIndex) {
                
                let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.items[safe: verticalIndex]?.collectionData?[safe: horizontalIndex]?.seasons)
                vm.delegate = self
                vc.viewModel = vm
            }
            
        case .fromHomeResources:
            
            if let verticalIndex = viewModel.currentHomeVideoVerticalItemIndex,
               let typeIndex = viewModel.items[verticalIndex].collectionData?[safe: 0]?.type,
               let type = CollectionType(rawValue: typeIndex),
               let resourceId = self.viewModel.currentHomeVideoId,
               let resources = viewModel.getAvailableResources(verticalIndex: verticalIndex, horizontalIndex: 0) {
                
                let vm = VideoPlayViewModel(video: video, resourceId: resourceId, type: type, resources: resources, seasons: viewModel.items[safe: verticalIndex]?.collectionData?[safe: 0]?.seasons)
                vm.delegate = self
                vc.viewModel = vm
            }
            
        default:
            break
        }
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension HomeViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print("anotherViewControllerYouWantToObserve was dismissed")
        self.viewModel.getCollectionList()
        return nil
    }
}

extension HomeViewController: AVPlayerDelegate {
    func onSuccessTimeCodeSend() {
        viewModel.getCollectionList()
    }
    
    func onFailTimeCodeSend() {
        print("CustomAVPlayerDelegate NOT WORKING")
    }
}
