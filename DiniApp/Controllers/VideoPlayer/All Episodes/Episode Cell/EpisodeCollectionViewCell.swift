//
//  EpisodeCollectionViewCell.swift
//  netflixCloneApp
//
//  Created by Rached Khoudi on 27/12/2021.
//

import UIKit
import Reusable

class EpisodeCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var posterView: UIView!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var newResourceViewImage: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    
    var progressValue: Float = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        posterView.layer.cornerRadius = 6
        posterView.clipsToBounds = true
        progressView.setProgress(0.0, animated: false)
        newResourceViewImage.isHidden = false
    }

    var viewModel: EpisodeCollectionViewModel! {
        didSet {
            if let url = viewModel.getPosterImageURL() {
                posterImage.kf.setImage(with: url)
            } else {
                posterImage.image = Asset.itemEmpty.image
            }
            newResourceViewImage.kf.setImage(with: URL(string: DefaultsHelper.newCollectionPicture ?? ""))
            newResourceViewImage.isHidden = !(viewModel.isNewResource ?? true)
            infoLabel.text = viewModel.title
            durationLabel.text = "\(viewModel.durationInMinutes) min"
            if let progress = viewModel.progress {
                self.progressView.setProgress(progress, animated: false)
            }
        }
    }
}
