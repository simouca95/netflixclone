//
//  NotificationViewModel.swift
//  DiniApp
//
//  Created by Wafa Labidi on 2/7/2022.
//

import Foundation

import RxSwift
import RxCocoa

class NotificationViewModel: BaseViewModel {
    
    var tableData = [NotificationData]()
    var launchVideo = BehaviorRelay<VideoResponse?>(value: nil)
    var resources = [DiniResource]()
    var resourceId = 0

    
    func getCollection(collectionId id: Int, resourceId: Int) {
        self.resourceId = resourceId
        guard let profileId = DefaultsHelper.profilId,
              let token = DefaultsHelper.userToken else { return }
        ApiCall.Details.getCollectionViewsInfo(token: token, profileId: profileId, id: id, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
    private func getMedia(media: String) {
        guard let tokenVimeo = DefaultsHelper.vimeoToken
        else { return }
        isLoading.accept(true)
        ApiCall.Video.getVideoData(tokenVimeo: tokenVimeo, media: media, onSuccess: self.onSuccess, onError: self.onFail)
    }
    
}

extension NotificationViewModel {
    // MARK: WS Callbacks
    
    private func onFail(_ error: DiniError?) {
        isLoading.accept(false)
    }
    
    private func onSuccess(_ response: CollectionListData?) {
        let resource = response?.resources?.filter({ $0.id == resourceId }).first
        self.resources = response?.resources ?? [DiniResource]()
        getMedia(media: resource?.media ?? "")
        
    }
    
    private func onSuccess(_ response: VideoResponse?) {
        isLoading.accept(false)
        guard let response = response else { return }
        launchVideo.accept(response)
    }
  
}
